﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class CommitIntegrationInfoMap : ClassMap<CommitIntegrationInfo>
    {
        public CommitIntegrationInfoMap()
        {
            Id(x => x.Id);
            Map(x => x.Revision);

            References(x => x.CommitsSet);
        }
    }
}

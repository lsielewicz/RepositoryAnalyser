﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class IssueTrackerUserMap : ClassMap<IssueTrackerUser>
    {
        public IssueTrackerUserMap()
        {
            Id(x => x.Id);
            Map(x => x.DisplayName);
            Map(x => x.Email);
            Map(x => x.IsActive);
            Map(x => x.Locale);
            Map(x => x.UserName);
        }
    }
}

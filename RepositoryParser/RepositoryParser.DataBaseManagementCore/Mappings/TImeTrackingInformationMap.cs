﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class TimeTrackingInformationMap : ClassMap<TimeTrackingInformation>
    {
        public TimeTrackingInformationMap()
        {
            Id(x => x.Id);
            Map(x => x.OriginalEstimate);
            Map(x => x.RemainingEstimate);
            Map(x => x.TimeSpent);

            References(x => x.Issue);
        }
    }
}

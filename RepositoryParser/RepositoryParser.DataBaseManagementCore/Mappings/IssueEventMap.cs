﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class IssueEventMap : ClassMap<IssueEvent>
    {
        public IssueEventMap()
        {
            Id(x => x.Id);
            Map(x => x.Type);
            Map(x => x.Date);

            References(x => x.User).Cascade.SaveUpdate();
            References(x => x.Issue);
        }
    }
}

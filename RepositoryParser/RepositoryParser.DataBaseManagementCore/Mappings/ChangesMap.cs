﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.DataBase.Mappings
{
    class ChangesMap : ClassMap<Changes>
    {
        public ChangesMap()
        { 
            Id(x => x.Id);
            Map(x => x.Type);
            Map(x => x.Path);
            Map(x => x.ChangeContent);

            References<Commit>(x => x.Commit);
        }
    }
}

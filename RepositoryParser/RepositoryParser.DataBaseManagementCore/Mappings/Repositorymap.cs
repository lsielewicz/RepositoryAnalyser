﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.DataBase.Mappings
{
    class RepositoryMap : ClassMap<Repository>
    {
        public RepositoryMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Type);
            Map(x => x.Url);

            HasMany<Branch>(x => x.Branches).Inverse().Cascade.AllDeleteOrphan();
        }
    }
}

﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class IssueTrackingSystemMap : ClassMap<IssueTrackingSystem>
    {
        public IssueTrackingSystemMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Type);
            Map(x => x.Url);
            HasMany(x => x.Projects).Cascade.All();
        }
    }
}

﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class ProjectVersionMap : ClassMap<ProjectVersion>
    {
        public ProjectVersionMap()
        {
            Id(x => x.Id);
            Map(x => x.Description);
            Map(x => x.IsArchieved);
            Map(x => x.IsReleased);
            Map(x => x.Name);
            Map(x => x.ProjectKey);
            Map(x => x.StartDate);
            Map(x => x.ReleaseDate);
        }
    }
}

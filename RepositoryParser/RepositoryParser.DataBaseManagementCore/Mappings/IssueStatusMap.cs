﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class IssueStatusMap : ClassMap<IssueStatus>
    {
        public IssueStatusMap()
        {
            Map(x => x.Description);
            Id(x => x.Name);
        }
    }
}

﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.DataBase.Mappings
{
    class BranchMap : ClassMap<Branch>
    {
        public BranchMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Path);

            References<Repository>(x => x.Repository);
            HasManyToMany<Commit>(x => x.Commits).Cascade.All().Table("CommitsBranches");
        }
    }
}

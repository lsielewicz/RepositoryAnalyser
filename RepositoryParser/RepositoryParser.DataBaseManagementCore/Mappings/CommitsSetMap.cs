﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class CommitsSetMap : ClassMap<CommitsSet>
    {
        public CommitsSetMap()
        {
            Id(x => x.Id);
            Map(x => x.Number);

            References(x => x.Project);
            HasMany(x => x.Commits).Cascade.All().Inverse().Not.LazyLoad();
        }
    }
}

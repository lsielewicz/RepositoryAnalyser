﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class IssueTypeMap : ClassMap<IssueType>
    {
        public IssueTypeMap()
        {
            Map(x => x.Description);
            Map(x => x.IsSubTask);
            Id(x => x.Name);
        }
    }
}

﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class CommentMap : ClassMap<Comment>
    {
        public CommentMap()
        {
            Id(x => x.Id);
            Map(x => x.Body);
            Map(x => x.Date);

            References(x => x.User).Cascade.SaveUpdate();
            References(x => x.Issue);
        }
    }
}

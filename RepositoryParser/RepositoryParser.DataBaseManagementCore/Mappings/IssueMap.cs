﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class IssueMap : ClassMap<Issue>
    {
        public IssueMap()
        {
            Id(x => x.Id);
            Map(x => x.Description);
            Map(x => x.Environemnt);
            Map(x => x.Key);
            Map(x => x.Summary);
            Map(x => x.CreatedDateTime);
            Map(x => x.ClosedDateTime);
            Map(x => x.UpdatedDateTime);
            Map(x => x.Priority);
            Map(x => x.IsSubTask);
            Map(x => x.TimeSpent);
            Map(x => x.AttachmentsCount);

            References(x => x.Asignee).Cascade.All();
            References(x => x.Reporter).Cascade.All();
            References(x => x.Status).Cascade.All();
            References(x => x.Project).Cascade.All();
            References(x => x.TimeTracking).Cascade.All();
            References(x => x.Type).Cascade.All();

            HasMany(x => x.AffectedVersions).Cascade.All().Inverse();
            HasMany(x => x.Fixedversions).Cascade.All().Inverse();
            HasMany(x => x.Comments).Cascade.All().Inverse();
            HasMany(x => x.Events).Cascade.All().Inverse();
        }
    }
}
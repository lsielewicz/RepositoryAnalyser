﻿using FluentNHibernate.Mapping;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.DataBase.Mappings
{
    public class IssueTrackingProjectMap : ClassMap<IssueTrackingProject>
    {
        public IssueTrackingProjectMap()
        {
            Id(x => x.Id);
            Map(x => x.Key);
            Map(x => x.Name);
            Map(x => x.Url);

            References(x => x.Leader).Cascade.All();
            References(x => x.System).Cascade.All();
            HasMany(x => x.Issues).Cascade.All().Inverse();
            HasMany(x => x.CommitsSets).Cascade.All().Inverse().Not.LazyLoad();
        }
    }
}

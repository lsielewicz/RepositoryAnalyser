﻿using RepositoryAnalyser.Domain.Entities;

namespace RepositoryAnalyser.DataBase.Interfaces
{
    public interface IEntityPersister
    {
        void Persist(EntityBase entity);
    }
}
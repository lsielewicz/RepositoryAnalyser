﻿using NHibernate;

namespace RepositoryAnalyser.DataBase.Interfaces
{
    public interface IDbService
    {
        void CreateDataBase();
        ISessionFactory SessionFactory { get; }
    }
}
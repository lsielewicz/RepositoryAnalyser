﻿using RepositoryAnalyser.DataBase.Interfaces;
using RepositoryAnalyser.Domain.Entities;


namespace RepositoryAnalyser.DataBase.Services
{
    public class EntityPersister : IEntityPersister
    {
        public void Persist(EntityBase entity)
        {
            var sessionFactory = DbService.Instance.SessionFactory;
            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    if (entity != null)
                    {
                        session.SaveOrUpdate(entity);
                        transaction.Commit();
                    }
                }
            }
        }
    }
}

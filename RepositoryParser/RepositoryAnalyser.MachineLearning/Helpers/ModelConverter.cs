﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.MachineLearning.Helpers
{
    public static class ModelConverter
    {
        private const string NoValueString = "-";

        private static float GetTotalCountOfWorkedIssuesPerMonths(IList<Issue> issues)
        {
            var totalCountOfWorkedIssues = 0;
            var totalCountOfWorkedMonths = 0;
            if (issues != null && issues.Any())
            {
                for (var monthNumber = 1; monthNumber <= 12; monthNumber++)
                {
                    var coutOfIssues = issues.Count(i => i.UpdatedDateTime.HasValue && i.UpdatedDateTime.Value.Month == monthNumber);
                    if (coutOfIssues > 0)
                    {
                        totalCountOfWorkedIssues += coutOfIssues;
                        totalCountOfWorkedMonths++;
                    }

                }
            }
            else
            {
                return 0;
            }

            return totalCountOfWorkedIssues / totalCountOfWorkedMonths;
        }

        private static float GetCommitsDataPerMonth(IList<Commit> commits, out float addedLines, out float deletedLines)
        {
            var countOfCommits = 0;
            var countOfWorkedMonths = 0;
            addedLines = 0;
            deletedLines = 0;

            if (commits != null && commits.Any())
            {
                var sb = new StringBuilder();
                foreach (var commit in commits)
                {
                    foreach (var change in commit.Changes)
                    {
                        sb.AppendLine(change.ChangeContent);
                    }
                }

                var changeContents = sb.ToString();

                var changesLines = changeContents.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None).ToList();

                addedLines = changesLines.Count(line => line.StartsWith("+"));
                deletedLines = changesLines.Count(line => line.StartsWith("-"));
                countOfCommits = commits.Count;

                var minDate = commits.Min(c => c.Date);
                var maxDate = commits.Max(c => c.Date);

                var dateDiff = maxDate - minDate;
                var totalDays = dateDiff.TotalDays;
                var totalMonths = totalDays / 30;

                countOfWorkedMonths = (int)totalMonths;
            }
            else
            {
                return 0;
            }

            addedLines /= countOfWorkedMonths;
            deletedLines /= countOfWorkedMonths;

            return countOfWorkedMonths > 0 ? countOfCommits / countOfWorkedMonths : 0;
        }

        public static DeveloperEfficiencyData ToEfficiencyData(this IssueTrackerUser user, IList<Issue> issues, EfficiencyWeightsKeeper weightsKeeper)
        {
            var totalReportedIssues = issues.Where(i => i.Reporter != null && i.Reporter.Id == user.Id).ToList();
            var totalAssignedIssues = issues.Where(i => i.Asignee != null && i.Asignee.Id == user.Id).ToList();
           
            var output = new DeveloperEfficiencyData()
            {
                CountOfAssignedIssues = totalAssignedIssues.Count(),
                CountOfClosedIssuesPerMonth = GetTotalCountOfWorkedIssuesPerMonths(totalAssignedIssues.Where(i=> i.Status.Name == "Closed").ToList()), //TODO: Build a dictinary with all possiblties
                CountOfOpenedIssuesPerMonth = GetTotalCountOfWorkedIssuesPerMonths(totalAssignedIssues.Where(i => i.Status.Name == "Open").ToList()), //TODO: Build a dictinary with all possiblties
                CountOfReopenedIssuesPerMonth = GetTotalCountOfWorkedIssuesPerMonths(totalAssignedIssues.Where(i => i.Events.Any(e=>e.Type == IssueEventType.Reopened)).ToList()),
                CountOfReportedIssuesPerMonth = GetTotalCountOfWorkedIssuesPerMonths(totalReportedIssues),
                DeveloperEmail = user.Email.ReplaceNotAllowedCharsWithWhiteSpace(),
                DeveloperName = user.UserName.ReplaceNotAllowedCharsWithWhiteSpace(),
            };

            output.Efficiency = (float)weightsKeeper.CalculateEfficiency(output);

            return output;
        }

        public static DeveloperEfficiencyIntegratedData ToIntegratedEfficientyData(this IssueTrackerUser user, IList<Issue> issues, IList<Commit> commits, EfficiencyWeightsKeeper weightsKeeper)
        {
            var usersCommits = CommitsFilteringHelper.FilterWithUser(user, commits);
            var commitsPerMonth = GetCommitsDataPerMonth(usersCommits, out var addedLinesPerMonth, out var deletedLinesPerMonth);

            var baseModel = user.ToEfficiencyData(issues, weightsKeeper);
            var output = new DeveloperEfficiencyIntegratedData(baseModel)
            {
                CommitsCountPerMonth = commitsPerMonth,          
                AddedLinesPerMonth = addedLinesPerMonth,
                DeletedLinesPerMonth = deletedLinesPerMonth,
               
            };
            output.Efficiency = (float)weightsKeeper.CalculateEfficiency(output);

            return output;
        }

        public static IssueData ToIssueData(this Issue issue)
        {
            var output = new IssueData()
            {
                CommentsLenght = issue.Comments == null ? 0 : string.Join("\n", issue.Comments.Select(c => c.Body)).ReplaceNotAllowedCharsWithWhiteSpace().Length,
                CommentsCount = issue.Comments?.Count ?? 0,
                ReporterCommentsCount = issue.Comments?.Count(c=>c.User != null && issue.Reporter != null && c.User.UserName.Equals(issue.Reporter.UserName)) ?? 0,
                Environment = issue.Environemnt.ReplaceNotAllowedCharsWithWhiteSpace(),
                Type = issue.Type.Name.ReplaceNotAllowedCharsWithWhiteSpace(),
                AsigneeEmail = issue.Asignee?.Email.ReplaceNotAllowedCharsWithWhiteSpace() ?? NoValueString,
                ProjectName = issue.Project?.Name.ReplaceNotAllowedCharsWithWhiteSpace() ?? NoValueString,
                ReporterEmail = issue.Reporter?.Email.ReplaceNotAllowedCharsWithWhiteSpace() ?? NoValueString,
                Priority = issue.Priority.ReplaceNotAllowedCharsWithWhiteSpace() ?? NoValueString,
                IsSubTask = issue.IsSubTask ? 1 : 0,
                TimeSpent = issue.TimeSpent,
                AttachmentsCount = issue.AttachmentsCount
            };

            return output;
        }

        public static TimeToFixIssueData ToTimeToFixIssueData(this Issue issue)
        {
            var openedDate = issue.CreatedDateTime;
            var closedDate = GetIssueClosedDateTime(issue);
            var timeToFix = closedDate - openedDate;
            var minutesToFix = (float) (timeToFix?.TotalMinutes ?? 0);
            var output = new TimeToFixIssueData(ToIssueData(issue))
            {
               TimeToFixInMinutes = minutesToFix
            };

            return output;
        }

        private static DateTime? GetIssueClosedDateTime(Issue issue)
        {
            if (issue.ClosedDateTime.HasValue)
            {
                return issue.ClosedDateTime.Value;
            }

            if (issue.Events != null && issue.Events.Any())
            {
                var lastClosedEvent = issue.Events.LastOrDefault(e => e.Type == IssueEventType.Closed);
                if (lastClosedEvent != null)
                {
                    return lastClosedEvent.Date;
                }
            }

            if (issue.Status != null && issue.Status.Name == "Closed" && issue.UpdatedDateTime.HasValue)
            {
                return issue.UpdatedDateTime.Value;
            }

            return null;
        }

        public static TimeToFixIntegratedData ToTimeToFixIntegratedIssueData(this Issue issue, IList<Commit> commits, IList<CommitsSet> commitsSet = null)
        {
            var openedDate = issue.CreatedDateTime;
            var closedDate = GetIssueClosedDateTime(issue);
            var timeToFix = closedDate - openedDate;
            var minutesToFix = timeToFix?.TotalMinutes ?? 0;

            var output = new TimeToFixIntegratedData(ToIssueData(issue))
            {
                TimeToFixInMinutes = (float) minutesToFix,
                
            };

            if (commits == null)
            {
                return output;
            }

            var issuesCommits = CommitsFilteringHelper.FilterWithIssue(issue, commits, commitsSet);
            var commitsMessages = string.Join(Environment.NewLine, issuesCommits.Select(c => c.Message)).ReplaceNotAllowedCharsWithWhiteSpace();
              
            output.CommitsMessagesLength = commitsMessages.Length;
            output.CommitsMessagesCount = issuesCommits.Count;
            output.CommitsCount = issuesCommits.Count;

            var countingResult = LinesCounter.CountLines(issuesCommits);

            output.CountOfModifiedFiles = countingResult.ChangesCount;
            output.AddedLines = countingResult.AddedLines;
            output.DeletedLines = countingResult.DeletedLines;
            output.Revision = issuesCommits.Count == 1 ? issuesCommits.FirstOrDefault().Revision : "Multiple";


            return output;
        }

        public static ReopenedIssueData ToReopenedIssueData(this Issue issue)
        {
            if (issue.Events?.Any(e => e.Type == IssueEventType.Reopened || e.Type == IssueEventType.ReviewDismissed) ??
                false)
            {
                //test
            }

            if (issue.Events != null && issue.Events.Any(i => i.Type == IssueEventType.Reopened))
            {
                //test
            }
            var output = new ReopenedIssueData(ToIssueData(issue))
            {
                WasReopened = issue.Events?.Any(e => e.Type == IssueEventType.Reopened || e.Type == IssueEventType.ReviewDismissed) ?? false
            };

            return output;
        }

        public static ReopenedIssueIntegratedData ToReopenedIntegratedIssueData(this Issue issue, IList<Commit> commits, IList<CommitsSet> commitsSet = null)
        {
            var output = new ReopenedIssueIntegratedData(ToIssueData(issue))
            {
                WasReopened = issue.Events?.Any(e => e.Type == IssueEventType.Reopened) ?? false
            };

            if (commits != null)
            {
                var issuesCommits = CommitsFilteringHelper.FilterWithIssue(issue, commits, commitsSet);
                var commitsMessages = string.Join(Environment.NewLine, issuesCommits.Select(c => c.Message)).ReplaceNotAllowedCharsWithWhiteSpace();

                output.CommitsMessagesLength = commitsMessages.Length;
                output.CommitsMessagesCount = issuesCommits.Count;
                output.CommitsCount = issuesCommits.Count;

                var countingResult = LinesCounter.CountLines(issuesCommits);

                output.CountOfModifiedFiles = countingResult.ChangesCount;
                output.AddedLines = countingResult.AddedLines;
                output.DeletedLines = countingResult.DeletedLines;
                output.Revision = issuesCommits.Count == 1 ? issuesCommits.FirstOrDefault().Revision : "Multiple";
            }


            return output;
        }

        public static string ReplaceNotAllowedCharsWithWhiteSpace(this string str)
        {
            const string noValueString = "-";
            if (str == null)
            {
                return noValueString;
            }

            const string whitespace = " ";
            const string tabulatorSign = "\t";
            const string newLineFirstSign = "\r\n";
            const string newLineSecondSign = "\n";
            const string newLineThirdSign = "\r";

            str = str.Replace(tabulatorSign, whitespace).Replace(newLineFirstSign, whitespace)
                .Replace(newLineSecondSign, whitespace).Replace(newLineThirdSign, whitespace);

            return str;
        }

        public static RepositoryAnalyser.MachineLearning.Models.ConfusionMatirx ToLocalDomain(
           this Microsoft.ML.Legacy.Models.ConfusionMatrix matrix)
        {
            if (matrix == null)
            {
                return null;
            }

            return new RepositoryAnalyser.MachineLearning.Models.ConfusionMatirx()
            {
                TruePositive = matrix[0,0],
                FalseNegative = matrix[0,1],
                FalsePositve = matrix[1,0],
                TrueNegative = matrix[1,1]
            };
        }
    }
}

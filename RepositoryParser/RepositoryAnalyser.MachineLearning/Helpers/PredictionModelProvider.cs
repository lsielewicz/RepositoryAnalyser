﻿using Microsoft.ML.Legacy;
using Microsoft.ML.Legacy.Trainers;
using RepositoryAnalyser.MachineLearning.Enums;

namespace RepositoryAnalyser.MachineLearning.Helpers
{
    public static class PredictionModelProvider
    {
        public static ILearningPipelineItem GetRegressor(RegressionTool tool)
        {
            ILearningPipelineItem regressionItem = null;
            switch (tool)
            {
                case RegressionTool.FastTreeRegressor:
                    regressionItem = new FastTreeRegressor();
                    break;
                case RegressionTool.FastTreeTweedieRegressor:
                    regressionItem = new FastTreeTweedieRegressor();
                    break;
                case RegressionTool.StochasticDualCoordinateAscentRegressor:
                    regressionItem = new StochasticDualCoordinateAscentRegressor();
                    break;
                case RegressionTool.OrdinaryLeastSquaresRegressor:
                    regressionItem = new StochasticDualCoordinateAscentRegressor();
                    break;
                case RegressionTool.OnlineGradientDescentRegressor:
                    regressionItem = new OnlineGradientDescentRegressor();
                    break;
                case RegressionTool.PoissonRegressor:
                    regressionItem = new PoissonRegressor();
                    break;
                case RegressionTool.GeneralizedAdditiveModelRegressor:
                    regressionItem = new GeneralizedAdditiveModelRegressor();
                    break;
            }
            return regressionItem;
        }

        public static ILearningPipelineItem GetClassifier(BinaryClassificationTool classifier)
        {
            ILearningPipelineItem classifierItem = null;
            switch (classifier)
            {
                case BinaryClassificationTool.FastTreeBinaryClassifier:
                    classifierItem = new FastTreeBinaryClassifier()
                    {
                        NumLeaves = 5,
                        NumTrees = 5,
                        MinDocumentsInLeafs = 2
                    } ;
                    break;
                case BinaryClassificationTool.StochasticDualCoordinateAscentBinaryClassifier:
                    classifierItem = new StochasticDualCoordinateAscentBinaryClassifier();
                    break;
                case BinaryClassificationTool.AveragedPerceptronBinaryClassifier:
                    classifierItem = new AveragedPerceptronBinaryClassifier();
                    break;
                case BinaryClassificationTool.FastForestBinaryClassifier:
                    classifierItem = new FastForestBinaryClassifier();
                    break;
                case BinaryClassificationTool.LinearSvmBinaryClassifier:
                    classifierItem = new LinearSvmBinaryClassifier();
                    break;
                case BinaryClassificationTool.GeneralizedAdditiveModelBinaryClassifier:
                    classifierItem = new GeneralizedAdditiveModelBinaryClassifier();
                    break;
            }

            return classifierItem;
        }
    }
}

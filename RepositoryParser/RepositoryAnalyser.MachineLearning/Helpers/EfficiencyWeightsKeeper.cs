﻿using System;
using System.Xml.Serialization;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.MachineLearning.Helpers
{
    [Serializable]
    public class EfficiencyWeightsKeeper
    {
        public EfficiencyWeightsKeeper()
        {
            ClosedIssuesPerMonthWeight = 1;
            OpenedIssuesPerMonthWeight = 1;
            ReopenedIssuesPerMonthWeight = 1;
            ReportedIssuesPerMonthWeight = 1;
            AssignedIssuesWeight = 1;
            AssignedIssuesWeight = 1;
            CommitsPerMonthWeight = 1;
            AddedLinesPerMonthWeight = 1;
            DeletedLinesPerMonthWeight = 1;
        }

        [XmlAttribute]
        public double ClosedIssuesPerMonthWeight { get; set; }

        [XmlAttribute]
        public double OpenedIssuesPerMonthWeight { get; set; }

        [XmlAttribute]
        public double ReopenedIssuesPerMonthWeight { get; set; }

        [XmlAttribute]
        public double ReportedIssuesPerMonthWeight { get; set; }

        [XmlAttribute]
        public double AssignedIssuesWeight { get; set; }

        [XmlAttribute]
        public double CommitsPerMonthWeight { get; set; }

        [XmlAttribute]
        public double AddedLinesPerMonthWeight { get; set; }

        [XmlAttribute]
        public double DeletedLinesPerMonthWeight { get; set; }

        
        internal double CalculateEfficiency(DeveloperEfficiencyData data)
        {
            var closedIssuesValue = data.CountOfClosedIssuesPerMonth * ClosedIssuesPerMonthWeight;
            var openedIssuesValue = data.CountOfOpenedIssuesPerMonth * OpenedIssuesPerMonthWeight;
            var reopenedIssuesValue = data.CountOfReportedIssuesPerMonth * ReopenedIssuesPerMonthWeight;
            var reportedIssuesValue = data.CountOfReportedIssuesPerMonth * ReportedIssuesPerMonthWeight;
            var assignedIssuesValue = data.CountOfAssignedIssues * AssignedIssuesWeight;

            return closedIssuesValue + openedIssuesValue + reopenedIssuesValue + reportedIssuesValue +
                   assignedIssuesValue;
        }

        internal double CalculateEfficiency(DeveloperEfficiencyIntegratedData data, double higestValue = 0)
        {
            var closedIssuesValue = data.CountOfClosedIssuesPerMonth * ClosedIssuesPerMonthWeight;
            var openedIssuesValue = data.CountOfOpenedIssuesPerMonth * OpenedIssuesPerMonthWeight;
            var reopenedIssuesValue = data.CountOfReportedIssuesPerMonth * ReopenedIssuesPerMonthWeight;
            var reportedIssuesValue = data.CountOfReportedIssuesPerMonth * ReportedIssuesPerMonthWeight;
            var assignedIssuesValue = data.CountOfAssignedIssues * AssignedIssuesWeight;
            var commitsValue = data.CommitsCountPerMonth * CommitsPerMonthWeight;
            var addedLinesValue = data.AddedLinesPerMonth * AddedLinesPerMonthWeight;
            var deletedLinesValue = data.DeletedLinesPerMonth;

            return closedIssuesValue + openedIssuesValue + reopenedIssuesValue + reportedIssuesValue +
                   assignedIssuesValue + commitsValue + addedLinesValue + deletedLinesValue;
        }



    }
}

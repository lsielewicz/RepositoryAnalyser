﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.MachineLearning.Helpers
{
    public static class CommitsFilteringHelper
    {
        public static IList<Commit> FilterWithUser(IssueTrackerUser user, IList<Commit> commits)
        {
            var output = new List<Commit>();

            var commitsByName = commits.Where(c => c.Email.Equals(user.Email, StringComparison.OrdinalIgnoreCase) || c.Author.Equals(user.UserName, StringComparison.OrdinalIgnoreCase)).ToList();
            if (commitsByName.Any())
            {
                output.AddRange(commitsByName);
            }

            var authorsCommits = commits.Where(c => c.Author.Equals(user.UserName, StringComparison.OrdinalIgnoreCase) || c.Email.Equals(user.Email, StringComparison.OrdinalIgnoreCase)).ToList();
            output.AddRange(authorsCommits);
            output = output.Distinct().ToList();

            return output;
        }

        public static IList<Commit> FilterWithIssue(Issue issue, IList<Commit> commits, IList<CommitsSet> commitsSets = null)
        {
            var output = new List<Commit>();
            var revisionsToInclude = new List<string>();

            var issuesCommentsString = string.Join(Environment.NewLine, issue.Comments.Select(c => c.Body)) + Environment.NewLine;

            if (commitsSets != null && commitsSets.Any())
            {
                foreach (var commitsSet in commitsSets)
                {
                    var commitsSetRegexPattern = $@"#{commitsSet.Number}\s";
                    if (Regex.IsMatch(issuesCommentsString, commitsSetRegexPattern))
                    {
                        revisionsToInclude.AddRange(commitsSet.Commits.Select(c => c.Revision));
                    }
                }
            }

            foreach (var commit in commits)
            {
                if (issuesCommentsString.IndexOf(commit.Revision, StringComparison.OrdinalIgnoreCase) > -1 ||
                    revisionsToInclude.Any(rev => rev.Equals(commit.Revision, StringComparison.OrdinalIgnoreCase)))

                {
                    output.Add(commit);
                }

            }

            return output;
        }

        public static IList<Commit> FilterWithIssues(IList<Issue> issues, IList<Commit> commits, IList<CommitsSet> commitsSets = null)
        {
            var output = new List<Commit>();

            foreach (var issue in issues)
            {
                output.AddRange(FilterWithIssue(issue, commits, commitsSets));
            }

            output = output.Distinct().ToList();

            return output;
        }

        public static IList<Commit> GetAllCommitsOfRepository(Repository repository)
        {
            var output = new List<Commit>();

            foreach (var branch in repository.Branches)
            {
                output.AddRange(branch.Commits);
            }

            output = output.Distinct().ToList();
            return output;
        }

        public static IList<Commit> GetAllCommitsOfRepositories(IList<Repository> repositories)
        {
            var output = new List<Commit>();
            foreach (var repository in repositories)
            {
                output.AddRange(GetAllCommitsOfRepository(repository));
            }

            output = output.Distinct().ToList();
            return output;
        }
    }
}

﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Constants;
using RepositoryAnalyser.MachineLearning.Models;
// ReSharper disable CompareOfFloatsByEqualityOperator

namespace RepositoryAnalyser.MachineLearning.Helpers
{
    public static class DataValidator
    {

        public static bool IsDevEfficiencyValid(IssueTrackerUser user, IList<Issue> issues, IList<Repository> repositories, EfficiencyWeightsKeeper weightsKeeper)
        {
            var commits = CommitsFilteringHelper.GetAllCommitsOfRepositories(repositories);
            var data = user.ToIntegratedEfficientyData(issues, commits, weightsKeeper);
            return IsValid(data);
        }
        public static bool IsDevEfficiencyValid(IssueTrackerUser user, IList<Issue> issues, EfficiencyWeightsKeeper weightsKeeper)
        {
            var data = user.ToEfficiencyData(issues, weightsKeeper);
            return IsValid(data);
        }

        public static bool IsValid(Issue issue)
        {
            var issueData = issue.ToIssueData();
            return IsValid(issueData);
        }

        public static bool IsReopenIssueDataValid(Issue issue, IList<Repository> repository)
        {
            var commits = CommitsFilteringHelper.GetAllCommitsOfRepositories(repository);
            return IsReopenIssueDataValid(issue, commits);
        }

        public static bool IsReopenIssueDataValid(Issue issue, IList<Commit> commits)
        {
            var issueData = issue.ToReopenedIntegratedIssueData(commits);
            return IsValid(issueData);
        }

        public static bool IsTimeToFixDataValid(Issue issue, IList<Repository> repository)
        {
            var commits = CommitsFilteringHelper.GetAllCommitsOfRepositories(repository);
            return IsTimeToFixDataValid(issue, commits);
        }

        public static bool IsTimeToFixDataValid(Issue issue, IList<Commit> commits)
        {
            var issueData = issue.ToTimeToFixIntegratedIssueData(commits);
            return IsValid(issueData);
        }

        public static bool IsValid(DeveloperEfficiencyData data)
        {
            if (data.CountOfAssignedIssues == 0 &&
                data.CountOfClosedIssuesPerMonth == 0 &&
                data.CountOfOpenedIssuesPerMonth == 0 &&
                data.CountOfReopenedIssuesPerMonth == 0 &&
                data.CountOfReportedIssuesPerMonth == 0)
            {
                return false;
            }

            return true;
        }

        public static bool IsValid(DeveloperEfficiencyIntegratedData data)
        {
            if (data.CountOfAssignedIssues == 0 &&
                data.CountOfClosedIssuesPerMonth == 0 &&
                data.CountOfOpenedIssuesPerMonth == 0 &&
                data.CountOfReopenedIssuesPerMonth == 0 &&
                data.CountOfReportedIssuesPerMonth == 0 ||(
                data.AddedLinesPerMonth == 0 &&
                data.DeletedLinesPerMonth == 0 &&
                data.CommitsCountPerMonth == 0))
            {
                return false;
            }

            return true;
        }

        public static bool IsValid(IssueData data)
        {
            if (data.CommentsCount == 0 &&
                data.CommentsLenght == 0 &&
                data.ReporterCommentsCount == 0 &&
                data.AsigneeEmail.Equals(Strings.NoDataValue) &&
                data.ReporterEmail.Equals(Strings.NoDataValue) &&
                data.Priority.Equals(Strings.NoDataValue) &&
                data.TimeSpent == 0 &&
                data.AttachmentsCount == 0)
            {
                return false;
            }

            return true;
        }

        public static bool IsValid(IntegratedIssueData data)
        {
            if (data.CommentsCount == 0 &&
                data.CommentsLenght == 0 &&
                data.ReporterCommentsCount == 0 &&
                data.AsigneeEmail.Equals(Strings.NoDataValue) &&
                data.ReporterEmail.Equals(Strings.NoDataValue) &&
                data.Priority.Equals(Strings.NoDataValue) &&
                data.TimeSpent == 0 &&
                data.AttachmentsCount == 0 ||
                (data.CommitsCount == 0 &&
                data.CommitsMessagesCount == 0 &&
                data.CountOfModifiedFiles == 0 &&
                data.AddedLines == 0 &&
                data.DeletedLines == 0))
            {
                return false;
            }

            return true;
        }
    }
}

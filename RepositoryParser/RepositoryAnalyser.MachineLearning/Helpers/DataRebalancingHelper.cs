﻿using System;
using System.Collections.Generic;
using System.Linq;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.MachineLearning.Helpers
{
    public static class DataRebalancingHelper
    {
        public static IList<ReopenedIssueData> BalanceData(IList<ReopenedIssueData> inputData, DataBalancingTechnique technique)
        {
            var outputData = new List<ReopenedIssueData>();

            var positives = inputData.Where(item => item.WasReopened).ToList();
            var negatives = inputData.Where(item => !item.WasReopened).ToList();

            var countOfPositives = positives.Count;
            var countOfNegatives = negatives.Count;

            if (countOfNegatives == countOfPositives)
            {
                return inputData;
            }

            switch (technique)
            {
                case DataBalancingTechnique.Undersampling:

                    if (countOfNegatives > countOfPositives)
                    {
                        outputData.AddRange(positives);
                        outputData.AddRange(negatives.Take(countOfPositives).ToList());
                    }
                    else
                    {
                        outputData.AddRange(negatives);
                        outputData.AddRange(positives.Take(countOfPositives).ToList());
                    }

                    break;
                case DataBalancingTechnique.Oversampling:

                    var itemsToCopy = Math.Abs(countOfNegatives - countOfPositives);
                    if (countOfNegatives > countOfPositives)
                    {
                        outputData.AddRange(negatives);
                        DoOversampling(ref outputData, positives, itemsToCopy);
                    }
                    else
                    {
                        outputData.AddRange(positives);
                        DoOversampling(ref outputData, negatives, itemsToCopy);
                    }
                    break;
            }

            return outputData;
        }

        public static IList<ReopenedIssueIntegratedData> BalanceData(IList<ReopenedIssueIntegratedData> inputData, DataBalancingTechnique technique)
        {
            var outputData = new List<ReopenedIssueIntegratedData>();

            var positives = inputData.Where(item => item.WasReopened).ToList();
            var negatives = inputData.Where(item => !item.WasReopened).ToList();

            var countOfPositives = positives.Count;
            var countOfNegatives = negatives.Count;

            if (countOfNegatives == countOfPositives)
            {
                return inputData;
            }

            switch (technique)
            {
                case DataBalancingTechnique.Undersampling:

                    if (countOfNegatives > countOfPositives)
                    {
                        outputData.AddRange(negatives);
                        outputData.AddRange(positives.Take(countOfPositives).ToList());
                    }
                    else
                    {
                        outputData.AddRange(positives);
                        outputData.AddRange(negatives.Take(countOfPositives).ToList());
                    }

                    break;
                case DataBalancingTechnique.Oversampling:

                    var itemsToCopy = Math.Abs(countOfNegatives - countOfPositives);
                    if (countOfNegatives > countOfPositives)
                    {
                        outputData.AddRange(negatives);
                        DoOversampling(ref outputData, positives, itemsToCopy);
                    }
                    else
                    {
                        outputData.AddRange(positives);
                        DoOversampling(ref outputData, negatives, itemsToCopy);
                    }
                    break;
            }

            return outputData;
        }

        private static void DoOversampling<T>(ref List<T> sourceItems, IList<T> itemsToOversample, int itemsToCopy)
        {
            var countOfItems = itemsToOversample.Count;
            sourceItems.AddRange(itemsToOversample);
            for (int i = 0; i < itemsToCopy; i++)
            {
                var counter = i;
                while (counter >= countOfItems - 1 && counter > 0)
                {
                    if (countOfItems == 1)
                    {
                        counter = 0;
                        break;
                    }
                    counter -= countOfItems - 1;
                }
                sourceItems.Add(itemsToOversample[counter]);
            }
        }
    }
}

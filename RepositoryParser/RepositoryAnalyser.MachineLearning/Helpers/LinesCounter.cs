﻿using System;
using System.Collections.Generic;
using System.Linq;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.MachineLearning.Helpers
{
    internal static class LinesCounter
    {
        public static LinesCountingResult CountLines(IList<Commit> commits)
        {
            var output = new LinesCountingResult();
            foreach (var commit in commits)
            {
                foreach (var change in commit.Changes)
                {
                    output.ChangesCount++;

                    var changesLines = change.ChangeContent.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None).ToList();
                    output.AddedLines += changesLines.Count(line => line.StartsWith("+"));
                    output.DeletedLines += changesLines.Count(line => line.StartsWith("-"));
                }
            }

            return output;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.MachineLearning.Helpers
{
    public static class DataCleaningHelper
    {
        public static List<ReopenedIssueData> Clean(IList<ReopenedIssueData> inputData)
        {
            return inputData.Where(DataValidator.IsValid).ToList();
        }

        public static List<TimeToFixIntegratedData> Clean(IList<TimeToFixIntegratedData> inputData)
        {
            return inputData.Where(DataValidator.IsValid).ToList();
        }

        public static List<ReopenedIssueIntegratedData> Clean(IList<ReopenedIssueIntegratedData> inputData)
        {
            return inputData.Where(DataValidator.IsValid).ToList();
        }

        public static List<TimeToFixIssueData> Clean(IList<TimeToFixIssueData> inputData)
        {
            return inputData.Where(DataValidator.IsValid).ToList();
        }

        public static List<DeveloperEfficiencyData> Clean(IList<DeveloperEfficiencyData> inputData)
        {
            return inputData.Where(DataValidator.IsValid).ToList();
        }

        public static List<DeveloperEfficiencyIntegratedData> Clean(IList<DeveloperEfficiencyIntegratedData> inputData)
        {
            return inputData.Where(DataValidator.IsValid).ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.ML.Legacy;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Interfaces;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.MachineLearning.Services;
using ZetaLongPaths;

namespace RepositoryAnalyser.MachineLearning.Predictors
{
    public class IssueReopenPredictor : PredictorBase, IIssueReopenPredictor
    {
        private readonly DataModelingService _dataModelingService;
        public IssueReopenPredictor()
        {
            _dataModelingService = new DataModelingService();
        }

        public TrainingOutputData TrainAndSavePredictionModel(ReopenTrainingIntegratedInputData data)
        {
            var model = this.TrainIntegratedPredictionModel(data, out var metrics, out var rawDataRows, out var formattedDataRows);
            this.AssertDirectoryExists(data.PredictiveModelPath);

            model.WriteAsync(data.PredictiveModelPath);

            return new TrainingOutputData()
            {
                FilePath = data.PredictiveModelPath,
                Metrics = metrics,
                DateTime = DateTime.Now,
                CountOfRawDataRows = rawDataRows,
                CountOfFormattedDataRows = formattedDataRows
            };
        }

        public TrainingOutputData TrainAndSavePredictionModel(ReopenTrainingInputData data)
        {
            var model = this.TrainPredictionModel(data, out var metrics, out var rawDataRows, out var formattedDataRows);
            this.AssertDirectoryExists(data.PredictiveModelPath);

            model.WriteAsync(data.PredictiveModelPath);

            return new TrainingOutputData()
            {
                FilePath = data.PredictiveModelPath,
                Metrics = metrics,
                DateTime = DateTime.Now,
                CountOfRawDataRows = rawDataRows,
                CountOfFormattedDataRows = formattedDataRows
            };
        }

        public async Task<IssueReopenPredictionResult> Predict(Issue issue, TrainingOutputData trainingModelData)
        {
            if (!ZlpIOHelper.FileExists(trainingModelData.FilePath))
            {
                throw new ArgumentException(); // TODO: Replace with an engine exceptionto 
            }

            var model = await PredictionModel.ReadAsync<ReopenedIssueData, ReopenedIssuePrediction>(trainingModelData.FilePath);

            var inputData = issue.ToReopenedIssueData();
            var actualValue = inputData.WasReopened;

            inputData.WasReopened =true;

            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = model.Predict(inputData);
            stopwach.Stop();
            inputData.WasReopened = actualValue;

            return new IssueReopenPredictionResult()
            {
                Data = inputData,
                PredictionDate = DateTime.Now.Date,
                Prediction = result,
                PredictionTime = stopwach.Elapsed,
                Accuracy = actualValue == result.IsGoingToBeReopened ? 1.0 : 0.0,
                OriginalKey = issue.Key,
                TrainingModel = trainingModelData,
                
            };
        }

        public async Task<IssueReopenIntegratedPredictionResult> Predict(Issue issue, IList<Commit> commits, TrainingOutputData trainingModelData, IList<CommitsSet> commitsSet = null)
        {
            
            if (!ZlpIOHelper.FileExists(trainingModelData.FilePath))
            {
                throw new ArgumentException(); // TODO: Replace with an engine exceptionto 
            }

            var model = await PredictionModel.ReadAsync<ReopenedIssueIntegratedData, ReopenedIssuePrediction>(trainingModelData.FilePath);

            var inputData = issue.ToReopenedIntegratedIssueData(commits, commitsSet);
            var actualValue = inputData.WasReopened;

            inputData.WasReopened = true;

            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = model.Predict(inputData);
            stopwach.Stop();
            inputData.WasReopened = actualValue;

            return new IssueReopenIntegratedPredictionResult()
            {
                Data = inputData,
                PredictionDate = DateTime.Now.Date,
                Prediction = result,
                PredictionTime = stopwach.Elapsed,
                Accuracy = GetAccuracy(result.IsGoingToBeReopened, actualValue),
                OriginalKey = issue.Key,
                TrainingModel = trainingModelData,
            };
        }


        private PredictionModel<ReopenedIssueData, ReopenedIssuePrediction> TrainPredictionModel(ReopenTrainingInputData data, out ModelMetrics metrics, out int countOfRawDataRows, out int countOfFormattedDataRows)
        {
            var modelingResult = _dataModelingService.PrepareReopenedIssueData(data.IssuesData, data.DataRebalancingTechnique);

            countOfFormattedDataRows = modelingResult.CountOfFormattedDataRows;
            countOfRawDataRows = modelingResult.CountOfRawDataRows;

            var pipeline = new LearningPipeline();
            foreach (var item in modelingResult.LearningItems)
            {
                pipeline.Add(item);
            }

            var classifier = PredictionModelProvider.GetClassifier(data.ClassificationTool);
            pipeline.Add(classifier);

            
            var model = pipeline.Train<ReopenedIssueData, ReopenedIssuePrediction>();
           
            //metrics = TrainingModelEvaluator.EvaluateModel(model, modelingResult.ValidationDatasetPath, MachineLearningMechanism.BinaryClassification);
            metrics = TrainingModelEvaluator.CrossValidate<ReopenedIssueData, ReopenedIssuePrediction>(pipeline, MachineLearningMechanism.BinaryClassification, data.CrossValidationFoldsCount);
            this.DoCleanup(modelingResult);
            return model;
        }

        private PredictionModel<ReopenedIssueIntegratedData, ReopenedIssuePrediction> TrainIntegratedPredictionModel(ReopenTrainingIntegratedInputData data, out ModelMetrics metrics, out int countOfRawDataRows, out int countOfFormattedDataRows)
        {
            var modelingResult = _dataModelingService.PrepareReopenedIssueIntegratedData(data.IssuesData, data.Commits, data.CommitsSets, dataReBalancingTechnique:data.DataRebalancingTechnique);

            countOfFormattedDataRows = modelingResult.CountOfFormattedDataRows;
            countOfRawDataRows = modelingResult.CountOfRawDataRows;

            var pipeline = new LearningPipeline();
            foreach (var item in modelingResult.LearningItems)
            {
                pipeline.Add(item);
            }

            var classifier = PredictionModelProvider.GetClassifier(data.ClassificationTool);
            pipeline.Add(classifier);

            var model = pipeline.Train<ReopenedIssueIntegratedData, ReopenedIssuePrediction>();

            //metrics = TrainingModelEvaluator.EvaluateModel(model, modelingResult.ValidationDatasetPath, MachineLearningMechanism.BinaryClassification);
            metrics = TrainingModelEvaluator.CrossValidate<ReopenedIssueIntegratedData, ReopenedIssuePrediction>(pipeline, MachineLearningMechanism.BinaryClassification, data.CrossValidationFoldsCount);
            this.DoCleanup(modelingResult);
            return model;
        }

    }
}

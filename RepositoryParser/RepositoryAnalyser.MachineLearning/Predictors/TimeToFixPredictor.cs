﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.ML.Legacy;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Interfaces;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.MachineLearning.Services;
using ZetaLongPaths;

namespace RepositoryAnalyser.MachineLearning.Predictors
{
    public class TimeToFixPredictor : PredictorBase, ITimeToFixPredictor
    {
        private readonly DataModelingService _dataModelingService;
        public TimeToFixPredictor()
        {
            _dataModelingService = new DataModelingService();
        }

        internal PredictionModel<TimeToFixIssueData, TimeToFixPrediction> TrainPredictionModel(TimeToFixInputData data, out ModelMetrics metrics, out int rawDataRows, out int formattedDataRows)
        {
            var modelingResult = _dataModelingService.PrepareTimeToFixIssueData(data.IssuesData);

            rawDataRows = modelingResult.CountOfRawDataRows;
            formattedDataRows = modelingResult.CountOfFormattedDataRows;

            var pipeline = new LearningPipeline();
            foreach (var item in modelingResult.LearningItems)
            {
                pipeline.Add(item);
            }

            var classifier = PredictionModelProvider.GetRegressor(data.RegressionTool);
            pipeline.Add(classifier);

            var model = pipeline.Train<TimeToFixIssueData, TimeToFixPrediction>();

            //metrics = TrainingModelEvaluator.EvaluateModel(model, modelingResult.ValidationDatasetPath, MachineLearningMechanism.Regression);
            metrics = TrainingModelEvaluator.CrossValidate<TimeToFixIssueData, TimeToFixPrediction>(pipeline, MachineLearningMechanism.Regression, data.CrossValidationFoldsCount);
            this.DoCleanup(modelingResult);
            return model;
        }

        internal PredictionModel<TimeToFixIntegratedData, TimeToFixPrediction> TrainIntegratedPredictionModel(TimeToFixIntegratedInputData data, out ModelMetrics metrics, out int rawDataRows, out int formattedDataRows)
        {
            var modelingResult = _dataModelingService.PrepareTimeToFixIssueIntegratedData(data.IssuesData, data.Commits, commitsSets:data.CommitsSets);

            rawDataRows = modelingResult.CountOfRawDataRows;
            formattedDataRows = modelingResult.CountOfFormattedDataRows;

            var pipeline = new LearningPipeline();
            foreach (var item in modelingResult.LearningItems)
            {
                pipeline.Add(item);
            }

            var classifier = PredictionModelProvider.GetRegressor(data.RegressionTool);
            pipeline.Add(classifier);

            var model = pipeline.Train<TimeToFixIntegratedData, TimeToFixPrediction>();
            //metrics = TrainingModelEvaluator.EvaluateModel(model, modelingResult.ValidationDatasetPath, MachineLearningMechanism.Regression);
            metrics = TrainingModelEvaluator.CrossValidate<TimeToFixIntegratedData, TimeToFixPrediction>(pipeline, MachineLearningMechanism.Regression, data.CrossValidationFoldsCount);
            this.DoCleanup(modelingResult);

            return model;
        }


        public TrainingOutputData TrainAndSavePredictionModel(TimeToFixInputData data)
        {
            var model = this.TrainPredictionModel(data, out var metrics, out var rawDataRows, out var formattedDataRows);
            this.AssertDirectoryExists(data.PredictiveModelPath);

            model.WriteAsync(data.PredictiveModelPath);

            return new TrainingOutputData()
            {
                FilePath = data.PredictiveModelPath,
                Metrics = metrics,
                DateTime = DateTime.Now,
                CountOfRawDataRows = rawDataRows,
                CountOfFormattedDataRows = formattedDataRows
            };
        }

        public TrainingOutputData TrainAndSavePredictionModel(TimeToFixIntegratedInputData data)
        {
            var model = this.TrainIntegratedPredictionModel(data, out var metrics, out var rawDataRows, out var formattedDataRows);
            this.AssertDirectoryExists(data.PredictiveModelPath);

            model.WriteAsync(data.PredictiveModelPath);

            return new TrainingOutputData()
            {
                FilePath = data.PredictiveModelPath,
                Metrics = metrics,
                DateTime = DateTime.Now,
                CountOfRawDataRows = rawDataRows,
                CountOfFormattedDataRows = formattedDataRows,
            };
        }

        public async Task<TimeToFixPredictionResult> Predict(Issue issue, TrainingOutputData trainingModelData)
        {
            if (!ZlpIOHelper.FileExists(trainingModelData.FilePath))
            {
                throw new ArgumentException(); // TODO: Replace with an engine exceptionto 
            }

            var model = await PredictionModel.ReadAsync<TimeToFixIssueData, TimeToFixPrediction>(trainingModelData.FilePath);
            
            var inputData = issue.ToTimeToFixIssueData();
            var actualValue = inputData.TimeToFixInMinutes;

            inputData.TimeToFixInMinutes = 0;

            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = model.Predict(inputData);
            stopwach.Stop();
            inputData.TimeToFixInMinutes = actualValue;

            return new TimeToFixPredictionResult()
            {
                Data = inputData,
                PredictionDate = DateTime.Now.Date,
                Prediction = result,
                PredictionTime = stopwach.Elapsed,
                Accuracy = GetAccuracy(actualValue, result.TimeToFixInMinutes),
                OriginalKey = issue.Key,
                TrainingModel = trainingModelData,
            };
        }

        public async Task<TimeToFixIntegratedPredictionResult> Predict(Issue issue, IList<Commit> commits, TrainingOutputData trainingModelData, IList<CommitsSet> commitsSet = null)
        {
            if (!ZlpIOHelper.FileExists(trainingModelData.FilePath))
            {
                throw new ArgumentException(); // TODO: Replace with an engine exceptionto 
            }

            var model = await PredictionModel.ReadAsync<TimeToFixIntegratedData, TimeToFixPrediction>(trainingModelData.FilePath);

            var inputData = issue.ToTimeToFixIntegratedIssueData(commits, commitsSet);
            var actualValue = inputData.TimeToFixInMinutes;

            inputData.TimeToFixInMinutes = 0;

            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = model.Predict(inputData);
            stopwach.Stop();
            inputData.TimeToFixInMinutes = actualValue;

            return new TimeToFixIntegratedPredictionResult()
            {
                Data = inputData,
                PredictionDate = DateTime.Now.Date,
                Prediction = result,
                PredictionTime = stopwach.Elapsed,
                Accuracy = GetAccuracy(actualValue, result.TimeToFixInMinutes),
                OriginalKey = issue.Key,
                TrainingModel = trainingModelData,
            };
        }
    }
}

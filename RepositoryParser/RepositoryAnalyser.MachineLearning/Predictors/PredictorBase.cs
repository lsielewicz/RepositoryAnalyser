﻿using System;
using RepositoryAnalyser.MachineLearning.Models;
using ZetaLongPaths;

namespace RepositoryAnalyser.MachineLearning.Predictors
{
    public abstract class PredictorBase
    {

        internal void DoCleanup(DataModelingResult modelingResult)
        {
            this.DeleteFileIfExists(modelingResult.ValidationDatasetPath);
            this.DeleteFileIfExists(modelingResult.DatasetPath);
        }

        protected void AssertDirectoryExists(string predictorPath)
        {
            var modelDirectoryPath = ZlpPathHelper.GetDirectoryPathNameFromFilePath(predictorPath);
            if (!ZlpIOHelper.DirectoryExists(modelDirectoryPath))
            {
                ZlpIOHelper.CreateDirectory(modelDirectoryPath);
            }
        }

        protected void DeleteFileIfExists(string path)
        {
            if (ZlpIOHelper.FileExists(path))
            {
                ZlpIOHelper.DeleteFile(path);
            }
        }

        public double GetAccuracy(double actualValue, double predictedValue)
        {
            if (actualValue <= 0)
            {
                return -1;
            }

            var f = actualValue - Math.Abs(actualValue - predictedValue);
            var g = actualValue;
            var acc =  f / g;
            if (acc < 0 || acc > 1)
            {
                return 0;
            }

            return acc;
        }

        protected double GetAccuracy(bool predictedValue, bool actualValue)
        {
            return actualValue == predictedValue ? 1.0 : 0.0;
        }
    }
}

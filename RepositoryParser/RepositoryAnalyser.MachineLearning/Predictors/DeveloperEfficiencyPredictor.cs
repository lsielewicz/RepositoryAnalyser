﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.ML.Legacy;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Interfaces;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.MachineLearning.Services;
using ZetaLongPaths;

namespace RepositoryAnalyser.MachineLearning.Predictors
{
    public class DeveloperEfficiencyPredictor : PredictorBase, IDeveloperEfficiencyPredictor
    {
        private readonly DataModelingService _dataModelingService;

        public DeveloperEfficiencyPredictor()
        {
            _dataModelingService = new DataModelingService();
        }

        public async Task<DeveloperEfficiencyPredictionResult> Predict(IssueTrackerUser user, IList<Issue> issues, TrainingOutputData trainingModelData, EfficiencyWeightsKeeper weightsKeeper)
        {

            if (!ZlpIOHelper.FileExists(trainingModelData.FilePath))
            {
                throw new ArgumentException(); // TODO: Replace with an engine exceptionto 
            }

            var model = await PredictionModel.ReadAsync<DeveloperEfficiencyData, DeveloperEfficiencyPrediction>(trainingModelData.FilePath);

            var inputData = user.ToEfficiencyData(issues, weightsKeeper);
            var actualValue = inputData.Efficiency;

            inputData.Efficiency = 0;

            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = model.Predict(inputData);
            stopwach.Stop();
            inputData.Efficiency =actualValue;

            return new DeveloperEfficiencyPredictionResult()
            {
                Data = inputData,
                PredictionDate = DateTime.Now.Date,
                Prediction = result,
                PredictionTime = stopwach.Elapsed,
                Accuracy = GetAccuracy(actualValue, result.Efficiency),
                OriginalKey = user.UserName,
                TrainingModel = trainingModelData,
            };
        }

        public async Task<DeveloperEfficiencyIntegratedPredictionResult> Predict(IssueTrackerUser user, IList<Issue> issues, IList<Commit> commits, TrainingOutputData trainingModelData, EfficiencyWeightsKeeper weightsKeeper)
        {

            if (!ZlpIOHelper.FileExists(trainingModelData.FilePath))
            {
                throw new ArgumentException(); // TODO: Replace with an engine exceptionto 
            }

            var model = await PredictionModel.ReadAsync<DeveloperEfficiencyIntegratedData, DeveloperEfficiencyPrediction>(trainingModelData.FilePath);

            var inputData = user.ToIntegratedEfficientyData(issues, commits, weightsKeeper);
            var actualValue = inputData.Efficiency;

            inputData.Efficiency = 0;

            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = model.Predict(inputData);
            stopwach.Stop();
            inputData.Efficiency = actualValue;

            return new DeveloperEfficiencyIntegratedPredictionResult()
            {
                Data = inputData,
                PredictionDate = DateTime.Now.Date,
                Prediction = result,
                PredictionTime = stopwach.Elapsed,
                Accuracy = GetAccuracy(actualValue, result.Efficiency),
                OriginalKey = user.UserName,
                TrainingModel = trainingModelData,
            };
        }

        public TrainingOutputData TrainAndSavePredictionModel(DeveloperEfficiencyInputData data)
        {
            var model = this.TrainPredictionModel(data, out var metrics, out var rawDataRows, out var formattedDataRows);
            this.AssertDirectoryExists(data.PredictiveModelPath);

            model.WriteAsync(data.PredictiveModelPath);

            return new TrainingOutputData()
            {
                FilePath = data.PredictiveModelPath,
                Metrics = metrics,
                DateTime = DateTime.Now,
                CountOfRawDataRows = rawDataRows,
                CountOfFormattedDataRows = formattedDataRows
            };
        }

        public TrainingOutputData TrainAndSavePredictionModel(DeveloperEfficiencyIntegratedDataInput data)
        {
            var model = this.TrainPredictionModel(data, out var metrics, out var countOfRawDataRows, out var countOfFormattedDataRows);
            this.AssertDirectoryExists(data.PredictiveModelPath);

            model.WriteAsync(data.PredictiveModelPath);

            return new TrainingOutputData()
            {
                FilePath = data.PredictiveModelPath,
                Metrics = metrics,
                DateTime = DateTime.Now,
                CountOfRawDataRows = countOfRawDataRows,
                CountOfFormattedDataRows = countOfFormattedDataRows
            };
        }

        private PredictionModel<DeveloperEfficiencyData, DeveloperEfficiencyPrediction> TrainPredictionModel(DeveloperEfficiencyInputData data, out ModelMetrics metrics, out int countOfRawDataRows, out int countOfFormattedDataRows)
        {

            var modelingResult = _dataModelingService.PrepareDeveloperEfficiencyModelingResult(data.UsersData,data.IssuesData, data.EfficiencyWeightsKeeper);

            countOfFormattedDataRows = modelingResult.CountOfFormattedDataRows;
            countOfRawDataRows = modelingResult.CountOfRawDataRows;

            var pipeline = new LearningPipeline();
            foreach (var item in modelingResult.LearningItems)
            {
                pipeline.Add(item);
            }

            var regressor = PredictionModelProvider.GetRegressor(data.RegressionTool);
            pipeline.Add(regressor);

            var model = pipeline.Train<DeveloperEfficiencyData, DeveloperEfficiencyPrediction>();

            metrics = TrainingModelEvaluator.CrossValidate<DeveloperEfficiencyData, DeveloperEfficiencyPrediction>(pipeline, MachineLearningMechanism.Regression, data.CrossValidationFoldsCount);
            this.DoCleanup(modelingResult);
            return model;
        }

        private PredictionModel<DeveloperEfficiencyIntegratedData, DeveloperEfficiencyPrediction> TrainPredictionModel(DeveloperEfficiencyIntegratedDataInput data, out ModelMetrics metrics, out int countOfRawDataRows, out int countOfFormattedDataRows)
        {
            var modelingResult = _dataModelingService.PrepareDeveloperEfficiencyIntegratedModelingResult(data.UsersData, data.IssuesData, data.Commits, data.EfficiencyWeightsKeeper);
            countOfFormattedDataRows = modelingResult.CountOfFormattedDataRows;
            countOfRawDataRows = modelingResult.CountOfRawDataRows;

            var pipeline = new LearningPipeline();
            foreach (var item in modelingResult.LearningItems)
            {
                pipeline.Add(item);
            }

            var regressor = PredictionModelProvider.GetRegressor(data.RegressionTool);
            pipeline.Add(regressor);

            var model = pipeline.Train<DeveloperEfficiencyIntegratedData, DeveloperEfficiencyPrediction>();

           
            metrics = TrainingModelEvaluator.CrossValidate<DeveloperEfficiencyIntegratedData, DeveloperEfficiencyPrediction>(pipeline, MachineLearningMechanism.Regression, data.CrossValidationFoldsCount);
            
            this.DoCleanup(modelingResult);
            return model;
        }
    
    }
}

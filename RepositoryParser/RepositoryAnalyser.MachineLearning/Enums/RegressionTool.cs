﻿namespace RepositoryAnalyser.MachineLearning.Enums
{
    public enum RegressionTool
    {
        FastTreeRegressor,
        FastTreeTweedieRegressor,
        StochasticDualCoordinateAscentRegressor,
        OrdinaryLeastSquaresRegressor,
        OnlineGradientDescentRegressor,
        PoissonRegressor,
        GeneralizedAdditiveModelRegressor
    }
}
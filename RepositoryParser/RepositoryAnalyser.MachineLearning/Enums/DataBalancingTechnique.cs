﻿namespace RepositoryAnalyser.MachineLearning.Enums
{
    public enum DataBalancingTechnique
    {
        Undersampling,
        Oversampling
    }
}
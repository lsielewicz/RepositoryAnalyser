﻿namespace RepositoryAnalyser.MachineLearning.Enums
{
    public enum MachineLearningMechanism
    {
        BinaryClassification,
        Regression
    }
}
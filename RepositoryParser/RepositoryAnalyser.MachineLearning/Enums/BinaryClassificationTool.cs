﻿namespace RepositoryAnalyser.MachineLearning.Enums
{
    public enum BinaryClassificationTool
    {
        FastTreeBinaryClassifier,
        StochasticDualCoordinateAscentBinaryClassifier,
        AveragedPerceptronBinaryClassifier,
        FastForestBinaryClassifier,
        LinearSvmBinaryClassifier,
        GeneralizedAdditiveModelBinaryClassifier
    }
}
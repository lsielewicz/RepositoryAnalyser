﻿using System;
using System.Collections.Generic;
using Microsoft.ML.Legacy;
using Microsoft.ML.Legacy.Models;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.MachineLearning.Services
{
    internal static class TrainingModelEvaluator
    {
        internal static ModelMetrics CrossValidate<TData, TPrediction>(LearningPipeline pipeline, MachineLearningMechanism mechanism, int numOfFolds = 2)
            where TData : class where TPrediction : class, new()
        {
            AverageClassificationMetrics classMetrics = null;
            AverageRegressionMetrics regMetrics = null;

            if (numOfFolds < 0)
            {
                return new ModelMetrics()
                {

                };
            }
            if (numOfFolds == 0)
            {
                numOfFolds = 10;
            }
            var trainerKind = mechanism == MachineLearningMechanism.BinaryClassification
                ? MacroUtilsTrainerKinds.SignatureBinaryClassifierTrainer
                : MacroUtilsTrainerKinds.SignatureRegressorTrainer;

            var crossValidator = new CrossValidator()
            {
                NumFolds = numOfFolds,
                Kind = trainerKind
            };

            var result = crossValidator.CrossValidate<TData, TPrediction>(pipeline);

            if (mechanism == MachineLearningMechanism.BinaryClassification)
            {
                var subMetrics = new List<Models.ClassificationMetrics>();

                var accuracy = 0.0;
                var auc = 0.0;
                var f1Score = 0.0;
                var truePositives = 0.0;
                var falseNegatives = 0.0;
                var falsePositives = 0.0;
                var trueNegatives = 0.0;
                var positivePrecisions = 0.0;
                var positiveRecalls = 0.0;
                var negativePrecisions = 0.0;
                var negativeRecalls = 0.0;
                var mathewsCorrelationSum = 0.0;

                var countOfResults = 0;
                foreach (var binaryResult in result.BinaryClassificationMetrics)
                {
                    var confusionMatrix = binaryResult.ConfusionMatrix.ToLocalDomain();
                    if (confusionMatrix == null)
                    {
                        continue;
                    }

                    countOfResults++;

                    var matthewsCorrelationCoeffident = CalculateMatthewsCorrelationCoefficient(confusionMatrix);

                    accuracy += binaryResult.Accuracy;
                    auc += binaryResult.Auc;
                    f1Score += binaryResult.F1Score;
                    truePositives += confusionMatrix.TruePositive;
                    falseNegatives += confusionMatrix.FalseNegative;
                    falsePositives += confusionMatrix.FalsePositve;
                    trueNegatives += confusionMatrix.TrueNegative;
                    positivePrecisions += binaryResult.PositivePrecision;
                    positiveRecalls += binaryResult.PositiveRecall;
                    negativePrecisions += binaryResult.NegativePrecision;
                    negativeRecalls += binaryResult.NegativeRecall;
                    mathewsCorrelationSum += matthewsCorrelationCoeffident;

                    subMetrics.Add(new Models.ClassificationMetrics()
                    {
                        F1Score = binaryResult.F1Score,
                        Accuracy = binaryResult.Accuracy,
                        Auc = binaryResult.Auc,
                        ConfusionMatirx = confusionMatrix,
                        PositivePrecision = binaryResult.PositivePrecision,
                        NegativePrecision = binaryResult.NegativePrecision,
                        NegativeRecall = binaryResult.NegativeRecall,
                        PositiveRecall = binaryResult.PositiveRecall,
                        MatthewsCorrelationCoefficient = matthewsCorrelationCoeffident
                    });

                }

                classMetrics = new AverageClassificationMetrics()
                {
                    Accuracy = accuracy / countOfResults,
                    Auc = auc / countOfResults,
                    F1Score = f1Score / countOfResults,
                    PositivePrecision = positivePrecisions / countOfResults,
                    NegativePrecision = negativePrecisions / countOfResults,
                    NegativeRecall = negativeRecalls / countOfResults,
                    PositiveRecall = positiveRecalls / countOfResults,
                    MatthewsCorrelationCoefficient = mathewsCorrelationSum / countOfResults,
                    ConfusionMatirx = new ConfusionMatirx()
                    {
                        TruePositive = truePositives / countOfResults,
                        TrueNegative = trueNegatives / countOfResults,
                        FalseNegative = falseNegatives / countOfResults,
                        FalsePositve = falsePositives / countOfResults
                    },
                    SubMetrics = subMetrics
                };
            }
            else
            {
                var l1 = 0.0;
                var l2 = 0.0;
                var lossFn = 0.0;
                var rSquared = 0.0;
                var rms = 0.0;
                var subMetrics = new List<Models.RegressionMetrics>();

                var countOfResults = 0;
                for (var index = 2; index < result.RegressionMetrics.Count; index++) //omit first two, to obtain a vaid result
                {
                    countOfResults++;
                    var r = result.RegressionMetrics[index];
                    subMetrics.Add(new Models.RegressionMetrics()
                    {
                        LossFn = r.LossFn,
                        RSquared = r.RSquared,
                        L1 = r.L1,
                        L2 = r.L2,
                        Rms = r.Rms
                    });

                    l1 += r.L1;
                    l2 += r.L2;
                    lossFn += r.LossFn;
                    rSquared += r.RSquared;
                    rms += r.Rms;
                }

                regMetrics = new Models.AverageRegressionMetrics()
                {
                    L1 = l1 / countOfResults,
                    L2 = l2 / countOfResults,
                    LossFn = lossFn / countOfResults,
                    RSquared = rSquared / countOfResults,
                    Rms = rms / countOfResults,
                    SubMetrics = subMetrics
                };
            }

            return new ModelMetrics()
            {
                ClassificationMetrics = classMetrics,
                RegressionMetrics = regMetrics
            };
        }

        private static double CalculateMatthewsCorrelationCoefficient(ConfusionMatirx matrix)
        {
            var tp = matrix.TruePositive;
            var tn = matrix.TrueNegative;
            var fp = matrix.FalsePositve;
            var fn = matrix.FalseNegative;

            var a = (tp * tn) - (fp * fn);
            var b = Math.Sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn));

            return a / b;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.ML.Legacy;
using Microsoft.ML.Legacy.Data;
using Microsoft.ML.Legacy.Transforms;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Constants;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Models;
using ZetaLongPaths;

namespace RepositoryAnalyser.MachineLearning.Services
{
    public class DataModelingService
    {
        private const string RowFormat = "{0}\t";
        private const char TabSeparator = '\t';
        private static readonly UTF8Encoding _encodingFormat = new UTF8Encoding(false);

        internal DataModelingResult PrepareDeveloperEfficiencyModelingResult(IList<IssueTrackerUser> usersData, IList<Issue> issuesData, EfficiencyWeightsKeeper weightsKeeper)
        {
            var fileName = $"{Guid.NewGuid()}.data";
            var validatingFileName = $"{Guid.NewGuid()}.data";

            var filePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), fileName);
            var testDataFilePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), validatingFileName);

            var dataRows = new List<DeveloperEfficiencyData>();
            foreach (var user in usersData)
            {
                var devEfficientyData = user.ToEfficiencyData(issuesData, weightsKeeper);
                dataRows.Add(devEfficientyData);
            }
            var cleanedDataRows = DataCleaningHelper.Clean(dataRows);
            var sortedDataRows = cleanedDataRows.OrderBy(row => row.DeveloperName).ToList();
            foreach (var row in sortedDataRows)
            {
                var rowToAppend = this.BuildRow(row);
                ZlpIOHelper.AppendText(filePath, rowToAppend, _encodingFormat);
            }

            var output = new List<ILearningPipelineItem>
            {
                new TextLoader(filePath).CreateFrom<DeveloperEfficiencyData>(),
                new ColumnCopier((Columns.Efficiency, Columns.Label)),
                new TextFeaturizer(Columns.DeveloperName, Columns.DeveloperName),
                new TextFeaturizer(Columns.DeveloperEmail, Columns.DeveloperEmail),
                new ColumnConcatenator(
                    Columns.Features,
                    Columns.DeveloperName,
                    Columns.DeveloperEmail,
                    Columns.CountOfClosedIssuesPerMonth,
                    Columns.CountOfOpenedIssuesPerMonth,
                    Columns.CountOfReopenedIssuesPerMonth,
                    Columns.CountOfReportedIssuesPerMonth,
                    Columns.CountOfAssignedIssues
                ),
            };

            return new DataModelingResult()
            {
                DatasetPath = testDataFilePath,
                ValidationDatasetPath = testDataFilePath,
                LearningItems = output,
                CountOfRawDataRows = dataRows.Count,
                CountOfFormattedDataRows = sortedDataRows.Count
            };
        }

        internal DataModelingResult PrepareDeveloperEfficiencyIntegratedModelingResult(IList<IssueTrackerUser> usersData, IList<Issue> issuesData, IList<Commit> commits, EfficiencyWeightsKeeper weightsKeeper)
        {

            var fileName = $"{Guid.NewGuid()}.data";
            var validatingFileName = $"{Guid.NewGuid()}.data";

            var filePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), fileName);
            var testDataFilePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), validatingFileName);


            var dataRows = new List<DeveloperEfficiencyIntegratedData>();
            foreach (var user in usersData)
            {
                var devEfficientyData = user.ToIntegratedEfficientyData(issuesData, commits, weightsKeeper);
                dataRows.Add(devEfficientyData);
            }

            var cleanedDataRows = DataCleaningHelper.Clean(dataRows);
            var sortedDataRows = cleanedDataRows.OrderBy(row => row.DeveloperName).ToList();
            foreach (var row in sortedDataRows)
            {
                var rowToAppend = this.BuildRow(row);

                ZlpIOHelper.AppendText(filePath, rowToAppend, _encodingFormat);
                ZlpIOHelper.AppendText(testDataFilePath, rowToAppend, _encodingFormat);
            }


            var output = new List<ILearningPipelineItem>
            {
                new TextLoader(filePath).CreateFrom<DeveloperEfficiencyIntegratedData>(false,TabSeparator),
                new ColumnCopier((Columns.Efficiency, Columns.Label)),
                new TextFeaturizer(Columns.DeveloperName, Columns.DeveloperName),
                new TextFeaturizer(Columns.DeveloperEmail, Columns.DeveloperEmail),
                new ColumnConcatenator(
                    Columns.Features,
                    Columns.DeveloperName,
                    Columns.DeveloperEmail,
                    Columns.CountOfClosedIssuesPerMonth,
                    Columns.CountOfOpenedIssuesPerMonth,
                    Columns.CountOfReopenedIssuesPerMonth,
                    Columns.CountOfReportedIssuesPerMonth,
                    Columns.CountOfAssignedIssues,
                    Columns.CommitsCountPerMonth,
                    Columns.AddedLinesPerMonth,
                    Columns.DeletedLinesPerMonth
                )
            };

            return new DataModelingResult()
            {
                DatasetPath = testDataFilePath,
                ValidationDatasetPath = testDataFilePath,
                LearningItems = output,
                CountOfRawDataRows = dataRows.Count,
                CountOfFormattedDataRows = sortedDataRows.Count
            };
        }

        public DataModelingResult PrepareReopenedIssueData(IList<Issue> issues, DataBalancingTechnique dataReBalancingTechnique = DataBalancingTechnique.Oversampling)
        {
            var fileName = $"{Guid.NewGuid()}.data";
            var validatingFileName = $"{Guid.NewGuid()}.data";

            var filePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), fileName);
            var testDataFilePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), validatingFileName);
            var issueDataRows = new List<ReopenedIssueData>(issues.Select(i => i.ToReopenedIssueData()));

            var cleanedDataRows = DataCleaningHelper.Clean(issueDataRows);
            var balancedIssueDataRows = DataRebalancingHelper.BalanceData(cleanedDataRows, dataReBalancingTechnique);
            var sortedDataRows = balancedIssueDataRows.OrderBy(row => row.ProjectName).ToList();
            foreach (var rowData in sortedDataRows)
            {
                var rowToAppend = this.BuildRow(rowData);
                ZlpIOHelper.AppendText(filePath, rowToAppend, _encodingFormat);
            }

            var output = new List<ILearningPipelineItem>
            {
                new TextLoader(filePath).CreateFrom<ReopenedIssueData>(),
                new TextFeaturizer(Columns.Environment, Columns.Environment),
                new TextFeaturizer(Columns.Type, Columns.Type),
                new TextFeaturizer(Columns.ProjectName, Columns.ProjectName),
                new TextFeaturizer(Columns.AsigneeEmail, Columns.AsigneeEmail),
                new TextFeaturizer(Columns.ReporterEmail, Columns.ReporterEmail),
                new TextFeaturizer(Columns.Priority, Columns.Priority),
                new ColumnConcatenator(
                    Columns.Features,
                    Columns.Environment,
                    Columns.Type,
                    Columns.CommentsCount,
                    Columns.CommentsLenght,
                    Columns.ReporterCommentsCount,
                    Columns.ProjectName,
                    Columns.AsigneeEmail,
                    Columns.ReporterEmail,
                    Columns.Priority,
                    Columns.TimeSpent,
                    Columns.IsSubTask,
                    Columns.AttachmentsCount
                    ),
            };

            return new DataModelingResult()
            {
                DatasetPath = testDataFilePath,
                ValidationDatasetPath = testDataFilePath,
                LearningItems = output,
                CountOfRawDataRows = issueDataRows.Count,
                CountOfFormattedDataRows = sortedDataRows.Count
            };
        }

        public DataModelingResult PrepareReopenedIssueIntegratedData(IList<Issue> issues, IList<Commit> commits, IList<CommitsSet> commitsSet = null, DataBalancingTechnique dataReBalancingTechnique = DataBalancingTechnique.Oversampling)
        {
            var fileName = $"{Guid.NewGuid()}.data";
            var validatingFileName = $"{Guid.NewGuid()}.data";

            var filePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), fileName);
            var testDataFilePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), validatingFileName);

            var issueDataRows = new List<ReopenedIssueIntegratedData>();
            foreach (var issue in issues)
            {
                issueDataRows.Add(issue.ToReopenedIntegratedIssueData(commits, commitsSet));
            }
            var cleanedDataRows = DataCleaningHelper.Clean(issueDataRows);
            var balancedIssueDataRows = DataRebalancingHelper.BalanceData(cleanedDataRows, dataReBalancingTechnique);
            var sortedDataRows = balancedIssueDataRows.OrderBy(row => row.ProjectName).ToList();
            foreach (var rowData in sortedDataRows)
            {
                var rowToAppend = this.BuildRow(rowData);
                ZlpIOHelper.AppendText(filePath, rowToAppend, _encodingFormat);
            }

            var output = new List<ILearningPipelineItem>
            {
                new TextLoader(filePath).CreateFrom<ReopenedIssueIntegratedData>(false,TabSeparator),
                new TextFeaturizer(Columns.Environment, Columns.Environment),
                new TextFeaturizer(Columns.Type, Columns.Type),
                new TextFeaturizer(Columns.ProjectName, Columns.ProjectName),
                new TextFeaturizer(Columns.AsigneeEmail, Columns.AsigneeEmail),
                new TextFeaturizer(Columns.ReporterEmail, Columns.ReporterEmail),
                new TextFeaturizer(Columns.Priority, Columns.Priority),
                new TextFeaturizer(Columns.Revision, Columns.Revision),
                new ColumnConcatenator(
                    Columns.Features,
                    Columns.Environment,
                    Columns.Type,
                    Columns.CommentsCount,
                    Columns.CommentsLenght,
                    Columns.ReporterCommentsCount,
                    Columns.ProjectName,
                    Columns.AsigneeEmail,
                    Columns.ReporterEmail,
                    Columns.Priority,
                    Columns.TimeSpent,
                    Columns.IsSubTask,
                    Columns.AttachmentsCount,
                    Columns.CommitsCount,
                    Columns.Revision,
                    Columns.CommitsMessagesCount,
                    Columns.CommitsMessagesLength,
                    Columns.CountOfModifiedFiles,
                    Columns.AddedLines,
                    Columns.DeletedLines)

            };

            return new DataModelingResult()
            {
                DatasetPath = testDataFilePath,
                ValidationDatasetPath = testDataFilePath,
                LearningItems = output,
                CountOfRawDataRows = issueDataRows.Count,
                CountOfFormattedDataRows = sortedDataRows.Count
            };
        }

        private IList<Issue> FillClosedTime(IList<Issue> inputIssues)
        {
            foreach (var issue in inputIssues)
            {
                if (issue.ClosedDateTime != null || issue.UpdatedDateTime == null || issue.Status == null || issue.Status.Name != "Closed")
                {
                    continue;
                }

                issue.ClosedDateTime = issue.UpdatedDateTime;
            }

            return inputIssues.ToList();
        }

        public DataModelingResult PrepareTimeToFixIssueData(IList<Issue> issues)
        {
            var fileName = $"{Guid.NewGuid()}.data";
            var validatingFileName = $"{Guid.NewGuid()}.data";

            var filePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), fileName);
            var testDataFilePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), validatingFileName);
            issues = this.FillClosedTime(issues);
            issues = issues.Where(issue => (issue.ClosedDateTime != null && issue.ClosedDateTime > issue.CreatedDateTime)).ToList();

            var issueDataRows = new List<TimeToFixIssueData>(issues.Select(i => i.ToTimeToFixIssueData()).Where(i => i.TimeToFixInMinutes > 0));
            var cleanedDataRows = DataCleaningHelper.Clean(issueDataRows);
            var sortedDataRows = cleanedDataRows.OrderBy(row => row.ProjectName).ToList();
            foreach (var rowData in sortedDataRows)
            {
                var rowToAppend = this.BuildRow(rowData);
                ZlpIOHelper.AppendText(filePath, rowToAppend, _encodingFormat);
            }


            var output = new List<ILearningPipelineItem>
            {
                new TextLoader(filePath).CreateFrom<TimeToFixIssueData>(false,TabSeparator),
                new ColumnCopier((Columns.TimeToFixInMinutes, Columns.Label)),
                new TextFeaturizer(Columns.Environment, Columns.Environment),
                new TextFeaturizer(Columns.Type, Columns.Type),
                new TextFeaturizer(Columns.ProjectName, Columns.ProjectName),
                new TextFeaturizer(Columns.AsigneeEmail, Columns.AsigneeEmail),
                new TextFeaturizer(Columns.ReporterEmail, Columns.ReporterEmail),
                new TextFeaturizer(Columns.Priority, Columns.Priority),
                new ColumnConcatenator(
                    Columns.Features,
                    Columns.Environment,
                    Columns.Type,
                    Columns.CommentsCount,
                    Columns.CommentsLenght,
                    Columns.ReporterCommentsCount,
                    Columns.ProjectName,
                    Columns.AsigneeEmail,
                    Columns.ReporterEmail,
                    Columns.Priority,
                    Columns.TimeSpent,
                    Columns.IsSubTask,
                    Columns.AttachmentsCount
                ),
                new MinMaxNormalizer(Columns.TimeSpent),
                //new MeanVarianceNormalizer(Columns.CommentsCount, Columns.CommentsLenght,Columns.ReporterCommentsCount, Columns.TimeSpent),
                //new RowRangeFilter()
            };

            return new DataModelingResult()
            {
                DatasetPath = testDataFilePath,
                ValidationDatasetPath = testDataFilePath,
                LearningItems = output,
                CountOfRawDataRows = issueDataRows.Count,
                CountOfFormattedDataRows = sortedDataRows.Count
            };
        }

        public DataModelingResult PrepareTimeToFixIssueIntegratedData(IList<Issue> issues, IList<Commit> commits, IList<CommitsSet> commitsSets = null)
        {
            var fileName = $"{Guid.NewGuid()}.data";
            var validatingFileName = $"{Guid.NewGuid()}.data";

            var filePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), fileName);
            var testDataFilePath = ZlpPathHelper.Combine(ZlpPathHelper.GetTempDirectoryPath(), validatingFileName);

            issues = issues.Where(issue => issue.ClosedDateTime != null && issue.ClosedDateTime > issue.CreatedDateTime).ToList();

            var issueDataRows = new List<TimeToFixIntegratedData>();
            foreach (var issue in issues)
            {
                issueDataRows.Add(issue.ToTimeToFixIntegratedIssueData(commits, commitsSets));
            }
            var cleanedDataRows = DataCleaningHelper.Clean(issueDataRows);
            var sortedDataRows = cleanedDataRows.OrderBy(row => row.ProjectName).ToList();
            foreach (var rowData in sortedDataRows)
            {
                var rowToAppend = this.BuildRow(rowData);
                ZlpIOHelper.AppendText(filePath, rowToAppend, _encodingFormat);
            }

            var output = new List<ILearningPipelineItem>
            {
                new TextLoader(filePath).CreateFrom<TimeToFixIntegratedData>(),
                new ColumnCopier((Columns.TimeToFixInMinutes, Columns.Label)),
                new TextFeaturizer(Columns.Environment, Columns.Environment),
                new TextFeaturizer(Columns.Type, Columns.Type),
                new TextFeaturizer(Columns.ProjectName, Columns.ProjectName),
                new TextFeaturizer(Columns.AsigneeEmail, Columns.AsigneeEmail),
                new TextFeaturizer(Columns.ReporterEmail, Columns.ReporterEmail),
                new TextFeaturizer(Columns.Priority, Columns.Priority),
                new TextFeaturizer(Columns.Revision, Columns.Revision),
                new ColumnConcatenator(
                    Columns.Features,
                    Columns.Environment,
                    Columns.Type,
                    Columns.CommentsCount,
                    Columns.CommentsLenght,
                    Columns.ReporterCommentsCount,
                    Columns.ProjectName,
                    Columns.AsigneeEmail,
                    Columns.ReporterEmail,
                    Columns.Priority,
                    Columns.TimeSpent,
                    Columns.IsSubTask,
                    Columns.AttachmentsCount,
                    Columns.CommitsCount,
                    Columns.Revision,
                    Columns.CommitsMessagesCount,
                    Columns.CommitsMessagesLength,
                    Columns.CountOfModifiedFiles,
                    Columns.AddedLines,
                    Columns.DeletedLines)
            };

            return new DataModelingResult()
            {
                DatasetPath = testDataFilePath,
                ValidationDatasetPath = testDataFilePath,
                LearningItems = output,
                CountOfRawDataRows = issueDataRows.Count,
                CountOfFormattedDataRows = sortedDataRows.Count
            };
        }



        private string BuildRow(ReopenedIssueIntegratedData issueData)
        {
            var rowBuilder = new StringBuilder();

            rowBuilder.AppendFormat(RowFormat, issueData.WasReopened);
            rowBuilder.AppendFormat(RowFormat, issueData.Environment);
            rowBuilder.AppendFormat(RowFormat, issueData.Type);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsLenght);
            rowBuilder.AppendFormat(RowFormat, issueData.ReporterCommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.ProjectName);
            rowBuilder.AppendFormat(RowFormat, issueData.AsigneeEmail);
            rowBuilder.AppendFormat(RowFormat, issueData.ReporterEmail);
            rowBuilder.AppendFormat(RowFormat, issueData.Priority);
            rowBuilder.AppendFormat(RowFormat, issueData.TimeSpent);
            rowBuilder.AppendFormat(RowFormat, issueData.IsSubTask);
            rowBuilder.AppendFormat(RowFormat, issueData.AttachmentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.Revision);
            rowBuilder.AppendFormat(RowFormat, issueData.CommitsMessagesCount);
            rowBuilder.AppendFormat(RowFormat, issueData.CommitsMessagesLength);
            rowBuilder.AppendFormat(RowFormat, issueData.CountOfModifiedFiles);
            rowBuilder.AppendFormat(RowFormat, issueData.AddedLines);
            rowBuilder.AppendFormat(RowFormat, issueData.DeletedLines);

            rowBuilder.AppendLine();
            return rowBuilder.ToString();
        }

        private string BuildRow(TimeToFixIntegratedData issueData)
        {
            var rowBuilder = new StringBuilder();

            rowBuilder.AppendFormat(RowFormat, issueData.TimeToFixInMinutes);
            rowBuilder.AppendFormat(RowFormat, issueData.Environment);
            rowBuilder.AppendFormat(RowFormat, issueData.Type);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsLenght);
            rowBuilder.AppendFormat(RowFormat, issueData.ReporterCommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.ProjectName);
            rowBuilder.AppendFormat(RowFormat, issueData.AsigneeEmail);
            rowBuilder.AppendFormat(RowFormat, issueData.ReporterEmail);
            rowBuilder.AppendFormat(RowFormat, issueData.Priority);
            rowBuilder.AppendFormat(RowFormat, issueData.TimeSpent);
            rowBuilder.AppendFormat(RowFormat, issueData.IsSubTask);
            rowBuilder.AppendFormat(RowFormat, issueData.AttachmentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.Revision);
            rowBuilder.AppendFormat(RowFormat, issueData.CommitsMessagesCount);
            rowBuilder.AppendFormat(RowFormat, issueData.CommitsMessagesLength);
            rowBuilder.AppendFormat(RowFormat, issueData.CountOfModifiedFiles);
            rowBuilder.AppendFormat(RowFormat, issueData.AddedLines);
            rowBuilder.AppendFormat(RowFormat, issueData.DeletedLines);
            rowBuilder.AppendLine();
            return rowBuilder.ToString();
        }

        private string BuildRow(TimeToFixIssueData issueData)
        {
            var rowBuilder = new StringBuilder();

            rowBuilder.AppendFormat(RowFormat, issueData.TimeToFixInMinutes);
            rowBuilder.AppendFormat(RowFormat, issueData.Environment);
            rowBuilder.AppendFormat(RowFormat, issueData.Type);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsLenght);
            rowBuilder.AppendFormat(RowFormat, issueData.ReporterCommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.ProjectName);
            rowBuilder.AppendFormat(RowFormat, issueData.AsigneeEmail);
            rowBuilder.AppendFormat(RowFormat, issueData.ReporterEmail);
            rowBuilder.AppendFormat(RowFormat, issueData.Priority);
            rowBuilder.AppendFormat(RowFormat, issueData.TimeSpent);
            rowBuilder.AppendFormat(RowFormat, issueData.IsSubTask);
            rowBuilder.AppendFormat(RowFormat, issueData.AttachmentsCount);
            rowBuilder.AppendLine();

            return rowBuilder.ToString();
        }

        private string BuildRow(DeveloperEfficiencyData data)
        {
            var rowBuilder = new StringBuilder();

            rowBuilder.AppendFormat(RowFormat, data.Efficiency);
            rowBuilder.AppendFormat(RowFormat, data.DeveloperName);
            rowBuilder.AppendFormat(RowFormat, data.DeveloperEmail);
            rowBuilder.AppendFormat(RowFormat, data.CountOfClosedIssuesPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.CountOfOpenedIssuesPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.CountOfReopenedIssuesPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.CountOfReportedIssuesPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.CountOfAssignedIssues);
            rowBuilder.AppendLine();

            return rowBuilder.ToString();
        }

        private string BuildRow(DeveloperEfficiencyIntegratedData data)
        {
            var rowBuilder = new StringBuilder();

            rowBuilder.AppendFormat(RowFormat, data.Efficiency);
            rowBuilder.AppendFormat(RowFormat, data.DeveloperName);
            rowBuilder.AppendFormat(RowFormat, data.DeveloperEmail);
            rowBuilder.AppendFormat(RowFormat, data.CountOfClosedIssuesPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.CountOfOpenedIssuesPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.CountOfReopenedIssuesPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.CountOfReportedIssuesPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.CountOfAssignedIssues);

            rowBuilder.AppendFormat(RowFormat, data.CommitsCountPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.AddedLinesPerMonth);
            rowBuilder.AppendFormat(RowFormat, data.DeletedLinesPerMonth);
            rowBuilder.AppendLine();

            return rowBuilder.ToString();
        }

        private string BuildRow(ReopenedIssueData issueData)
        {
            var rowBuilder = new StringBuilder();

            rowBuilder.AppendFormat(RowFormat, issueData.WasReopened);
            rowBuilder.AppendFormat(RowFormat, issueData.Environment);
            rowBuilder.AppendFormat(RowFormat, issueData.Type);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.CommentsLenght);
            rowBuilder.AppendFormat(RowFormat, issueData.ReporterCommentsCount);
            rowBuilder.AppendFormat(RowFormat, issueData.ProjectName);
            rowBuilder.AppendFormat(RowFormat, issueData.AsigneeEmail);
            rowBuilder.AppendFormat(RowFormat, issueData.ReporterEmail);
            rowBuilder.AppendFormat(RowFormat, issueData.Priority);
            rowBuilder.AppendFormat(RowFormat, issueData.TimeSpent);
            rowBuilder.AppendFormat(RowFormat, issueData.IsSubTask);
            rowBuilder.AppendFormat(RowFormat, issueData.AttachmentsCount);
            rowBuilder.AppendLine();

            return rowBuilder.ToString();
        }


    }
}

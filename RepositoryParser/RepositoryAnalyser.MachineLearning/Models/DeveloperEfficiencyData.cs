﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class DeveloperEfficiencyData
    {
        [Column(ordinal: "0")]
        public float Efficiency;

        [Column("1")]
        public string DeveloperName;

        [Column("2")]
        public string DeveloperEmail;

        [Column("3")]
        public float CountOfClosedIssuesPerMonth;

        [Column("4")]
        public float CountOfOpenedIssuesPerMonth;

        [Column("5")]
        public float CountOfReopenedIssuesPerMonth;

        [Column("6")]
        public float CountOfReportedIssuesPerMonth;

        [Column("7", Columns.CountOfAssignedIssues)]
        public float CountOfAssignedIssues;
    }
}

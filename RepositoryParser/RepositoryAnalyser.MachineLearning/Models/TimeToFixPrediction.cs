﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class TimeToFixPrediction
    {
        [ColumnName(Columns.Score)]
        public float TimeToFixInMinutes;
    }
}

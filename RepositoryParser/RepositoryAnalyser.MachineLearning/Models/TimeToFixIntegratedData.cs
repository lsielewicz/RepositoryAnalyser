﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class TimeToFixIntegratedData : IntegratedIssueData
    {
        public TimeToFixIntegratedData()
        {

        }

        public TimeToFixIntegratedData(IssueData issueData) : base(issueData)
        {

        }

        [Column(ordinal: "0", name: Columns.TimeToFixInMinutes)]
        public float TimeToFixInMinutes;
    }
}

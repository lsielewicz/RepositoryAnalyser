﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class TimeToFixIntegratedInputData : TimeToFixInputData
    {
        public IList<Commit> Commits { get; set; }

        public IList<CommitsSet> CommitsSets { get; set; }
    }
}

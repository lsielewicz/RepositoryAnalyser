﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class DeveloperEfficiencyIntegratedData : DeveloperEfficiencyData
    {
        [Column("8", Columns.CommitsCountPerMonth)]
        public float CommitsCountPerMonth;

        [Column("9", Columns.AddedLinesPerMonth)]
        public float AddedLinesPerMonth;

        [Column("10", Columns.DeletedLinesPerMonth)]
        public float DeletedLinesPerMonth;

        public DeveloperEfficiencyIntegratedData()
        {
            
        }

        public DeveloperEfficiencyIntegratedData(DeveloperEfficiencyData baseModel)
        {
            this.DeveloperName = baseModel.DeveloperName;
            this.DeveloperEmail = baseModel.DeveloperEmail;
            this.CountOfClosedIssuesPerMonth = baseModel.CountOfClosedIssuesPerMonth;
            this.CountOfOpenedIssuesPerMonth = baseModel.CountOfOpenedIssuesPerMonth;
            this.CountOfReopenedIssuesPerMonth = baseModel.CountOfReopenedIssuesPerMonth;
            this.CountOfReportedIssuesPerMonth = baseModel.CountOfReportedIssuesPerMonth;
            this.CountOfAssignedIssues = baseModel.CountOfAssignedIssues;
        }
    }
}

﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class DeveloperEfficiencyIntegratedDataInput : DeveloperEfficiencyInputData
    {
        public IList<Commit> Commits { get; set; }
    }
}

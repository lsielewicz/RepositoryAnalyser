﻿namespace RepositoryAnalyser.MachineLearning.Models
{
    public class InputData
    {
        public string PredictiveModelPath { get; set; }
        public int CrossValidationFoldsCount { get; set; }
    }
}

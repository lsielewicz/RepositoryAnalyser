﻿
using System;
using System.Text;
using RepositoryAnalyser.Locale.Languages;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class TimeToFixPredictionResult : PredictionResult<TimeToFixIssueData, TimeToFixPrediction>
    {
        public override string ToString()
        {
            var resManager = Resources.ResourceManager;
            var stringBuilder = new StringBuilder();
            var beginning = base.ToString();
            var predictedValueTimeSpan = new TimeSpan(0, (int) this.Prediction.TimeToFixInMinutes, 0);

            stringBuilder.AppendLine(beginning);
            stringBuilder.AppendLine(resManager.GetString("InputData"));
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("AsigneeEmail")}:{this.Data.AsigneeEmail}");
            //stringBuilder.AppendLine($"{resManager.GetString("Comments")}:{this.Data.Comments}");
          
            stringBuilder.AppendLine($"{resManager.GetString("Environment")}:{this.Data.Environment}");
            stringBuilder.AppendLine($"{resManager.GetString("ProjectName")}:{this.Data.ProjectName}");
            stringBuilder.AppendLine($"{resManager.GetString("ReporterEmail")}:{this.Data.ReporterEmail}");
            stringBuilder.AppendLine($"{resManager.GetString("Type")}:{this.Data.Type}");
            stringBuilder.AppendLine($"{resManager.GetString("TimeToFixInMinutes")}:{this.Data.TimeToFixInMinutes}");
            stringBuilder.AppendLine();
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("PredictedValue")}:{this.Prediction.TimeToFixInMinutes}");
            stringBuilder.AppendLine($"{predictedValueTimeSpan.Hours}h::{predictedValueTimeSpan.Minutes}m::{predictedValueTimeSpan.Seconds}s");
            return stringBuilder.ToString();
        }
    }

    public class TimeToFixIntegratedPredictionResult : PredictionResult<TimeToFixIntegratedData, TimeToFixPrediction>
    {
        public override string ToString()
        {
            var resManager = Resources.ResourceManager;
            var stringBuilder = new StringBuilder();
            var beginning = base.ToString();
            var predictedValueTimeSpan = new TimeSpan(0, (int)this.Prediction.TimeToFixInMinutes, 0);

            stringBuilder.AppendLine(beginning);
            stringBuilder.AppendLine(resManager.GetString("InputData"));
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("AsigneeEmail")}:{this.Data.AsigneeEmail}");
           // stringBuilder.AppendLine($"{resManager.GetString("Comments")}:{this.Data.Comments}");
            stringBuilder.AppendLine($"{resManager.GetString("Environment")}:{this.Data.Environment}");
            stringBuilder.AppendLine($"{resManager.GetString("ProjectName")}:{this.Data.ProjectName}");
            stringBuilder.AppendLine($"{resManager.GetString("ReporterEmail")}:{this.Data.ReporterEmail}");
            stringBuilder.AppendLine($"{resManager.GetString("Type")}:{this.Data.Type}");
            stringBuilder.AppendLine($"{resManager.GetString("TimeToFixInMinutes")}:{this.Data.TimeToFixInMinutes}");
           // stringBuilder.AppendLine($"{resManager.GetString("ChangesPaths")}:{this.Data.ChangesPaths}");
          //  stringBuilder.AppendLine($"{resManager.GetString("CommitMessage")}:{this.Data.CommitMessage}");
          //  stringBuilder.AppendLine($"{resManager.GetString("CommitsRevisions")}:{this.Data.CommitsRevisions}");
            stringBuilder.AppendLine();
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("PredictedValue")}:{this.Prediction.TimeToFixInMinutes}");
            stringBuilder.AppendLine($"{predictedValueTimeSpan.Hours}h::{predictedValueTimeSpan.Minutes}m::{predictedValueTimeSpan.Seconds}s");
            return stringBuilder.ToString();
        }
    }

    public class IssueReopenPredictionResult : PredictionResult<ReopenedIssueData, ReopenedIssuePrediction>
    {
        public override string ToString()
        {
            var resManager = Resources.ResourceManager;
            var stringBuilder = new StringBuilder();
            var beginning = base.ToString();

            stringBuilder.AppendLine(beginning);
            stringBuilder.AppendLine(resManager.GetString("InputData"));
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("AsigneeEmail")}:{this.Data.AsigneeEmail}");
         //   stringBuilder.AppendLine($"{resManager.GetString("Comments")}:{this.Data.Comments}");
            stringBuilder.AppendLine($"{resManager.GetString("Environment")}:{this.Data.Environment}");
            stringBuilder.AppendLine($"{resManager.GetString("ProjectName")}:{this.Data.ProjectName}");
            stringBuilder.AppendLine($"{resManager.GetString("ReporterEmail")}:{this.Data.ReporterEmail}");
            stringBuilder.AppendLine($"{resManager.GetString("Type")}:{this.Data.Type}");
            stringBuilder.AppendLine();
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("PredictedValue")}:{this.Prediction.IsGoingToBeReopened}");
            
            return stringBuilder.ToString();
        }
    }

    public class IssueReopenIntegratedPredictionResult : PredictionResult<ReopenedIssueIntegratedData, ReopenedIssuePrediction>
    {
        public override string ToString()
        {
            var resManager = Resources.ResourceManager;
            var stringBuilder = new StringBuilder();
            var beginning = base.ToString();

            stringBuilder.AppendLine(beginning);
            stringBuilder.AppendLine(resManager.GetString("InputData"));
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("AsigneeEmail")}:{this.Data.AsigneeEmail}");
            //stringBuilder.AppendLine($"{resManager.GetString("Comments")}:{this.Data.Comments}");
            stringBuilder.AppendLine($"{resManager.GetString("Environment")}:{this.Data.Environment}");
            stringBuilder.AppendLine($"{resManager.GetString("ProjectName")}:{this.Data.ProjectName}");
            stringBuilder.AppendLine($"{resManager.GetString("ReporterEmail")}:{this.Data.ReporterEmail}");
            stringBuilder.AppendLine($"{resManager.GetString("Type")}:{this.Data.Type}");
            stringBuilder.AppendLine($"{resManager.GetString("WasReopened")}:{this.Data.WasReopened}");
         //   stringBuilder.AppendLine($"{resManager.GetString("ChangesPaths")}:{this.Data.ChangesPaths}");
          //  stringBuilder.AppendLine($"{resManager.GetString("CommitMessage")}:{this.Data.CommitMessage}");
          //  stringBuilder.AppendLine($"{resManager.GetString("CommitsRevisions")}:{this.Data.CommitsRevisions}");
            stringBuilder.AppendLine();
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("PredictedValue")}:{this.Prediction.IsGoingToBeReopened}");
          
            return stringBuilder.ToString();
        }
    }

    public class DeveloperEfficiencyPredictionResult : PredictionResult<DeveloperEfficiencyData, DeveloperEfficiencyPrediction>
    {
        public override string ToString()
        {
            var resManager = Resources.ResourceManager;
            var stringBuilder = new StringBuilder();
            var beginning = base.ToString();

            stringBuilder.AppendLine(beginning);
            stringBuilder.AppendLine(resManager.GetString("InputData"));
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("CountOfAssignedIssues")}:{this.Data.CountOfAssignedIssues}");
            stringBuilder.AppendLine($"{resManager.GetString("CountOfClosedIssuesPerMonth")}:{this.Data.CountOfClosedIssuesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("CountOfOpenedIssuesPerMonth")}:{this.Data.CountOfOpenedIssuesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("CountOfReopenedIssuesPerMonth")}:{this.Data.CountOfReopenedIssuesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("CountOfReportedIssuesPerMonth")}:{this.Data.CountOfReportedIssuesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("DeveloperEmail")}:{this.Data.DeveloperEmail}");
            stringBuilder.AppendLine($"{resManager.GetString("DeveloperName")}:{this.Data.DeveloperName}");
            stringBuilder.AppendLine($"{resManager.GetString("Efficiency")}:{this.Data.Efficiency}");
            stringBuilder.AppendLine();
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("PredictedValue")}:{this.Prediction.Efficiency}");

            return stringBuilder.ToString();
        }
    }

    public class DeveloperEfficiencyIntegratedPredictionResult : PredictionResult<DeveloperEfficiencyIntegratedData, DeveloperEfficiencyPrediction>
    {
        public override string ToString()
        {
            var resManager = Resources.ResourceManager;
            var stringBuilder = new StringBuilder();
            var beginning = base.ToString();

            stringBuilder.AppendLine(beginning);
            stringBuilder.AppendLine(resManager.GetString("InputData"));
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("CountOfAssignedIssues")}:{this.Data.CountOfAssignedIssues}");
            stringBuilder.AppendLine($"{resManager.GetString("CountOfClosedIssuesPerMonth")}:{this.Data.CountOfClosedIssuesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("CountOfOpenedIssuesPerMonth")}:{this.Data.CountOfOpenedIssuesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("CountOfReopenedIssuesPerMonth")}:{this.Data.CountOfReopenedIssuesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("CountOfReportedIssuesPerMonth")}:{this.Data.CountOfReportedIssuesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("DeveloperEmail")}:{this.Data.DeveloperEmail}");
            stringBuilder.AppendLine($"{resManager.GetString("DeveloperName")}:{this.Data.DeveloperName}");
            stringBuilder.AppendLine($"{resManager.GetString("AddedLinesPerMonth")}:{this.Data.AddedLinesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("DeletedLinesPerMonth")}:{this.Data.DeletedLinesPerMonth}");
            stringBuilder.AppendLine($"{resManager.GetString("CommitsCountPerMonth")}:{this.Data.CommitsCountPerMonth}");
          //  stringBuilder.AppendLine($"{resManager.GetString("CommitsMessages")}:{this.Data.CommitsMessages}");
         //   stringBuilder.AppendLine($"{resManager.GetString("CommitsRevisions")}:{this.Data.CommitsRevisions}");
            stringBuilder.AppendLine($"{resManager.GetString("Efficiency")}:{this.Data.Efficiency}");
            stringBuilder.AppendLine();
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"{resManager.GetString("PredictedValue")}:{this.Prediction.Efficiency}");

            return stringBuilder.ToString();
        }
    }
}

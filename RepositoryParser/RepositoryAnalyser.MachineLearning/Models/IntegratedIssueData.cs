﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class IntegratedIssueData : IssueData
    {
        public IntegratedIssueData()
        {
            
        }

        public IntegratedIssueData(IssueData issueData) : base(issueData)
        {
            
        }

        [Column("13", Columns.CommitsCount)]
        public float CommitsCount;

        [Column("14", Columns.Revision)]
        public string Revision;

        [Column("15", Columns.CommitsMessagesCount)]
        public float CommitsMessagesCount;

        [Column("16", Columns.CommitsMessagesLength)]
        public float CommitsMessagesLength;

        [Column("17", Columns.CountOfModifiedFiles)]
        public float CountOfModifiedFiles;

        [Column("18", Columns.AddedLines)]
        public float AddedLines;

        [Column("19", Columns.DeletedLines)]
        public float DeletedLines;
    }
}

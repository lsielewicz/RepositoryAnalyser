﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.MachineLearning.Enums;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class TimeToFixInputData : InputData
    {
        public IList<Issue> IssuesData { get; set; }
        public RegressionTool RegressionTool { get; set; }
    }
}

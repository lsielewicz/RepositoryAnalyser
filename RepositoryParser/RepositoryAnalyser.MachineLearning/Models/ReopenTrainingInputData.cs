﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.MachineLearning.Enums;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class ReopenTrainingInputData : InputData
    {
        public DataBalancingTechnique DataRebalancingTechnique { get; set; }
        public IList<Issue> IssuesData { get; set; }
        public BinaryClassificationTool ClassificationTool { get; set; }
        
    }
}
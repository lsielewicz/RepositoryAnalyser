﻿using System;
using System.Xml.Serialization;

namespace RepositoryAnalyser.MachineLearning.Models
{
    [Serializable]
    public class ModelMetrics
    {
        [XmlElement]
        public AverageClassificationMetrics ClassificationMetrics {get; set;}

        [XmlElement]
        public AverageRegressionMetrics RegressionMetrics { get; set; }
    }
}

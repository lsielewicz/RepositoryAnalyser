﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class DeveloperEfficiencyInputData : InputData
    {
        public IList<IssueTrackerUser> UsersData { get; set; }
        public IList<Issue> IssuesData { get; set; }
        public RegressionTool RegressionTool { get; set; }

        public EfficiencyWeightsKeeper EfficiencyWeightsKeeper { get; set; }
    }
}

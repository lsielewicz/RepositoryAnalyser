﻿namespace RepositoryAnalyser.MachineLearning.Models
{
    public class ConfusionMatirx
    {
        public double TruePositive { get; set; }
        public double TrueNegative { get; set; }
        public double FalsePositve { get; set; }
        public double FalseNegative { get; set; }
    }
}

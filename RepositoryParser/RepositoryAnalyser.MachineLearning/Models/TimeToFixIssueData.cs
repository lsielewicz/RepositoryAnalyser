﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class TimeToFixIssueData : IssueData
    {
        public TimeToFixIssueData()
        {

        }

        public TimeToFixIssueData(IssueData issueData) : base(issueData)
        {

        }

        [Column(ordinal: "0", name: Columns.TimeToFixInMinutes)]
        public float TimeToFixInMinutes;
    }
}

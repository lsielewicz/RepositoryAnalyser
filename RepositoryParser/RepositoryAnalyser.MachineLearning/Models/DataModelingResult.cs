﻿using System.Collections.Generic;
using Microsoft.ML.Legacy;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class DataModelingResult
    {
        public string DatasetPath { get; set; }
        public string ValidationDatasetPath { get; set; }
        public IList<ILearningPipelineItem> LearningItems { get; set; }
        public int CountOfRawDataRows { get; set; }
        public int CountOfFormattedDataRows { get; set; }
        
    }
}

﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class ReopenedIssueIntegratedData : IntegratedIssueData
    {
        public ReopenedIssueIntegratedData()
        {
            
        }

        public ReopenedIssueIntegratedData(IssueData issueData) : base(issueData)
        {
            
        }

        [Column(ordinal: "0", name: Columns.Label)]
        public bool WasReopened;
    }
}

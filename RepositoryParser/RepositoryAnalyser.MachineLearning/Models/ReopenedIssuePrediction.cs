﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class ReopenedIssuePrediction
    {
        [ColumnName(Columns.PredictedLabel)]
        public bool IsGoingToBeReopened;
    }
}

﻿using System;
using System.Xml.Serialization;

namespace RepositoryAnalyser.MachineLearning.Models
{
    [Serializable]
    public class TrainingOutputData
    {
        [XmlAttribute]
        public int CountOfRawDataRows { get; set; }

        [XmlAttribute]
        public int CountOfFormattedDataRows { get; set; }

        [XmlElement]
        public string FilePath { get; set; }

        [XmlElement]
        public ModelMetrics Metrics { get; set; }

        [XmlElement]
        public DateTime DateTime { get; set; }
    }
}

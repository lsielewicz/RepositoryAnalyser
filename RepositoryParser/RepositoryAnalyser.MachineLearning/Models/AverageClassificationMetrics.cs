﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using RepositoryAnalyser.MachineLearning.Enums;

namespace RepositoryAnalyser.MachineLearning.Models
{
    [Serializable]
    public class AverageClassificationMetrics : ClassificationMetrics
    {
        public AverageClassificationMetrics()
        {
            SubMetrics = new List<ClassificationMetrics>();    
        }

        [XmlIgnore]
        public DataBalancingTechnique DataBalancingTechnique { get; set; }

        [XmlIgnore]
        public IList<ClassificationMetrics> SubMetrics { get; set; }
    }
}

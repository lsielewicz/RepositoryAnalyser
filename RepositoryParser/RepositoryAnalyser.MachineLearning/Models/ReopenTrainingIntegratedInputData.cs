﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class ReopenTrainingIntegratedInputData : ReopenTrainingInputData
    {
        public IList<Commit> Commits { get; set; }

        public IList<CommitsSet> CommitsSets { get; set; }
    }
}

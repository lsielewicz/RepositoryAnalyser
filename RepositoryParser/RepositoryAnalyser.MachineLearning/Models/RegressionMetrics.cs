﻿using System;
using System.Xml.Serialization;

namespace RepositoryAnalyser.MachineLearning.Models
{
    [Serializable]
    public class RegressionMetrics
    {
        [XmlAttribute]
        public double L1 { get; set; }

        [XmlAttribute]
        public double L2 { get; set; }

        [XmlAttribute]
        public double LossFn { get; set; }

        [XmlAttribute]
        public double Rms { get; set; }

        [XmlAttribute]
        public double RSquared { get; set; }
    }
}

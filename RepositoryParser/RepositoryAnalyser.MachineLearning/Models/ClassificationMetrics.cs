﻿using System;
using System.Xml.Serialization;

namespace RepositoryAnalyser.MachineLearning.Models
{
    [Serializable]
    public class ClassificationMetrics
    {
        [XmlAttribute]
        public double Accuracy { get; set; }

        [XmlAttribute]
        public double F1Score { get; set; }

        [XmlAttribute]
        public double Auc { get; set; }

        [XmlAttribute]
        public double MatthewsCorrelationCoefficient { get; set; }

        [XmlAttribute]
        public double PositivePrecision { get; set; }

        [XmlAttribute]
        public double PositiveRecall { get; set; }

        [XmlAttribute]
        public double NegativePrecision { get; set; }

        [XmlAttribute]
        public double NegativeRecall { get; set; }

        [XmlElement]
        public ConfusionMatirx ConfusionMatirx { get; set; }
    }
}

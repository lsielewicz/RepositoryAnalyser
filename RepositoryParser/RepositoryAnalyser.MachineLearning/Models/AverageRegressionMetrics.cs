﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace RepositoryAnalyser.MachineLearning.Models
{
    [Serializable]
    public class AverageRegressionMetrics : RegressionMetrics
    {
        public AverageRegressionMetrics()
        {
            this.SubMetrics = new List<RegressionMetrics>();
        }

        [XmlIgnore]
        public IList<RegressionMetrics> SubMetrics { get; set; }
    }
}

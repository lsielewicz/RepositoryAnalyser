﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class DeveloperEfficiencyPrediction
    {
        [ColumnName(Columns.Score)]
        public float Efficiency;
    }
}

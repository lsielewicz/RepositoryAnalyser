﻿using Microsoft.ML.Runtime.Api;
using RepositoryAnalyser.MachineLearning.Constants;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class ReopenedIssueData : IssueData
    {
        public ReopenedIssueData()
        {
            
        }

        public ReopenedIssueData(IssueData issueData) : base(issueData)
        {
            
        }

        [Column(ordinal: "0", name:Columns.Label)]
        public bool WasReopened;
    }
}

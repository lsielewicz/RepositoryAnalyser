﻿namespace RepositoryAnalyser.MachineLearning.Models
{
    internal class LinesCountingResult
    {
        public int ChangesCount { get; set; }
        public int AddedLines { get; set; }
        public int DeletedLines { get; set; }
    }
}

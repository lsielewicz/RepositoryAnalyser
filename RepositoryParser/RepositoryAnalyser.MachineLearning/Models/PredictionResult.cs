﻿using System;
using System.Text;
using RepositoryAnalyser.Locale.Languages;

namespace RepositoryAnalyser.MachineLearning.Models
{
    public class PredictionResult<TData, TPrediction>
    {
        public TData Data { get; set; }
        public TPrediction Prediction { get; set; }
        public TimeSpan PredictionTime { get; set; }
        public DateTime PredictionDate { get; set; }
        public double Accuracy { get; set; }
        public TrainingOutputData TrainingModel { get; set; }
        public string ModelName { get; set; }

        public string OriginalKey { get; set; }

        public override string ToString()
        {
            var resManager = Resources.ResourceManager;
            var summaryBuilder = new StringBuilder();
            summaryBuilder.AppendLine(resManager.GetString("PredictionSummaryHeader"));
            summaryBuilder.AppendLine($"{resManager.GetString("Accuracy")}: {this.Accuracy * 100}%");
            summaryBuilder.AppendLine();
            summaryBuilder.AppendLine(resManager.GetString("DateAndTimeInfo"));
            summaryBuilder.AppendLine();
            summaryBuilder.AppendLine($"-{resManager.GetString("PredictionDate")}: {this.PredictionDate:f}");
            summaryBuilder.AppendLine($"-{resManager.GetString("PredictionTime")}: {this.PredictionTime.Hours}h::{this.PredictionTime.Minutes}m::{this.PredictionTime.Seconds}s::{this.PredictionTime.Milliseconds}ms");

            return summaryBuilder.ToString();
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.MachineLearning.Interfaces
{
    public interface IDeveloperEfficiencyPredictor
    {
        TrainingOutputData TrainAndSavePredictionModel(DeveloperEfficiencyInputData data);
        TrainingOutputData TrainAndSavePredictionModel(DeveloperEfficiencyIntegratedDataInput data);
        Task<DeveloperEfficiencyPredictionResult> Predict(IssueTrackerUser user, IList<Issue> issues, TrainingOutputData trainingModelData, EfficiencyWeightsKeeper weightsKeeper);
        Task<DeveloperEfficiencyIntegratedPredictionResult> Predict(IssueTrackerUser user, IList<Issue> issues, IList<Commit> commits, TrainingOutputData trainingModelData, EfficiencyWeightsKeeper weightsKeeper);
    }
}
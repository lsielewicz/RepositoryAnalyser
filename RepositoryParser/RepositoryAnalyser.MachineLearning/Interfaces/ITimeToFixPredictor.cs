﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.MachineLearning.Interfaces
{
    public interface ITimeToFixPredictor
    {
        TrainingOutputData TrainAndSavePredictionModel(TimeToFixInputData data);
        TrainingOutputData TrainAndSavePredictionModel(TimeToFixIntegratedInputData data);
        Task<TimeToFixPredictionResult> Predict(Issue issue, TrainingOutputData trainingModelData);
        Task<TimeToFixIntegratedPredictionResult> Predict(Issue issue, IList<Commit> commits, TrainingOutputData trainingModelData, IList<CommitsSet> commitsSet = null);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.MachineLearning.Interfaces
{
    public interface IIssueReopenPredictor
    {
        TrainingOutputData TrainAndSavePredictionModel(ReopenTrainingInputData data);
        TrainingOutputData TrainAndSavePredictionModel(ReopenTrainingIntegratedInputData data);
        Task<IssueReopenPredictionResult> Predict(Issue issue, TrainingOutputData trainingModelData);
        Task<IssueReopenIntegratedPredictionResult> Predict(Issue issue, IList<Commit> commit, TrainingOutputData trainingModelData, IList<CommitsSet> commitsSet = null);
    }
}
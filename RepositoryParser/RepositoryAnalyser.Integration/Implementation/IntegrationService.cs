﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using RepositoryAnalyser.Domain.Entities.Integration;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Integration.Interfaces;

namespace RepositoryAnalyser.Integration.Implementation
{
    public class IntegrationService : IIntegrationService
    {
        public IntegrationInstance FindIntegration(IList<Repository> repositories, Issue issue, IList<CommitsSet> commitsSets)
        {
            var commits = new List<Commit>();
            foreach (var repository in repositories)
            {
                foreach (var branch in repository.Branches)
                {
                    commits.AddRange(branch.Commits);
                }
            }

            commits = commits.Distinct().ToList();
            

            var issuesCommentsString = issue.Comments != null ? string.Join(Environment.NewLine, issue?.Comments.Select(c => c.Body)) + Environment.NewLine : "-";
            var instanceToAdd = new IntegrationInstance()
            {
                IntegrationEntity = null,
                Issue = issue,
                Commits = new List<Commit>(),
                InteractionSubject = InteractionSubject.RevisionsInComments
            };

            var revisionsToInclude = new List<string>();
            if (commitsSets != null && commitsSets.Any())
            {
                foreach (var commitsSet in commitsSets)
                {
                    var commitsSetRegexPattern = $@"#{commitsSet.Number}\s";
                    if (Regex.IsMatch(issuesCommentsString, commitsSetRegexPattern))
                    {
                        revisionsToInclude.AddRange(commitsSet.Commits.Select(c => c.Revision));
                    }
                }
            }

            foreach (var commit in commits)
            {
                if (issuesCommentsString.IndexOf(commit.Revision, StringComparison.OrdinalIgnoreCase) > -1 ||
                    revisionsToInclude.Any(rev => rev.Equals(commit.Revision, StringComparison.OrdinalIgnoreCase)))
                {
                    instanceToAdd.Commits.Add(commit);
                }
            }

            return instanceToAdd;
        }

        public IntegrationEntity FindAssociations(IList<Repository> repositories, IssueTrackingProject project)
        {
            var integrationEntity = new IntegrationEntity()
            {
                IssueTrackingProject = project,
                Repositories = repositories,
            };

            integrationEntity.EmailAssociations = this.FindEmailsAssotations(integrationEntity, repositories, project);
            integrationEntity.Interactions = this.FindIntegrations(integrationEntity, repositories, project);

            return integrationEntity;
        }

        public IntegrationInstance GetCommitsOfUser(IssueTrackerUser user, Repository repository, IList<CommitsSet> commitsSets = null)
        {
            var output = new IntegrationInstance()
            {
                Issue = null,
                IntegrationEntity = null,
                Commits = new List<Commit>(),
               
            };

            var commits = new List<Commit>();
            foreach (var branch in repository.Branches)
            {
                commits.AddRange(branch.Commits);
            }

            commits = commits.Distinct().ToList();

            if (commitsSets != null)
            {
                foreach (var pr in commitsSets)
                {
                    foreach (var prCommit in pr.Commits)
                    {
                        var commitToAdd = commits.FirstOrDefault(c => c.Revision == prCommit.Revision);
                        if (commitToAdd != null)
                        {
                            output.Commits.Add(commitToAdd);
                        }
                    }
                }
            }

            var authorsCommits = commits.Where(c => c.Author.Equals(user.UserName,
                    StringComparison.OrdinalIgnoreCase) ||
                    c.Email.Equals(user.Email, StringComparison.OrdinalIgnoreCase))
                .ToList();

            ((List<Commit>) output.Commits).AddRange(authorsCommits);
            output.Commits = output.Commits.Distinct().ToList();

            return output;
        }

        public IntegrationEntity FindIntegration(IList<Repository> repositories, IList<Issue> issues, IList<CommitsSet> commitsSets)
        {
            var output = new IntegrationEntity()
            {
                Repositories = repositories,
                IssueTrackingProject = new IssueTrackingProject()
                {
                    Issues = issues,
                    CommitsSets = commitsSets,
                }
            };
            var commits = new List<Commit>();
            foreach (var repository in repositories)
            {
                foreach (var branch in repository.Branches)
                {
                    commits.AddRange(branch.Commits);
                }
            }

            commits = commits.Distinct().ToList();

            foreach (var issue in issues.Where(i => i.Comments != null && i.Comments.Any()))
            {
                var instanceToAdd = new IntegrationInstance()
                {
                    IntegrationEntity = null,
                    Issue = issue,
                    Commits = new List<Commit>(),
                    InteractionSubject = InteractionSubject.RevisionsInComments
                };

                var issuesCommentsString = string.Join(Environment.NewLine, issue.Comments.Select(c => c.Body)) + Environment.NewLine;

                var revisionsToInclude = new List<string>();
                if (commitsSets != null && commitsSets.Any())
                {
                    foreach (var commitsSet in commitsSets)
                    {
                        var commitsSetRegexPattern = $@"#{commitsSet.Number}\s";
                        if (Regex.IsMatch(issuesCommentsString, commitsSetRegexPattern))
                        {
                            revisionsToInclude.AddRange(commitsSet.Commits.Select(c => c.Revision));
                        }
                    }
                }

                foreach (var commit in commits)
                {
                    if (issuesCommentsString.IndexOf(commit.Revision, StringComparison.OrdinalIgnoreCase) > -1 ||
                        revisionsToInclude.Any(rev => rev.Equals(commit.Revision, StringComparison.OrdinalIgnoreCase)))
                    {
                        instanceToAdd.Commits.Add(commit);
                    }
                }

                if (instanceToAdd.Commits.Any())
                {
                    output.Interactions.Add(instanceToAdd);
                }
            }

            return output;
        }

        private IList<IntegrationInstance> FindIntegrations(IntegrationEntity entity, IList<Repository> repositories, IssueTrackingProject project)
        {
            var output = new List<IntegrationInstance>();
            var commits = new List<Commit>();
            foreach (var repository in repositories)
            {
                foreach (var branch in repository.Branches)
                {
                    commits.AddRange(branch.Commits);
                }
            }

            commits = commits.Distinct().ToList();
            
            foreach (var issue in project.Issues.Where(i=>i.Comments != null && i.Comments.Any()))
            {
                var instanceToAdd = new IntegrationInstance()
                {
                    IntegrationEntity = entity,
                    Issue = issue,
                    Commits = new List<Commit>(),
                    InteractionSubject = InteractionSubject.RevisionsInComments
                };

                var issuesCommentsString = string.Join(Environment.NewLine, issue.Comments.Select(c => c.Body)) + Environment.NewLine;

                var revisionsToInclude = new List<string>();
                if (project.CommitsSets != null && project.CommitsSets.Any())
                {
                    foreach (var commitsSet in project.CommitsSets)
                    {
                        var commitsSetRegexPattern = $@"#{commitsSet.Number}\s";
                        if (Regex.IsMatch(issuesCommentsString, commitsSetRegexPattern))
                        {
                            revisionsToInclude.AddRange(commitsSet.Commits.Select(c => c.Revision));
                        }
                    }
                }

                foreach (var commit in commits)
                {
                    if (issuesCommentsString.IndexOf(commit.Revision, StringComparison.OrdinalIgnoreCase) > -1 ||
                        revisionsToInclude.Any(rev => rev.Equals(commit.Revision, StringComparison.OrdinalIgnoreCase)))
                    {
                        instanceToAdd.Commits.Add(commit);
                    }
                }

                if (instanceToAdd.Commits.Any())
                {
                    output.Add(instanceToAdd);
                }
            }

            return output;
        }

        private IList<EmailAssociation> FindEmailsAssotations(IntegrationEntity integrationEntity, IList<Repository> repositories, IssueTrackingProject project)
        {
            var output = new List<EmailAssociation>();

            var commits = new List<Commit>();
            foreach (var repository in repositories)
            {
                foreach (var branch in repository.Branches)
                {
                    commits.AddRange(branch.Commits);
                }
            }

            commits = commits.Distinct().ToList();

            foreach (var commit in commits)
            {
                foreach (var issue in project.Issues)
                {
                    if ((issue.Asignee != null &&
                         commit.Email.Equals(issue.Asignee.Email, StringComparison.OrdinalIgnoreCase)
                         || issue.Reporter != null &&
                         commit.Email.Equals(issue.Reporter.Email, StringComparison.OrdinalIgnoreCase)) &&
                        output.All(a => !a.Email.Equals(commit.Email)))
                    {
                        output.Add(new EmailAssociation()
                        {
                            IntegrationEntity = integrationEntity,
                            Email = commit.Email
                        });
                    }

                }
            }

            return output;
        }
    }
}

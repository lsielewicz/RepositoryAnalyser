﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.Integration;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.Integration.Interfaces
{
    public interface IIntegrationService
    {
        IntegrationInstance GetCommitsOfUser(IssueTrackerUser user, Repository repository, IList<CommitsSet> commitsSets = null);
        IntegrationEntity FindIntegration(IList<Repository> repositories, IList<Issue> issues, IList<CommitsSet> commitsSets);
        IntegrationInstance FindIntegration(IList<Repository> repositories, Issue issue, IList<CommitsSet> commitsSets);
        IntegrationEntity FindAssociations(IList<Repository> repositoriesy, IssueTrackingProject project);
    }
}
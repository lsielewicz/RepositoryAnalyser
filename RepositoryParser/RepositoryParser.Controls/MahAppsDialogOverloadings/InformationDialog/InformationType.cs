﻿namespace RepositoryAnalyser.Controls.MahAppsDialogOverloadings.InformationDialog
{
    public enum InformationType
    {
        Information,
        Warning,
        Error
    }
}
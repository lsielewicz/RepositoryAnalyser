﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.MonthActivityViewModels;

namespace RepositoryAnalyser.View.MonthActivityViews
{
    /// <summary>
    /// Interaction logic for MonthActivityFilesAnalyseView.xaml
    /// </summary>
    public partial class MonthActivityFilesAnalyseView : UserControl
    {
        public MonthActivityFilesAnalyseView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<MonthActivityFilesAnalyseViewModel>(this,this.ChartViewInstance,this.ChartViewInstance2);
        }
    }
}

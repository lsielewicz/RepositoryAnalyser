﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.MonthActivityViews
{
    /// <summary>
    /// Interaction logic for MonthActivityContentProviderView.xaml
    /// </summary>
    public partial class MonthActivityContentProviderView : UserControl
    {
        public MonthActivityContentProviderView()
        {
            InitializeComponent();
        }
    }
}

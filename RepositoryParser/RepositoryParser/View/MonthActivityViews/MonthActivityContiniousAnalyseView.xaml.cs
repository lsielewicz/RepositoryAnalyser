﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.MonthActivityViewModels;

namespace RepositoryAnalyser.View.MonthActivityViews
{
    /// <summary>
    /// Interaction logic for MonthActivityContiniousAnalyseView.xaml
    /// </summary>
    public partial class MonthActivityContiniousAnalyseView : UserControl
    {
        public MonthActivityContiniousAnalyseView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<MonthActivityContiniousAnalyseViewModel>(this,this.ChartViewInstance,this.ChartViewInstance2);
        }
    }
}

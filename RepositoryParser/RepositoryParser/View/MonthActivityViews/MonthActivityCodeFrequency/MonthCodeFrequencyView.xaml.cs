﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.MonthActivityViews.MonthActivityCodeFrequency
{
    /// <summary>
    /// Interaction logic for MonthCodeFrequencyView.xaml
    /// </summary>
    public partial class MonthCodeFrequencyView : UserControl
    {
        public MonthCodeFrequencyView()
        {
            InitializeComponent();
        }
    }
}

﻿<UserControl x:Class="RepositoryAnalyser.View.MonthActivityViews.MonthActivityCodeFrequency.MonthCodeFrequencyView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
             xmlns:local="clr-namespace:RepositoryAnalyser.View.MonthActivityViews.MonthActivityCodeFrequency"
             xmlns:resw="clr-namespace:RepositoryAnalyser.Properties"
             xmlns:conventers="clr-namespace:RepositoryAnalyser.Conventers"
             xmlns:enums="clr-namespace:RepositoryAnalyser.Helpers.Enums"
             xmlns:hourActivityCodeFrequency="clr-namespace:RepositoryAnalyser.View.HourActivityViews.HourActivityCodeFrequency"
             xmlns:extensions="clr-namespace:RepositoryAnalyser.Controls.Extensions;assembly=RepositoryAnalyser.Controls"
             xmlns:metroLoadingProgressRing="clr-namespace:RepositoryAnalyser.Controls.MetroLoadingProgressRing;assembly=RepositoryAnalyser.Controls"
             xmlns:circleHeaderButton="clr-namespace:RepositoryAnalyser.Controls.CircleHeaderButton;assembly=RepositoryAnalyser.Controls"
             xmlns:languages="clr-namespace:RepositoryAnalyser.Locale.Languages;assembly=RepositoryAnalyser.Locale"
             mc:Ignorable="d" 
             d:DesignHeight="300" d:DesignWidth="300"
             x:Name="Root"
             DataContext="{Binding Source={StaticResource Locator}, Path=MonthCodeFrequencyViewModel}">
    <UserControl.Resources>
        <conventers:BoolToVisibilityConverter x:Key="BoolToVisibilityConverter"/>
        <conventers:IntegerToBooleanConventer x:Key="IntegerToBooleanConventer"/>
        <conventers:CountOfRepositoriesToVisibilityConventer x:Key="CountOfRepositoriesToVisibilityConventer"/>
    </UserControl.Resources>

    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*"/>
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="*"/>
            <RowDefinition Height="Auto"/>
        </Grid.RowDefinitions>

        <TextBlock
            Grid.Row="0"
            Grid.RowSpan="2"
            Visibility="{Binding Path=CountOfSelectedRepositories, 
                                 ConverterParameter={x:Static enums:VisibilityConventerEnumDirection.Inverse},
                                 Converter={StaticResource CountOfRepositoriesToVisibilityConventer}}"
            Text="{x:Static languages:Resources.NoDataToDisplay}"
            Foreground="#666666"
            FontSize="28"
            HorizontalAlignment="Center"
            VerticalAlignment="Center"
            FontWeight="Light"
            TextWrapping="WrapWithOverflow"/>

        <TabControl
            Grid.Row="0"
            Visibility="{Binding Path=CountOfSelectedRepositories, 
                        ConverterParameter={x:Static enums:VisibilityConventerEnumDirection.Normal},
                        Converter={StaticResource CountOfRepositoriesToVisibilityConventer}}">
            <!--Added Chart-->
            <TabItem 
                Header="{x:Static languages:Resources.AddedChart}"
                DataContext="{Binding ElementName=Root, Path=DataContext.AddedChartViewModel}">
                <local:MonthAddedChartView/>
            </TabItem>
            <!--Deleted Chart-->
            <TabItem
                Header="{x:Static languages:Resources.DeletedChart}"
                DataContext="{Binding ElementName=Root, Path=DataContext.DeletedChartViewModel}">
                <local:MonthDeletedChartView/>
            </TabItem>
            <!--Summary-->
            <TabItem Header="{x:Static languages:Resources.Summary}">
                <Grid>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="*" />
                    </Grid.RowDefinitions>

                    <Border Grid.Row="0" 
                            Background="{StaticResource AppMainColor}"
                            HorizontalAlignment="Stretch"
                            VerticalAlignment="Stretch"
                            >
                        <StackPanel HorizontalAlignment="Center" VerticalAlignment="Center" Margin="0,5">
                            <TextBlock Foreground="{StaticResource Grey}"
                                       Text="{Binding SummaryString}"
                                       FontWeight="Light"
                                       VerticalAlignment="Center"
                                       HorizontalAlignment="Center"
                                       FontSize="14" 
                                       />
                        </StackPanel>
                    </Border>
                    <DataGrid Grid.Row="1" Grid.Column="0"
                        ItemsSource="{Binding CodeFrequencyDataRows}"
                        CanUserSortColumns="True"
                        AutoGenerateColumns="False"
                        ColumnWidth="auto"
                        CanUserAddRows="False"
                        IsReadOnly="True"
                        VerticalScrollBarVisibility="Auto"
                        Style="{StaticResource AzureDataGrid}"
                        extensions:DataGridExtensions.LastColumnFill="True"
                        >
                        <DataGrid.Columns>
                            <DataGridTextColumn Binding="{Binding ChartKey}" Header="{x:Static languages:Resources.Month}" SortDirection="Ascending"/>
                            <DataGridTextColumn Binding="{Binding Repository}" Header="{x:Static languages:Resources.Repository}" Width="200">
                                <DataGridTextColumn.CellStyle>
                                    <Style>
                                        <Setter Property="FrameworkElement.HorizontalAlignment" Value="Center"/>
                                    </Style>
                                </DataGridTextColumn.CellStyle>
                            </DataGridTextColumn>
                            <DataGridTextColumn Binding="{Binding AddedLines}" Header="{x:Static languages:Resources.Added}" Width="Auto"/>
                            <DataGridTextColumn Binding="{Binding DeletedLines}" Header="{x:Static languages:Resources.Deleted}"/>
                        </DataGrid.Columns>

                    </DataGrid>
                </Grid>
            </TabItem>
        </TabControl>
        <!--Progress-->
        <metroLoadingProgressRing:MetroLoadingProgressRing
            Grid.Row="0"
            Grid.Column="0"
            Grid.RowSpan="2"
            IsDataLoadingFlag="{Binding IsLoading,UpdateSourceTrigger=PropertyChanged}"
            Text="{x:Static languages:Resources.AnalysingDataMessage}"/>
        <DockPanel
            Grid.Row="1" 
            HorizontalAlignment="Left"
            Margin="0"
            IsEnabled="{Binding CountOfSelectedRepositories, Converter={StaticResource IntegerToBooleanConventer}}">
            <circleHeaderButton:CircleHeaderButton 
                CommandParameter="{Binding ElementName=Root, Path=DataContext}"
                Command="{Binding ExportFileCommand}"
                ImageSource="../../../Assets/Icons/PNG_DarkGray/Export-100.png"
                ToolTip="{x:Static languages:Resources.ExportToFileButton}"
                Height="56"
                Width="56"
                ImageHeight="28" 
                ImageWidth="28"
                BorderBrush="{StaticResource AppMainColor}"
                />
            <circleHeaderButton:CircleHeaderButton 
                Command="{Binding SwitchChartTypeCommand}"
                ImageSource="../../../Assets/Icons/PNG_DarkGray/Switch On-100.png"
                ToolTip="{x:Static languages:Resources.SwitchChartType}"
                Height="56"
                Width="56"
                ImageHeight="28" 
                ImageWidth="28"
                BorderBrush="{StaticResource AppMainColor}"
                />
        </DockPanel>
    </Grid>
</UserControl>

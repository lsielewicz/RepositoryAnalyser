﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.MonthActivityViewModels;

namespace RepositoryAnalyser.View.MonthActivityViews
{
    /// <summary>
    /// Interaction logic for MonthActivityChartView.xaml
    /// </summary>
    public partial class MonthActivityChartView : UserControl
    {
        public MonthActivityChartView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<MonthActivityChartViewModel>(this, this.ChartViewInstance, this.ChartViewInstance2);
        }
    }
}

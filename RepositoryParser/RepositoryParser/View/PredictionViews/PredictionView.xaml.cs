﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.PredictionViews
{
    /// <summary>
    /// Interaction logic for PredictionView.xaml
    /// </summary>
    public partial class PredictionView : UserControl
    {
        public PredictionView()
        {
            InitializeComponent();
        }
    }
}

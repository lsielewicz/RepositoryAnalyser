﻿using System.Windows.Controls;
using RepositoryAnalyser.ViewModel;

namespace RepositoryAnalyser.View
{
    /// <summary>
    /// Interaction logic for PresentationView.xaml
    /// </summary>
    public partial class PresentationView : UserControl
    {

        public PresentationView()
        {
            InitializeComponent();
            PresentationViewModel viewModel = DataContext as PresentationViewModel;
            if (viewModel != null)
            {
                viewModel.ViewInstance = this;
                if (!viewModel.IsDocked)
                {
                    this.RootGrid.Children.Remove(this.PresentationGrid);
                }
            }
        }
    }
}

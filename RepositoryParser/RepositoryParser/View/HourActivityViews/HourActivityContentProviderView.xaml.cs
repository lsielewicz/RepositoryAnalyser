﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.HourActivityViews
{
    /// <summary>
    /// Interaction logic for HourActivityContentProviderView.xaml
    /// </summary>
    public partial class HourActivityContentProviderView : UserControl
    {
        public HourActivityContentProviderView()
        {
            InitializeComponent();
        }
    }
}

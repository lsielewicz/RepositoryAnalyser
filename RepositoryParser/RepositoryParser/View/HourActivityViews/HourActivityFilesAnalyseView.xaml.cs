﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.HourActivityViewModels;

namespace RepositoryAnalyser.View.HourActivityViews
{
    /// <summary>
    /// Interaction logic for HourActivityFilesAnalyseView.xaml
    /// </summary>
    public partial class HourActivityFilesAnalyseView : UserControl
    {
        public HourActivityFilesAnalyseView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<HourActivityFilesAnalyseViewModel>(this, this.ChartViewInstance,this.ChartViewInstance2);
        }
    }
}

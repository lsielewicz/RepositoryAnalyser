﻿using System.Windows.Controls;
using RepositoryAnalyser.CommonUI.CodeFrequency;
using RepositoryAnalyser.Helpers;

namespace RepositoryAnalyser.View.HourActivityViews.HourActivityCodeFrequency
{
    /// <summary>
    /// Interaction logic for HourAddedChartView.xaml
    /// </summary>
    public partial class HourAddedChartView : UserControl
    {
        public HourAddedChartView()
        {
            InitializeComponent();
            this.Loaded += (s, e) =>
            {
                ChartingHelper.Instance.DrawCharts<CodeFrequencySubChartViewModel>(this, this.ChartViewInstance,this.ChartViewInstance2);
            };
        }
    }
}

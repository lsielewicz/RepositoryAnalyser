﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.HourActivityViews.HourActivityCodeFrequency
{
    /// <summary>
    /// Interaction logic for HourCodeFrequencyView.xaml
    /// </summary>
    public partial class HourCodeFrequencyView : UserControl
    {
        public HourCodeFrequencyView()
        {
            InitializeComponent();
        }
    }
}

﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.HourActivityViewModels;

namespace RepositoryAnalyser.View.HourActivityViews
{
    /// <summary>
    /// Interaction logic for HourActivityChartView.xaml
    /// </summary>
    public partial class HourActivityChartView : UserControl
    {
        public HourActivityChartView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<HourActivityViewModel>(this,this.ChartViewInstance,this.ChartViewInstance2);
        }
    }
}

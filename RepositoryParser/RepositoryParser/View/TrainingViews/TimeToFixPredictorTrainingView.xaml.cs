﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.TrainingViews
{
    /// <summary>
    /// Interaction logic for TimeToFixPredictorTrainingView.xaml
    /// </summary>
    public partial class TimeToFixPredictorTrainingView : UserControl
    {
        public TimeToFixPredictorTrainingView()
        {
            InitializeComponent();
        }
    }
}

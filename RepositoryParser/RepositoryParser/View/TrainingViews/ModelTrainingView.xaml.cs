﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.TrainingViews
{
    /// <summary>
    /// Interaction logic for ModelTrainingView.xaml
    /// </summary>
    public partial class ModelTrainingView : UserControl
    {
        public ModelTrainingView()
        {
            InitializeComponent();
        }
    }
}

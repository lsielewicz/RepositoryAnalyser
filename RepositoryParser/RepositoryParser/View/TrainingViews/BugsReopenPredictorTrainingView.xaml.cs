﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.TrainingViews
{
    /// <summary>
    /// Interaction logic for BugsReopenPredictorTrainingView.xaml
    /// </summary>
    public partial class BugsReopenPredictorTrainingView : UserControl
    {
        public BugsReopenPredictorTrainingView()
        {
            InitializeComponent();
        }
    }
}

﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.TrainingViews
{
    /// <summary>
    /// Interaction logic for DevEfficiencyPredictorTrainingView.xaml
    /// </summary>
    public partial class DevEfficiencyPredictorTrainingView : UserControl
    {
        public DevEfficiencyPredictorTrainingView()
        {
            InitializeComponent();
        }
    }
}

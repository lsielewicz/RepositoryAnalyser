﻿using System.Windows;
using System.Windows.Controls;
using RepositoryAnalyser.CommonUI.CodeFrequency;
using RepositoryAnalyser.Helpers;

namespace RepositoryAnalyser.View.UsersActivityViews.UsersActivityCodeFrequency
{
    /// <summary>
    /// Interaction logic for UsersCodeFrequencyAddedChartView.xaml
    /// </summary>
    public partial class UsersCodeFrequencyAddedChartView : UserControl
    {
        public UsersCodeFrequencyAddedChartView()
        {
            InitializeComponent();
        }

        private void UsersCodeFrequencyAddedChartView_OnLoaded(object sender, RoutedEventArgs e)
        {
            ChartingHelper.Instance.DrawCharts<CodeFrequencySubChartViewModel>(this, this.ChartViewInstance,this.ChartViewInstance2);
        }
    }
}

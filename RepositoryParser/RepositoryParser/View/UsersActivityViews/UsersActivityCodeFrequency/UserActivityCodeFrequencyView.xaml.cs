﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.UsersActivityViews.UsersActivityCodeFrequency
{
    /// <summary>
    /// Interaction logic for UserActivityCodeFrequencyView.xaml
    /// </summary>
    public partial class UserActivityCodeFrequencyView : UserControl
    {
        public UserActivityCodeFrequencyView()
        {
            InitializeComponent();
        }
    }
}

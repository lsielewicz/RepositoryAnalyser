﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.UserActivityViewModels;

namespace RepositoryAnalyser.View.UsersActivityViews
{
    /// <summary>
    /// Interaction logic for UserActivityChartView.xaml
    /// </summary>
    public partial class UserActivityChartView : UserControl
    {
        public UserActivityChartView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<UsersActivityViewModel>(this, this.ChartViewInstance,this.ChartViewInstance2);
        }
    }
}

﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.UserActivityViewModels;

namespace RepositoryAnalyser.View.UsersActivityViews
{
    /// <summary>
    /// Interaction logic for UserActivityFilesAnalyseView.xaml
    /// </summary>
    public partial class UserActivityFilesAnalyseView : UserControl
    {
        public UserActivityFilesAnalyseView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<UsersActivityFilesAnalyseViewModel>(this, this.ChartViewInstance, this.ChartViewInstance2);
        }
    }
}

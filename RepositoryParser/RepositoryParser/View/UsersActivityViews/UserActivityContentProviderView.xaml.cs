﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.UsersActivityViews
{
    /// <summary>
    /// Interaction logic for UserActivityContentProviderView.xaml
    /// </summary>
    public partial class UserActivityContentProviderView : UserControl
    {
        public UserActivityContentProviderView()
        {
            InitializeComponent();
        }
    }
}

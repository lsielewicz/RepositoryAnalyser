﻿using MahApps.Metro.Controls;

namespace RepositoryAnalyser.View
{
    /// <summary>
    /// Interaction logic for AnalysisWindowView.xaml
    /// </summary>
    public partial class AnalysisWindowView : MetroWindow
    {
        public AnalysisWindowView()
        {
            InitializeComponent();
        }
    }
}

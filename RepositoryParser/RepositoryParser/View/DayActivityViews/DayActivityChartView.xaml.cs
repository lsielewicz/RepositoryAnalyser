﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.DayActivityViewModels;

namespace RepositoryAnalyser.View.DayActivityViews
{
    /// <summary>
    /// Interaction logic for DayActivityChartView.xaml
    /// </summary>
    public partial class DayActivityChartView : UserControl
    {
        public DayActivityChartView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<DayActivityViewModel>(this, this.ChartViewInstance, this.ChartViewInstance2);
        }
    }
}

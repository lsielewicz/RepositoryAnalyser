﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.DayActivityViews
{
    /// <summary>
    /// Interaction logic for DayActivityContentProviderView.xaml
    /// </summary>
    public partial class DayActivityContentProviderView : UserControl
    {
        public DayActivityContentProviderView()
        {
            InitializeComponent();
        }
    }
}

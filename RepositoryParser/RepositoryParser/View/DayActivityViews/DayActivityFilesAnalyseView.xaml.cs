﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.DayActivityViewModels;

namespace RepositoryAnalyser.View.DayActivityViews
{
    /// <summary>
    /// Interaction logic for DayActivityFilesAnalyseView.xaml
    /// </summary>
    public partial class DayActivityFilesAnalyseView : UserControl
    {
        public DayActivityFilesAnalyseView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<DayActivityFilesAnalyseViewModel>(this, this.ChartViewInstance, this.ChartViewInstance2);
        }
    }
}

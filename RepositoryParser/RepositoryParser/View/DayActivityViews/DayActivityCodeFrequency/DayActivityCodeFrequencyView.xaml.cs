﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.DayActivityViews.DayActivityCodeFrequency
{
    /// <summary>
    /// Interaction logic for DayActivityCodeFrequencyView.xaml
    /// </summary>
    public partial class DayActivityCodeFrequencyView : UserControl
    {
        public DayActivityCodeFrequencyView()
        {
            InitializeComponent();
        }
    }
}

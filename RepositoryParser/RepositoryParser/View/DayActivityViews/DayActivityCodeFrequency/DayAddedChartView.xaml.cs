﻿using System.Windows.Controls;
using RepositoryAnalyser.CommonUI.CodeFrequency;
using RepositoryAnalyser.Helpers;

namespace RepositoryAnalyser.View.DayActivityViews.DayActivityCodeFrequency
{
    /// <summary>
    /// Interaction logic for DayAddedChartView.xaml
    /// </summary>
    public partial class DayAddedChartView : UserControl
    {
        public DayAddedChartView()
        {
            InitializeComponent();
            this.Loaded += (s, e) =>
            {
                ChartingHelper.Instance.DrawCharts<CodeFrequencySubChartViewModel>(this,this.ChartViewInstance, this.ChartViewInstance2);
            };
        }
    }
}

﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View
{
    /// <summary>
    /// Interaction logic for AnalysisView.xaml
    /// </summary>
    public partial class AnalysisView : UserControl
    {
        public AnalysisView()
        {
            InitializeComponent();
        }
    }
}

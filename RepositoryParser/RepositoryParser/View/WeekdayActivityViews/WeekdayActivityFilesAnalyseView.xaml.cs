﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.WeekdayActivityViewModels;

namespace RepositoryAnalyser.View.WeekdayActivityViews
{
    /// <summary>
    /// Interaction logic for WeekdayActivityFilesAnalyseView.xaml
    /// </summary>
    public partial class WeekdayActivityFilesAnalyseView : UserControl
    {
        public WeekdayActivityFilesAnalyseView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<WeekdayActivityFilesAnalyseViewModel>(this, this.ChartViewInstance,this.ChartViewInstance2);
        }
    }
}

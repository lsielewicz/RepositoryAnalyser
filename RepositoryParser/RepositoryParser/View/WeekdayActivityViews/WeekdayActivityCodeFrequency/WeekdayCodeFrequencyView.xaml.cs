﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.WeekdayActivityViews.WeekdayActivityCodeFrequency
{
    /// <summary>
    /// Interaction logic for WeekdayCodeFrequencyView.xaml
    /// </summary>
    public partial class WeekdayCodeFrequencyView : UserControl
    {
        public WeekdayCodeFrequencyView()
        {
            InitializeComponent();
        }
    }
}

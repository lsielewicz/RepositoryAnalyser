﻿using System.Windows.Controls;
using RepositoryAnalyser.CommonUI.CodeFrequency;
using RepositoryAnalyser.Helpers;

namespace RepositoryAnalyser.View.WeekdayActivityViews.WeekdayActivityCodeFrequency
{
    /// <summary>
    /// Interaction logic for WeekdayDeletedChartView.xaml
    /// </summary>
    public partial class WeekdayDeletedChartView : UserControl
    {
        public WeekdayDeletedChartView()
        {
            InitializeComponent();
            this.Loaded += (s, e) =>
            {
                ChartingHelper.Instance.DrawCharts<CodeFrequencySubChartViewModel>(this, this.ChartViewInstance,this.ChartViewInstance2);
            };
        }
    }
}

﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.WeekdayActivityViewModels;

namespace RepositoryAnalyser.View.WeekdayActivityViews
{
    /// <summary>
    /// Interaction logic for WeekdayActivityContiniousAnalyseView.xaml
    /// </summary>
    public partial class WeekdayActivityContiniousAnalyseView : UserControl
    {
        public WeekdayActivityContiniousAnalyseView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<WeekdayActivityContiniousAnalyseViewModel>(this,this.ChartViewInstance, this.ChartViewInstance2);
        }
    }
}

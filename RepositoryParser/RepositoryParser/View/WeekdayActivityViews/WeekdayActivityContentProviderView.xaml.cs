﻿using System.Windows.Controls;

namespace RepositoryAnalyser.View.WeekdayActivityViews
{
    /// <summary>
    /// Interaction logic for WeekdayActivityContentProviderView.xaml
    /// </summary>
    public partial class WeekdayActivityContentProviderView : UserControl
    {
        public WeekdayActivityContentProviderView()
        {
            InitializeComponent();
        }
    }
}

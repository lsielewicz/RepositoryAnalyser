﻿using System.Windows.Controls;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.ViewModel.WeekdayActivityViewModels;

namespace RepositoryAnalyser.View.WeekdayActivityViews
{
    /// <summary>
    /// Interaction logic for WeekdayActivityChartView.xaml
    /// </summary>
    public partial class WeekdayActivityChartView : UserControl
    {
        public WeekdayActivityChartView()
        {
            InitializeComponent();
            ChartingHelper.Instance.DrawCharts<WeekDayActivityViewModel>(this, this.ChartViewInstance, this.ChartViewInstance2);
        }
    }
}

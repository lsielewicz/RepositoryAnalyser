﻿using System;
using RepositoryAnalyser.DataBase.Configuration;

namespace RepositoryAnalyser.Services.Coloring
{
    public class ChangesColorModel
    {
        public string Line { get; set; }
        public ChangeType Color { get; set; }



        public ChangesColorModel()
        {
            this.Line = String.Empty;
            this.Color = ChangeType.Unmodified;
        }

        public ChangesColorModel(string line, ChangeType color)
        {
            this.Line = line;
            this.Color = color;
        }
    }
}

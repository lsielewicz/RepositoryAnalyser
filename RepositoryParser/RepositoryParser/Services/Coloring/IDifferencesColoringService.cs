﻿namespace RepositoryAnalyser.Services.Coloring
{
    public interface IDifferencesColoringService
    {
        void FillColorDifferences();
    }
}

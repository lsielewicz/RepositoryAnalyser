﻿using System;
using System.Windows;
using System.Windows.Data;

namespace RepositoryAnalyser.Conventers
{
    public class BooleanToEnumConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter == null || value == null)
            {
                return value;
            }

            var parameterValue = Enum.Parse(value.GetType(), parameter.ToString());
            return parameterValue.Equals(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string parameterString = parameter.ToString();
            if (parameterString == null)
                return DependencyProperty.UnsetValue;

            return Enum.Parse(targetType, parameterString);
        }
        #endregion
    }
}

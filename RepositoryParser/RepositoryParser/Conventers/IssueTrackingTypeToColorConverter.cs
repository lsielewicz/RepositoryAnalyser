﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using RepositoryAnalyser.Domain.Enums;

namespace RepositoryAnalyser.Conventers
{
    public class IssueTrackingTypeToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is IssueTrackingType)
            {
                
                switch ((IssueTrackingType)value)
                {
                    case IssueTrackingType.Jira:
                        return (Brush)Application.Current.FindResource("JiraColor");
                    case IssueTrackingType.Github:
                        return (Brush)Application.Current.FindResource("GithubColor");
                }

            }
            return Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

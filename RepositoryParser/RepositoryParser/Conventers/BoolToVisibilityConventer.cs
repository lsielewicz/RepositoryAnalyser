﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using RepositoryAnalyser.Controls.Common;

namespace RepositoryAnalyser.Conventers
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is bool))
            {
                return value;
            }

            if (parameter != null && parameter is ConventerDirection &&
                ((ConventerDirection) parameter == ConventerDirection.Inverse))
            {
                return (bool)value ? Visibility.Collapsed : Visibility.Visible;
            }
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

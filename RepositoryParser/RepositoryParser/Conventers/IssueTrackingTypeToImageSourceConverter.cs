﻿using System;
using System.Globalization;
using System.Windows.Data;
using RepositoryAnalyser.Domain.Enums;

namespace RepositoryAnalyser.Conventers
{
    public class IssueTrackingTypeToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is IssueTrackingType)
            {
                switch ((IssueTrackingType)value)
                {
                    case IssueTrackingType.Jira:
                        return new Uri("../Assets/Images/Jira_logo.png", UriKind.Relative); 
                    case IssueTrackingType.Github:
                        return new Uri("../Assets/Images/Github_logo.png", UriKind.Relative); 
                }

            }
            return new Uri("../Assets/Images/transparent", UriKind.Relative);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

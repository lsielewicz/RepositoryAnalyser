﻿using System;
using System.Threading.Tasks;

namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    public class ProgressViewModel : WizardPage
    {

        private readonly IDataCollector _dataCollector;
        private bool _isWorking;

        public ProgressViewModel(IDataCollector dataCollector)
        {
            _dataCollector = dataCollector;
        }

        public bool IsWorking
        {
            get { return _isWorking; }
            set
            {
                if (_isWorking == value)
                {
                    return;
                }

                _isWorking = value;
                this.RaisePropertyChanged();
            }
        }

        public override string DisplayName
        {
            get { return this.GetLocalizedString("Progress"); }
        }

        public override bool CanGoForward
        {
            get { return false; }
        }

        public override bool IsCancelEnabled
        {
            get { return false; }
        }

        public override async void OnLoaded()
        {
            try
            {
                this.IsWorking = true;

                await Task.Run(async () =>
                {
                    await _dataCollector.DoWork();

                    this.Wizard.Result = _dataCollector.ResultInfo.Result;
                });

                this.Wizard.NextPageCommand.Execute(null);
            }
            catch (Exception)
            {
                this.Wizard.Result = WizardResult.Failed;
            }
            finally
            {
                this.IsWorking = false;
            }

        }

    }
}
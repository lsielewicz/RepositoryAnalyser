﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    /// <summary>
    /// Interaction logic for ControlVersionParsingDataControl.xaml
    /// </summary>
    public partial class ControlVersionParsingDataControl : UserControl
    {
        public ControlVersionParsingDataControl()
        {
            InitializeComponent();
        }
    }
}

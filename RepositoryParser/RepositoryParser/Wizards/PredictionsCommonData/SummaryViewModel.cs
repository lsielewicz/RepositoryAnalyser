﻿using System.Collections.Generic;

namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    public class SummaryViewModel : WizardPage
    {
        private readonly IDataCollector _dataCollector;

        public SummaryViewModel(IDataCollector dataCollector)
        {
            _dataCollector = dataCollector;
        }

        public override string DisplayName
        {
            get { return this.GetLocalizedString("Summary"); }
        }

        public override bool CanGoForward
        {
            get { return true; }
        }

        public WizardResult Result
        {
            get { return _dataCollector.ResultInfo.Result; }
        }

        public string Info
        {
            get { return _dataCollector.ResultInfo.Description; }
        }

        public override bool IsCancelEnabled
        {
            get { return false; }
        }

        public IList<ReportKeyValue> ReportsPaths
        {
            get { return _dataCollector.ResultInfo.ReportsPaths; }
        }

        
    }

}

﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    /// <summary>
    /// Interaction logic for IssuesSelectionControl.xaml
    /// </summary>
    public partial class IssuesSelectionControl : UserControl
    {
        public IssuesSelectionControl()
        {
            InitializeComponent();
        }
    }
}

﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    /// <summary>
    /// Interaction logic for PredictionTypeControl.xaml
    /// </summary>
    public partial class PredictionTypeControl : UserControl
    {
        public PredictionTypeControl()
        {
            InitializeComponent();
        }
    }
}

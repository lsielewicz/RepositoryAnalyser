﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    /// <summary>
    /// Interaction logic for RepositoriesSelectionControl.xaml
    /// </summary>
    public partial class RepositoriesSelectionControl : UserControl
    {
        public RepositoriesSelectionControl()
        {
            InitializeComponent();
        }
    }
}

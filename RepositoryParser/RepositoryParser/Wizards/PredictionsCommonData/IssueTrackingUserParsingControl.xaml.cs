﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    /// <summary>
    /// Interaction logic for IssueTrackingUserParsingControl.xaml
    /// </summary>
    public partial class IssueTrackingUserParsingControl : UserControl
    {
        public IssueTrackingUserParsingControl()
        {
            InitializeComponent();
        }
    }
}

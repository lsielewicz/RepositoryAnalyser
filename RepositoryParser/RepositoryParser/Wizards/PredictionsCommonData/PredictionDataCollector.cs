﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using MahApps.Metro.Controls;
using NHibernate;
using NHibernate.Transform;
using RepositoryAnalyser.Annotations;
using RepositoryAnalyser.Configuration;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Domain.Models;
using RepositoryAnalyser.IssueTracking.Interfaces;
using RepositoryAnalyser.Locale.Languages;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.VersionControl.Interfaces;
using RepositoryAnalyser.VersionControl.Models;
using RepositoryAnalyser.ViewModel;
using RepositoryAnalyser.ViewModel.TrainingViewModels;
using VersionControlSystemType = RepositoryAnalyser.Domain.Enums.VersionControlSystemType;

namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    public abstract class PredictionDataCollector : INotifyPropertyChanged, IDataCollector
    {
        public List<string> NotValidKeys { get; set; }
        public MetroWindow OwningScreen { get; }

        protected PredictionDataCollector(MetroWindow owningScreen) : this()
        {
            this.OwningScreen = owningScreen;
        }

        private DataSource _issuesDataSource;
        public DataSource IssuesDataSource
        {
            get
            {
                return _issuesDataSource;
            }
            set
            {
                if (_issuesDataSource == value)
                {
                    return;
                }
                _issuesDataSource = value;
                this.OnPropertyChanged();
            }
        }

        private DataSource _repositoryDataSoruce;
        public DataSource RepositoryDataSource
        {
            get
            {
                return _repositoryDataSoruce;
            }
            set
            {
                if (_repositoryDataSoruce == value)
                {
                    return;
                }
                _repositoryDataSoruce = value;
                this.OnPropertyChanged();
            }
        }

        public ObservableCollection<IssueTrackerUserViewModel> UsersCollection { get; set; }
        public ObservableCollection<IssueViewModel> IssuesCollection { get; set; }
        public ObservableCollection<RepositoryViewModel> RepositoriesCollection { get; set; }

        private MlDataType _dataType;
        public MlDataType DataType {
            get
            {
                return _dataType;
            }
            set
            {
                if (_dataType == value)
                {
                    return;
                }
                _dataType = value;
                this.OnPropertyChanged();
            }
        }


        private IssueTrackingType _issueTrakcingType;
        public IssueTrackingType IssueTrakcingType
        {
            get
            {
                return _issueTrakcingType;
            }
            set
            {
                if (_issueTrakcingType == value)
                {
                    return;
                }

                _issueTrakcingType = value; 
                this.OnPropertyChanged();
            }
        }

        private VersionControlSystemType _versionControlSystemType;
        public VersionControlSystemType VersionControlSystemType
        {
            get
            {
                return _versionControlSystemType;
            }
            set
            {
                if (_versionControlSystemType == value)
                {
                    return;
                }

                _versionControlSystemType = value;
                this.OnPropertyChanged();

                if (VersionControlSystemType == VersionControlSystemType.Svn)
                {
                    IsVersionControlSystemLocal = true;
                    this.OnPropertyChanged(nameof(IsVersionControlSystemLocal));
                    return;
                }
            }
        }

        private bool _isVersionControlSystemPrivate;
        public bool IsVersionControlSystemPrivate
        {
            get
            {
                return _isVersionControlSystemPrivate;
            }
            set
            {
                if (_isVersionControlSystemPrivate == value)
                {
                    return;
                }

                _isVersionControlSystemPrivate = value;
                this.OnPropertyChanged();
            }
        }


        private bool _isVersionControlSystemLocal;
        public bool IsVersionControlSystemLocal
        {
            get
            {
                return _isVersionControlSystemLocal;
            }
            set
            {
                if (VersionControlSystemType == VersionControlSystemType.Svn)
                {
                    _isVersionControlSystemLocal = true;
                    this.OnPropertyChanged();
                    return;
                }

                if (_isVersionControlSystemLocal == value)
                {
                    return;
                }

                _isVersionControlSystemLocal = value;
                this.OnPropertyChanged();
            }
        }

        private string _jiraUsername;
        public string JiraUsername
        {
            get
            {
                return _jiraUsername;
            }
            set
            {
                if (_jiraUsername == value)
                {
                    return;
                }

                _jiraUsername = value;
                this.OnPropertyChanged();
            }
        }

        private string _versionControlSystemUrl;
        public string VersionControlSystemUrl
        {
            get
            {
                return _versionControlSystemUrl;
            }
            set
            {
                if (_versionControlSystemUrl == value)
                {
                    return;
                }

                _versionControlSystemUrl = value;
                this.OnPropertyChanged();
            }
        }

        private string _issueTrackingIssueUrl;
        public string IssueTrackingIssueUrl
        {
            get
            {
                return _issueTrackingIssueUrl;
            }
            set
            {
                if (_issueTrackingIssueUrl == value)
                {
                    return;
                }

                _issueTrackingIssueUrl = value;
                this.OnPropertyChanged();
            }
        }

        private Credentials _issueTrackingCredentials;
        public Credentials IssueTrackingCredentials
        {
            get
            {
                return _issueTrackingCredentials;
            }
            set
            {
                if (_issueTrackingCredentials == value)
                {
                    return;
                }

                _issueTrackingCredentials = value;
                this.OnPropertyChanged();
            }
        }

        private Credentials _versionControlSystemCredentials;
        public Credentials VersionControlSystemCredentials
        {
            get
            {
                return _versionControlSystemCredentials;
            }
            set
            {
                if (_versionControlSystemCredentials == value)
                {
                    return;
                }

                _versionControlSystemCredentials = value;
                this.OnPropertyChanged();
            }
        }

        private TrainingOutputDataViewModel _trainingOutputDataViewModel;
        public TrainingOutputDataViewModel TrainingOutputDataViewModel
        {
            get
            {
                return _trainingOutputDataViewModel;
            }
            set
            {
                if (_trainingOutputDataViewModel == value)
                {
                    return;
                }

                _trainingOutputDataViewModel = value;
                this.OnPropertyChanged();
            }
        }

        protected IGithubIssuesParser _githubIssueFileParser;
        protected IJiraIssuesParser _jiraIssuesFileParser;

        protected PredictionDataCollector()
        {
            _githubIssueFileParser = SimpleIoc.Default.GetInstance<IGithubIssuesParser>();
            _jiraIssuesFileParser = SimpleIoc.Default.GetInstance<IJiraIssuesParser>();
            _gitSystemParser = SimpleIoc.Default.GetInstance<IGitSystemParser>();
            _svnSystemParser = SimpleIoc.Default.GetInstance<ISvnSystemParser>();

            this.IssueTrackingCredentials = new Credentials();
            this.VersionControlSystemCredentials = new Credentials();

            //this.IssuesCollection = new ObservableCollection<IssueViewModel>(this.GetIssueViewModels());
            //this.RepositoriesCollection = new ObservableCollection<RepositoryViewModel>(this.GetRepositoriesViewModels());
            //this.UsersCollection = new ObservableCollection<IssueTrackerUserViewModel>(this.GetUsersViewModels());
        }

        public async Task InitializeIssuesCollection()
        {
            IList<IssueViewModel> issuesVms = null;
            await Task.Run(() => { issuesVms = GetIssueViewModels(); });
            if (issuesVms != null)
            {
                this.IssuesCollection = new ObservableCollection<IssueViewModel>(issuesVms);
                this.OnPropertyChanged(nameof(IssuesCollection));
            }
        }

        public async Task InitializeRepositoriesCollection()
        {
            
            IList<RepositoryViewModel> repositoriesVms = null;
            await Task.Run(() => { repositoriesVms = GetRepositoriesViewModels(); });
            if (repositoriesVms != null)
            {
                this.RepositoriesCollection = new ObservableCollection<RepositoryViewModel>(repositoriesVms);
                this.OnPropertyChanged(nameof(RepositoriesCollection));
            }
        }

        public async Task InitializeUsersCollection()
        {
            IList<IssueTrackerUserViewModel> usersVms = null;
            await Task.Run(() => { usersVms = GetUsersViewModels(); });
            if (usersVms != null)
            {
                this.UsersCollection = new ObservableCollection<IssueTrackerUserViewModel>(usersVms);
                this.OnPropertyChanged(nameof(UsersCollection));
            }
        }

        

        private IList<IssueTrackerUserViewModel> GetUsersViewModels()
        {
            IList<IssueTrackerUserViewModel> output = null;
            //await Task.Run(() =>
            //{
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var users = session.QueryOver<IssueTrackerUser>().List();
                output = users.Select(user => new IssueTrackerUserViewModel(user)).ToList();
            }
            // });
            output = output.Distinct().ToList();
            return output;
        }

        private IList<IssueViewModel> GetIssueViewModels()
        {
            IList<IssueViewModel> output = null;
            //await Task.Run(() =>
            //{
                using (var session = DbService.Instance.SessionFactory.OpenSession())
                {
                    var issues = session.QueryOver<Issue>().List();
                    output = issues.Select(issue => new IssueViewModel(issue)).ToList();
                }
           // });

            return output;
        }

        private IList<RepositoryViewModel> GetRepositoriesViewModels()
        {
            IList<RepositoryViewModel> output = null;
            //await Task.Run(() =>
            //{
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var repositories = session.QueryOver<Repository>().List();
                output = repositories.Select(repository => new RepositoryViewModel(repository)).ToList();
            }
            // });

            return output;
        }

        public abstract Task DoWork();
        public WizardResultInfo ResultInfo { get; protected set; }

        private RelayCommand<PasswordBox> _issueTrackingPasswordChangedCommand;

        public RelayCommand<PasswordBox> IssueTrackingPasswordChangedCommand
        {
            get
            {
                return _issueTrackingPasswordChangedCommand ?? (_issueTrackingPasswordChangedCommand = new RelayCommand<PasswordBox>((param) =>
                           {
                               this.IssueTrackingCredentials.Password = param.Password;
                           }));
            }
        }

        private RelayCommand<PasswordBox> _repositoryPasswordChangedCommand;
        protected IGitSystemParser _gitSystemParser;
        protected ISvnSystemParser _svnSystemParser;

        public RelayCommand<PasswordBox> RepositoryPasswordChangedCommand
        {
            get
            {
                return _repositoryPasswordChangedCommand ?? (_repositoryPasswordChangedCommand = new RelayCommand<PasswordBox>((param) =>
                {
                    this.VersionControlSystemCredentials.Password = param.Password;
                }));
            }
        }

        private RelayCommand _openDirectoryPickerCommand;
        public RelayCommand OpenDirectoryPickerCommand
        {
            get
            {
                return _openDirectoryPickerCommand ?? (_openDirectoryPickerCommand =
                           new RelayCommand(() =>
                           {

                               FolderBrowserDialog fbd = new FolderBrowserDialog
                               {
                                   SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory,
                                   Description = Resources.PickFolderWithRepo
                               };

                               if (fbd.ShowDialog() == DialogResult.OK)
                               {
                                   VersionControlSystemUrl = fbd.SelectedPath;
                                   this.OnPropertyChanged(nameof(VersionControlSystemUrl));
                               }
                           }));
            }
        }

        public IList<Issue> GetIssuesFromDb(ISession session)
        {
            var selectedIds = this.IssuesCollection.Where(issue => issue.IsSelected).Select(i => i.Id).ToList();

            var issues = session.QueryOver<Issue>().WhereRestrictionOn((issue) => issue.Id)
                .IsIn(selectedIds).TransformUsing(Transformers.DistinctRootEntity).List();

            return issues;
        }

        public IList<Repository> GetRepositoriesFromDb(ISession session)
        {
            var selectedIds = this.RepositoriesCollection.Where(r => r.IsSelected).Select(repo => repo.Entity.Id).ToList();

            var repositories = session.QueryOver<Repository>().WhereRestrictionOn((r) => r.Id)
                .IsIn(selectedIds).TransformUsing(Transformers.DistinctRootEntity).List();

            return repositories;
        }

        public IList<IssueTrackerUser> GetUsersFromDb(ISession session)
        {
            var selectedIds = this.UsersCollection.Where(r => r.IsSelected).Select(repo => repo.Entity.Id).ToList();

            var users = session.QueryOver<IssueTrackerUser>().WhereRestrictionOn((r) => r.Id)
                .IsIn(selectedIds).TransformUsing(Transformers.DistinctRootEntity).List();

            return users;
        }

        protected Repository GetRepositoryFromNetwork()
        {
            Repository repository = null;
            switch (this.VersionControlSystemType)
            {
                case VersionControlSystemType.Git:

                    if (IsVersionControlSystemLocal)
                    {
                        repository = _gitSystemParser.Parse(this.VersionControlSystemUrl);
                    }
                    else
                    {
                        repository = _gitSystemParser.Parse(new GitCloningData()
                        {
                            CloneType = this.IsVersionControlSystemPrivate
                                ? RepositoryCloneType.Private
                                : RepositoryCloneType.Public,
                            Credentials = VersionControlSystemCredentials,
                            Url = VersionControlSystemUrl,
                            IncludeAllBranches = false,
                            TargetPath = ConfigurationService.Instance.Configuration.SavingRepositoryPath
                        });
                    }

                    break;
                case VersionControlSystemType.Svn:
                    repository = _svnSystemParser.Parse(this.VersionControlSystemUrl);
                    break;
            }

            return repository;
        }

        protected async Task<IssueTrackerUserRequest> GetUserFromNetwork()
        {
            IssueTrackerUserRequest userRequest = null;
            var connectionData = new ConnectionData()
            {
                Url = this.IssueTrackingIssueUrl,
                Credentials = IssueTrackingCredentials
            };
            if (!this.TrainingOutputDataViewModel.IsTrained)
            {
                this.TrainingOutputDataViewModel.TrainCommand.Execute(null);
            }
            switch (IssueTrakcingType)
            {
                case IssueTrackingType.Jira:
                    userRequest = await _jiraIssuesFileParser.GetUser(connectionData, this.JiraUsername);
                    break;
                case IssueTrackingType.Github:
                    userRequest = await _githubIssueFileParser.GetUser(connectionData);
                    break;
            }

            return userRequest;
        }

        protected async Task<IssueRequest> GetIssueFromNetwork()
        {
            IssueRequest issueRequest = null;
            var connectionData = new ConnectionData()
            {
                Url = this.IssueTrackingIssueUrl,
                Credentials = IssueTrackingCredentials
            };
            if (!this.TrainingOutputDataViewModel.IsTrained)
            {
                this.TrainingOutputDataViewModel.TrainCommand.Execute(null);
            }
            switch (IssueTrakcingType)
            {
                case IssueTrackingType.Jira:
                    issueRequest = await _jiraIssuesFileParser.GetIssue(connectionData);
                    break;
                case IssueTrackingType.Github:
                    issueRequest = await _githubIssueFileParser.GetIssue(connectionData);
                    break;
            }

            if (issueRequest == null)
            {
                this.ResultInfo = new WizardResultInfo()
                {
                    Result = WizardResult.Failed,
                    Description = "Failed to parse the issue"
                };
                return null;
            }

            return issueRequest;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

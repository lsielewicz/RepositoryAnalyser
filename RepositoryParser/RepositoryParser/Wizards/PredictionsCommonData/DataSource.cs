﻿namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    public enum DataSource
    {
        Database,
        Network
    }
}
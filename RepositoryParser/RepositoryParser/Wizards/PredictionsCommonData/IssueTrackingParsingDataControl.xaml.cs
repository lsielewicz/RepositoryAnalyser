﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.PredictionsCommonData
{
    /// <summary>
    /// Interaction logic for IssueTrackingParsingDataControl.xaml
    /// </summary>
    public partial class IssueTrackingParsingDataControl : UserControl
    {
        public IssueTrackingParsingDataControl()
        {
            InitializeComponent();
        }
    }
}

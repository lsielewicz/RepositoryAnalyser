﻿using System.Threading.Tasks;

namespace RepositoryAnalyser.Wizards
{
    public interface IDataCollector
    {
        Task DoWork();
        WizardResultInfo ResultInfo { get;}
    }
}
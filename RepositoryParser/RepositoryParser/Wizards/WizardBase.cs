﻿using System;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using RepositoryAnalyser.Configuration;

namespace RepositoryAnalyser.Wizards
{
    public abstract class WizardBase : ViewModelBase
    {
        public WizardPage[] Pages { get; protected set; }
        public WizardResult Result { get; set; }
        public Action CloseHanlder { get; set; }

        private WizardPage _currentPage;
        private RelayCommand _nextPageCommand;
        private RelayCommand _previousPageCommand;
        private RelayCommand _cancelCommand;

        public WizardPage CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                _currentPage.Wizard = this;
                this.RaisePropertyChanged();
                this.RaisePropertyChanged(nameof(IsFirstPage));
                this.RaisePropertyChanged(nameof(IsLastPage));
            }
        }

        public RelayCommand NextPageCommand
        {
            get
            {
                return _nextPageCommand ?? (_nextPageCommand = new RelayCommand(() =>
                {
                    this.CurrentPage.BeforeGoForward();
                    if (this.IsLastPage)
                    {
                        this.CloseHanlder();
                        return;
                    }
                    this.CurrentPage = this.Pages[CurrentPageIndex + 1];

                    this.CurrentPage.OnLoaded();

                }));
            }
        }

        public bool IsLastPage
        {
            get
            {
                return CurrentPageIndex == this.Pages.Length - 1;
            }
        }

        public bool IsFirstPage
        {
            get
            {
                return CurrentPageIndex == 0;
            }
        }

        public int CurrentPageIndex
        {
            get
            {
                return Array.FindIndex(this.Pages, page => page.Id == this.CurrentPage.Id);

            }
        }

        public void AddPageAfterCurrentPage(WizardPage page)
        {
            var pagesList = Pages.ToList();

            pagesList.Insert(this.CurrentPageIndex + 1, page);

            this.Pages = pagesList.ToArray();
        }

        public RelayCommand PreviousPageCommand
        {
            get
            {
                return _previousPageCommand ?? (_previousPageCommand = new RelayCommand(() =>
                {

                    if (IsFirstPage)
                    {
                        return;
                    }
                    this.CurrentPage.BeforeGoBackward();
                    this.CurrentPage = this.Pages[CurrentPageIndex - 1];
                    this.CurrentPage.OnLoaded();
                }));
            }
        }

        public RelayCommand CancelCommand
        {
            get
            {
                return _cancelCommand ?? (_cancelCommand = new RelayCommand(() =>
                {
                    this.Result = WizardResult.Cancelled;
                    this.CloseHanlder();
                }));
            }
        }

        public string GetLocalizedString(string resourceKey)
        {
            return RepositoryAnalyser.Locale.Languages.Resources.ResourceManager.GetString(resourceKey, ConfigurationService.Instance.CultureInfo);
        }

    }
}

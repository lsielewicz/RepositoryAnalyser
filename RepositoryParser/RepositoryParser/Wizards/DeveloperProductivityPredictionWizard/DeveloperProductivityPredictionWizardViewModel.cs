﻿using System.Linq;
using MahApps.Metro.Controls;
using RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Models;
using RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Pages;
using RepositoryAnalyser.Wizards.PredictionsCommonData;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard
{
    public class DeveloperProductivityPredictionWizardViewModel : WizardBase
    {
        public DeveloperProductivityPredictionWizardViewModel(MetroWindow owningScreen)
        {
            var dataCollector = new DeveloperProductivityDataCollector(owningScreen);

            Pages = new WizardPage[]
            {
                new DeveloperProductivityPredictionTypeViewModel(dataCollector),
                new DevProductivityIssueTrackingDataViewModel(dataCollector), 
                new DevProductivityTrainingModelViewModel(dataCollector), 
                new ProgressViewModel(dataCollector),
                new SummaryViewModel(dataCollector),
            };

            this.CurrentPage = Pages.First();
        }
    }
}

﻿using System.ComponentModel;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard
{
    /// <summary>
    /// Interaction logic for DeveloperProductivityPredictionWizard.xaml
    /// </summary>
    public partial class DeveloperProductivityPredictionWizard
    {
        public WizardResult WizardResult { get; set; }

        public DeveloperProductivityPredictionWizard()
        {
            this.DataContext = new DeveloperProductivityPredictionWizardViewModel(this)
            {
                CloseHanlder = this.Close,
                Result = this.WizardResult
            };
            InitializeComponent();
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            var wizardResult = (this.DataContext as DeveloperProductivityPredictionWizardViewModel)?.Result;
            if (wizardResult != null)
            {
                this.WizardResult = (WizardResult)wizardResult;
            }
        }
    }
}

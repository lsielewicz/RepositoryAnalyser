﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Ioc;
using MahApps.Metro.Controls;
using NHibernate;
using RepositoryAnalyser.Configuration;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Interfaces;
using RepositoryAnalyser.Reporting.Interfaces;
using RepositoryAnalyser.Wizards.PredictionsCommonData;
using ZetaLongPaths;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Models
{
    public class DeveloperProductivityDataCollector : PredictionDataCollector
    {
        private readonly IDeveloperEfficiencyPredictor _predictor;
        private readonly IDevEfficiencyPredictionResultPdfExporter _exporter;
        private readonly IDevEfficiencyIntegratedPredictionResultPdfExporter _integratedExporter;


        public DeveloperProductivityDataCollector(MetroWindow owningScreen) : base(owningScreen)
        {
            _predictor = SimpleIoc.Default.GetInstance<IDeveloperEfficiencyPredictor>();
            _exporter = SimpleIoc.Default.GetInstance<IDevEfficiencyPredictionResultPdfExporter>();
            _integratedExporter = SimpleIoc.Default.GetInstance<IDevEfficiencyIntegratedPredictionResultPdfExporter>();
        }

        public IList<Commit> GetCommitsFromCommitsSet(ISession session, CommitsSet commitsSet)
        {
            var output = new List<Commit>();

            if (commitsSet != null && commitsSet.Commits != null && commitsSet.Commits.Any())
            {
                var revisions = commitsSet.Commits.Select(c => c.Revision).ToList();

            }

            return output;
        }

        public override async Task DoWork()
        {
            IList<Commit> commits = null;
            IList<IssueTrackerUser> users = null;
            IList<CommitsSet> commitsSets = null;
            IList<Issue> issues = null;
            var reports = new List<ReportKeyValue>();
            var resultDescription = string.Empty;
            var weightsKeeper = ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper;
            if (!this.TrainingOutputDataViewModel.IsTrained)
            {
                this.TrainingOutputDataViewModel.TrainCommand.Execute(null);
            }

            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                issues = (List<Issue>)this.GetIssuesFromDb(session);
                switch (IssuesDataSource)
                {
                    case DataSource.Database:
                        users = this.GetUsersFromDb(session);
                        break;

                    case DataSource.Network:
                        var userRequest = await this.GetUserFromNetwork();
                        if (userRequest != null)
                        {
                            users = new List<IssueTrackerUser>() { userRequest.User };
                            commitsSets = userRequest.CommitsSets;
                            ((List<Issue>)issues).AddRange(userRequest.Issues);
                            issues = issues.Distinct().ToList();
                        }

                        break;
                }

                if (users == null)
                {
                    this.ResultInfo = new WizardResultInfo()
                    {
                        Result = WizardResult.Failed,
                        Description = "No users to use"
                    };
                    return;
                }

                if (this.DataType == MlDataType.Integrated)
                {
                    switch (RepositoryDataSource)
                    {
                        case DataSource.Database:
                            var repositories = this.GetRepositoriesFromDb(session);
                            commits = CommitsFilteringHelper.GetAllCommitsOfRepositories(repositories);
                            break;
                        case DataSource.Network:
                            repositories = new List<Repository>() { this.GetRepositoryFromNetwork() };
                            break;
                    }

                    if (commits == null || !commits.Any())
                    {
                        this.ResultInfo = new WizardResultInfo()
                        {
                            Result = WizardResult.Failed,
                            Description = "No repositories to use"
                        };
                        return;
                    }


                    foreach (var user in users) //TODO: DO FORMING RESULT !!
                    {
                        var predictionResult = await _predictor.Predict(user,issues, commits, this.TrainingOutputDataViewModel.Entity, weightsKeeper);
                        predictionResult.ModelName = TrainingsManagementHelper.DevelopersEfficiencyPrediction.GetModelNameFromFilePath(predictionResult.TrainingModel.FilePath);

                        var name = $"DeveloperEfficiencyPrediction_{Guid.NewGuid()}.pdf";
                        var path = ZlpPathHelper.Combine(LocalizationConstants.PrognosisReportsPath, name);
                        _integratedExporter.Export(predictionResult, path);

                        reports.Add(new ReportKeyValue()
                        {
                            Key = user.UserName,
                            Path = path,
                            IsValid = this.NotValidKeys == null || (this.NotValidKeys != null && !this.NotValidKeys.Contains(user.UserName))
                        });
                    }
                }
                else
                {
                    foreach (var user in users)  //TODO: DO FORMING RESULT !!
                    {
                        var predictionResult =
                            await _predictor.Predict(user,issues, this.TrainingOutputDataViewModel.Entity, weightsKeeper);
                        predictionResult.ModelName = TrainingsManagementHelper.DevelopersEfficiencyPrediction.GetModelNameFromFilePath(predictionResult.TrainingModel.FilePath);

                        var name = $"DeveloperEfficiencyPrediction_{Guid.NewGuid()}.pdf";
                        var path = ZlpPathHelper.Combine(LocalizationConstants.PrognosisReportsPath, name);
                        _exporter.Export(predictionResult, path);

                        reports.Add(new ReportKeyValue()
                        {
                            Key = user.UserName,
                            Path = path,
                            IsValid = this.NotValidKeys == null || (this.NotValidKeys != null && !this.NotValidKeys.Contains(user.UserName))
                        });
                    }

                }


                resultDescription = RepositoryAnalyser.Locale.Languages.Resources.PredictionsSuccessed;
                this.ResultInfo = new WizardResultInfo()
                {
                    Result = WizardResult.Successed,
                    Description = resultDescription,
                    ReportsPaths = reports
                };

            }
        }
    }
}

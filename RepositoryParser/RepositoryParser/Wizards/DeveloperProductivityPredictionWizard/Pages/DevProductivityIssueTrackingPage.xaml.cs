﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for DevProductivityIssueTrackingPage.xaml
    /// </summary>
    public partial class DevProductivityIssueTrackingPage : UserControl
    {
        public DevProductivityIssueTrackingPage()
        {
            InitializeComponent();
        }
    }
}

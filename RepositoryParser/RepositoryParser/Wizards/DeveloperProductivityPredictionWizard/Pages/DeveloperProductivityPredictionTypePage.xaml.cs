﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for DeveloperProductivityPredictionTypePage.xaml
    /// </summary>
    public partial class DeveloperProductivityPredictionTypePage : UserControl
    {
        public DeveloperProductivityPredictionTypePage()
        {
            InitializeComponent();
        }
    }
}

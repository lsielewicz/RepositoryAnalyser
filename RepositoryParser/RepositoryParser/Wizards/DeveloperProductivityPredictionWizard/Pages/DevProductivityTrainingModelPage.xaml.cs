﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for DevProductivityTrainingModelPage.xaml
    /// </summary>
    public partial class DevProductivityTrainingModelPage : UserControl
    {
        public DevProductivityTrainingModelPage()
        {
            InitializeComponent();
        }
    }
}

﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for DevProductivityVersionControlPage.xaml
    /// </summary>
    public partial class DevProductivityVersionControlPage : UserControl
    {
        public DevProductivityVersionControlPage()
        {
            InitializeComponent();
        }
    }
}

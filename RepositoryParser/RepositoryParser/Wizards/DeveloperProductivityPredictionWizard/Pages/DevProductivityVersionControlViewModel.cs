﻿using RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Models;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Pages
{
    public class DevProductivityVersionControlViewModel : WizardPage
    {
        public DevProductivityVersionControlViewModel(DeveloperProductivityDataCollector dc)
        {
            this.DataCollector = dc;
        }

        public DeveloperProductivityDataCollector DataCollector { get; set; }

        public override string DisplayName => this.GetLocalizedString("VersionControlSystemData");
        public override bool CanGoForward => true;
        public override bool IsCancelEnabled => true;

        public override async void OnLoaded()
        {
            base.OnLoaded();
            this.IsLoading = true;
            await this.DataCollector.InitializeUsersCollection();
            await this.DataCollector.InitializeRepositoriesCollection();
            this.RaisePropertyChanged(nameof(CanGoForward));
            this.IsLoading = false;
        }
        public override void BeforeGoForward()
        {
            var dc = this.DataCollector;
        }
    }
}

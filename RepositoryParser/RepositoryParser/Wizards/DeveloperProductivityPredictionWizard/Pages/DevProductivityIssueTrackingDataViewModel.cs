﻿using System;
using System.Collections.Generic;
using System.Linq;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings.InformationDialog;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Models;
using RepositoryAnalyser.Wizards.PredictionsCommonData;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Pages
{
    public class DevProductivityIssueTrackingDataViewModel : WizardPage
    {
        public DevProductivityIssueTrackingDataViewModel(DeveloperProductivityDataCollector dc)
        {
            this.DataCollector = dc;
        }

        public DeveloperProductivityDataCollector DataCollector { get; set; }

        public override string DisplayName => this.GetLocalizedString("IssueTrackingData");
        public override bool CanGoForward => true;
        public override bool IsCancelEnabled => true;

        public override async void OnLoaded()
        {
            base.OnLoaded();
            this.IsLoading = true;
            await this.DataCollector.InitializeIssuesCollection();
            this.RaisePropertyChanged(nameof(CanGoForward));
            this.IsLoading = false;
        }

        public override async void BeforeGoForward()
        {
            if (this.DataCollector.IssuesDataSource != DataSource.Database ||
                this.DataCollector.RepositoryDataSource != DataSource.Database)
            {
                return;
            }

            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                IList<IssueTrackerUser> invalidIssues = null;
                var issues = DataCollector.GetIssuesFromDb(session);
                var users = DataCollector.GetUsersFromDb(session);
                if (this.DataCollector.DataType != MlDataType.Integrated)
                {
                    invalidIssues = users.Where(u => !DataValidator.IsDevEfficiencyValid(u, issues, Configuration.ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper)).ToList();
                }
                else
                {
                    var repositories = DataCollector.GetRepositoriesFromDb(session);
                    invalidIssues = users.Where(u => !DataValidator.IsDevEfficiencyValid(u, issues, repositories, Configuration.ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper)).ToList();
                }

                if (invalidIssues.Any())
                {
                    var invalidIssuesKeys = invalidIssues.Select(i => i.UserName).ToList();
                    await DialogHelper.Instance.ShowDialog(new CustomDialogEntryData()
                    {
                        MetroWindow = this.DataCollector.OwningScreen,
                        DialogTitle = this.GetLocalizedString("Warning"),
                        DialogMessage =
                            $"{this.GetLocalizedString("FoundInvalidItemsToPredict")}{Environment.NewLine}{Environment.NewLine}{string.Join("\n", invalidIssuesKeys)}",
                        OkButtonMessage = "Ok",
                        InformationType = InformationType.Warning
                    });

                    this.DataCollector.NotValidKeys = invalidIssues.Select(i => i.UserName).ToList();
                }
            }

        }
    }
    
}

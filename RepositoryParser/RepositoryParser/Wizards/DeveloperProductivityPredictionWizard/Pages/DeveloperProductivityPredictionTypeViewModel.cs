﻿using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Models;

namespace RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard.Pages
{
    public class DeveloperProductivityPredictionTypeViewModel : WizardPage
    {
        public DeveloperProductivityDataCollector DataCollector { get; set; }

        public DeveloperProductivityPredictionTypeViewModel(DeveloperProductivityDataCollector dc)
        {
            this.DataCollector = dc;
        }

        public override string DisplayName => this.GetLocalizedString("PredictionType");
        public override bool CanGoForward => true;
        public override bool IsCancelEnabled => true;

        public override void BeforeGoForward()
        {
            if (DataCollector.DataType == MlDataType.Integrated)
            {
                this.Wizard.AddPageAfterCurrentPage(new DevProductivityVersionControlViewModel(this.DataCollector));
            }
        }
    }
}

﻿using System;
using GalaSoft.MvvmLight;

namespace RepositoryAnalyser.Wizards
{
    public abstract class WizardPage : ViewModelBase
    {
        private bool _isLoading;

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                if (_isLoading == value)
                {
                    return;
                }

                _isLoading = value;
                this.RaisePropertyChanged();
            }
        }

        protected WizardPage()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; private set; }
        public abstract string DisplayName { get; }

        public abstract bool CanGoForward { get; }

        public abstract bool IsCancelEnabled { get; }

        public WizardBase Wizard { get; set; }

        public virtual void BeforeGoForward()
        {

        }

        public virtual void BeforeGoBackward()
        {

        }

        public virtual void OnLoaded() { }

        public string GetLocalizedString(string resourceKey)
        {
            return this.Wizard.GetLocalizedString(resourceKey);
        }
    }
}

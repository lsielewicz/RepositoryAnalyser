﻿using RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Models;

namespace RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Pages
{
    public class TimeToFixVersionControlDataPageViewModel : WizardPage
    {
        public TimeToFixVersionControlDataPageViewModel(TimeToFixDataCollector dc)
        {
            this.DataCollector = dc;
        }

        public TimeToFixDataCollector DataCollector { get; set; }

        public override string DisplayName => this.GetLocalizedString("VersionControlSystemData");
        public override bool CanGoForward => true;
        public override bool IsCancelEnabled => true;

        public override async void OnLoaded()
        {
            base.OnLoaded();
            this.IsLoading = true;
            await this.DataCollector.InitializeRepositoriesCollection();
            this.RaisePropertyChanged(nameof(CanGoForward));
            this.IsLoading = false;
        }
        public override void BeforeGoForward()
        {
            var dc = this.DataCollector;
        }
    }
}

﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for TimeToFixVersionControlDataPage.xaml
    /// </summary>
    public partial class TimeToFixVersionControlDataPage : UserControl
    {
        public TimeToFixVersionControlDataPage()
        {
            InitializeComponent();
        }
    }
}

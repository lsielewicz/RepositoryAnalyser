﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for TimeToFixPredictionTypePage.xaml
    /// </summary>
    public partial class TimeToFixPredictionTypePage : UserControl
    {
        public TimeToFixPredictionTypePage()
        {
            InitializeComponent();
        }
    }
}

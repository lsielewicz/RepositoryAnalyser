﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight.Ioc;
using RepositoryAnalyser.ViewModel.TrainingViewModels;
using RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Models;

namespace RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Pages
{
    public class TimeToFixTrainingModelViewModel : WizardPage
    {
        public TimeToFixTrainingModelViewModel(TimeToFixDataCollector dc)
        {
            this.IncludeNotTrained = true;
            this.DataCollector = dc;
        }

        public override void OnLoaded()
        {
            base.OnLoaded();
            this.RefreshTrainingCollection();
        }

        private bool _includeNotTrained;

        public bool IncludeNotTrained
        {
            get { return _includeNotTrained; }
            set
            {
                if (_includeNotTrained == value)
                {
                    return;
                }

                _includeNotTrained = value;
                this.RaisePropertyChanged();
                this.RefreshTrainingCollection();
                
            }
        }

        private void RefreshTrainingCollection()
        {
            if (this.DataCollector == null || this.DataCollector.DataType == null)
            {
                return;
            }

            var trainingVm = SimpleIoc.Default.GetInstance<TimeToFixPredictorTrainingViewModel>();

            IList<TrainingOutputDataViewModel> trainingModels = null;
            switch (DataCollector.DataType)
            {
                case MachineLearning.Enums.MlDataType.Normal:
                    trainingModels = new List<TrainingOutputDataViewModel>(trainingVm.TrainingModels);
                    break;
                case MachineLearning.Enums.MlDataType.Integrated:
                    trainingModels = new List<TrainingOutputDataViewModel>(trainingVm.IntegratedTrainingModels);
                    break;
            }

            if (trainingModels == null)
            {
                return;
            }

            if (!this.IncludeNotTrained)
            {
                trainingModels = trainingModels.Where(t=>t.IsTrained).ToList();
            }

            this.TrainingDataViewModels = new ObservableCollection<TrainingOutputDataViewModel>(trainingModels);
            this.RaisePropertyChanged(nameof(TrainingDataViewModels));

            if (this.TrainingDataViewModels != null && this.TrainingDataViewModels.Any())
            {
                this.SelectedTrainingOutputDataViewModel = this.TrainingDataViewModels.First();
            }
        }

        public TimeToFixDataCollector DataCollector { get; set; }

        public ObservableCollection<TrainingOutputDataViewModel> TrainingDataViewModels { get; set; }

        private TrainingOutputDataViewModel _selectedTrainingOutputDataViewModel;

        public TrainingOutputDataViewModel SelectedTrainingOutputDataViewModel
        {
            get { return _selectedTrainingOutputDataViewModel; }
            set
            {
                if (_selectedTrainingOutputDataViewModel == value)
                {
                    return;
                }

                _selectedTrainingOutputDataViewModel = value;
                this.RaisePropertyChanged();
                this.RaisePropertyChanged(nameof(CanGoForward));
            }
        }

        public override void BeforeGoForward()
        {
            this.DataCollector.TrainingOutputDataViewModel = this.SelectedTrainingOutputDataViewModel;
        }

        public override string DisplayName => this.GetLocalizedString("TrainingModelData");
        public override bool CanGoForward => SelectedTrainingOutputDataViewModel != null;
        public override bool IsCancelEnabled => true;
    }
}

﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for TimeToFixPIssueTrackingDataPage.xaml
    /// </summary>
    public partial class TimeToFixPIssueTrackingDataPage : UserControl
    {
        public TimeToFixPIssueTrackingDataPage()
        {
            InitializeComponent();
        }
    }
}

﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for TimeToFixTrainingModelPage.xaml
    /// </summary>
    public partial class TimeToFixTrainingModelPage : UserControl
    {
        public TimeToFixTrainingModelPage()
        {
            InitializeComponent();
        }
    }
}

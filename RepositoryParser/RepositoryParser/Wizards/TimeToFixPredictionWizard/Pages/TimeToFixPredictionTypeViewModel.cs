﻿using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Models;

namespace RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Pages
{
    public class TimeToFixPredictionTypeViewModel : WizardPage
    {
        public TimeToFixDataCollector DataCollector { get; private set; }
        public TimeToFixPredictionTypeViewModel(TimeToFixDataCollector dc)
        {
            DataCollector = dc;
        }

        public override void BeforeGoForward()
        {
            if (DataCollector.DataType == MlDataType.Integrated)
            {
                this.Wizard.AddPageAfterCurrentPage(new TimeToFixVersionControlDataPageViewModel(this.DataCollector));
            }
        }

        public override string DisplayName
        {
            get { return this.GetLocalizedString("PredictionType");  }
        }

        public override bool CanGoForward
        {
            get { return true; }
        }

        public override bool IsCancelEnabled
        {
            get { return true; }
        }
    }
}

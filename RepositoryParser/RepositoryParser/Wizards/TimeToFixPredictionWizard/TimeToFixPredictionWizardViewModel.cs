﻿using System.Linq;
using MahApps.Metro.Controls;
using RepositoryAnalyser.Wizards.PredictionsCommonData;
using RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Models;
using RepositoryAnalyser.Wizards.TimeToFixPredictionWizard.Pages;

namespace RepositoryAnalyser.Wizards.TimeToFixPredictionWizard
{
    public class TimeToFixPredictionWizardViewModel : WizardBase
    {
        public TimeToFixPredictionWizardViewModel(MetroWindow owningScreen)
        {
            var dataCollector = new TimeToFixDataCollector(owningScreen);

            Pages = new WizardPage[]
            {
                new TimeToFixPredictionTypeViewModel(dataCollector), 
                new TimeToFixPIssueTrackingDataPageViewModel(dataCollector), 
                new TimeToFixTrainingModelViewModel(dataCollector), 
                new ProgressViewModel(dataCollector),
                new SummaryViewModel(dataCollector), 
            };

            this.CurrentPage = Pages.First();
        }
    }
}

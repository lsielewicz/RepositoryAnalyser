﻿using System.ComponentModel;

namespace RepositoryAnalyser.Wizards.TimeToFixPredictionWizard
{
    /// <summary>
    /// Interaction logic for TimeToFixPredictionWizard.xaml
    /// </summary>
    public partial class TimeToFixPredictionWizard
    {
        public WizardResult WizardResult { get; set; }

        public TimeToFixPredictionWizard()
        {
            this.DataContext = new TimeToFixPredictionWizardViewModel(this)
            {
                CloseHanlder = this.Close,
                Result = this.WizardResult
            };
            InitializeComponent();
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            var wizardResult = (this.DataContext as TimeToFixPredictionWizardViewModel)?.Result;
            if (wizardResult != null)
            {
                this.WizardResult = (WizardResult)wizardResult;
            }
        }
    }
}

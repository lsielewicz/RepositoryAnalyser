﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for ReopenIssueTrackingDataPage.xaml
    /// </summary>
    public partial class ReopenIssueTrackingDataPage : UserControl
    {
        public ReopenIssueTrackingDataPage()
        {
            InitializeComponent();
        }
    }
}

﻿using RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Models;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Pages
{
    public class ReopenVersionControlDataPageViewModel : WizardPage
    {
        public ReopenVersionControlDataPageViewModel(IssueReopenDataCollector dc)
        {
            this.DataCollector = dc;
        }

        public IssueReopenDataCollector DataCollector { get; set; }

        public override async void OnLoaded()
        {
            base.OnLoaded();
            this.IsLoading = true;
            await this.DataCollector.InitializeRepositoriesCollection();
            this.RaisePropertyChanged(nameof(CanGoForward));
            this.IsLoading = false;
        }

        public override string DisplayName => this.GetLocalizedString("VersionControlSystemData");
        public override bool CanGoForward => true;
        public override bool IsCancelEnabled => true;
    }
}

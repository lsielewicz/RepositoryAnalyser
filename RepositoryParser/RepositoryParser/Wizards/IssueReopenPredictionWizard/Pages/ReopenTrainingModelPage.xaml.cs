﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for ReopenTrainingModelPage.xaml
    /// </summary>
    public partial class ReopenTrainingModelPage : UserControl
    {
        public ReopenTrainingModelPage()
        {
            InitializeComponent();
        }
    }
}

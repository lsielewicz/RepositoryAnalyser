﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for IssueReopenPredictionTypePage.xaml
    /// </summary>
    public partial class IssueReopenPredictionTypePage : UserControl
    {
        public IssueReopenPredictionTypePage()
        {
            InitializeComponent();
        }
    }
}

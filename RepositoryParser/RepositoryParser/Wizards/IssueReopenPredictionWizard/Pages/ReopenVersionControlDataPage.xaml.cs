﻿using System.Windows.Controls;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Pages
{
    /// <summary>
    /// Interaction logic for ReopenVersionControlDataPage.xaml
    /// </summary>
    public partial class ReopenVersionControlDataPage : UserControl
    {
        public ReopenVersionControlDataPage()
        {
            InitializeComponent();
        }
    }
}

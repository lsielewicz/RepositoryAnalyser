﻿using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Models;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Pages
{
    public class IssueReopenPredictionTypeViewModel : WizardPage
    {
        public IssueReopenDataCollector DataCollector { get; set; }
        public IssueReopenPredictionTypeViewModel(IssueReopenDataCollector dc)
        {
            DataCollector = dc;
        }

        public override void BeforeGoForward()
        {
            if (DataCollector.DataType == MlDataType.Integrated)
            {
                this.Wizard.AddPageAfterCurrentPage(new ReopenVersionControlDataPageViewModel(this.DataCollector));
            }
        }
        public override string DisplayName => this.GetLocalizedString("PredictionType");
        public override bool CanGoForward => true;
        public override bool IsCancelEnabled => true;
    }
}

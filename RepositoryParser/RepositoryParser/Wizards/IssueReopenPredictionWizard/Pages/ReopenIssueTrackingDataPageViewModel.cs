﻿using System;
using System.Collections.Generic;
using System.Linq;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings.InformationDialog;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Models;
using RepositoryAnalyser.Wizards.PredictionsCommonData;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Pages
{
    public class ReopenIssueTrackingDataPageViewModel : WizardPage
    {
        public ReopenIssueTrackingDataPageViewModel(IssueReopenDataCollector dc)
        {
            this.DataCollector = dc;
        }

        public override async void OnLoaded()
        {
            base.OnLoaded();
            this.IsLoading = true;
            await this.DataCollector.InitializeIssuesCollection();
            this.RaisePropertyChanged(nameof(CanGoForward));
            this.IsLoading = false;
        }

        public IssueReopenDataCollector DataCollector { get; set; }

        public override string DisplayName => this.GetLocalizedString("IssueTrackingData");
        public override bool CanGoForward => this.DataCollector.IssuesCollection != null && this.DataCollector.IssuesCollection.Any();
        public override bool IsCancelEnabled => true;

        public override async void BeforeGoForward()
        {
            if (this.DataCollector.IssuesDataSource != DataSource.Database ||
                this.DataCollector.RepositoryDataSource != DataSource.Database)
            {
                return;
            }

            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                List<Issue> invalidIssues = null;
                var issues = DataCollector.GetIssuesFromDb(session);
                if (this.DataCollector.DataType != MlDataType.Integrated)
                {
                    invalidIssues = issues.Where(i => !DataValidator.IsValid(i)).ToList();
                }
                else
                {
                    var repositories = DataCollector.GetRepositoriesFromDb(session);
                    invalidIssues = issues.Where(i => !DataValidator.IsReopenIssueDataValid(i, repositories))
                        .ToList();
                }

                if (invalidIssues.Any())
                {
                    var invalidIssuesKeys = invalidIssues.Select(i => i.Key).ToList();
                    await DialogHelper.Instance.ShowDialog(new CustomDialogEntryData()
                    {
                        MetroWindow = this.DataCollector.OwningScreen,
                        DialogTitle = this.GetLocalizedString("Warning"),
                        DialogMessage =
                            $"{this.GetLocalizedString("FoundInvalidItemsToPredict")}{Environment.NewLine}{Environment.NewLine}{string.Join("\n",invalidIssuesKeys)}",
                        OkButtonMessage = "Ok",
                        InformationType = InformationType.Warning
                    });
                    this.DataCollector.NotValidKeys = invalidIssues.Select(i => i.Key).ToList();
                }
            }   
                
            }
        }
    }
    


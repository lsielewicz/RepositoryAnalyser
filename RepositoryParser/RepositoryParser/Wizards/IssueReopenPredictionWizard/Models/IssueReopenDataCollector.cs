﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Ioc;
using MahApps.Metro.Controls;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Interfaces;
using RepositoryAnalyser.Reporting.Interfaces;
using RepositoryAnalyser.Wizards.PredictionsCommonData;
using ZetaLongPaths;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Models
{
    public class IssueReopenDataCollector : PredictionDataCollector
    {
        private readonly IIssueReopenedPredictionResultPdfExporter _exporter;
        private readonly IIssueReopenedIntegratedPredictionResultPdfExporter _integratedExporter;

        private readonly IIssueReopenPredictor _predictor;

        public IssueReopenPredictionWizardViewModel ParentViewModel { get; set; }

        public IssueReopenDataCollector(MetroWindow owningScreen) : base(owningScreen)
        {
            _predictor = SimpleIoc.Default.GetInstance<IIssueReopenPredictor>();
            _exporter = SimpleIoc.Default.GetInstance<IIssueReopenedPredictionResultPdfExporter>();
            _integratedExporter = SimpleIoc.Default.GetInstance<IIssueReopenedIntegratedPredictionResultPdfExporter>();
        }

        public override async Task DoWork()
        {
            IList<Commit> commits = null;
            IList<Issue> issues = null;
            IList<CommitsSet> commitsSets = null;
            var resultDescription = string.Empty;
            var reports = new List<ReportKeyValue>();

            if (!this.TrainingOutputDataViewModel.IsTrained)
            {
                this.TrainingOutputDataViewModel.TrainCommand.Execute(null);
            }

            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                switch (IssuesDataSource)
                {
                    case DataSource.Database:
                        issues = this.GetIssuesFromDb(session);
                        break;

                    case DataSource.Network:
                        var issueRequest = await this.GetIssueFromNetwork();
                        if (issueRequest != null)
                        {
                            issues = new List<Issue>() { issueRequest.Issue };
                            commitsSets = issueRequest.CommitsSets;
                        }
                        
                        break;
                }

                if (issues == null)
                {
                    this.ResultInfo = new WizardResultInfo()
                    {
                        Result = WizardResult.Failed,
                        Description = "No issues to use"
                    };
                    return;
                }

                if (this.DataType == MlDataType.Integrated)
                {
                    switch (RepositoryDataSource)
                    {
                        case DataSource.Database:
                            var repositories = this.GetRepositoriesFromDb(session);
                            commits = CommitsFilteringHelper.GetAllCommitsOfRepositories(repositories);
                            break;
                        case DataSource.Network:
                            repositories = new List<Repository>() {this.GetRepositoryFromNetwork()};
                            break;
                    }

                    if (commits == null || !commits.Any())
                    {
                        this.ResultInfo = new WizardResultInfo()
                        {
                            Result = WizardResult.Failed,
                            Description = "No repositories to use"
                        };
                        return;
                    }


                    foreach (var issue in issues) //TODO: DO FORMING RESULT !!
                    {
                        var predictionResult = await _predictor.Predict(issue, commits, this.TrainingOutputDataViewModel.Entity, commitsSets);
                        predictionResult.ModelName = TrainingsManagementHelper.BugsReopenPrediction.GetModelNameFromFilePath(predictionResult.TrainingModel.FilePath);

                        var name = $"IssueReopenIntegratedPrediction_{Guid.NewGuid()}.pdf";
                        var path = ZlpPathHelper.Combine(LocalizationConstants.PrognosisReportsPath, name);
                        _integratedExporter.Export(predictionResult, path);

                        reports.Add(new ReportKeyValue()
                        {
                            Key = issue.Key,
                            Path = path,
                            IsValid = this.NotValidKeys == null || (this.NotValidKeys != null && !this.NotValidKeys.Contains(issue.Key))
                        });
                    }
                }
                else
                {
                    foreach (var issue in issues)  //TODO: DO FORMING RESULT !!
                    {
                        var predictionResult = await _predictor.Predict(issue, this.TrainingOutputDataViewModel.Entity);
                        predictionResult.ModelName = TrainingsManagementHelper.BugsReopenPrediction.GetModelNameFromFilePath(predictionResult.TrainingModel.FilePath);

                        var name = $"IssueReopenPrediction_{Guid.NewGuid()}.pdf";
                        var path = ZlpPathHelper.Combine(LocalizationConstants.PrognosisReportsPath, name);
                        _exporter.Export(predictionResult, path);

                        reports.Add(new ReportKeyValue()
                        {
                            Key = issue.Key,
                            Path = path,
                            IsValid = this.NotValidKeys == null || (this.NotValidKeys != null && !this.NotValidKeys.Contains(issue.Key))
                        });
                    }
                    
                }


                resultDescription = RepositoryAnalyser.Locale.Languages.Resources.PredictionsSuccessed;
                this.ResultInfo = new WizardResultInfo()
                {
                    Result = WizardResult.Successed,
                    Description = resultDescription,
                    ReportsPaths = reports
                };

            }
        }
    }
}
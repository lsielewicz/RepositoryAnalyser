﻿using System.Linq;
using MahApps.Metro.Controls;
using RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Models;
using RepositoryAnalyser.Wizards.IssueReopenPredictionWizard.Pages;
using RepositoryAnalyser.Wizards.PredictionsCommonData;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard
{
    public class IssueReopenPredictionWizardViewModel : WizardBase
    {
        public IssueReopenPredictionWizardViewModel(MetroWindow owningScrren)
        {
            var dataCollector = new IssueReopenDataCollector(owningScrren);

            Pages = new WizardPage[]
            {
                new IssueReopenPredictionTypeViewModel(dataCollector),
                new ReopenIssueTrackingDataPageViewModel(dataCollector), 
                new ReopenTrainingModelViewModel(dataCollector), 
                new ProgressViewModel(dataCollector),
                new SummaryViewModel(dataCollector),
            };

            this.CurrentPage = Pages.First();
        }
    }
}

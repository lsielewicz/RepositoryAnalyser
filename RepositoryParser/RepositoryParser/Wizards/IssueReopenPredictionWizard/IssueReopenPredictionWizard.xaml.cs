﻿using System.ComponentModel;

namespace RepositoryAnalyser.Wizards.IssueReopenPredictionWizard
{
    /// <summary>
    /// Interaction logic for IssueReopenPredictionWizard.xaml
    /// </summary>
    public partial class IssueReopenPredictionWizard
    {
        public WizardResult WizardResult { get; set; }

        public IssueReopenPredictionWizard()
        {
            this.DataContext = new IssueReopenPredictionWizardViewModel(this)
            {
                CloseHanlder = this.Close,
                Result = this.WizardResult
            };
            InitializeComponent();
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            var wizardResult = (this.DataContext as IssueReopenPredictionWizardViewModel)?.Result;
            if (wizardResult != null)
            {
                this.WizardResult = (WizardResult)wizardResult;
            }
        }
    }
}

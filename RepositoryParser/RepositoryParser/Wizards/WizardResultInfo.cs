﻿using System.Collections.Generic;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;

namespace RepositoryAnalyser.Wizards
{
    public class WizardResultInfo
    {
        public string Description { get; set; }
        public WizardResult Result { get; set; }
        public IList<ReportKeyValue> ReportsPaths { get; set; }

      
    }

    public class ReportKeyValue
    {
        public string Key { get; set; }
        public string Path { get; set; }

        public bool IsValid { get; set; }

        private RelayCommand _openReportCommand;

        public RelayCommand OpenReportCommand
        {
            get
            {
                return _openReportCommand ?? (_openReportCommand = new RelayCommand(() =>
                { 
                        Process.Start(Path);
                }));
            }
        }
    }

}

﻿using System.Text;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Locale.Languages;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.ViewModel.TrainingViewModels;

namespace RepositoryAnalyser.Helpers
{
    public static class SummaryHelper
    {
        public static string GetSummaryStringOfTrainingModel(TrainingOutputDataViewModel vm)
        {
            var rm = Resources.ResourceManager;
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine(rm.GetString("TrainingModelInfo"));
            sb.AppendLine(rm.GetString($"{rm.GetString("Name")}: {vm.Name}"));
            if (vm.IsBinaryClassificationMetricsVisibile)
            {
                sb.AppendLine(rm.GetString($"{rm.GetString("Accuracy")}: {vm.Accuracy}"));
                sb.AppendLine(rm.GetString($"{rm.GetString("Auc")}: {vm.Auc}"));
                sb.AppendLine(rm.GetString($"{rm.GetString("MatthewsCorrelationCoefficient")}: {vm.MatthewsCorrelationCoefficient}"));
                sb.AppendLine(rm.GetString($"{rm.GetString("F1Score")}: {vm.F1Score}"));
            }
            else
            {
                sb.AppendLine(rm.GetString($"L1: {vm.L1}"));
                sb.AppendLine(rm.GetString($"L2: {vm.L2}"));
                sb.AppendLine(rm.GetString($"{rm.GetString("RSquared")}: {vm.RSquared}"));
                sb.AppendLine(rm.GetString($"{rm.GetString("LossFn")}: {vm.LossFn}"));
                sb.AppendLine(rm.GetString($"{rm.GetString("Rms")}: {vm.Rms}"));
            }

            return sb.ToString();
        }

        public static string GetSummaryStringOfTrainingModel(TrainingOutputData tmData, PredictionType predictionType)
        {

            var vm = new TrainingOutputDataViewModel(tmData, predictionType);
            return GetSummaryStringOfTrainingModel(vm);
        }
    }
}

﻿namespace RepositoryAnalyser.Helpers.Enums
{
    public enum FilteringColumn
    {
        AuthorsColumn,
        DateColumn,
        MessageSearchingColumn
    }
}
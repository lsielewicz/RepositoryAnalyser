﻿using System;
using System.Collections.Generic;
using RepositoryAnalyser.Locale.Languages;
using RepositoryAnalyser.MachineLearning.Enums;
using ZetaLongPaths;

namespace RepositoryAnalyser.Helpers
{
    internal static class TrainingsManagementHelper
    {
        public static string TrainingsBaseDirectoryPath { get; private set; }
        static TrainingsManagementHelper()
        {
            TrainingsBaseDirectoryPath =
                ZlpPathHelper.Combine(LocalizationConstants.AppDataPath, LocalizationConstants.ProductName);
        }

        public static class TimeToFixPrediction
        {
            private const string DirectoryName = "TimeToFixPrediction";
            private const string IntegratedPredictionPrefix = "p_";
            private const string FastTreeRegressorTypeFileName = "ftr.model";
            private const string FastTreeTweedieRegressorTypeFileName = "fttr.model";
            private const string StochasticDualCoordinateAscentRegressorTypeFileName = "sdcar.model";
            private const string OrdinaryLeastSquaresRegressorTypeFileName = "olsr.model";
            private const string OnlineGradientDescentRegressorTypeFileName = "ogdr.model";
            private const string PoissonRegressorTypeFileName = "pr.model";
            private const string GeneralizedAdditiveModelRegressorTypeFileName = "gamr.model";

            private static readonly string _baseDirectory;
            static TimeToFixPrediction()
            {
                _baseDirectory = ZlpPathHelper.Combine(TrainingsBaseDirectoryPath, DirectoryName);
            }

            public static bool IsIntegrated(string modelPath)
            {
                var fileName = ZlpPathHelper.GetFileNameFromFilePath(modelPath);
                return fileName.StartsWith(IntegratedPredictionPrefix);
            }

            public static string GetFilePath(RegressionTool tool, bool isItegrated)
            {
                return ZlpPathHelper.Combine(_baseDirectory, GetFileName(tool, isItegrated));
            }

            public static string GetModelNameFromFilePath(string modelPath)
            {
                var fileName = ZlpPathHelper.GetFileNameFromFilePath(modelPath);

                var isIntegrated = fileName.StartsWith(IntegratedPredictionPrefix);
                if (isIntegrated)
                {
                    fileName = fileName.Remove(0, IntegratedPredictionPrefix.Length);
                }

                switch (fileName)
                {
                    case FastTreeRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("FastTreeRegressor");

                    case FastTreeTweedieRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("FastTreeTweedieRegressor");

                    case StochasticDualCoordinateAscentRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("StochasticDualCoordinateAscentRegressor");

                    case OrdinaryLeastSquaresRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("OrdinaryLeastSquaresRegressor");

                    case OnlineGradientDescentRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("OnlineGradientDescentRegressor");

                    case PoissonRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("PoissonRegressor");

                    case GeneralizedAdditiveModelRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("GeneralizedAdditiveModelRegressor");
                }

                return Resources.ResourceManager.GetString("Unknown");

            }

            public static RegressionTool GetModelTypeFromFilePath(string modelPath)
            {
                var fileName = ZlpPathHelper.GetFileNameFromFilePath(modelPath);

                var isIntegrated = fileName.StartsWith(IntegratedPredictionPrefix);
                if (isIntegrated)
                {
                    fileName = fileName.Remove(0, IntegratedPredictionPrefix.Length);
                }

                switch (fileName)
                {
                    case FastTreeRegressorTypeFileName:
                        return RegressionTool.FastTreeRegressor;

                    case FastTreeTweedieRegressorTypeFileName:
                        return RegressionTool.FastTreeTweedieRegressor;

                    case StochasticDualCoordinateAscentRegressorTypeFileName:
                        return RegressionTool.StochasticDualCoordinateAscentRegressor;

                    case OrdinaryLeastSquaresRegressorTypeFileName:
                        return RegressionTool.OrdinaryLeastSquaresRegressor;

                    case OnlineGradientDescentRegressorTypeFileName:
                        return RegressionTool.OnlineGradientDescentRegressor;

                    case PoissonRegressorTypeFileName:
                        return RegressionTool.PoissonRegressor;

                    case GeneralizedAdditiveModelRegressorTypeFileName:
                        return RegressionTool.GeneralizedAdditiveModelRegressor;
                }

                throw new ArgumentOutOfRangeException($"There is no known type of name {fileName}");
            }

            private static string GetFileName(RegressionTool tool, bool isIntegrated)
            {
                string fileName = string.Empty;
                switch (tool)
                {
                    case RegressionTool.FastTreeRegressor:
                        fileName = FastTreeRegressorTypeFileName;
                        break;

                    case RegressionTool.StochasticDualCoordinateAscentRegressor:
                        fileName = StochasticDualCoordinateAscentRegressorTypeFileName;
                        break;

                    case RegressionTool.FastTreeTweedieRegressor:
                        fileName = FastTreeTweedieRegressorTypeFileName;
                        break;

                    case RegressionTool.GeneralizedAdditiveModelRegressor:
                        fileName = GeneralizedAdditiveModelRegressorTypeFileName;
                        break;

                    case RegressionTool.OnlineGradientDescentRegressor:
                        fileName = OnlineGradientDescentRegressorTypeFileName;
                        break;

                    case RegressionTool.OrdinaryLeastSquaresRegressor:
                        fileName = OrdinaryLeastSquaresRegressorTypeFileName;
                        break;

                    case RegressionTool.PoissonRegressor:
                        fileName = PoissonRegressorTypeFileName;
                        break;
                }

                if (isIntegrated)
                {
                    fileName = $"{IntegratedPredictionPrefix}{fileName}";
                }

                return fileName;
            }

            public static IList<string> GetAllPaths()
            {
                var output = new List<string>()
                {
                    GetFilePath(RegressionTool.FastTreeRegressor, false),
                    GetFilePath(RegressionTool.GeneralizedAdditiveModelRegressor, false),
                    GetFilePath(RegressionTool.FastTreeTweedieRegressor, false),
                    GetFilePath(RegressionTool.OnlineGradientDescentRegressor, false),
                    GetFilePath(RegressionTool.OrdinaryLeastSquaresRegressor, false),
                    GetFilePath(RegressionTool.PoissonRegressor, false),
                    GetFilePath(RegressionTool.StochasticDualCoordinateAscentRegressor, false),

                    GetFilePath(RegressionTool.FastTreeRegressor, true),
                    GetFilePath(RegressionTool.GeneralizedAdditiveModelRegressor, true),
                    GetFilePath(RegressionTool.FastTreeTweedieRegressor, true),
                    GetFilePath(RegressionTool.OnlineGradientDescentRegressor, true),
                    GetFilePath(RegressionTool.OrdinaryLeastSquaresRegressor, true),
                    GetFilePath(RegressionTool.PoissonRegressor, true),
                    GetFilePath(RegressionTool.StochasticDualCoordinateAscentRegressor, true),
                };

                return output;
            }
        }

        public static class DevelopersEfficiencyPrediction
        {
            private const string DirectoryName = "DevelopersEfficiencyPrediction";
            private const string IntegratedPredictionPrefix = "p_";
            private const string FastTreeRegressorTypeFileName = "ftr.model";
            private const string FastTreeTweedieRegressorTypeFileName = "fttr.model";
            private const string StochasticDualCoordinateAscentRegressorTypeFileName = "sdcar.model";
            private const string OrdinaryLeastSquaresRegressorTypeFileName = "olsr.model";
            private const string OnlineGradientDescentRegressorTypeFileName = "ogdr.model";
            private const string PoissonRegressorTypeFileName = "pr.model";
            private const string GeneralizedAdditiveModelRegressorTypeFileName = "gamr.model";

            private static readonly string _baseDirectory;
            static DevelopersEfficiencyPrediction()
            {
                _baseDirectory = ZlpPathHelper.Combine(TrainingsBaseDirectoryPath, DirectoryName);
            }

            public static bool IsIntegrated(string modelPath)
            {
                var fileName = ZlpPathHelper.GetFileNameFromFilePath(modelPath);
                return fileName.StartsWith(IntegratedPredictionPrefix);
            }

            public static string GetFilePath(RegressionTool tool, bool isItegrated)
            {
                return ZlpPathHelper.Combine(_baseDirectory, GetFileName(tool, isItegrated));
            }

            public static string GetModelNameFromFilePath(string modelPath)
            {
                var fileName = ZlpPathHelper.GetFileNameFromFilePath(modelPath);

                var isIntegrated = fileName.StartsWith(IntegratedPredictionPrefix);
                if (isIntegrated)
                {
                    fileName = fileName.Remove(0, IntegratedPredictionPrefix.Length);
                }

                switch (fileName)
                {
                    case FastTreeRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("FastTreeRegressor");

                    case FastTreeTweedieRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("FastTreeTweedieRegressor");

                    case StochasticDualCoordinateAscentRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("StochasticDualCoordinateAscentRegressor");

                    case OrdinaryLeastSquaresRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("OrdinaryLeastSquaresRegressor");

                    case OnlineGradientDescentRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("OnlineGradientDescentRegressor");

                    case PoissonRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("PoissonRegressor");

                    case GeneralizedAdditiveModelRegressorTypeFileName:
                        return Resources.ResourceManager.GetString("GeneralizedAdditiveModelRegressor");
                }

                return Resources.ResourceManager.GetString("Unknown");

            }

            public static RegressionTool GetModelTypeFromFilePath(string modelPath)
            {
                var fileName = ZlpPathHelper.GetFileNameFromFilePath(modelPath);

                var isIntegrated = fileName.StartsWith(IntegratedPredictionPrefix);
                if (isIntegrated)
                {
                    fileName = fileName.Remove(0, IntegratedPredictionPrefix.Length);
                }

                switch (fileName)
                {
                    case FastTreeRegressorTypeFileName:
                        return RegressionTool.FastTreeRegressor;

                    case FastTreeTweedieRegressorTypeFileName:
                        return RegressionTool.FastTreeTweedieRegressor;

                    case StochasticDualCoordinateAscentRegressorTypeFileName:
                        return RegressionTool.StochasticDualCoordinateAscentRegressor;

                    case OrdinaryLeastSquaresRegressorTypeFileName:
                        return RegressionTool.OrdinaryLeastSquaresRegressor;

                    case OnlineGradientDescentRegressorTypeFileName:
                        return RegressionTool.OnlineGradientDescentRegressor;

                    case PoissonRegressorTypeFileName:
                        return RegressionTool.PoissonRegressor;

                    case GeneralizedAdditiveModelRegressorTypeFileName:
                        return RegressionTool.GeneralizedAdditiveModelRegressor;
                }

                throw new ArgumentOutOfRangeException($"There is no known type of name {fileName}");
            }

            private static string GetFileName(RegressionTool tool, bool isIntegrated)
            {
                string fileName = string.Empty;
                switch (tool)
                {
                    case RegressionTool.FastTreeRegressor:
                        fileName = FastTreeRegressorTypeFileName;
                        break;

                    case RegressionTool.StochasticDualCoordinateAscentRegressor:
                        fileName = StochasticDualCoordinateAscentRegressorTypeFileName;
                        break;

                    case RegressionTool.FastTreeTweedieRegressor:
                        fileName = FastTreeTweedieRegressorTypeFileName;
                        break;

                    case RegressionTool.GeneralizedAdditiveModelRegressor:
                        fileName = GeneralizedAdditiveModelRegressorTypeFileName;
                        break;

                    case RegressionTool.OnlineGradientDescentRegressor:
                        fileName = OnlineGradientDescentRegressorTypeFileName;
                        break;

                    case RegressionTool.OrdinaryLeastSquaresRegressor:
                        fileName = OrdinaryLeastSquaresRegressorTypeFileName;
                        break;

                    case RegressionTool.PoissonRegressor:
                        fileName = PoissonRegressorTypeFileName;
                        break;
                }

                if (isIntegrated)
                {
                    fileName = $"{IntegratedPredictionPrefix}{fileName}";
                }

                return fileName;
            }

            public static IList<string> GetAllPaths()
            {
                var output = new List<string>()
                {
                    GetFilePath(RegressionTool.FastTreeRegressor, false),
                    GetFilePath(RegressionTool.GeneralizedAdditiveModelRegressor, false),
                    GetFilePath(RegressionTool.FastTreeTweedieRegressor, false),
                    GetFilePath(RegressionTool.OnlineGradientDescentRegressor, false),
                    GetFilePath(RegressionTool.OrdinaryLeastSquaresRegressor, false),
                    GetFilePath(RegressionTool.PoissonRegressor, false),
                    GetFilePath(RegressionTool.StochasticDualCoordinateAscentRegressor, false),

                    GetFilePath(RegressionTool.FastTreeRegressor, true),
                    GetFilePath(RegressionTool.GeneralizedAdditiveModelRegressor, true),
                    GetFilePath(RegressionTool.FastTreeTweedieRegressor, true),
                    GetFilePath(RegressionTool.OnlineGradientDescentRegressor, true),
                    GetFilePath(RegressionTool.OrdinaryLeastSquaresRegressor, true),
                    GetFilePath(RegressionTool.PoissonRegressor, true),
                    GetFilePath(RegressionTool.StochasticDualCoordinateAscentRegressor, true),
                };

                return output;
            }
        }

        public static class BugsReopenPrediction
        {
            private const string DirectoryName = "BugsReopenPrediction";
            private const string IntegratedPredictionPrefix = "p_";
            private const string FastTreeBinaryClassifierTypeFileName = "ftb.model";
            private const string StochasticDualCoordinateAscentBinaryClassifierTypeFileName = "sdcabc.model";
            private const string AveragedPerceptronBinaryClassifierTypeFileName = "apbc.model";
            private const string LightGbmBinaryClassifierTypeFileName = "blr.model";
            private const string FastForestBinaryClassifierTypeFileName = "ffbc.model";
            private const string LinearSvmBinaryClassifierTypeFileName = "lsbc.model";
            private const string GeneralizedAdditiveModelBinaryClassifierTypeFileName = "gambc.model";

            private static readonly string _baseDirectory;

            static BugsReopenPrediction()
            {
                _baseDirectory = ZlpPathHelper.Combine(TrainingsBaseDirectoryPath, DirectoryName);
            }

            public static IList<string> GetAllPaths()
            {
                var output = new List<string>()
                {
                    GetFilePath(BinaryClassificationTool.FastTreeBinaryClassifier, false),
                    GetFilePath(BinaryClassificationTool.AveragedPerceptronBinaryClassifier, false),
                    GetFilePath(BinaryClassificationTool.FastForestBinaryClassifier, false),
                    GetFilePath(BinaryClassificationTool.LinearSvmBinaryClassifier, false),
                    GetFilePath(BinaryClassificationTool.StochasticDualCoordinateAscentBinaryClassifier, false),
                    GetFilePath(BinaryClassificationTool.GeneralizedAdditiveModelBinaryClassifier, false),

                    GetFilePath(BinaryClassificationTool.FastTreeBinaryClassifier, true),
                    GetFilePath(BinaryClassificationTool.AveragedPerceptronBinaryClassifier, true),
                    GetFilePath(BinaryClassificationTool.FastForestBinaryClassifier, true),
                    GetFilePath(BinaryClassificationTool.LinearSvmBinaryClassifier, true),
                    GetFilePath(BinaryClassificationTool.StochasticDualCoordinateAscentBinaryClassifier, true),
                    GetFilePath(BinaryClassificationTool.GeneralizedAdditiveModelBinaryClassifier, true),
                };

                return output;
            }

            public static string GetFilePath(BinaryClassificationTool tool, bool isItegrated)
            {
                return ZlpPathHelper.Combine(_baseDirectory, GetFileName(tool,isItegrated));
            }

            public static bool IsIntegrated(string modelPath)
            {
                var fileName = ZlpPathHelper.GetFileNameFromFilePath(modelPath);
                return fileName.StartsWith(IntegratedPredictionPrefix);
            }
            
            public static BinaryClassificationTool GetModelTypeFromFilePath(string modelPath)
            {
                var fileName = ZlpPathHelper.GetFileNameFromFilePath(modelPath);

                var isIntegrated = fileName.StartsWith(IntegratedPredictionPrefix);
                if (isIntegrated)
                {
                    fileName = fileName.Remove(0, IntegratedPredictionPrefix.Length);
                }

                switch (fileName)
                {
                    case FastTreeBinaryClassifierTypeFileName:
                        return BinaryClassificationTool.FastTreeBinaryClassifier;

                    case StochasticDualCoordinateAscentBinaryClassifierTypeFileName:
                        return BinaryClassificationTool.StochasticDualCoordinateAscentBinaryClassifier;

                    case AveragedPerceptronBinaryClassifierTypeFileName:
                        return BinaryClassificationTool.AveragedPerceptronBinaryClassifier;

                    case FastForestBinaryClassifierTypeFileName:
                        return BinaryClassificationTool.FastForestBinaryClassifier;

                    case LinearSvmBinaryClassifierTypeFileName:
                        return BinaryClassificationTool.LinearSvmBinaryClassifier;

                    case GeneralizedAdditiveModelBinaryClassifierTypeFileName:
                        return BinaryClassificationTool.GeneralizedAdditiveModelBinaryClassifier;
                }

                throw new ArgumentOutOfRangeException($"There is no known type of name {fileName}");
            }

            public static string GetModelNameFromFilePath(string modelPath)
            {
                var fileName = ZlpPathHelper.GetFileNameFromFilePath(modelPath);

                var isIntegrated = fileName.StartsWith(IntegratedPredictionPrefix);
                if (isIntegrated)
                {
                    fileName = fileName.Remove(0, IntegratedPredictionPrefix.Length);
                }

                switch (fileName)
                {
                    case FastTreeBinaryClassifierTypeFileName:
                        return Resources.ResourceManager.GetString("FastTreeBinaryClassifier");

                    case StochasticDualCoordinateAscentBinaryClassifierTypeFileName:
                        return Resources.ResourceManager.GetString("StochasticDualCoordinateAscentBinaryClassifier");

                    case AveragedPerceptronBinaryClassifierTypeFileName:
                        return Resources.ResourceManager.GetString("AveragedPerceptronBinaryClassifier");

                    case LightGbmBinaryClassifierTypeFileName:
                        return Resources.ResourceManager.GetString("BinaryLogisticRegressor");

                    case FastForestBinaryClassifierTypeFileName:
                        return Resources.ResourceManager.GetString("FastForestBinaryClassifier");

                    case LinearSvmBinaryClassifierTypeFileName:
                        return Resources.ResourceManager.GetString("LinearSvmBinaryClassifier");

                    case GeneralizedAdditiveModelBinaryClassifierTypeFileName:
                        return Resources.ResourceManager.GetString("GeneralizedAdditiveModelBinaryClassifier");
                }

                return Resources.ResourceManager.GetString("Unknown");

            }

            private static string GetFileName(BinaryClassificationTool tool, bool isIntegrated)
            {
                string fileName = string.Empty;
                switch (tool)
                {
                    case BinaryClassificationTool.FastTreeBinaryClassifier:
                        fileName = FastTreeBinaryClassifierTypeFileName;
                        break;

                    case BinaryClassificationTool.StochasticDualCoordinateAscentBinaryClassifier:
                        fileName = StochasticDualCoordinateAscentBinaryClassifierTypeFileName;
                        break;

                    case BinaryClassificationTool.AveragedPerceptronBinaryClassifier:
                        fileName = AveragedPerceptronBinaryClassifierTypeFileName;
                        break;

                    case BinaryClassificationTool.FastForestBinaryClassifier:
                        fileName = FastForestBinaryClassifierTypeFileName;
                        break;

                    case BinaryClassificationTool.LinearSvmBinaryClassifier:
                        fileName = LinearSvmBinaryClassifierTypeFileName;
                        break;

                    case BinaryClassificationTool.GeneralizedAdditiveModelBinaryClassifier:
                        fileName = GeneralizedAdditiveModelBinaryClassifierTypeFileName;
                        break;
                }

                if (isIntegrated)
                {
                    fileName = $"{IntegratedPredictionPrefix}{fileName}";
                }

                return fileName;
            }
        }
    }
}

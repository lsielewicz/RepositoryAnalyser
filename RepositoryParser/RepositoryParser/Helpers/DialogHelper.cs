﻿using System.Threading.Tasks;
using MahApps.Metro.Controls.Dialogs;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings.InformationDialog;

namespace RepositoryAnalyser.Helpers
{
    public class DialogHelper
    {
        private static DialogHelper _instance;

        public static DialogHelper Instance
        {
            get { return _instance ?? (_instance = new DialogHelper()); }
        }

        private DialogHelper()
        {
        }


        public async Task ShowDialog(CustomDialogEntryData data)
        {
            
            await DialogCoordinator.Instance.ShowMetroDialogAsync(data.MetroWindow, new InformationDialog(data));
        }
    }
}

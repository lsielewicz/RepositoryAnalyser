﻿using System;
using RepositoryAnalyser.Configuration;
using ZetaLongPaths;

namespace RepositoryAnalyser.Helpers
{
    public class LocalizationConstants
    {
        public static string TrainingReportsPath
        {
            get
            {
                var path = ZetaLongPaths.ZlpPathHelper.Combine(
                    ConfigurationService.Instance.Configuration.ReportsSavingLocation, "Models");
                if (!ZlpIOHelper.DirectoryExists(path))
                {
                    ZlpIOHelper.CreateDirectory(path);
                }
                return path;
            }
        }

        public static string PrognosisReportsPath
        {
            get
            {
                var path = ZetaLongPaths.ZlpPathHelper.Combine(
                    ConfigurationService.Instance.Configuration.ReportsSavingLocation, "Predictions");
                if (!ZlpIOHelper.DirectoryExists(path))
                {
                    ZlpIOHelper.CreateDirectory(path);
                }

                return path;
            }
        }

        public static string ProductName
        {
            get { return "RepositoryAnalyser"; }
        }
        public static string AppDataPath
        {
            get
            {
                return Environment.ExpandEnvironmentVariables("%AppData%");
            }
        }

        public static string ProgramDataApplicationDirectory
        {
            get { return ZetaLongPaths.ZlpPathHelper.Combine(AppDataPath, ProductName); }
        }

        public static string ConfigFilePath
        {
            get { return ZetaLongPaths.ZlpPathHelper.Combine(AppDataPath, ProductName, "RepositoryAnalyser.config"); }
        }

        public static string TrainingModelInfoFilePath
        {
            get { return ZetaLongPaths.ZlpPathHelper.Combine(AppDataPath, ProductName, "TrainingModelInfo.xml"); }
        }

        public static string DefaultRepositorySavingPath
        {
            get
            {
                return
                    ZetaLongPaths.ZlpPathHelper.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), ProductName, "Repositories");
            }
        }

        public static string DefaultReportsSavingPath
        {
            get
            {
                return
                    ZetaLongPaths.ZlpPathHelper.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), ProductName, "Reports");
            }
        }
    }
}

﻿using System.Windows;
using RepositoryAnalyser.Helpers;

namespace RepositoryAnalyser
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            SplashScreenHelper.StartApplicationWithSplashScreen(this);
        }
    }
}

﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls.Dialogs;
using NHibernate.Criterion;
using RepositoryAnalyser.CommonUI;
using RepositoryAnalyser.CommonUI.BaseViewModels;
using RepositoryAnalyser.Configuration;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings.InformationDialog;
using RepositoryAnalyser.DataBase.Interfaces;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Domain.Models;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.IssueTracking.Interfaces;
using RepositoryAnalyser.Messages;
using RepositoryAnalyser.VersionControl.Interfaces;
using RepositoryAnalyser.VersionControl.Models;

namespace RepositoryAnalyser.ViewModel
{
    public class DataBaseManagementViewModel : RepositoryAnalyserViewModelBase
    {
        #region private variables

        private const string JiraKeyword = "atlassian";
        private const string GithubKeyword = "github";
        private const string GitKeyword = "git";

        private IVersionControlSystemParser _versionControlSystemParser;
        private readonly IJiraIssuesParser _jiraIssuesParser;
        private readonly IGithubIssuesParser _githubIssuesParser;
        private readonly IEntityPersister _entityPersister;

        private string _urlTextBox;
        private string _issueTrackerUrl;
        private bool _isOpening;
        private bool _isGitRepositoryPicked;
        private IssueTrackingType _issueTrackingType;
        private readonly BackgroundWorker _clearDbWorker;
        private RelayCommand _startWorkCommand;
        private RelayCommand _asyncClearDbCommand;
        private RelayCommand _pickFileCommand;
        private RelayCommand _pickGitRepositoryCommand;
        private RelayCommand _pickSvnRepositoryCommand;
        private RelayCommand _parseIssueTrackingSystemCommand;
        private RelayCommand<IssueTrackingType> _setIssueTrackingTypeCommand;
        private RelayCommand _addIntergrationCommand;
        private RelayCommand _deleteIntegrationCommand;
        private RelayCommand _refreshIntegrationsCommand;

        public bool IsIntegrationEnabled
        {
            get
            {
                using (var session = DbService.Instance.SessionFactory.OpenSession())
                {
                    var countOfProjects = session.QueryOver<IssueTrackingProject>().Select(Projections.RowCount())
                        .FutureValue<int>()
                        .Value;
                    var countOfRepositories  = session.QueryOver<Repository>().Select(Projections.RowCount())
                        .FutureValue<int>()
                        .Value;

                    return countOfRepositories * countOfProjects > 0;
                }
            } 
        }
        #endregion

        private readonly IDialogCoordinator _dialogCoordinator;
/*        private readonly DialogView _dialogView = new DialogView();*/
        public DataBaseManagementViewModel(IDialogCoordinator dialogCoordinator, IEntityPersister entityPersister, IJiraIssuesParser jiraIssuesParser, IGithubIssuesParser githubIssuesParser)
        {
            _dialogCoordinator = dialogCoordinator;
            _entityPersister = entityPersister;
            _githubIssuesParser = githubIssuesParser;
            _jiraIssuesParser = jiraIssuesParser;

            this._clearDbWorker = new BackgroundWorker();
            this._clearDbWorker.DoWork += this.DoClearWork;
            this._clearDbWorker.RunWorkerCompleted += this.DoClearWorkCompleted;
        }

        public bool IsIssueTrackingParsingEnabled
        {
            get { return !string.IsNullOrEmpty(this.IssueTrackerUrl); }
        }

        #region Getters/Setters
        public string IssueTrackerUrl
        {
            get
            {
                return _issueTrackerUrl;
            }
            set
            {
                if (_issueTrackerUrl == value)
                {
                    return;
                }
                
                _issueTrackerUrl = value;

                if (_issueTrackerUrl.IndexOf(GithubKeyword, StringComparison.OrdinalIgnoreCase) > 0)
                {
                    this.SetIssueTrackingTypeCommand.Execute(IssueTrackingType.Github);
                }
                else if (_issueTrackerUrl.IndexOf(JiraKeyword, StringComparison.OrdinalIgnoreCase) > 0)
                {
                    this.SetIssueTrackingTypeCommand.Execute(IssueTrackingType.Jira);
                }

                RaisePropertyChanged();
                RaisePropertyChanged("IsIssueTrackingParsingEnabled");
            }
        }

        public IssueTrackingType IssueTrackingType
        {
            get
            {
                return _issueTrackingType;
            }
            set
            {
                if (_issueTrackingType == value)
                {
                    return;
                }

                _issueTrackingType = value;
                this.RaisePropertyChanged();
            }
        }

        public bool IsGitRepositoryPicked
        {
            get { return _isGitRepositoryPicked; }
            set
            {
                if (_isGitRepositoryPicked != value)
                {
                    _isGitRepositoryPicked = value;
                    RaisePropertyChanged("IsGitRepositoryPicked");
                }
            }
        }

        public bool IsOpening
        {
            get
            {
                return _isOpening;
            }
            set
            {
                if (_isOpening != value)
                {
                    _isOpening = value;
                    RaisePropertyChanged("IsOpening");
                }
            }
        }

        public string UrlTextBox
        {
            get
            {
                return _urlTextBox;
            }
            set
            {
                if (_urlTextBox != value)
                {
                    _urlTextBox = value;
                    if (_urlTextBox.IndexOf(GitKeyword, StringComparison.OrdinalIgnoreCase) > 0)
                    {

                        this.IsGitRepositoryPicked = true;
                    }
                    RaisePropertyChanged("UrlTextBox");
                    RaisePropertyChanged("IsCloneButtonEnabled");
                }
            }
        }
        public bool IsCloneButtonEnabled
        {
            get { return !string.IsNullOrEmpty(this.UrlTextBox); }

        }
        #endregion

        #region Buttons getters

        public RelayCommand<IssueTrackingType> SetIssueTrackingTypeCommand
        {
            get
            {
                return _setIssueTrackingTypeCommand ?? (_setIssueTrackingTypeCommand =
                           new RelayCommand<IssueTrackingType>(
                               param =>
                               {
                                   this.IssueTrackingType = param;
                               }));
            }
        }

        public RelayCommand ParseIssueTrackingSystemCommand
        {
            get
            {
                return _parseIssueTrackingSystemCommand ?? (_parseIssueTrackingSystemCommand = new RelayCommand(
                           async () =>
                           {
                               this.IsLoading = true;

                               try
                               {
                                   var username = string.Empty;
                                   var password = string.Empty;

                                   var loginResult =
                                       await
                                           _dialogCoordinator.ShowLoginAsync(ViewModelLocator.Instance.Main,
                                               this.GetLocalizedString("LoginInformation"),
                                               this.GetLocalizedString("EnterCredentials"));

                                   if (loginResult != null)
                                   {
                                       username = loginResult.Username;
                                       password = loginResult.Password;
                                   }

                                   IssueTrackingSystem system = null;
                                   var connectionData = new ConnectionData()
                                   {
                                       Url = this.IssueTrackerUrl,
                                       Credentials = new Credentials()
                                       {
                                           Username = username,
                                           Password = password
                                       }
                                   };

                                   await Task.Run(async () =>
                                   {
                                       switch (this.IssueTrackingType)
                                       {
                                           case IssueTrackingType.Jira:
                                               system = await _jiraIssuesParser.Parse(connectionData);
                                               break;
                                           case IssueTrackingType.Github:
                                               system = await _githubIssuesParser.Parse(connectionData);
                                               break;
                                           case IssueTrackingType.Bugzilla:
                                               throw new NotImplementedException();
                                       }

                                       _entityPersister.Persist(system);
                                   });
                                   
                                   this.IssueTrackerUrl = string.Empty;
                               }
                               catch (Exception ex)
                               {
                                   await this.ShowErrorDialog(ex);
                               }
                               finally
                               {
                                   this.IsLoading = false;
                               }
                              
                               this.RaisePropertyChanged(nameof(IsIntegrationEnabled));
                           }));
            }
        }

        public RelayCommand PickGitRepositoryCommand
        {
            get
            {
                return _pickGitRepositoryCommand ?? (_pickGitRepositoryCommand = new RelayCommand(PickGitRepository));
            }
        }
        public RelayCommand PickSvnRepositoryCommand
        {
            get
            {
                return _pickSvnRepositoryCommand ?? (_pickSvnRepositoryCommand = new RelayCommand(PickSvnRepository));
            }
        }

        public RelayCommand PickFileCommand
        {
            get
            {
                return _pickFileCommand ?? (_pickFileCommand = new RelayCommand(PickFile));
            }
        }

        public RelayCommand StartWorkCommand
        {
            get
            {
                return _startWorkCommand ??
                       (_startWorkCommand = new RelayCommand(this.OpenRepository));
            }
        }


        public RelayCommand AsyncClearDbCommand
        {
            get
            {
                return _asyncClearDbCommand ??
                       (_asyncClearDbCommand =
                           new RelayCommand(_clearDbWorker.RunWorkerAsync, () => !_clearDbWorker.IsBusy));
            }
        }
        #endregion

        #region Buttons actions

        private bool IsLocal()
        {
            const string pattern = @"https?.*";
            var rgx = new Regex(pattern);
            var m = rgx.Match(this.UrlTextBox);

            return !m.Success;
        }

        private void PickGitRepository()
        {
            IsGitRepositoryPicked = true;
        }

        private void PickSvnRepository()
        {
            IsGitRepositoryPicked = false;
        }


        public void PickFile()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory;
            fbd.Description = this.GetLocalizedString("PickFolderWithRepo");

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                UrlTextBox = fbd.SelectedPath;
            }
        }

        public async void OpenRepository()
        {
            if (!string.IsNullOrEmpty(UrlTextBox))
            {
                var isLocal = IsLocal();
                try
                {
                    IsLoading = true;
                    Repository repository = null;
                    if (IsGitRepositoryPicked)
                    {
                        _versionControlSystemParser = ServiceLocator.Current.GetInstance<IGitSystemParser>();
                        var cloningService = ServiceLocator.Current.GetInstance<IGitCloneService>();

                        var currentRepositoryType = RepositoryCloneType.Public;
                        string username = string.Empty, password = string.Empty;

                        if (!isLocal && cloningService.CheckRepositoryCloneType(UrlTextBox) == RepositoryCloneType.Private)
                        {
                            currentRepositoryType = RepositoryCloneType.Private;
                             var loginResult =
                                 await
                                     _dialogCoordinator.ShowLoginAsync(ViewModelLocator.Instance.Main, 
                                     this.GetLocalizedString("LoginInformation"),
                                     this.GetLocalizedString("EnterCredentials"));

                            if (loginResult != null)
                            {
                                username = loginResult.Username;
                                password = loginResult.Password;
                            }
                        }

                        await Task.Run(() =>
                        {
                            this.IsOpening = true;
                            if (isLocal)
                            {
                                repository = ((IGitSystemParser)_versionControlSystemParser).Parse(this.UrlTextBox);
                            }
                            else
                            {
                                var cloningOptions = new GitCloningData()
                                {
                                    CloneType = currentRepositoryType,
                                    Credentials = new Credentials()
                                    {
                                        Password = password,
                                        Username = username
                                    },
                                    Url = UrlTextBox,
                                    IncludeAllBranches = ConfigurationService.Instance.Configuration.CloneAllBranches,
                                    TargetPath = ConfigurationService.Instance.Configuration.SavingRepositoryPath
                                };

                                repository = ((IGitSystemParser)_versionControlSystemParser).Parse(cloningOptions);
                            }

                        });
                    }
                    else
                    {
                        await Task.Run(() =>
                        {
                            _versionControlSystemParser = ServiceLocator.Current.GetInstance<ISvnSystemParser>();
                            repository = _versionControlSystemParser.Parse(this.UrlTextBox);
                        });
                    }

                    _entityPersister.Persist(repository);

                    this.UrlTextBox = string.Empty;
                    Messenger.Default.Send<RefreshMessageToPresentation>(new RefreshMessageToPresentation(true));
                    ViewModelLocator.Instance.Filtering.ResetInitialization();
                    Messenger.Default.Send<RefreshMessageToFiltering>(new RefreshMessageToFiltering(true));
                    ViewModelLocator.Instance.Main.OnLoad();
                }
                catch (Exception ex)
                {
                    await DialogHelper.Instance.ShowDialog(new CustomDialogEntryData()
                    {
                        MetroWindow = StaticServiceProvider.MetroWindowInstance,
                        DialogTitle = this.GetLocalizedString("Error"),
                        DialogMessage = ex.Message,
                        OkButtonMessage = "Ok",
                        InformationType = InformationType.Error
                    });
                }
                finally
                {
                    this.IsLoading = false;
                }
                this.RaisePropertyChanged(nameof(IsIntegrationEnabled));
            }
        }

        private void ClearDataBase()
        {
            DbService.Instance.CreateDataBase();
            Messenger.Default.Send<RefreshMessageToPresentation>(new RefreshMessageToPresentation(true));
        }
        #endregion

        #region BackgroundWorker
        private void DoClearWork(object sender, DoWorkEventArgs e)
        {
            IsOpening = false;
            IsLoading = true;
            ClearDataBase();
        }

        private async void DoClearWorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                await DialogHelper.Instance.ShowDialog(new CustomDialogEntryData()
                {
                    MetroWindow = StaticServiceProvider.MetroWindowInstance,
                    DialogTitle = this.GetLocalizedString("Error"),
                    DialogMessage = e.Error.Message,
                    OkButtonMessage = "Ok",
                    InformationType = InformationType.Error
                });
            }
            else
            {
                Messenger.Default.Send<RefreshMessageToPresentation>(new RefreshMessageToPresentation(true));
                Messenger.Default.Send<RefreshMessageToFiltering>(new RefreshMessageToFiltering(true));
                IsLoading = false;
                ViewModelLocator.Instance.Main.OnLoad();
            }
            this.RaisePropertyChanged(nameof(IsIntegrationEnabled));
        }
        #endregion
    }
}

﻿using System.Diagnostics;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Command;
using RepositoryAnalyser.CommonUI.BaseViewModels;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Helpers;
using ZetaLongPaths;

namespace RepositoryAnalyser.ViewModel.TrainingViewModels
{
    public class ModelTrainingViewModel : RepositoryAnalyserViewModelBase
    {

        private RelayCommand<PredictionType> _openTrainingViewCommand;
        private RelayCommand _openReportsDirectory;
        public RelayCommand OpenReportsDirectory
        {
            get { return _openReportsDirectory ?? (_openReportsDirectory = new RelayCommand(() =>
            {
                var trainingsReportsDirectoryPath = LocalizationConstants.TrainingReportsPath;
                if (!ZlpIOHelper.DirectoryExists(trainingsReportsDirectoryPath))
                {
                    ZlpIOHelper.CreateDirectory(trainingsReportsDirectoryPath);
                }

                Process.Start(trainingsReportsDirectoryPath);
            })); }
        }

        public RelayCommand<PredictionType> OpenTrainingViewCommand
        {
            get
            {
                return _openTrainingViewCommand ?? (_openTrainingViewCommand = new RelayCommand<PredictionType>(
                           (param) =>
                           {
                               switch (param)
                               {
                                   case PredictionType.WhichBugGetReopened:
                                       this.NavigateTo(ServiceLocator.Current.GetInstance<BugsReopenPredictorTrainingViewModel>());
                                       break;

                                   case PredictionType.DevelopersEfficiency:
                                       this.NavigateTo(ServiceLocator.Current.GetInstance<DevEfficiencyPredictorTrainingViewModel>());
                                       break;
                                   case PredictionType.TimeToFix:
                                       this.NavigateTo(ServiceLocator.Current.GetInstance<TimeToFixPredictorTrainingViewModel>());
                                       break;
                               }
                           }));
            }
        }
    }
}

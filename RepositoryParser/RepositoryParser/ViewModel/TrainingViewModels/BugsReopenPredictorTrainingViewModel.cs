﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using RepositoryAnalyser.CommonUI.BaseViewModels;
using RepositoryAnalyser.Configuration.ModelTraining;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.ViewModel.TrainingViewModels
{
    public class BugsReopenPredictorTrainingViewModel : RepositoryAnalyserViewModelBase
    {
        public BugsReopenPredictorTrainingViewModel()
        {
            this.InitializeTrainingModels();
        }
        public override void OnLoad()
        {
            base.OnLoad();
            
        }

        public ObservableCollection<TrainingOutputDataViewModel> TrainingModels { get; private set; }
        public ObservableCollection<TrainingOutputDataViewModel> IntegratedTrainingModels { get; private set; }

        private void InitializeTrainingModels()
        {
            this.TrainingModels = new ObservableCollection<TrainingOutputDataViewModel>();
            this.IntegratedTrainingModels = new ObservableCollection<TrainingOutputDataViewModel>();

            var trainedModels = ModelTrainingInfoPersistingService.Instance.ModelTrainingInfo.ReopenedBuggsInfo;
            foreach (var model in trainedModels)
            {
                if (!TrainingsManagementHelper.BugsReopenPrediction.IsIntegrated(model.FilePath))
                {
                    TrainingModels.Add(new TrainingOutputDataViewModel(model, PredictionType.WhichBugGetReopened));
                }
                else
                {
                    IntegratedTrainingModels.Add(new TrainingOutputDataViewModel(model, PredictionType.WhichBugGetReopened));
                }

            }

            var allModelsPaths = TrainingsManagementHelper.BugsReopenPrediction.GetAllPaths();
            foreach (var path in allModelsPaths.Where(p => trainedModels.All(tm => !tm.FilePath.Equals(p, StringComparison.OrdinalIgnoreCase))))
            {
                var notTrainedModel = new TrainingOutputData()
                {
                    DateTime = DateTime.Now,
                    FilePath = path,
                    Metrics = new ModelMetrics()
                    {
                        ClassificationMetrics = new AverageClassificationMetrics()
                        {
                            Accuracy = 0.0,
                            F1Score = 0.0,
                            Auc = 0.0,
                            MatthewsCorrelationCoefficient = 0.0
                        }
                    }
                };
                if (!TrainingsManagementHelper.BugsReopenPrediction.IsIntegrated(notTrainedModel.FilePath))
                {
                    TrainingModels.Add(new TrainingOutputDataViewModel(notTrainedModel, PredictionType.WhichBugGetReopened));
                }
                else
                {
                    IntegratedTrainingModels.Add(new TrainingOutputDataViewModel(notTrainedModel, PredictionType.WhichBugGetReopened));
                }

                
            }
        }
    }
}

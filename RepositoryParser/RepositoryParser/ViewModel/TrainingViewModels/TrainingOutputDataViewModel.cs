﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using NHibernate;
using RepositoryAnalyser.CommonUI.BaseViewModels;
using RepositoryAnalyser.Configuration;
using RepositoryAnalyser.Configuration.ModelTraining;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.MachineLearning.Interfaces;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.Reporting.Interfaces;
using ZetaLongPaths;

namespace RepositoryAnalyser.ViewModel.TrainingViewModels
{
    public class TrainingOutputDataViewModel : RepositoryAnalyserViewModelBase
    {
        public TrainingOutputData Entity{ get; private set; }
        private readonly PredictionType _predictionType;
        private ITrainingOutputDataPdfExporter _exporter;
        public TrainingOutputDataViewModel(TrainingOutputData entity, PredictionType predictionType)
        {
            _predictionType = predictionType;
            _exporter = CommonServiceLocator.ServiceLocator.Current.GetInstance<ITrainingOutputDataPdfExporter>();
            Entity = entity;
        }

        public bool IsTrained
        {
            get { return ZlpIOHelper.FileExists(this.Entity.FilePath); }
        }

        public string Name
        {
            get
            {
                switch (_predictionType)
                {
                    case PredictionType.WhichBugGetReopened:
                        return TrainingsManagementHelper.BugsReopenPrediction.GetModelNameFromFilePath(Entity.FilePath);

                    case PredictionType.DevelopersEfficiency:
                        return TrainingsManagementHelper.DevelopersEfficiencyPrediction.GetModelNameFromFilePath(Entity.FilePath);
                    case PredictionType.TimeToFix:
                        return TrainingsManagementHelper.TimeToFixPrediction.GetModelNameFromFilePath(Entity.FilePath);
                    default:
                        return string.Empty;
                }
            }
        }

        public double Accuracy
        {
            get { return this.IsTrained && Entity.Metrics.ClassificationMetrics != null ? Entity.Metrics.ClassificationMetrics.Accuracy : 0.0; }
        }

        public double MatthewsCorrelationCoefficient
        {
            get { return this.IsTrained && Entity.Metrics.ClassificationMetrics != null ? Entity.Metrics.ClassificationMetrics.MatthewsCorrelationCoefficient : 0.0; }
        }

        public double Auc
        {
            get { return this.IsTrained && Entity.Metrics.ClassificationMetrics != null ? Entity.Metrics.ClassificationMetrics.Auc : 0.0; ; }
        }

        public double F1Score
        {
            get { return this.IsTrained && Entity.Metrics.ClassificationMetrics != null ? Entity.Metrics.ClassificationMetrics.Accuracy : 0.0; }
        }

        public double L1
        {
            get { return this.IsTrained && Entity.Metrics.RegressionMetrics != null ? Entity.Metrics.RegressionMetrics.L1 : 0.0; }
        }

        public double L2
        {
            get { return this.IsTrained && Entity.Metrics.RegressionMetrics != null ? Entity.Metrics.RegressionMetrics.L2 : 0.0; }
        }

        public double LossFn
        {
            get { return this.IsTrained && Entity.Metrics.RegressionMetrics != null ? Entity.Metrics.RegressionMetrics.LossFn : 0.0; }
        }

        public double RSquared
        {
            get { return this.IsTrained && Entity.Metrics.RegressionMetrics != null ? Entity.Metrics.RegressionMetrics.RSquared : 0.0; }
        }

        public double Rms
        {
            get { return this.IsTrained && Entity.Metrics.RegressionMetrics != null ? Entity.Metrics.RegressionMetrics.Rms : 0.0; }
        }

        public string Date
        {
            get { return this.IsTrained ? Entity.DateTime.ToString("f") : "-"; }
        }

        public bool IsIntegrated
        {
            get
            {
                switch (_predictionType)
                {
                    case PredictionType.WhichBugGetReopened:
                        return TrainingsManagementHelper.BugsReopenPrediction.IsIntegrated(Entity.FilePath);

                    case PredictionType.DevelopersEfficiency:
                        return TrainingsManagementHelper.DevelopersEfficiencyPrediction.IsIntegrated(Entity.FilePath);
                    case PredictionType.TimeToFix:
                        return TrainingsManagementHelper.TimeToFixPrediction.IsIntegrated(Entity.FilePath);
                    default:
                        return false;
                }
            }
        }

        public bool IsBinaryClassificationMetricsVisibile
        {
            get
            {
                return this._predictionType == PredictionType.WhichBugGetReopened &&
                       this.Entity.Metrics.ClassificationMetrics != null;
            }
        }

        public bool IsRegressionMetricsVisibile
        {
            get
            {
                return (this._predictionType == PredictionType.DevelopersEfficiency ||
                        this._predictionType == PredictionType.TimeToFix) &&
                       this.Entity.Metrics.RegressionMetrics != null;
            }
        }

        private void RaiseAllPropertiesChanged()
        {
            this.RaisePropertyChanged(nameof(IsTrained));
            this.RaisePropertyChanged(nameof(Name));
            this.RaisePropertyChanged(nameof(Accuracy));
            this.RaisePropertyChanged(nameof(MatthewsCorrelationCoefficient));
            this.RaisePropertyChanged(nameof(Auc));
            this.RaisePropertyChanged(nameof(F1Score));
            this.RaisePropertyChanged(nameof(Date));
            this.RaisePropertyChanged(nameof(L1));
            this.RaisePropertyChanged(nameof(L2));
            this.RaisePropertyChanged(nameof(RSquared));
            this.RaisePropertyChanged(nameof(Rms));
            this.RaisePropertyChanged(nameof(LossFn));
        }

        private RelayCommand _trainCommand;

        private void TrainIssueReopenClassificationPredictor(ISession session)
        {
            var predictor = CommonServiceLocator.ServiceLocator.Current.GetInstance<IIssueReopenPredictor>();

            var tool =
                TrainingsManagementHelper.BugsReopenPrediction.GetModelTypeFromFilePath(
                    this.Entity.FilePath);
            var issues = session.QueryOver<Issue>().List();
            TrainingOutputData trainingOutputData;

            if (!IsIntegrated)
            {
                trainingOutputData = predictor.TrainAndSavePredictionModel(
                    new ReopenTrainingInputData()
                    {
                        CrossValidationFoldsCount = Configuration.ConfigurationService.Instance.Configuration.CrossValidationNumOfFolds,
                        ClassificationTool = tool,
                        PredictiveModelPath = Entity.FilePath,
                        IssuesData = issues,
                        DataRebalancingTechnique = Configuration.ConfigurationService.Instance.Configuration.BinaryClassificationDataBalancingTechnique
                    });
            }
            else
            {
                var commits = session.QueryOver<Commit>().List();

                trainingOutputData =
                    predictor.TrainAndSavePredictionModel(
                        new ReopenTrainingIntegratedInputData()
                        {
                            ClassificationTool = tool,
                            PredictiveModelPath = Entity.FilePath,
                            IssuesData = issues,
                            Commits = commits,
                            CrossValidationFoldsCount = Configuration.ConfigurationService.Instance.Configuration.CrossValidationNumOfFolds,
                            DataRebalancingTechnique = Configuration.ConfigurationService.Instance.Configuration.BinaryClassificationDataBalancingTechnique
                        });
            }



            if (trainingOutputData != null)
            {
                trainingOutputData.Metrics.ClassificationMetrics.DataBalancingTechnique = ConfigurationService.Instance
                    .Configuration.BinaryClassificationDataBalancingTechnique;

                var name = AssertFreeName($"IssueReopenTraining_{Guid.NewGuid()}.pdf");
                var path = ZlpPathHelper.Combine(LocalizationConstants.TrainingReportsPath, name);
                _exporter.Export(trainingOutputData, path);
                Process.Start(path);

                this.Entity = trainingOutputData;
                ModelTrainingInfoPersistingService.Instance.AddOrUpdate(this.Entity,PredictionType.WhichBugGetReopened);
                this.RaiseAllPropertiesChanged();
            }
        }

        private string AssertFreeName(string name)
        {
            const int fileExtensionLenght = 4;
            var outputName = name;
            var path = ZlpPathHelper.Combine(LocalizationConstants.TrainingReportsPath, outputName);
            int index = 0;
            while (ZlpIOHelper.FileExists(path))
            {
                index++;
                outputName = $"{name.Substring(0, name.Length - fileExtensionLenght)} ({index}).pdf";
                path = ZlpPathHelper.Combine(LocalizationConstants.TrainingReportsPath, name);
            }

            return outputName;
        }

        private void TrainDeveloperEfficiencyPredictor(ISession session)
        {
            var predictor = CommonServiceLocator.ServiceLocator.Current
                .GetInstance<IDeveloperEfficiencyPredictor>();


            var tool =
                TrainingsManagementHelper.DevelopersEfficiencyPrediction.GetModelTypeFromFilePath(this.Entity.FilePath);

            var issues = session.QueryOver<Issue>().List();
            var users = session.QueryOver<IssueTrackerUser>().List();
            TrainingOutputData trainingOutputData;

            if (!IsIntegrated)
            {
                trainingOutputData = predictor.TrainAndSavePredictionModel(
                    new DeveloperEfficiencyInputData()
                    {
                        CrossValidationFoldsCount = Configuration.ConfigurationService.Instance.Configuration.CrossValidationNumOfFolds,
                        RegressionTool = tool,
                        PredictiveModelPath = Entity.FilePath,
                        IssuesData = issues,
                        UsersData = users,
                        EfficiencyWeightsKeeper = Configuration.ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper
                    });
            }
            else
            {
                var commits = session.QueryOver<Commit>().List();

                trainingOutputData =
                    predictor.TrainAndSavePredictionModel(
                        new DeveloperEfficiencyIntegratedDataInput()
                        {
                            CrossValidationFoldsCount = Configuration.ConfigurationService.Instance.Configuration.CrossValidationNumOfFolds,
                            RegressionTool = tool,
                            PredictiveModelPath = Entity.FilePath,
                            IssuesData = issues,
                            Commits = commits,
                            UsersData = users,
                            EfficiencyWeightsKeeper = Configuration.ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper
                        });
            }

            if (trainingOutputData != null)
            {
                var name = AssertFreeName($"DeveloperEfficiencyTraining_{Guid.NewGuid()}.pdf");
                var path = ZlpPathHelper.Combine(LocalizationConstants.TrainingReportsPath, name);
                _exporter.Export(trainingOutputData, path);
                Process.Start(path);

                this.Entity = trainingOutputData;
                ModelTrainingInfoPersistingService.Instance.AddOrUpdate(this.Entity, PredictionType.DevelopersEfficiency);
                this.RaiseAllPropertiesChanged();
            }
        }

        private void TrainTimeToFixPredictor(ISession session)
        {
            var predictor = CommonServiceLocator.ServiceLocator.Current
                .GetInstance<ITimeToFixPredictor>();

            var tool =
                TrainingsManagementHelper.TimeToFixPrediction.GetModelTypeFromFilePath(this.Entity.FilePath);

            var issues = session.QueryOver<Issue>().List();
           
            TrainingOutputData trainingOutputData;

            if (!IsIntegrated)
            {
                trainingOutputData = predictor.TrainAndSavePredictionModel(
                    new TimeToFixInputData()
                    {
                        CrossValidationFoldsCount = Configuration.ConfigurationService.Instance.Configuration.CrossValidationNumOfFolds,
                        RegressionTool = tool,
                        PredictiveModelPath = Entity.FilePath,
                        IssuesData = issues,
                    });
            }
            else
            {
                var commits = session.QueryOver<Commit>().List();

                trainingOutputData =
                    predictor.TrainAndSavePredictionModel(
                        new TimeToFixIntegratedInputData()
                        {
                            CrossValidationFoldsCount = Configuration.ConfigurationService.Instance.Configuration.CrossValidationNumOfFolds,
                            RegressionTool = tool,
                            PredictiveModelPath = Entity.FilePath,
                            IssuesData = issues,
                            Commits = commits
                        });
            }

            if (trainingOutputData != null)
            {
                
                var name = AssertFreeName($"TimeToFixTraining_{Guid.NewGuid()}.pdf");
                var path = ZlpPathHelper.Combine(LocalizationConstants.TrainingReportsPath, name);
                _exporter.Export(trainingOutputData, path);
                Process.Start(path);

                this.Entity = trainingOutputData;
                ModelTrainingInfoPersistingService.Instance.AddOrUpdate(this.Entity, PredictionType.TimeToFix);
                this.RaiseAllPropertiesChanged();
            }
        }

        public RelayCommand TrainCommand
        {
            get { return _trainCommand ?? (_trainCommand = new RelayCommand(async () =>
                {
                    try
                    {
                        this.IsLoading = true;
                        await Task.Run(() =>
                        {
                            using (var session = DbService.Instance.SessionFactory.OpenSession())
                            {
                                switch (_predictionType)
                                {
                                    case PredictionType.WhichBugGetReopened:
                                        this.TrainIssueReopenClassificationPredictor(session);
                                        break;

                                    case PredictionType.DevelopersEfficiency:
                                        this.TrainDeveloperEfficiencyPredictor(session);
                                        break;
                                    case PredictionType.TimeToFix:
                                        this.TrainTimeToFixPredictor(session);
                                        break;
                                }
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        await this.ShowErrorDialog(ex);
                    }
                    finally
                    {
                        this.IsLoading = false;
                    }
                }));
            }
        }
    }
}

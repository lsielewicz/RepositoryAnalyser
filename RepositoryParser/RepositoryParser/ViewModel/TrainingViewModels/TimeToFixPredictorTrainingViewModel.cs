﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using RepositoryAnalyser.CommonUI.BaseViewModels;
using RepositoryAnalyser.Configuration.ModelTraining;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.ViewModel.TrainingViewModels
{
    public class TimeToFixPredictorTrainingViewModel : RepositoryAnalyserViewModelBase
    {

        public TimeToFixPredictorTrainingViewModel()
        {
            this.InitializeTrainingModels();
        }

        public override void OnLoad()
        {
            base.OnLoad();
            
        }

        public ObservableCollection<TrainingOutputDataViewModel> TrainingModels { get; private set; }
        public ObservableCollection<TrainingOutputDataViewModel> IntegratedTrainingModels { get; private set; }

        private void InitializeTrainingModels()
        {
            this.TrainingModels = new ObservableCollection<TrainingOutputDataViewModel>();
            this.IntegratedTrainingModels = new ObservableCollection<TrainingOutputDataViewModel>();

            var trainedModels = ModelTrainingInfoPersistingService.Instance.ModelTrainingInfo.TimeToFixInfo;
            foreach (var model in trainedModels)
            {
                if (!TrainingsManagementHelper.TimeToFixPrediction.IsIntegrated(model.FilePath))
                {
                    TrainingModels.Add(new TrainingOutputDataViewModel(model, PredictionType.TimeToFix));
                }
                else
                {
                    IntegratedTrainingModels.Add(new TrainingOutputDataViewModel(model, PredictionType.TimeToFix));
                }
            }

            var allModelsPaths = TrainingsManagementHelper.TimeToFixPrediction.GetAllPaths();
            foreach (var path in allModelsPaths.Where(p => trainedModels.All(tm => !tm.FilePath.Equals(p, StringComparison.OrdinalIgnoreCase))))
            {
                var notTrainedModel = new TrainingOutputData()
                {
                    DateTime = DateTime.Now,
                    FilePath = path,
                    Metrics = new ModelMetrics()
                    {
                        RegressionMetrics = new AverageRegressionMetrics()
                        {
                            Rms = 0.0,
                            L1 = 0.0,
                            LossFn = 0.0,
                            L2 = 0.0,
                            RSquared = 0.0
                        }
                    }
                };
                if (!TrainingsManagementHelper.TimeToFixPrediction.IsIntegrated(notTrainedModel.FilePath))
                {
                    TrainingModels.Add(new TrainingOutputDataViewModel(notTrainedModel, PredictionType.TimeToFix));
                }
                else
                {
                    IntegratedTrainingModels.Add(new TrainingOutputDataViewModel(notTrainedModel, PredictionType.TimeToFix));
                }


            }
        }
    }
}

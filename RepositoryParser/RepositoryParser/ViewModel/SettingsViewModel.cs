﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using GalaSoft.MvvmLight.Command;
using RepositoryAnalyser.CommonUI.BaseViewModels;
using RepositoryAnalyser.Configuration;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings;
using RepositoryAnalyser.Controls.MahAppsDialogOverloadings.InformationDialog;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.MachineLearning.Enums;
using ZetaLongPaths;

namespace RepositoryAnalyser.ViewModel
{
    public class SettingsViewModel : RepositoryAnalyserViewModelBase
    {

        private readonly bool _isInitialized;
        private bool _cloneWithAllBranches;
        private string _selectedLangauge;
        private string _currentRepositorySavingPath;
        private RelayCommand _restartApplicationCommand;
        private RelayCommand _openRepositoriesDirectory;
        private RelayCommand _openDirectoryFilePicker;
        private RelayCommand _setCloneAllBranchesCommand;

        public SettingsViewModel()
        {
            if (ConfigurationService.Instance.Configuration.CurrentLanguage == ApplicationLanguage.Polish)
                SelectedLanguage = this.GetLocalizedString("Polish");
            if (ConfigurationService.Instance.Configuration.CurrentLanguage == ApplicationLanguage.English)
                SelectedLanguage = this.GetLocalizedString("English");

            CurrentRepositorySavingPath = ConfigurationService.Instance.Configuration.SavingRepositoryPath;
            CurrentReportsSavingPath = ConfigurationService.Instance.Configuration.ReportsSavingLocation;
            CloneWithAllBranches = ConfigurationService.Instance.Configuration.CloneAllBranches;

            _isInitialized = true;
        }

        public bool CloneWithAllBranches
        {
            get
            {
                return _cloneWithAllBranches;
            }
            set
            {
                if (_cloneWithAllBranches == value)
                    return;
                _cloneWithAllBranches = value;
                if (_isInitialized)
                {
                    ConfigurationService.Instance.Configuration.CloneAllBranches = _cloneWithAllBranches;
                    ConfigurationService.Instance.SaveChanges();
                }
                RaisePropertyChanged();
            }
        }

        public string CurrentRepositorySavingPath
        {
            get { return _currentRepositorySavingPath; }
            set
            {
                if (_currentRepositorySavingPath == value)
                {
                    return;
                }

                _currentRepositorySavingPath = value;
                if (_isInitialized && ZetaLongPaths.ZlpIOHelper.DirectoryExists(_currentRepositorySavingPath))
                {
                    ConfigurationService.Instance.Configuration.SavingRepositoryPath = _currentRepositorySavingPath;
                    ConfigurationService.Instance.SaveChanges();
                }

                RaisePropertyChanged();
            }
        }

        private string _currentReportsSavingPath;
        public string CurrentReportsSavingPath
        {
            get
            {
                return _currentReportsSavingPath;
            }
            set
            {
                if (_currentReportsSavingPath == value)
                    return;
                _currentReportsSavingPath = value;
                if (_isInitialized && ZetaLongPaths.ZlpIOHelper.DirectoryExists(_currentReportsSavingPath))
                {
                    ConfigurationService.Instance.Configuration.ReportsSavingLocation = _currentReportsSavingPath;
                    ConfigurationService.Instance.SaveChanges();
                }

                RaisePropertyChanged();
            }
        }

        public string SelectedLanguage
        {
            get { return _selectedLangauge; }
            set
            {
                if (_selectedLangauge == value)
                    return;
                _selectedLangauge = value;
                if(_isInitialized)
                    SelectedLanguageItemChanged(_selectedLangauge);
                this.RaisePropertyChanged();
            }
        }

        
        public int NumOfFolds
        {
            get { return ConfigurationService.Instance.Configuration.CrossValidationNumOfFolds; }
            set
            {
                if (ConfigurationService.Instance.Configuration.CrossValidationNumOfFolds == value)
                {
                    return;
                }

                ConfigurationService.Instance.Configuration.CrossValidationNumOfFolds = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }

        public DataBalancingTechnique BinaryClassificationDataBalancingTechnique
        {
            get
            {
                return ConfigurationService.Instance.Configuration.BinaryClassificationDataBalancingTechnique;
            }
            set
            {
                if (ConfigurationService.Instance.Configuration.BinaryClassificationDataBalancingTechnique == value)
                {
                    return;
                }

                ConfigurationService.Instance.Configuration.BinaryClassificationDataBalancingTechnique = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }

        private async void SelectedLanguageItemChanged(string item)
        {
            if (item == this.GetLocalizedString("Polish"))
                ConfigurationService.Instance.Configuration.CurrentLanguage = ApplicationLanguage.Polish;
            if (item == this.GetLocalizedString("English"))
                ConfigurationService.Instance.Configuration.CurrentLanguage = ApplicationLanguage.English;

            ConfigurationService.Instance.SaveChanges();

            await DialogHelper.Instance.ShowDialog(new CustomDialogEntryData()
            {
                MetroWindow = StaticServiceProvider.MetroWindowInstance,
                InformationType = InformationType.Information,
                DialogMessage = this.GetLocalizedString("RestartRequired"),
                DialogTitle = this.GetLocalizedString("Information"),
                OkButtonMessage = "Restart",
                OkCommand = this.RestartApplicationCommand
            });
        }

        public RelayCommand SetCloneAllBranchesCommand
        {
            get
            {
                return _setCloneAllBranchesCommand ?? (_setCloneAllBranchesCommand = new RelayCommand(() =>
                {
                    CloneWithAllBranches = !CloneWithAllBranches;
                }));
            }
        }

        public RelayCommand RestartApplicationCommand
        {
            get
            {
                return _restartApplicationCommand ?? (_restartApplicationCommand = new RelayCommand(() =>
                {
                    System.Windows.Forms.Application.Restart();
                    System.Windows.Application.Current.Shutdown();
                }));
            }
        }

        public RelayCommand OpenRepositoriesDirectoryCommand
        {
            get
            {
                return _openRepositoriesDirectory ?? (_openRepositoriesDirectory = new RelayCommand(() =>
                {
                    OpenDirectory(ConfigurationService.Instance.Configuration.SavingRepositoryPath);
                }));
            }
        }

        private void OpenDirectory(string dirPath)
        {
            if (!ZlpIOHelper.DirectoryExists(dirPath))
            {
                ZlpIOHelper.CreateDirectory(dirPath);
            }
            Process.Start(dirPath);
        }

        private RelayCommand _openReportsDirectoryCommand;
        public RelayCommand OpenReportsDirectoryCommand
        {
            get
            {
                return _openReportsDirectoryCommand ?? (_openReportsDirectoryCommand = new RelayCommand(() =>
                {
                    OpenDirectory(ConfigurationService.Instance.Configuration.ReportsSavingLocation);
                }));
            }
        }

        public RelayCommand OpenDirectoryFilePicker
        {
            get
            {
                return _openDirectoryFilePicker ?? (_openDirectoryFilePicker = new RelayCommand(() =>
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    fbd.SelectedPath = AppDomain.CurrentDomain.BaseDirectory;
                    fbd.Description = this.GetLocalizedString("PickFolderWithRepo");

                    if (fbd.ShowDialog() == DialogResult.OK)
                    {
                        this.CurrentRepositorySavingPath = fbd.SelectedPath;
                    }
                }));
            }
        }

        private RelayCommand _setReportsSavingLocationCommand;
        public RelayCommand SetReportsSavingLocationCommand
        {
            get
            {
                return _setReportsSavingLocationCommand ?? (_setReportsSavingLocationCommand = new RelayCommand(() =>
                {
                    var fbd = new FolderBrowserDialog()
                    {
                        SelectedPath = AppDomain.CurrentDomain.BaseDirectory,
                        Description = this.GetLocalizedString("SetReportsSavingLocalization")
                    };


                    if (fbd.ShowDialog() == DialogResult.OK)
                    {
                        this.CurrentRepositorySavingPath = fbd.SelectedPath;
                    }
                }));
            }
        }

        public double ClosedIssuesPerMonthWeight
        {
            get
            {
                return ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.ClosedIssuesPerMonthWeight;
            }
            set
            {

                ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.ClosedIssuesPerMonthWeight = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }

        public double OpenedIssuesPerMonthWeight
        {
            get
            {
                return ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.OpenedIssuesPerMonthWeight;
            }
            set
            {

                ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.OpenedIssuesPerMonthWeight = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }

        public double ReopenedIssuesPerMonthWeight
        {
            get
            {
                return ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.ReopenedIssuesPerMonthWeight;
            }
            set
            {

                ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.ReopenedIssuesPerMonthWeight = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }

        public double ReportedIssuesPerMonthWeight
        {
            get
            {
                return ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.ReportedIssuesPerMonthWeight;
            }
            set
            {

                ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.ReportedIssuesPerMonthWeight = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }

        public double AssignedIssuesWeight
        {
            get
            {
                return ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.AssignedIssuesWeight;
            }
            set
            {

                ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.AssignedIssuesWeight = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }

        public double CommitsPerMonthWeight
        {
            get
            {
                return ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.CommitsPerMonthWeight;
            }
            set
            {

                ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.CommitsPerMonthWeight = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }

        public double AddedLinesPerMonthWeight
        {
            get
            {
                return ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.AddedLinesPerMonthWeight;
            }
            set
            {

                ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.AddedLinesPerMonthWeight = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }

        public double DeletedLinesPerMonthWeight
        {
            get
            {
                return ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.DeletedLinesPerMonthWeight;
            }
            set
            {

                ConfigurationService.Instance.Configuration.EfficiencyWeightsKeeper.DeletedLinesPerMonthWeight = value;

                ConfigurationService.Instance.SaveChanges();
                RaisePropertyChanged();
            }
        }



    }
}

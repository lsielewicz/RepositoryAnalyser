﻿using RepositoryAnalyser.CommonUI;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.ViewModel
{
    public class IssueTrackerUserViewModel : SelectableItemViewModel<IssueTrackerUser>
    {
        public IssueTrackerUserViewModel(IssueTrackerUser entity) : base(entity)
        {
        }

        public string UserName
        {
            get { return this.Entity.UserName; }
        }

        public string Email
        {
            get { return this.Entity.Email; }
        }
    }
}

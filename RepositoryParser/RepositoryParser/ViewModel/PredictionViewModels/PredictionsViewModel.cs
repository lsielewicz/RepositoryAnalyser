﻿using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using RepositoryAnalyser.CommonUI.BaseViewModels;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.Wizards;
using RepositoryAnalyser.Wizards.DeveloperProductivityPredictionWizard;
using RepositoryAnalyser.Wizards.IssueReopenPredictionWizard;
using RepositoryAnalyser.Wizards.TimeToFixPredictionWizard;
using ZetaLongPaths;

namespace RepositoryAnalyser.ViewModel.PredictionViewModels
{
    public class PredictionsViewModel : RepositoryAnalyserViewModelBase
    {
        private RelayCommand _openReportsDirectoryCommand;
        public RelayCommand OpenReportsDirectoryCommand
        {
            get
            {
                return _openReportsDirectoryCommand ?? (_openReportsDirectoryCommand = new RelayCommand(() =>
                {

                    var reportsDirectoryPath = LocalizationConstants.PrognosisReportsPath;
                    if (!ZlpIOHelper.DirectoryExists(reportsDirectoryPath))
                    {
                        ZlpIOHelper.CreateDirectory(reportsDirectoryPath);
                    }

                    Process.Start(reportsDirectoryPath);
                }));
            }
        }


        private RelayCommand<PredictionType> _runPredictionWizardCommand;
        public RelayCommand<PredictionType> RunPredictionWizardCommand
        {
            get
            {
                return _runPredictionWizardCommand ?? (_runPredictionWizardCommand = new RelayCommand<PredictionType>(
                           (param) =>
                           {
                               switch (param)
                               {
                                   case PredictionType.WhichBugGetReopened:
                                       var issueWizard = new IssueReopenPredictionWizard();
                                       issueWizard.ShowDialog();
                                       var issueResult = issueWizard.WizardResult;
                                       if (issueResult == WizardResult.Successed)
                                       {
                                           //todo: do something with report maybe?
                                       }
                                       break;

                                   case PredictionType.DevelopersEfficiency:
                                       var devWizard = new DeveloperProductivityPredictionWizard();
                                       devWizard.ShowDialog();
                                       var devResult = devWizard.WizardResult;
                                       if (devResult == WizardResult.Successed)
                                       {
                                           //todo: do something with report maybe?
                                       }
                                       break;

                                   case PredictionType.TimeToFix:
                                       var wizard = new TimeToFixPredictionWizard();
                                       wizard.ShowDialog();
                                       var wResult = wizard.WizardResult;
                                       if (wResult == WizardResult.Successed)
                                       {
                                           //todo: do something with report maybe?
                                       }
                                       break;
                               }
                           }));
            }

        }
    }
}

﻿using RepositoryAnalyser.CommonUI;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.ViewModel
{
    public class RepositoryViewModel : SelectableItemViewModel<Repository>
    {
        public RepositoryViewModel(Repository entity) : base(entity)
        {
            
        }

        public string Name
        {
            get { return Entity.Name; }
        }
    }
}

﻿using RepositoryAnalyser.CommonUI;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.ViewModel
{
    public class IssueViewModel : SelectableItemViewModel<Issue>
    {
        public IssueViewModel(Issue isssue) : base(isssue)
        {
            this.IsSelected = false;
        }

        public int Id
        {
            get { return Entity.Id; }
        }

        public string Key
        {
            get { return Entity.Key; }
        }

        public string Description
        {
            get { return Entity.Description; }
        }
    }
}

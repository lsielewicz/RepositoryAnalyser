﻿using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;
using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.Messages
{
    public class DataMessageToDisplay : MessageBase
    {
        public List<Commit> CommitList { get; set; }

        public DataMessageToDisplay(List<Commit> list)
        {
            CommitList = list;
        }
    }
}

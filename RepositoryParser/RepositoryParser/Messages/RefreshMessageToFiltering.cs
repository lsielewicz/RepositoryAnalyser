﻿namespace RepositoryAnalyser.Messages
{
    public class RefreshMessageToFiltering : RefreshMessageToPresentation
    {
        public RefreshMessageToFiltering(bool refresh) : base(refresh)
        {
        }
    }
}

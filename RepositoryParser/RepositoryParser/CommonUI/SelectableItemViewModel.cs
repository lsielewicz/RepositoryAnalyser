﻿using RepositoryAnalyser.CommonUI.BaseViewModels;

namespace RepositoryAnalyser.CommonUI
{
    public abstract class SelectableItemViewModel<T> : RepositoryAnalyserViewModelBase
    {
        public T Entity { get; }

        protected SelectableItemViewModel(T entity)
        {
            this.Entity = entity;
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected == value)
                {
                    return;
                }

                _isSelected = value;
                this.RaisePropertyChanged();
            }
        }
    }
}

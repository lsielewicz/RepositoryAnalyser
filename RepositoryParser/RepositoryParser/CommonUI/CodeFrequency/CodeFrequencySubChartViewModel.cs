﻿using System.Collections.Generic;
using System.Linq;
using RepositoryAnalyser.CommonUI.BaseViewModels;
using RepositoryAnalyser.Models;

namespace RepositoryAnalyser.CommonUI.CodeFrequency
{
    public class CodeFrequencySubChartViewModel : ChartViewModelBase
    {
        public override void FillChartData()
        {
            if (this.ExtendedChartSeries == null)
                base.FillChartData();
        }

        public void RedrawChart(List<ExtendedChartSeries> chartSeries)
        {
            this.ExtendedChartSeries = chartSeries;
            if (this.ExtendedChartSeries != null && this.ExtendedChartSeries.Any())
            {
                this.DrawChart();
                this.FillDataCollection();
            }
        }
    }
}

﻿using System;
using System.Xml.Serialization;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;

namespace RepositoryAnalyser.Configuration
{
    [Serializable]
    public class RepositoryAnalyserConfiguration
    {
        [XmlElement("BinaryClassificationDataBalancingTechnique")]
        public DataBalancingTechnique BinaryClassificationDataBalancingTechnique { get; set; }

        [XmlElement("CrossValidationNumOfFolds")]
        public int CrossValidationNumOfFolds { get; set; }

        [XmlElement("CurrentLanguage")]
        public string CurrentLanguage { get; set; }

        [XmlElement("DynamicFiltering")]
        public bool DynamicFiltering { get; set; }

        [XmlElement("CloneAllBranches")]
        public bool CloneAllBranches { get; set; }

        [XmlElement("SavingRepositoryPath")]
        public string SavingRepositoryPath { get; set; }

        [XmlElement]
        public string ReportsSavingLocation { get; set; }

        [XmlElement]
        public EfficiencyWeightsKeeper EfficiencyWeightsKeeper { get; set; }

        
    }
}

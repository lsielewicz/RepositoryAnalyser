﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Configuration.ModelTraining
{
    [Serializable]
    public class ModelTrainingInfo
    {
        [XmlElement]
        public List<TrainingOutputData> ReopenedBuggsInfo { get; set; }

        [XmlElement]
        public List<TrainingOutputData> DevelopersEfficiencyInfo { get; set; }

        [XmlElement]
        public List<TrainingOutputData> TimeToFixInfo { get; set; }
    }
}

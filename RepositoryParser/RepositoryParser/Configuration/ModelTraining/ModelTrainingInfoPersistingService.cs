﻿using System;
using System.Collections.Generic;
using System.Linq;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Helpers;
using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Configuration.ModelTraining
{
    public class ModelTrainingInfoPersistingService
    {
        #region Singleton
        private static ModelTrainingInfoPersistingService _instance;

        public static ModelTrainingInfoPersistingService Instance
        {
            get { return _instance ?? (_instance = new ModelTrainingInfoPersistingService()); }
        }

        private ModelTrainingInfoPersistingService()
        {
            this.ModelTrainingInfo = this.LoadFromFile();
        }
        #endregion

        public ModelTrainingInfo ModelTrainingInfo { get; }

        public void SaveChanges()
        {
            XmlSerializeHelper<ModelTrainingInfo>.Serialize(ModelTrainingInfo, LocalizationConstants.TrainingModelInfoFilePath);
        }

        public void Remove(TrainingOutputData data)
        {
            var reopenPrediction = this.ModelTrainingInfo.ReopenedBuggsInfo.FirstOrDefault(row =>
                row.FilePath.Equals(data.FilePath, StringComparison.OrdinalIgnoreCase));
            if (reopenPrediction != null)
            {
                this.ModelTrainingInfo.ReopenedBuggsInfo.Remove(reopenPrediction);
            }

            var devEffortIssue = this.ModelTrainingInfo.DevelopersEfficiencyInfo.FirstOrDefault(row =>
                row.FilePath.Equals(data.FilePath, StringComparison.OrdinalIgnoreCase));
            if (devEffortIssue != null)
            {
                this.ModelTrainingInfo.ReopenedBuggsInfo.Remove(devEffortIssue);
            }

            var timeToFixIssue = this.ModelTrainingInfo.TimeToFixInfo.FirstOrDefault(row =>
                row.FilePath.Equals(data.FilePath, StringComparison.OrdinalIgnoreCase));
            if (devEffortIssue != null)
            {
                this.ModelTrainingInfo.TimeToFixInfo.Remove(devEffortIssue);
            }
        }

        public void AddOrUpdate(TrainingOutputData data, PredictionType type)
        {
            List<TrainingOutputData> listToEdit;
            switch (type)
            {
                case PredictionType.WhichBugGetReopened:
                    listToEdit = this.ModelTrainingInfo.ReopenedBuggsInfo;
                    break;
                case PredictionType.DevelopersEfficiency:
                    listToEdit = this.ModelTrainingInfo.DevelopersEfficiencyInfo;
                    break;
                case PredictionType.TimeToFix:
                    listToEdit = this.ModelTrainingInfo.TimeToFixInfo;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            var dataPath = data.FilePath;
            var existingRow =
                listToEdit.FirstOrDefault(row => row.FilePath.Equals(dataPath, StringComparison.OrdinalIgnoreCase));

            if (existingRow != null)
            {
                listToEdit.Remove(existingRow);
            }
            listToEdit.Add(data);

            this.SaveChanges();
        }

        private ModelTrainingInfo LoadFromFile()
        {
            try
            {
                ModelTrainingInfo info = null;
                if (ZetaLongPaths.ZlpIOHelper.FileExists(LocalizationConstants.TrainingModelInfoFilePath))
                {
                    info = XmlSerializeHelper<ModelTrainingInfo>.Deserialize(LocalizationConstants.TrainingModelInfoFilePath);
                }
                else
                {
                    info = new ModelTrainingInfo()
                    {
                        DevelopersEfficiencyInfo = new List<TrainingOutputData>(),
                        ReopenedBuggsInfo = new List<TrainingOutputData>(),
                        TimeToFixInfo = new List<TrainingOutputData>()
                    };
                }

                return info;
            }
            catch (Exception)
            {
                //todo loging
                return new ModelTrainingInfo()
                {
                    DevelopersEfficiencyInfo = new List<TrainingOutputData>(),
                    ReopenedBuggsInfo = new List<TrainingOutputData>(),
                    TimeToFixInfo = new List<TrainingOutputData>()
                };
            }
        }
    }
}

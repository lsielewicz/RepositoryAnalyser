﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.MachineLearning.Predictors;

namespace RepositoryAnalyser.Console.Tester
{
    internal static class PredictionTester
    {
        public static void TestDeveloperEfficiencyPredictions(bool isIntegrated, string filePath, string title = null)
        {
            var sb = new StringBuilder();
            if (title != null)
            {
                sb.AppendLine(title);
            }
            sb.AppendLine($"TIME TO FIX PREDICTION");
            sb.AppendLine($"[IsIntegrated]: {isIntegrated}");

            var devEfficiencyPredictor = new DeveloperEfficiencyPredictor();
            var weightsKeeper = new EfficiencyWeightsKeeper();
            var overallPositives = 0;
            var overallNegatives = 0;
            var rnd = new Random();
            const int countOfTries = 5;
            sb.AppendLine($"[CountOfTests]: {countOfTries}");
            var counter = 1;
            for (int i = 0; i < countOfTries; i++)
            {
                using (var session = DbService.Instance.SessionFactory.OpenSession())
                {
                    var issues = session.QueryOver<Issue>().List();
                    var commits = session.QueryOver<Commit>().List();
                    var users = session.QueryOver<IssueTrackerUser>().List();

                    var usersToBeTrained = (int)Math.Floor(users.Count * 0.8);
                    var usersToBePredictedCount = users.Count - usersToBeTrained;

                    var usersToBePredicted = new List<IssueTrackerUser>();
                    for (int index = 0; index < usersToBePredictedCount; index++)
                    {
                        var randIndex = rnd.Next(0, users.Count);
                        if (users[randIndex] != null)
                        {
                            usersToBePredicted.Add(users[randIndex]);
                            users.RemoveAt(randIndex);
                        }
                    }

                    TrainingOutputData trainingOutputData = null;
                    if (isIntegrated)
                    {
                        trainingOutputData = devEfficiencyPredictor.TrainAndSavePredictionModel(
                            new DeveloperEfficiencyIntegratedDataInput()
                            {
                                CrossValidationFoldsCount = -1,
                                IssuesData = issues,
                                UsersData = users,
                                PredictiveModelPath = Path.GetTempFileName(),
                                RegressionTool = RegressionTool.FastTreeRegressor,
                                EfficiencyWeightsKeeper = weightsKeeper,
                                Commits = commits
                            });
                    }
                    else
                    {
                        trainingOutputData = devEfficiencyPredictor.TrainAndSavePredictionModel(new DeveloperEfficiencyInputData()
                        {
                            CrossValidationFoldsCount = -1,
                            IssuesData = issues,
                            UsersData = users,
                            PredictiveModelPath = Path.GetTempFileName(),
                            RegressionTool = RegressionTool.FastTreeRegressor,
                            EfficiencyWeightsKeeper = weightsKeeper
                        });
                    }


                    foreach (var user in usersToBePredicted)
                    {

                        double actualValue = 0.0;
                        double predictedValue = 0.0;
                        if (isIntegrated)
                        {
                            var predictionResult = devEfficiencyPredictor.Predict(user, issues,commits, trainingOutputData, weightsKeeper).Result;
                            actualValue = predictionResult.Data.Efficiency;
                            predictedValue = predictionResult.Prediction.Efficiency;
                        }
                        else
                        {
                            var predictionResult = devEfficiencyPredictor.Predict(user,issues, trainingOutputData, weightsKeeper).Result;
                            actualValue = predictionResult.Data.Efficiency;
                            predictedValue = predictionResult.Prediction.Efficiency;
                        }

                        if (actualValue == 0.0)
                        {
                            continue;
                        }

                        var acc = devEfficiencyPredictor.GetAccuracy(actualValue, predictedValue);
                        sb.AppendLine($"{counter++};{acc}");
                    }
                }
            }

            File.WriteAllText(filePath, sb.ToString());
            Process.Start(filePath);
        }

        public static void TestTimeToFIxPredictions(bool isIntegrated, string filePath, string title = null)
        {
            var sb = new StringBuilder();
            if (title != null)
            {
                sb.AppendLine(title);
            }
            sb.AppendLine($"TIME TO FIX PREDICTION");
            sb.AppendLine($"[IsIntegrated]: {isIntegrated}");

            var timeToFixPredictor = new TimeToFixPredictor();

            var overallPositives = 0;
            var overallNegatives = 0;
            var rnd = new Random();
            const int countOfTries = 5;
            sb.AppendLine($"[CountOfTests]: {countOfTries}");
            var counter = 1;
            for (int i = 0; i < countOfTries; i++)
            {
                using (var session = DbService.Instance.SessionFactory.OpenSession())
                {
                    var issues = session.QueryOver<Issue>().List();
                    var commits = session.QueryOver<Commit>().List();
                    var commitsSets = session.QueryOver<CommitsSet>().List();

                    var issuesToBeTrained = (int)Math.Floor(issues.Count * 0.8);
                    var issuesToBePredictedCount = issues.Count - issuesToBeTrained;

                    var issuesToBePredicted = new List<Issue>();
                    for (int index = 0; index < issuesToBePredictedCount; index++)
                    {
                        var randIndex = rnd.Next(0, issues.Count);
                        if (issues[randIndex] != null)
                        {
                            issuesToBePredicted.Add(issues[randIndex]);
                            issues.RemoveAt(randIndex);
                        }
                    }

                    TrainingOutputData trainingOutputData = null;
                    if (isIntegrated)
                    {
                        trainingOutputData = timeToFixPredictor.TrainAndSavePredictionModel(
                            new TimeToFixIntegratedInputData()
                            {
                                CrossValidationFoldsCount = -1,
                                IssuesData = issues,
                                Commits = commits,
                                PredictiveModelPath = Path.GetTempFileName(),
                                CommitsSets = commitsSets,
                                RegressionTool = RegressionTool.FastTreeRegressor
                            });
                    }
                    else
                    {
                        trainingOutputData = timeToFixPredictor.TrainAndSavePredictionModel(new TimeToFixInputData()
                        {
                            PredictiveModelPath = Path.GetTempFileName(),
                            CrossValidationFoldsCount = -1,
                            IssuesData = issues,
                            RegressionTool = RegressionTool.FastTreeRegressor
                        });
                    }

                    
                    foreach (var issue in issuesToBePredicted)
                    {

                        double actualValue = 0.0;
                        double predictedValue = 0.0;
                        if (isIntegrated)
                        {
                            var predictionResult = timeToFixPredictor.Predict(issue, commits, trainingOutputData, commitsSets).Result;
                            actualValue = predictionResult.Data.TimeToFixInMinutes;
                            predictedValue = predictionResult.Prediction.TimeToFixInMinutes;
                        }
                        else
                        {
                            var predictionResult = timeToFixPredictor.Predict(issue, trainingOutputData).Result;
                            actualValue = predictionResult.Data.TimeToFixInMinutes;
                            predictedValue = predictionResult.Prediction.TimeToFixInMinutes;
                        }

                        if (actualValue == 0.0)
                        {
                            continue;
                        }

                        var acc = timeToFixPredictor.GetAccuracy(actualValue, predictedValue);
                        sb.AppendLine($"{counter++};{acc}");
                    }
                }
            }

            File.WriteAllText(filePath, sb.ToString());
            Process.Start(filePath);
        }

        public static void TestIssueReopenPredictions(bool isIntegrated, string filePath, string title = null)
        {
            var sb = new StringBuilder();
            if (title != null)
            {
                sb.AppendLine(title);
            }
            sb.AppendLine($"REOPEN ISSUE PREDICTION");
            sb.AppendLine($"[IsIntegrated]: {isIntegrated}");

            var issueReopenPredictor = new IssueReopenPredictor();
            var timeToFixPredictor = new TimeToFixPredictor();
            var devEfficiencyPredictor = new DeveloperEfficiencyPredictor();

            var overallPositives = 0;
            var overallNegatives = 0;
            var rnd = new Random();
            const int countOfTries = 5;
            sb.AppendLine($"[CountOfTests]: {countOfTries}");
            for (int i = 0; i < countOfTries; i++)
            {
                using (var session = DbService.Instance.SessionFactory.OpenSession())
                {
                    var issues = session.QueryOver<Issue>().List();
                    var commits = session.QueryOver<Commit>().List();
                    var commitsSets = session.QueryOver<CommitsSet>().List();

                    var issuesToBeTrained = (int)Math.Floor(issues.Count * 0.8);
                    var issuesToBePredictedCount = issues.Count - issuesToBeTrained;

                    var issuesToBePredicted = new List<Issue>();
                    for (int index = 0; index < issuesToBePredictedCount; index++)
                    {
                        var randIndex = rnd.Next(0, issues.Count);
                        if (issues[randIndex] != null)
                        {
                            issuesToBePredicted.Add(issues[randIndex]);
                            issues.RemoveAt(randIndex);
                        }
                    }

                    TrainingOutputData trainingOutputData = null;
                    if (isIntegrated)
                    {
                        trainingOutputData = issueReopenPredictor.TrainAndSavePredictionModel(
                            new ReopenTrainingIntegratedInputData()
                            {
                                CrossValidationFoldsCount = -1,
                                DataRebalancingTechnique = DataBalancingTechnique.Oversampling,
                                IssuesData = issues,
                                Commits = commits,
                                ClassificationTool = BinaryClassificationTool.FastForestBinaryClassifier,
                                PredictiveModelPath = Path.GetTempFileName(),
                                CommitsSets = commitsSets
                            });
                    }
                    else
                    {
                        trainingOutputData = issueReopenPredictor.TrainAndSavePredictionModel(new ReopenTrainingInputData()
                        {
                            PredictiveModelPath = Path.GetTempFileName(),
                            ClassificationTool = BinaryClassificationTool.FastForestBinaryClassifier,
                            CrossValidationFoldsCount = -1,
                            DataRebalancingTechnique = DataBalancingTechnique.Oversampling,
                            IssuesData = issues
                        });
                    }

                  
                    int positiveClassificationCount = 0;
                    int negativeClassificationCount = 0;
                    foreach (var issue in issuesToBePredicted)
                    {
                        
                        if (isIntegrated)
                        {
                            var predictionResult = issueReopenPredictor.Predict(issue, commits, trainingOutputData, commitsSets).Result;
                            if (predictionResult.Data.WasReopened == predictionResult.Prediction.IsGoingToBeReopened)
                            {
                                positiveClassificationCount++;
                            }
                            else
                            {
                                negativeClassificationCount++;
                            }
                        }
                        else
                        {
                            var predictionResult = issueReopenPredictor.Predict(issue, trainingOutputData).Result;
                            if (predictionResult.Data.WasReopened == predictionResult.Prediction.IsGoingToBeReopened)
                            {
                                positiveClassificationCount++;
                            }
                            else
                            {
                                negativeClassificationCount++;
                            }
                        }
                        
                       
                    }

                    overallNegatives += negativeClassificationCount;
                    overallPositives += positiveClassificationCount;

                    System.Console.WriteLine($"_______________________________________________");
                    System.Console.WriteLine($"TRY = {i + 1}");
                    System.Console.WriteLine($"POSITIVES = {positiveClassificationCount}");
                    System.Console.WriteLine($"NEGATIVES = {negativeClassificationCount}");
                    sb.AppendLine(
                        $"[{i + 1}]: positives={positiveClassificationCount}; negatives={negativeClassificationCount} |");



                }
            }

            sb.AppendLine("SUMMARY");
            sb.AppendLine($"Overall positives = {overallPositives}");
            sb.AppendLine($"Overall negatives = {overallNegatives}");

            System.Console.WriteLine($"_______________________________________________");
            System.Console.WriteLine($"___________SUMMARY____________");
            System.Console.WriteLine($"OVERALL POSITIVES = {overallPositives}");
            System.Console.WriteLine($"OVERALL NEGATIVES = {overallNegatives}");
            

            File.WriteAllText(filePath, sb.ToString());
            Process.Start(filePath);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using FluentNHibernate.Utils;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.MachineLearning.Predictors;
using RepositoryAnalyser.Reporting.Implementation;
using RepositoryAnalyser.VersionControl.Services;

namespace RepositoryAnalyser.Console.Tester
{
    
    class Program
    {
        static void Test(string path)
        {
            var sb = new StringBuilder();
            var rnd = new Random();
            int limit = 90000;
            for (int i = 0; i < limit; i++)
            {
                sb.AppendLine($"{i};{rnd.NextDouble()}");
            }

            File.WriteAllText(path,sb.ToString());
            Process.Start(path);
        }


        static void Main(string[] args)
        {
            var mlnetDbPath = @"C:\ProgramData\RepositoryAnalyser\Database\_machinelearning\RepositoryAnalyserData.sqlite";
            var mahappsDbPath = @"C:\ProgramData\RepositoryAnalyser\Database\_mahapps\RepositoryAnalyserData.sqlite";
            var jiraDbPath = @"C:\ProgramData\RepositoryAnalyser\Database\_jira_w_events\RepositoryAnalyserData.sqlite";

            var reopenNormalMLnetFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MLNET_ISSUEREOPEN_Normal.txt";
            var reopenIntegratedMLnetFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MLNET_ISSUEREOPEN_Integrated.txt";
            var reopenNormalMahappsFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MAHAPPS_ISSUEREOPEN_Normal.txt";
            var reopenIntegratedMahappsFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MAHAPPS_ISSUEREOPEN_Integrated.txt";
            var reopenNormalJiraFilePath = @"C:\Users\lsiel\Desktop\BADANIA\JIRA_ISSUEREOPEN_Normal.txt";

            var timeToFixNormalMlnetFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MLNET_TIMETOFIX_Normal.txt"; ;
            var timeToFixIntegratedMlnetFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MLNET_TIMETOFIX_Integrated.txt"; ;
            var timeToFixNormalMahappsFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MAHAPPS_TIMETOFIX_Normal.txt"; ;
            var timeToFixIntegratedMahappsFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MAHAPPS_TIMETOFIX_Integrated.txt"; ;
            var timeToFixJiraFilePath = @"C:\Users\lsiel\Desktop\BADANIA\JIRA_TIMETOFIX_Normal.txt";

            var devEfficiencyNormalMlnetFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MLNET_devEfficiency_Normal.txt"; ;
            var devEfficiencyIntegratedMlnetFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MLNET_devEfficiency_Integrated.txt"; ;
            var devEfficiencyNormalMahappsFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MAHAPPS_devEfficiency_Normal.txt"; ;
            var devEfficiencyIntegratedMahappsFilePath = @"C:\Users\lsiel\Desktop\BADANIA\MAHAPPS_devEfficiency_Integrated.txt"; ;
            var devEfficiencyJiraFilePath = @"C:\Users\lsiel\Desktop\BADANIA\JIRA_devEfficiency_Normal.txt";

/*            DbService.Instance.ChangeDataBaseLocation(mlnetDbPath, Path.GetDirectoryName(mlnetDbPath));
            PredictionTester.TestIssueReopenPredictions(false, reopenNormalMLnetFilePath, mlnetDbPath);
            PredictionTester.TestIssueReopenPredictions(true, reopenIntegratedMLnetFilePath, mlnetDbPath);
            PredictionTester.TestTimeToFIxPredictions(false, timeToFixNormalMlnetFilePath, mlnetDbPath);
            PredictionTester.TestTimeToFIxPredictions(true, timeToFixIntegratedMlnetFilePath, mlnetDbPath);
            PredictionTester.TestDeveloperEfficiencyPredictions(false, devEfficiencyNormalMlnetFilePath, mlnetDbPath);
            PredictionTester.TestDeveloperEfficiencyPredictions(true, devEfficiencyIntegratedMlnetFilePath, mlnetDbPath);

            DbService.Instance.ChangeDataBaseLocation(mahappsDbPath, Path.GetDirectoryName(mahappsDbPath));
            PredictionTester.TestIssueReopenPredictions(false, reopenNormalMahappsFilePath, mahappsDbPath);
            PredictionTester.TestIssueReopenPredictions(true, reopenIntegratedMahappsFilePath, mahappsDbPath);
            PredictionTester.TestTimeToFIxPredictions(false, timeToFixNormalMahappsFilePath, mahappsDbPath);
            PredictionTester.TestTimeToFIxPredictions(true, timeToFixIntegratedMahappsFilePath, mahappsDbPath);
            PredictionTester.TestDeveloperEfficiencyPredictions(false, devEfficiencyNormalMahappsFilePath, mahappsDbPath);
            PredictionTester.TestDeveloperEfficiencyPredictions(true, devEfficiencyIntegratedMahappsFilePath, mahappsDbPath);*/

            DbService.Instance.ChangeDataBaseLocation(jiraDbPath, Path.GetDirectoryName(jiraDbPath));
            //PredictionTester.TestIssueReopenPredictions(false, reopenNormalJiraFilePath, jiraDbPath);
            PredictionTester.TestDeveloperEfficiencyPredictions(false, devEfficiencyJiraFilePath, jiraDbPath);
            PredictionTester.TestTimeToFIxPredictions(false, timeToFixJiraFilePath, jiraDbPath);


            System.Console.ReadKey();
        }

        private static void ReportTest()
        {
            var reportsDirPath = @"C:\Users\lsiel\Desktop\raporty";
            var reportName = Guid.NewGuid().ToString() + ".pdf";
            var reportPath = Path.Combine(reportsDirPath, reportName);

            TestReopenPredictionReport(reportPath);


            Process.Start(reportPath);
        }

        private static IssueReopenIntegratedPredictionResult GetIssueReopenIntegratedPredictionResultMock()
        {
            var random = new Random();
            return new IssueReopenIntegratedPredictionResult()
            {
                OriginalKey = "TestIssue",
                Accuracy = random.NextDouble(),
                Data = new ReopenedIssueIntegratedData()
                {
                    Environment = "Win10",
                    Priority = "Critical",
                    AsigneeEmail = "test@gmail.com",
                    AttachmentsCount = random.Next(0, 10),
                    CommentsCount = random.Next(0, 30),
                    CommentsLenght = random.Next(0, 1000),
                    IsSubTask = 0,
                    ProjectName = "TestProject",
                    ReporterCommentsCount = random.Next(0, 22),
                    TimeSpent = random.Next(0, 70000),
                    ReporterEmail = "Test2@test.com",
                    Type = "Bug",
                    WasReopened = true,
                    Revision = "Multiple",
                    DeletedLines = random.Next(100, 20032123),
                    AddedLines = random.Next(100, 123213132),
                    CommitsCount = random.Next(123, 123123123),
                    CountOfModifiedFiles = random.Next(0, 500),
                    CommitsMessagesLength = random.Next(100, 900000),
                    CommitsMessagesCount = random.Next(4000, 123123123),


                },
                PredictionTime = TimeSpan.FromHours(2.1),
                ModelName = "BinaryTreeOrSomething",
                PredictionDate = DateTime.Now,
                Prediction = new ReopenedIssuePrediction()
                {
                    IsGoingToBeReopened = true
                },
                TrainingModel = new TrainingOutputData()
                {
                    DateTime = DateTime.Now,
                    FilePath = @"C:\Users\lsiel\Desktop\raporty",
                    CountOfRawDataRows = 12000,
                    CountOfFormattedDataRows = 8000,
                    Metrics = new ModelMetrics()
                    {
                        ClassificationMetrics = new AverageClassificationMetrics()
                        {
                            ConfusionMatirx = new ConfusionMatirx()
                            {
                                TruePositive = random.NextDouble() * 100,
                                TrueNegative = random.NextDouble() * 100,
                                FalseNegative = random.NextDouble() * 100,
                                FalsePositve = random.NextDouble() * 100,
                            },
                            F1Score = random.NextDouble(),
                            Accuracy = random.NextDouble() * 10,
                            Auc = random.NextDouble() * 10,
                            PositivePrecision = random.NextDouble(),
                            PositiveRecall = random.NextDouble(),
                            NegativeRecall = random.NextDouble(),
                            NegativePrecision = random.NextDouble(),
                            MatthewsCorrelationCoefficient = random.NextDouble(),
                        },
                    }

                }
            };
        }
        private static IssueReopenPredictionResult GetIssueReopenPredictionResultMock()
        {
            var random = new Random();
            return new IssueReopenPredictionResult()
            {
                OriginalKey = "TestIssue",
                Accuracy = random.NextDouble(),
                Data = new ReopenedIssueData()
                {
                    Environment = "Win10",
                    Priority = "Critical",
                    AsigneeEmail = "test@gmail.com",
                    AttachmentsCount = random.Next(0, 10),
                    CommentsCount = random.Next(0, 30),
                    CommentsLenght = random.Next(0, 1000),
                    IsSubTask = 0,
                    ProjectName = "TestProject",
                    ReporterCommentsCount = random.Next(0, 22),
                    TimeSpent = random.Next(0, 70000),
                    ReporterEmail = "Test2@test.com",
                    Type = "Bug",
                    WasReopened = true

                },
                PredictionTime = TimeSpan.FromHours(2.1),
                ModelName = "BinaryTreeOrSomething",
                PredictionDate = DateTime.Now,
                Prediction = new ReopenedIssuePrediction()
                {
                    IsGoingToBeReopened = true
                },
                TrainingModel = new TrainingOutputData()
                {
                    DateTime = DateTime.Now,
                    FilePath = @"C:\Users\lsiel\Desktop\raporty",
                    CountOfRawDataRows = 12000,
                    CountOfFormattedDataRows = 8000,
                    Metrics = new ModelMetrics()
                    {
                        ClassificationMetrics = new AverageClassificationMetrics()
                        {
                            ConfusionMatirx = new ConfusionMatirx()
                            {
                                TruePositive = random.NextDouble() * 100,
                                TrueNegative = random.NextDouble() * 100,
                                FalseNegative = random.NextDouble() * 100,
                                FalsePositve = random.NextDouble() * 100,
                            },
                            F1Score = random.NextDouble(),
                            Accuracy = random.NextDouble() * 10,
                            Auc = random.NextDouble() * 10,
                            PositivePrecision = random.NextDouble(),
                            PositiveRecall = random.NextDouble(),
                            NegativeRecall = random.NextDouble(),
                            NegativePrecision = random.NextDouble(),
                            MatthewsCorrelationCoefficient = random.NextDouble(),
                        },
                    }

                }
            };
        }

        private static TimeToFixIntegratedPredictionResult GetTimeToFixIntegratedPredictionResultMock()
        {
            var random = new Random();
            return new TimeToFixIntegratedPredictionResult()
            {
                OriginalKey = "TestIssue",
                Accuracy = random.NextDouble(),
                Data = new TimeToFixIntegratedData()
                {
                    Environment = "Win10",
                    Priority = "Critical",
                    AsigneeEmail = "test@gmail.com",
                    AttachmentsCount = random.Next(0, 10),
                    CommentsCount = random.Next(0, 30),
                    CommentsLenght = random.Next(0, 1000),
                    IsSubTask = 0,
                    ProjectName = "TestProject",
                    ReporterCommentsCount = random.Next(0, 22),
                    TimeSpent = random.Next(0, 70000),
                    ReporterEmail = "Test2@test.com",
                    Type = "Bug",
                    TimeToFixInMinutes = random.Next(10,9000),
                    Revision = "Multiple",
                    DeletedLines = random.Next(100, 20032123),
                    AddedLines = random.Next(100, 123213132),
                    CommitsCount = random.Next(123, 123123123),
                    CountOfModifiedFiles = random.Next(0, 500),
                    CommitsMessagesLength = random.Next(100, 900000),
                    CommitsMessagesCount = random.Next(4000, 123123123),


                },
                PredictionTime = TimeSpan.FromHours(2.1),
                ModelName = "BinaryTreeOrSomething",
                PredictionDate = DateTime.Now,
                Prediction = new TimeToFixPrediction()
                {
                    TimeToFixInMinutes = random.Next(10,9000)
                },
                TrainingModel = new TrainingOutputData()
                {
                    DateTime = DateTime.Now,
                    FilePath = @"C:\Users\lsiel\Desktop\raporty",
                    CountOfRawDataRows = 12000,
                    CountOfFormattedDataRows = 8000,
                    
                    Metrics = new ModelMetrics()
                    {
                        RegressionMetrics = new AverageRegressionMetrics()
                        {
                            LossFn = random.NextDouble(),
                            RSquared = random.NextDouble(),
                            L1 = random.NextDouble(),
                            L2 = random.NextDouble(),
                            Rms = random.NextDouble()
                        }
                    }

                }
            };
        }
        private static TimeToFixPredictionResult GetTimeToFixPredictionResultMock()
        {
            var random = new Random();
            return new TimeToFixPredictionResult()
            {
                OriginalKey = "TestIssue",
                Accuracy = random.NextDouble(),
                Data = new TimeToFixIssueData()
                {
                    Environment = "Win10",
                    Priority = "Critical",
                    AsigneeEmail = "test@gmail.com",
                    AttachmentsCount = random.Next(0, 10),
                    CommentsCount = random.Next(0, 30),
                    CommentsLenght = random.Next(0, 1000),
                    IsSubTask = 0,
                    ProjectName = "TestProject",
                    ReporterCommentsCount = random.Next(0, 22),
                    TimeSpent = random.Next(0, 70000),
                    ReporterEmail = "Test2@test.com",
                    Type = "Bug",
                    TimeToFixInMinutes = random.Next(10,9000)

                },
                PredictionTime = TimeSpan.FromHours(2.1),
                ModelName = "BinaryTreeOrSomething",
                PredictionDate = DateTime.Now,
                Prediction = new TimeToFixPrediction()
                {
                    TimeToFixInMinutes = random.Next(10,9000)
                },
                TrainingModel = new TrainingOutputData()
                {
                    DateTime = DateTime.Now,
                    FilePath = @"C:\Users\lsiel\Desktop\raporty",
                    CountOfRawDataRows = 12000,
                    CountOfFormattedDataRows = 8000,
                    Metrics = new ModelMetrics()
                    {
                        RegressionMetrics = new AverageRegressionMetrics()
                        {
                            LossFn = random.NextDouble(),
                            RSquared = random.NextDouble(),
                            L1 = random.NextDouble(),
                            L2 = random.NextDouble(),
                            Rms = random.NextDouble()
                        }
                    }

                }
            };
        }

        private static void TestReopenPredictionReport(string path)
        {
            var random = new Random();
            var numOfFolds = 10;
            
            //new TimeToFixPredictionResultPdfExporter().Export(GetTimeToFixPredictionResultMock(),path);
            //new IssueReopenedIntegratedPredictionResultPdfExporter().Export(GetIssueReopenIntegratedPredictionResultMock(),path);
            //new IssueReopenedPredictionResultPdfExporter().Export(GetIssueReopenPredictionResultMock(),path);
            //new TimeToFixIntegratedPredictionResultPdfExporter().Export(GetTimeToFixIntegratedPredictionResultMock(), path);
            //new DevEfficiencyPredictionResultPdfExporter().Export(GetDeveloperEfficiencyPredictionResultMock(),path);
            new DevEfficiencyIntegratedPredictionResultPdfExporter().Export(GetDeveloperEfficiencyIntegratedPredictionResultMock(),path);
            Process.Start(path);
        }

        private static DeveloperEfficiencyPredictionResult GetDeveloperEfficiencyPredictionResultMock()
        {
            var random = new Random();
            return new DeveloperEfficiencyPredictionResult()
            {
                Data = new DeveloperEfficiencyData()
                {
                    CountOfAssignedIssues       = random.Next(0,100),
                    CountOfReportedIssuesPerMonth = random.Next(0,100),
                    CountOfClosedIssuesPerMonth = random.Next(0,100),
                    Efficiency = (float)random.NextDouble() * 100,
                    DeveloperName = "Janusz",
                    CountOfReopenedIssuesPerMonth = random.Next(0,100),
                    CountOfOpenedIssuesPerMonth = random.Next(0,100),
                    DeveloperEmail = "Janusz@janusz.com"
                },
                Accuracy = random.NextDouble(),
                PredictionTime = TimeSpan.FromMinutes(random.Next(1,5)),
                OriginalKey = "Janusz",
                TrainingModel = new TrainingOutputData()
                {
                    DateTime = DateTime.Now,
                    FilePath = @"C:\Users\lsiel\Desktop\raporty",
                    CountOfRawDataRows = 12000,
                    CountOfFormattedDataRows = 8000,
                    Metrics = new ModelMetrics()
                    {
                        RegressionMetrics = new AverageRegressionMetrics()
                        {
                            LossFn = random.NextDouble(),
                            RSquared = random.NextDouble(),
                            L1 = random.NextDouble(),
                            L2 = random.NextDouble(),
                            Rms = random.NextDouble()
                        }
                    }

                },
                Prediction = new DeveloperEfficiencyPrediction()
                {
                    Efficiency = (float)random.NextDouble() * 100,
                    
                },
                PredictionDate = DateTime.Now,
                ModelName = "RegressorJanusza"
            };
        }

        private static DeveloperEfficiencyIntegratedPredictionResult GetDeveloperEfficiencyIntegratedPredictionResultMock()
        {
            var random = new Random();
            return new DeveloperEfficiencyIntegratedPredictionResult()
            {
                Data = new DeveloperEfficiencyIntegratedData()
                {
                    CountOfAssignedIssues = random.Next(0, 100),
                    CountOfReportedIssuesPerMonth = random.Next(0, 100),
                    CountOfClosedIssuesPerMonth = random.Next(0, 100),
                    Efficiency = (float)random.NextDouble() * 100,
                    DeveloperName = "Janusz",
                    CountOfReopenedIssuesPerMonth = random.Next(0, 100),
                    CountOfOpenedIssuesPerMonth = random.Next(0, 100),
                    DeveloperEmail = "Janusz@janusz.com",
                    CommitsCountPerMonth = (float)random.NextDouble() * 100,
                    DeletedLinesPerMonth = (float)random.NextDouble() * 100,
                    AddedLinesPerMonth = (float)random.NextDouble() * 100,

                },
                Accuracy = random.NextDouble(),
                PredictionTime = TimeSpan.FromMinutes(random.Next(1, 5)),
                OriginalKey = "Janusz",
                TrainingModel = new TrainingOutputData()
                {
                    DateTime = DateTime.Now,
                    FilePath = @"C:\Users\lsiel\Desktop\raporty",
                    CountOfRawDataRows = 12000,
                    CountOfFormattedDataRows = 8000,
                    Metrics = new ModelMetrics()
                    {
                        RegressionMetrics = new AverageRegressionMetrics()
                        {
                            LossFn = random.NextDouble(),
                            RSquared = random.NextDouble(),
                            L1 = random.NextDouble(),
                            L2 = random.NextDouble(),
                            Rms = random.NextDouble()
                        }
                    }

                },
                Prediction = new DeveloperEfficiencyPrediction()
                {
                    Efficiency = (float)random.NextDouble() * 100,

                },
                PredictionDate = DateTime.Now,
                ModelName = "RegressorJanusza"
            };
        }

        private static void TestTrainingRaports(string path)
        {
            var random = new Random();
            var numOfFolds = 10;
            var trainingOutputDataMock = new TrainingOutputData()
            {
                DateTime = DateTime.Now,
                FilePath = @"C:\Users\lsiel\Desktop\raporty",
                CountOfRawDataRows = 12000,
                CountOfFormattedDataRows = 8000,

                Metrics = new ModelMetrics()
                {
                    //ClassificationMetrics = new AverageClassificationMetrics()
                    //{
                    //    ConfusionMatirx = new ConfusionMatirx()
                    //    {
                    //        TruePositive = random.NextDouble() * 100,
                    //        TrueNegative = random.NextDouble() * 100,
                    //        FalseNegative = random.NextDouble() * 100,
                    //        FalsePositve = random.NextDouble() * 100,
                    //    },
                    //    F1Score = random.NextDouble(),
                    //    Accuracy = random.NextDouble() * 10,
                    //    Auc = random.NextDouble() * 10,
                    //    PositivePrecision = random.NextDouble(),
                    //    SubMetrics = new List<ClassificationMetrics>(),
                    //    PositiveRecall = random.NextDouble(),
                    //    NegativeRecall = random.NextDouble(),
                    //    NegativePrecision = random.NextDouble(),
                    //    MatthewsCorrelationCoefficient = random.NextDouble(),
                    //},
                    RegressionMetrics = new AverageRegressionMetrics()
                    {
                        L1 = random.NextDouble(),
                        L2 = random.NextDouble(),
                        LossFn = random.NextDouble(),
                        RSquared = random.NextDouble(),
                        Rms = random.NextDouble(),
                        SubMetrics = new List<RegressionMetrics>()
                    }
                }
            };
            for (int i = 0; i < numOfFolds; i++)
            {
                trainingOutputDataMock.Metrics.RegressionMetrics.SubMetrics.Add(new RegressionMetrics()
                {
                    L1 = random.NextDouble(),
                    L2 = random.NextDouble(),
                    LossFn = random.NextDouble(),
                    RSquared = random.NextDouble(),
                    Rms = random.NextDouble(),
                });

                //trainingOutputDataMock.Metrics.ClassificationMetrics.SubMetrics.Add(new ClassificationMetrics()
                //{
                //    ConfusionMatirx = new ConfusionMatirx()
                //    {
                //        TruePositive = random.NextDouble() * 100,
                //        TrueNegative = random.NextDouble() * 100,
                //        FalseNegative = random.NextDouble() * 100,
                //        FalsePositve = random.NextDouble() * 100,
                //    },
                //    F1Score = random.NextDouble(),
                //    Accuracy = random.NextDouble() * 10,
                //    Auc = random.NextDouble() * 10,
                //    PositivePrecision = random.NextDouble(),
                //    PositiveRecall = random.NextDouble(),
                //    NegativeRecall = random.NextDouble(),
                //    NegativePrecision = random.NextDouble(),
                //    MatthewsCorrelationCoefficient = random.NextDouble(),

                //});
            }

            var exporter = new TrainingOutputDataPdfExporter();
            exporter.Export(trainingOutputDataMock, path);
        }
    }
}

﻿using NUnit.Framework;

namespace RepositoryAnalyser.DataBaseTests.Tests
{
    [TestFixture]
    public class IssueTrackingTest
    {
        /*[Test]
        public void TestJiraParsing()
        {
            var jiraUrl = "https://raynet.atlassian.net";
            var connectionData = new ConnectionData()
            {
                Url = jiraUrl,
                Credentials = new Credentials()
                {
                    Password = "asdasd",
                    Username = "l.sielewicz@raynet.de"
                }
            };

            var jiraParser = new JiraIssuesParser();
            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = jiraParser.Parse(connectionData).Result;
            stopwach.Stop();
            
            Assert.IsNotNull(result);
        }

        [Test]
        public void TestGitubParsing()
        {
            var jiraUrl = "https://github.com/lsielewicz/HomeBudgetViewer";
            var connectionData = new ConnectionData()
            {
                Url = jiraUrl,
                Credentials = new Credentials()
                {
                    Password = "v",
                    Username = "cxv"
                }
            };

            var parser = new GithubIssuesParser();
            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = parser.Parse(connectionData).Result;
            stopwach.Stop();

            Assert.IsNotNull(result);
        }

        [Test]
        public void TestGithubIssueParsing()
        {
            var jiraUrl = "https://github.com/octokit/octokit.net/issues/1800";
            var connectionData = new ConnectionData()
            {
                Url = jiraUrl,
                Credentials = new Credentials()
                {
                    Password = "asdsad",
                    Username = "asdsad"
                }
            };

            var parser = new GithubIssuesParser();
            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = parser.GetIssue(connectionData).Result;
            stopwach.Stop();

            Assert.IsNotNull(result);
        }

        [Test]
        public void TestGithubUserParsing()
        {
            var jiraUrl = "https://github.com/lsielewicz";
            var connectionData = new ConnectionData()
            {
                Url = jiraUrl,
                Credentials = new Credentials()
                {
                    Password = "zzz",
                    Username = "zzz"
                }
            };

            var parser = new GithubIssuesParser();
            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = parser.GetUser(connectionData).Result;
            stopwach.Stop();

            Assert.IsNotNull(result);
        }/*l.sielewicz#1#

        [Test]
        public void TestJiraUserParsing()
        {
            var jiraUrl = "https://raynet.atlassian.net";
            var connectionData = new ConnectionData()
            {
                Url = jiraUrl,
                Credentials = new Credentials()
                {
                    Username = "l.hg@raynet.de",
                    Password = "ghj"
                }
            };

            var parser = new JiraIssuesParser();
            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = parser.GetUser(connectionData,"l.sielewicz").Result;
            stopwach.Stop();

            Assert.IsNotNull(result);
        }/*l.sielewicz#1#

        [Test]
        public void TestJiraIssueParsing()
        {
            var jiraUrl = "https://raynet.atlassian.net/browse/RVL-126";
            var connectionData = new ConnectionData()
            {
                Url = jiraUrl,
                Credentials = new Credentials()
                {
                    Password = "Dwakrakersy7",
                    Username = "l.sielewicz@raynet.de"
                }
            };

            var parser = new JiraIssuesParser();
            var stopwach = new Stopwatch();
            stopwach.Start();
            var result = parser.GetIssue(connectionData).Result;
            stopwach.Stop();

            Assert.IsNotNull(result);
        }*/
    }
}

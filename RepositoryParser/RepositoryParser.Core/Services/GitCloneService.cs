﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using LibGit2Sharp;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.VersionControl.Interfaces;
using RepositoryAnalyser.VersionControl.Models;

namespace RepositoryAnalyser.VersionControl.Services
{
    public class GitCloneService : IGitCloneService
    {
        private List<GitCloneBranch> _branches;
        private List<string> _remotes;

        public string GetRepositoryNameFromUrl(string url)
        {
            string repositoryName = url;
            string rg = @"(.*\/)(.*)\.git";
            Regex regex = new Regex(rg);
            Match match = regex.Match(url);
            if (match.Success)
            {
                if (match.Groups.Count >= 3)
                {
                    repositoryName = match.Groups[2].Value;
                }
            }

            return repositoryName;
        }

        public string CloneRepository(GitCloningData cloningData)
        {
            const string gitExtension = ".git";

            if (!cloningData.Url.EndsWith(gitExtension))
                cloningData.Url += gitExtension;

            string repositoryPath = string.IsNullOrEmpty(cloningData.TargetPath)
                ? "./" + GetRepositoryNameFromUrl(cloningData.Url)
                : ZetaLongPaths.ZlpPathHelper.Combine(cloningData.TargetPath, GetRepositoryNameFromUrl(cloningData.Url));

            if (!Directory.Exists(repositoryPath))
                Directory.CreateDirectory(repositoryPath);
            else
                DeleteDirectory(repositoryPath, true);

            if (cloningData.CloneType == RepositoryCloneType.Public)
            {
                Repository.Clone(cloningData.Url, repositoryPath);
            }
            else
            {
                Repository.Clone(cloningData.Url, repositoryPath, new CloneOptions()
                {
                    CredentialsProvider = (url, dd, a) => new UsernamePasswordCredentials()
                    {
                        Username = cloningData.Credentials.Username,
                        Password = cloningData.Credentials.Password
                    }
                });
            }

            if (cloningData.IncludeAllBranches)
            {
                this.FillBranches(cloningData);
                this.CloneAllBranches(cloningData);
            }

            return repositoryPath;
        }

        public RepositoryCloneType CheckRepositoryCloneType(string path)
        {
            const string gitPathEnding = ".git";
            if (!path.EndsWith(gitPathEnding))
            {
                path += gitPathEnding;
            }

            try
            {
                var references = Repository.ListRemoteReferences(path);

                return references == null ? RepositoryCloneType.Private : RepositoryCloneType.Public;
            }
            catch
            {
                return RepositoryCloneType.Private;
            }    
        }

        private void FillBranches(GitCloningData cloningData)
        {
            _branches = new List<GitCloneBranch>();
            _remotes = new List<string>();
            try
            {
                string repositoryPath = string.IsNullOrEmpty(cloningData.TargetPath)
                    ? "./" + GetRepositoryNameFromUrl(cloningData.Url)
                    : ZetaLongPaths.ZlpPathHelper.Combine(cloningData.TargetPath, GetRepositoryNameFromUrl(cloningData.Url));

                using (Repository repository = new Repository(repositoryPath))
                {
                    List<GitCloneBranch> tempBranches = new List<GitCloneBranch>();
                    foreach (Branch branch in repository.Branches)
                    {
                        GitCloneBranch temp = new GitCloneBranch();
                        if (!branch.IsRemote || branch.IsCurrentRepositoryHead || branch.FriendlyName == "origin/master" ||
                            branch.FriendlyName == "origin/HEAD")
                        {
                            if (!branch.IsRemote)
                                _remotes.Add(branch.FriendlyName);
                            continue;
                        }
                        // Display the branch name
                        string tempName = branch.FriendlyName;

                        if (tempName.Contains("origin"))
                        {
                            tempName = tempName.Remove(0, 7);
                        }
                        temp.BranchName = tempName;
                        temp.OriginName = branch.FriendlyName;
                        tempBranches.Add(temp);
                    }

                    tempBranches.ForEach(branch =>
                    {
                        if (!IsBranchCloned(_remotes, branch.BranchName))
                        {
                            _branches.Add(branch);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
        }

        private bool IsBranchCloned(List<string> remotes, string branch)
        {
            int index = remotes.FindIndex(x => x == branch);
            if (index >= 0)
                return true;
            else
                return false;
        }

        private void CloneAllBranches(GitCloningData cloningData)
        {
            if (_branches == null || _branches.Count == 0)
                return;

            string repositoryPath = string.IsNullOrEmpty(cloningData.TargetPath)
                               ? "./" + GetRepositoryNameFromUrl(cloningData.Url)
                               : ZetaLongPaths.ZlpPathHelper.Combine(cloningData.TargetPath, GetRepositoryNameFromUrl(cloningData.Url));
            using (Repository repository = new Repository(repositoryPath))
            {
                foreach (GitCloneBranch branch in _branches)
                {
                    Branch remoteBranch = repository.Branches[branch.OriginName];
                    Branch newLoaclBranch = repository.CreateBranch(branch.BranchName, remoteBranch.Tip);

                    repository.Branches.Update(newLoaclBranch, b => b.TrackedBranch = remoteBranch.CanonicalName);
                }
            }

        }
        private void DeleteDirectory(string path, bool recursive)
        {
            if (recursive)
            {
                var subfolders = Directory.GetDirectories(path);
                foreach (var s in subfolders)
                {
                    DeleteDirectory(s, true);
                }
            }
            var files = Directory.GetFiles(path);
            foreach (var f in files)
            {
                var attr = File.GetAttributes(f);
                if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    File.SetAttributes(f, attr ^ FileAttributes.ReadOnly);

                File.Delete(f);
            }
            Directory.Delete(path);
        }

    }
}

﻿using System;
using System.Net;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.VisualStudio.Services.Client;
using Microsoft.VisualStudio.Services.Common;
using WindowsCredential = Microsoft.VisualStudio.Services.Common.WindowsCredential;

namespace RepositoryAnalyser.VersionControl.Services
{
    public class TfsSystemParser
    {
        public void Test()
        {
            const string tfsURL = "hurl/tfs";

            var username = @"l.sielewicz";
            var username1 = @"l.sielewicz@raynet.de";
            var password = "abcd";
            var netCred = new NetworkCredential(username1,password);
            var winCred = new WindowsCredential(netCred);

            var vssCred = new VssClientCredentials(winCred)
            {
                PromptType = CredentialPromptType.DoNotPrompt,            
            };

            var tpc = new TfsTeamProjectCollection(new Uri(tfsURL),vssCred);
            tpc.EnsureAuthenticated();
            var vcs = (VersionControlServer)tpc.GetService(typeof(VersionControlServer));

        }
    }
}

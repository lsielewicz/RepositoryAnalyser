﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using RepositoryAnalyser.DataBase.Configuration;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.VersionControl.Interfaces;
using SharpSvn;
using Branch = RepositoryAnalyser.Domain.Entities.VersionControl.Branch;
using Commit = RepositoryAnalyser.Domain.Entities.VersionControl.Commit;
using Repository = RepositoryAnalyser.Domain.Entities.VersionControl.Repository;

namespace RepositoryAnalyser.VersionControl.Services
{
    public class SvnSystemParser : ISvnSystemParser
    {
        private string _localPath;

        public Repository Parse(string localPath)
        {
            localPath = CheckPath(localPath);
            _localPath = localPath;
            Repository repository = new Repository()
            {
                Name = GetNameFromPath(localPath),
                Type = RepositoryType.Svn
            };
            List<Branch> branches = GetBranches(localPath);
            branches.ForEach(branch=> repository.AddBranch(branch));

            return repository;
        }

        private List<Branch> GetBranches(string path)
        {
            List<Branch> branches = new List<Branch>();
            string branchesPath;
            if (path.EndsWith("/"))
                branchesPath = path + "branches";
            else
                branchesPath = path + "/branches";
            //key-key : path, key-value : fullPath, value: count of revisions
            Dictionary<KeyValuePair<string,string>, int> branchesPathes = new Dictionary<KeyValuePair<string,string>, int>();
            using (SvnClient svnClient = new SvnClient())
            {
                Collection<SvnListEventArgs> contents;
                if (svnClient.GetList(new Uri(branchesPath), out contents))
                {
                    foreach (var content in contents)
                    {
                        string name = !string.IsNullOrEmpty(content.Path) ? content.Path : "trunk";
                        string fullPath = ZetaLongPaths.ZlpPathHelper.Combine(branchesPath, content.Path);
                        branchesPathes.Add(new KeyValuePair<string, string>(name, fullPath), GetCountOfCommitsInBranch(fullPath));
                    }
                }

                Dictionary<KeyValuePair<string, string>, int> orderedPathes =
                    branchesPathes.OrderByDescending(p => p.Value)
                        .ToDictionary(p => new KeyValuePair<string, string>(p.Key.Key, p.Key.Value), p => p.Value);
                if (orderedPathes.Any())
                {
                    string headPath = orderedPathes.First().Key.Value;
                    List<Commit> headCommits = GetCommits(headPath);
                    foreach (var branch in branchesPathes)
                    {
                        List<Commit> commitItems = GetCommits(branch.Key.Value, headCommits);
                        Branch branchItem = new Branch()
                        {
                            Name = branch.Key.Key,
                            Path = branch.Key.Value
                        };
                        commitItems.ForEach(commitItem=>branchItem.AddCommit(commitItem));
                        branches.Add(branchItem);
                    }
                }
            }

            return branches;
        }

        private int GetCountOfCommitsInBranch(string path)
        {
            using (SvnClient svnClient = new SvnClient())
            {
                Collection<SvnLogEventArgs> logEventArgs;
                svnClient.GetLog(new Uri(path), out logEventArgs);
                if (logEventArgs != null)
                    return logEventArgs.Count;
                return 0;
            }
        }

        private List<Commit> GetCommits(string path, List<Commit> alreadyDeclaredCommits = null)
        {
            List<Commit> commits = new List<Commit>();
            using (SvnClient svnClient = new SvnClient())
            {
                System.Collections.ObjectModel.Collection<SvnLogEventArgs> logEventArgs;
                svnClient.GetLog(new Uri(path), out logEventArgs);
                foreach (var commit in logEventArgs)
                {
                    if (alreadyDeclaredCommits != null &&
                        alreadyDeclaredCommits.Any(
                            declaredCommit => declaredCommit.Revision == commit.Revision.ToString()))
                    {
                        commits.Add(alreadyDeclaredCommits.Find(c=>c.Revision == commit.Revision.ToString()));
                        continue;
                    }
                    List<Changes> changes = GetChanges(commit.Revision, path);
                    Commit commitItem = new Commit()
                    {
                        Author = commit.Author,
                        Date = commit.Time.Date,
                        Email = " - ",
                        Message = commit.LogMessage,
                        Revision = commit.Revision.ToString()
                    };
                    changes.ForEach(change=>commitItem.AddChanges(change));
                    commits.Add(commitItem);
                }
            }

            return commits;
        }

        private List<Changes> GetChanges(long revision, string path)
        {
            List<Changes> changes = new List<Changes>();
            using (SvnClient svnClient = new SvnClient())
                svnClient.Log(
                    new Uri(path),
                    new SvnLogArgs
                    {
                        Range = new SvnRevisionRange(revision, revision - 1)
                    },
                    (o, e) =>
                    {
                        foreach (var change in e.ChangedPaths)
                        {
                            Changes item = new Changes()
                            {
                                ChangeContent = GetPatchOfChange(revision,change.Path),
                                Path = change.Path,
                                Type = SvnChangeActionToChangeType(change.Action)
                            };

                            changes.Add(item);
                        }
                    });
            
            return changes;
        }

        private string GetNameFromPath(string path)
        {
            string output = path;
            if (output.EndsWith(@"/") || output.EndsWith(@"\"))
                output = output.Remove(output.Length - 1, 1);
            string pattern = @".*(\/|\\).*(\/|\\)([a-zA-Z0-9]*)";
            Regex r = new Regex(pattern);
            Match m = r.Match(output);
            if (m.Success)
            {
                if (m.Groups.Count >= 3)
                {
                    output = m.Groups[3].Value;
                }
            }
            return output;
        }

        private string GetPatchOfChange(long revision, string path)
        {
            if (revision == 1)
                return "";
            path = _localPath + path;

            var range = new SvnRevisionRange(revision - 1, revision);
            MemoryStream diffResult = new MemoryStream();
            string theFile = String.Empty;
            int counter = 0;
            bool descFlag = false;
            using (SvnClient client = new SvnClient())
            {
                if (client.Diff(new SvnUriTarget(path), range, diffResult))
                {
                    diffResult.Position = 0;
                    StreamReader strReader = new StreamReader(diffResult);
                    string diff = strReader.ReadToEnd();
                    diff = diff.Insert(diff.Length, "\0");
                    if (diff.Length >= 50000)
                    {
                        double size = diff.Length / 500;
                        return String.Format("File content: {0} kb", size);
                    }
                    foreach (char c in diff)
                    {
                        counter++;
                        if (c != '@' && descFlag == false) //getting past the first description part
                            continue;
                        else if (c == '@' && descFlag == false) //we know we are towards the end of it
                        {
                            if (diff.Substring(counter, 1) == "\n" || diff.Substring(counter, 1) == "\r") //at the end of the line with the '@' symbols
                            {
                                descFlag = true;
                            }
                            else
                                continue;
                        }
                        else if (descFlag == true) //now reading the actual file
                        {
                            theFile += diff.Substring(counter);
                            break;
                        }
                    }
                }
            }
            return theFile;
        }

        private string SvnChangeActionToChangeType(SvnChangeAction changeAction)
        {
            switch (changeAction)
            {
                case SvnChangeAction.Add:
                    return ChangeType.Added;
                case SvnChangeAction.Modify:
                    return ChangeType.Modified;
                case SvnChangeAction.Delete:
                    return ChangeType.Deleted;
            }

            return ChangeType.Unmodified;
        }

        private string CheckPath(string path)
        {
            using (var client = new SvnClient())
            {
                var checker = client.GetUriFromWorkingCopy(path);
                if (checker != null)
                {
                    return checker.ToString();
                }
            }
            return path;

        }
    }
}

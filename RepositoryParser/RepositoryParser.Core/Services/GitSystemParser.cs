﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using LibGit2Sharp;
using RepositoryAnalyser.DataBase.Configuration;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.VersionControl.Interfaces;
using RepositoryAnalyser.VersionControl.Models;
using Branch = RepositoryAnalyser.Domain.Entities.VersionControl.Branch;
using Commit = RepositoryAnalyser.Domain.Entities.VersionControl.Commit;
using GitRepository = LibGit2Sharp.Repository;
using GitBranch = LibGit2Sharp.Branch;
using GitCommit = LibGit2Sharp.Commit;
using Repository = RepositoryAnalyser.Domain.Entities.VersionControl.Repository;

namespace RepositoryAnalyser.VersionControl.Services
{
    public class GitSystemParser : IGitSystemParser
    {
        private readonly IGitCloneService _gitCloneService;

        public GitSystemParser(IGitCloneService gitCloneService)
        {
            _gitCloneService = gitCloneService;
        }

        public Repository Parse(GitCloningData cloningData)
        {
            var localPath = _gitCloneService.CloneRepository(cloningData);
            return this.Parse(localPath);
        }

        public Repository Parse(string localPath)
        {
            var repository = new Repository();
            using (var gitRepositoryInfo = new GitRepository(localPath))
            {
                var branches = GetBranches(localPath);

                repository.Type = RepositoryType.Git;
                repository.Name = GetNameFromPath(gitRepositoryInfo.Info.Path);
                repository.Url = localPath;

                branches.ForEach(branch => repository.AddBranch(branch));
            }
            return repository;
        }


        private List<Commit> GetCommits(GitBranch branch, string repositoryPath)
        {
            List<Commit> commits = new List<Commit>();
            foreach (GitCommit gitCommitInfo in branch.Commits)
            {
                Commit commit = GetCommitFromGitCommit(gitCommitInfo, repositoryPath);
                commits.Add(commit);
            }
            return commits;
        }

        private List<Changes> GetChanges(GitCommit commit, string repositoryPath)
        {
            List<Changes> changes = new List<Changes>();
            using (GitRepository gitRepositoryInfo = new GitRepository(repositoryPath))
            {
                bool isInitial = false;
                var firstCommit = commit.Parents.FirstOrDefault();
                if (firstCommit == null)
                {
                    isInitial = true;
                }
                Tree rootCommitTree = gitRepositoryInfo.Lookup<GitCommit>(commit.Id.ToString()).Tree;
                Patch patch;
                if (!isInitial)
                {
                    Tree commitTreeWithUpdatedFile =
                        gitRepositoryInfo.Lookup<GitCommit>(commit.Parents.FirstOrDefault().Sha).Tree;
                    patch = gitRepositoryInfo.Diff.Compare<Patch>(commitTreeWithUpdatedFile, rootCommitTree);
                }
                else
                {
                    patch = gitRepositoryInfo.Diff.Compare<Patch>(null, rootCommitTree);
                }
                foreach (var change in patch)
                {
                    changes.Add(new Changes()
                    {
                        Type = ConvertChangeKindToChangeType(change.Status),
                        Path = change.Path,
                        ChangeContent = change.Patch
                    });
                }

            }
            return changes;
        }

        private string GetNameFromPath(string path)
        {
            string output = path;
            string pattern = @"(.*)\\(.*)\\.git";
            Regex r = new Regex(pattern);
            Match m = r.Match(path);
            if (m.Success)
            {
                if (m.Groups.Count >= 2)
                {
                    output = m.Groups[2].Value;
                }
            }
            return output;
        }

        private List<Branch> GetBranches(string repositoryPath)
        {
            List<Branch> branches = new List<Branch>();
            using (GitRepository gitRepositoryInfo = new GitRepository(repositoryPath))
            {
                GitBranch headBranch = null;
                foreach (var branch in gitRepositoryInfo.Branches)
                {
                    if (branch.IsRemote)
                        continue;
                    if (headBranch == null || headBranch.Commits.Count() < branch.Commits.Count())
                        headBranch = branch;
                }

                List<Commit> commits = GetCommits(headBranch, repositoryPath);

                foreach (var branch in gitRepositoryInfo.Branches)
                {
                    if (branch.IsRemote)
                        continue;
                    Branch branchInstance = new Branch() { Name = branch.FriendlyName };

                    var uniqueCommits = branch.Commits.Where(c => commits.All(c1 => c1.Revision != c.Sha));
                    var enumerable = uniqueCommits as IList<GitCommit> ?? uniqueCommits.ToList();
                    if (enumerable.Any())
                    {
                        foreach (var uCommit in enumerable)
                        {
                            var newCommit = GetCommitFromGitCommit(uCommit, repositoryPath);
                            branchInstance.AddCommit(newCommit);
                        }
                    }

                    commits.ForEach(commit =>
                    {
                        if (branch.Commits.Any(c => c.Sha == commit.Revision))
                        {
                            branchInstance.AddCommit(commit);
                        }
                    });
                    branches.Add(branchInstance);
                }
            }
            return branches;
        }

        private Commit GetCommitFromGitCommit(GitCommit gitCommit, string repositoryPath)
        {
            List<Changes> changes = GetChanges(gitCommit, repositoryPath);
            Commit newCommit = new Commit()
            {
                Revision = gitCommit.Sha,
                Author = gitCommit.Author.Name,
                Date = gitCommit.Author.When.DateTime,
                Email = gitCommit.Author.Email,
                Message = gitCommit.MessageShort
            };
            changes.ForEach(change => newCommit.AddChanges(change));
            return newCommit;
        }

        private string ConvertChangeKindToChangeType(ChangeKind changeKind)
        {
            switch (changeKind)
            {
                case ChangeKind.Added:
                    return ChangeType.Added;
                case ChangeKind.Deleted:
                    return ChangeType.Deleted;
                case ChangeKind.Unmodified:
                    return ChangeType.Unmodified;
            }

            return ChangeType.Modified;
        }
    }

}

﻿using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Domain.Models;

namespace RepositoryAnalyser.VersionControl.Models
{
    public class GitCloningData
    {
        public string Url { get; set; }
        public string TargetPath { get; set; }
        public bool IncludeAllBranches { get; set; }
        public RepositoryCloneType CloneType { get; set; }
        public Credentials Credentials { get; set; }

    }
}

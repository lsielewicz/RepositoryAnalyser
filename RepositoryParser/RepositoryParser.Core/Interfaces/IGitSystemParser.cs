﻿using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.VersionControl.Models;

namespace RepositoryAnalyser.VersionControl.Interfaces
{
    public interface IGitSystemParser : IVersionControlSystemParser
    {
        Repository Parse(GitCloningData cloningData);
    }
}
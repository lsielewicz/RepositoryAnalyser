﻿using RepositoryAnalyser.Domain.Entities.VersionControl;

namespace RepositoryAnalyser.VersionControl.Interfaces
{
    public interface IVersionControlSystemParser
    {
        Repository Parse(string localPath);
    }
}
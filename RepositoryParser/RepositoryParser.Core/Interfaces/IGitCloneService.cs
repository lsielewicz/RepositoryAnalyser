﻿using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.VersionControl.Models;

namespace RepositoryAnalyser.VersionControl.Interfaces
{
    public interface IGitCloneService
    {
        string GetRepositoryNameFromUrl(string url);
        RepositoryCloneType CheckRepositoryCloneType(string path);
        string CloneRepository(GitCloningData cloningData);
    }
}
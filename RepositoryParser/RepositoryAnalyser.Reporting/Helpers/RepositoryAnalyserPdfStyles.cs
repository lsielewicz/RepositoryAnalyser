﻿using System.Collections.Generic;
using MigraDoc.DocumentObjectModel;

namespace RepositoryAnalyser.Reporting.Helpers
{
    internal static class RepositoryAnalyserPdfStyles
    {
        public const string NormalKey = "RepositoryAnalyser_Normal";
        public const string TitleKey = "RepositoryAnalyser_Title";
        public const string TableContentKey = "RepositoryAnalyser_TableContent";
        public const string Header3Key = "RepositoryAnalyser_Header3";
        public const string Header2Key = "RepositoryAnalyser_Header2";
        public const string Header1Key = "RepositoryAnalyser_Header1";

        public const string FontName = "Verdana";
        public const string NormalBuiltInStyleName = "Normal";

        public static Color AppGreen = new Color(255,26,71,42);
        public static Color AppGray = new Color(255,102,102,102);

        public static IList<Style> GetStylesCollection()
        {
            var styles = new List<Style>()
            {
                new Style(TableContentKey, NormalBuiltInStyleName)
                {
                    ParagraphFormat = new ParagraphFormat()
                    {
                        Font = new Font(FontName, 9),
                        Alignment = ParagraphAlignment.Center,
                        SpaceAfter = "0.25cm",
                        SpaceBefore = "0.25cm",
                        Borders = new Borders()
                        {
                            Color = Colors.Black,
                            Width = 0.25,
                        }
                    }

                },

                new Style(Header1Key, NormalBuiltInStyleName)
                {
                    ParagraphFormat = new ParagraphFormat()
                    {
                        Alignment = ParagraphAlignment.Left,
                        Font = new Font(FontName, 16)
                        {
                            Color = AppGreen
                        },
                        SpaceBefore = "1cm",
                        SpaceAfter = "1cm"
                    },
                },

                new Style(Header2Key, NormalBuiltInStyleName)
                {
                    ParagraphFormat = new ParagraphFormat()
                    {
                        Alignment = ParagraphAlignment.Left,
                        Font = new Font(FontName, 14)
                        {
                            Color = AppGray
                        },
                        SpaceBefore = "1cm",
                        SpaceAfter = "1cm",
                        LeftIndent = "0.5cm"
                    },
                },


                new Style(Header3Key, NormalBuiltInStyleName)
                {
                    ParagraphFormat = new ParagraphFormat()
                    {
                        Alignment = ParagraphAlignment.Left,
                        Font = new Font(FontName, 12)
                        {
                            Color = AppGray
                        },
                        SpaceBefore = "0.5cm",
                        SpaceAfter = "0.5cm",
                        LeftIndent = "1.0cm"
                    },
                },

                new Style(NormalKey, NormalBuiltInStyleName)
                {
                    
                    ParagraphFormat = new ParagraphFormat()
                    {
                        Alignment = ParagraphAlignment.Left,
                        Font = new Font(FontName, 10)
                        {
                            Color = Colors.Black
                        },
                        SpaceBefore = "0.25cm",
                        LeftIndent = "1cm"
                    },
                },

                new Style(Header1Key, NormalBuiltInStyleName)
                {
                    ParagraphFormat = new ParagraphFormat()
                    {
                        Alignment = ParagraphAlignment.Left,
                        Font = new Font(FontName, 16)
                        {
                            Color = Colors.Black
                        },
                    },
                },

            };

           



            return styles;
        }

    }
}

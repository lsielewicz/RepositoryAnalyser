﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;

namespace RepositoryAnalyser.Reporting.Helpers
{
    internal static class PdfRenderingHelper
    {
        public static void RenderDocument(Document document, string path)
        {
            var pdfRenderer = new PdfDocumentRenderer(true)
            {
                Document = document
            };

            pdfRenderer.RenderDocument();
            pdfRenderer.PdfDocument.Save(path);
        }
    }
}

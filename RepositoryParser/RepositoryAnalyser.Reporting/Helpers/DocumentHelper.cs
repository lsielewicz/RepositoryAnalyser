﻿using System;
using System.Text;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using RepositoryAnalyser.Reporting.Domain;

namespace RepositoryAnalyser.Reporting.Helpers
{
    internal static class DocumentHelper
    {
        public static string ToPrettyFormat(this TimeSpan span)
        {

            if (span == TimeSpan.Zero) return "0 minutes";

            var sb = new StringBuilder();
            if (span.Days > 0)
                sb.AppendFormat("{0} day{1} ", span.Days, span.Days > 1 ? "s" : string.Empty);
            if (span.Hours > 0)
                sb.AppendFormat("{0} hour{1} ", span.Hours, span.Hours > 1 ? "s" : string.Empty);
            if (span.Minutes > 0)
                sb.AppendFormat("{0} minute{1} ", span.Minutes, span.Minutes > 1 ? "s" : string.Empty);
            

            if (span.Days == 0 && span.Hours == 0 && span.Minutes == 0)
            {
                if (span.Seconds > 0)
                {
                    sb.AppendFormat("{0} second{1} ", span.Seconds, span.Seconds > 1 ? "s" : string.Empty);
                }           
                if (span.Milliseconds > 0)
                {
                    sb.AppendFormat("{0} millisecond{1} ", span.Milliseconds, span.Milliseconds > 1 ? "s" : string.Empty);
                }
            }

            return sb.ToString();

        }

        public static Table CreateKeyValueTable(ref TextFrame frame)
        {
            frame.AddParagraph(Environment.NewLine);
            var table = frame.AddTable();

            table.Format.Alignment = ParagraphAlignment.Left;
            table.Format.SpaceAfter = "0.0cm";
            table.Format.SpaceBefore = "0.0cm";
            table.Format.LineSpacing = "0.3cm";
            
            table.Borders.Color = Colors.Black;
            table.Borders.Width = 0.25;
            

            var keyColumn = table.Columns.AddColumn();
            keyColumn.Width = "6cm";
            var valueColumn = table.Columns.AddColumn();
            valueColumn.Width = "9cm";

            return table;
        }

        public static Row AddValueToKeyValueTable(ref Table table, string key, string value)
        {
            var row = table.AddRow();
            row[0].Shading = new Shading()
            {
                Color = RepositoryAnalyserPdfStyles.AppGray,
            };

            var keyCell = row[0].AddParagraph(key);
            keyCell.Format.SpaceBefore = "0.125cm";
            keyCell.Format.SpaceAfter = "0.125cm";
            keyCell.Format.Font = new Font("Verdana", 10) { Color = Colors.White };
            
            var valueCell = row[1].AddParagraph(value);
            valueCell.Format.SpaceBefore = "0.125cm";
            valueCell.Format.SpaceAfter = "0.125cm";
            return row;
        }


        public static Table CreateTable(ref TextFrame frame)
        {
            var table = frame.AddTable();

            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.SpaceAfter = "0.1cm";
            table.Format.SpaceBefore = "0.1cm";
            table.Borders.Color = Colors.Black;
            table.Borders.Width = 0.25;

            return table;
        }

        public static Paragraph AppendText(ref TextFrame frame, string text, string styleKey = RepositoryAnalyserPdfStyles.NormalKey)
        {
            var paragraph = frame.AddParagraph(text);
            paragraph.Style = styleKey;
            return paragraph;
        }

        public static Paragraph AppendText(ref TextFrame frame, string text, Color color, bool isBold = false)
        {
            var paragraph = frame.AddParagraph(text);
            paragraph.Style = RepositoryAnalyserPdfStyles.NormalKey;
            paragraph.Format.Font.Color = color;
            paragraph.Format.Font.Bold = isBold;
            return paragraph;
        }

        public static TextFrame AddTextFrame(ref Section section, double topMargin)
        {
            var frame = section.AddTextFrame();
            frame.RelativeVertical = RelativeVertical.Page;
            frame.MarginTop = $"{topMargin}cm";
            frame.MarginLeft = "1cm";
            frame.Width = "21cm";
            return frame;
        }
        public static void DefineStyles(ref Document document)
        {
            var styles = RepositoryAnalyserPdfStyles.GetStylesCollection();
            foreach (var style in styles)
            {
                document.Styles.Add(style);
            }
        }

        public static void InsertLogo(ref Section section, ReportSubject subject)
        {
            var titleFrame = section.AddTextFrame();

            titleFrame.Left = ShapePosition.Center;

            titleFrame.RelativeVertical = RelativeVertical.Page;
            titleFrame.WrapFormat.DistanceTop = "1cm";
            titleFrame.Width = "21cm";
            titleFrame.Height = "4cm";
            titleFrame.LineFormat.Visible = false;

            Image img = null;
            if (subject == ReportSubject.Training)
            {
                img = titleFrame.AddImage("Resources/training_report_header.png");
            }
            else
            {
                img = titleFrame.AddImage("Resources/prediction_report_header.png");
            }

            img.ScaleHeight = 0.5;
            img.ScaleWidth = 0.5;
        }

        public static void AppendFooter(ref Section section)
        {
            // Create a paragraph with centered page number. See definition of style "Footer".
            var paragraph = new Paragraph
            {
                Format =
                {
                    Alignment = ParagraphAlignment.Center
                }
            };
            paragraph.AddTab();
            paragraph.AddPageField();
            section.Footers.Primary.Add(paragraph);
            section.Footers.EvenPage.Add(paragraph.Clone());
        }
        public static Document CreateDocumentWithDefaultSettings()
        {
            return new Document()
            {
                Info = new DocumentInfo()
                {
                    Author = "RepositoryAnalyser",
                    Subject = "Generated by Repository Analyser",
                    Title = "RepositoryAnalyser raport"
                },

                DefaultPageSetup =
                {
                    PageHeight = "29.7cm",
                    LeftMargin = 25,
                    RightMargin = 25,
                    TopMargin = 35,
                    BottomMargin = 120,
                    
                }
            };
        }
    }
}

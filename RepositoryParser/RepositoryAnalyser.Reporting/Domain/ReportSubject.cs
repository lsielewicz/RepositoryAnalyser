﻿namespace RepositoryAnalyser.Reporting.Domain
{
    public enum ReportSubject
    {
        Training,
        Prediction
    }
}
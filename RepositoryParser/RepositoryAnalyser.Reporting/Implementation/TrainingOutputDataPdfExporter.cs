﻿using System.Resources;
using MigraDoc.DocumentObjectModel;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.Reporting.Domain;
using RepositoryAnalyser.Reporting.Helpers;
using RepositoryAnalyser.Reporting.Interfaces;

namespace RepositoryAnalyser.Reporting.Implementation
{
    public class TrainingOutputDataPdfExporter : ITrainingOutputDataPdfExporter
    {
        private readonly ResourceManager _rm = Locale.Languages.Resources.ResourceManager;

        public void Export(TrainingOutputData objectToExport, string path)
        {
            var document = DocumentHelper.CreateDocumentWithDefaultSettings();
            var mainSection = document.AddSection();

            DocumentHelper.DefineStyles(ref document);
            DocumentHelper.AppendFooter(ref mainSection);
            DocumentHelper.InsertLogo(ref mainSection, ReportSubject.Training);

            /*BASIC INFO*/
            var basicInfoTextFrame = DocumentHelper.AddTextFrame(ref mainSection, 6);
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"1. {_rm.GetString("BasicInformation")}", RepositoryAnalyserPdfStyles.Header1Key);

            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("TrainingDate")}: {objectToExport.DateTime:g}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("RowDataRows")}: {objectToExport.CountOfRawDataRows}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("DataRows")}: {objectToExport.CountOfFormattedDataRows}");
           
            /*RESULTS*/
            var resultsTextFrame = DocumentHelper.AddTextFrame(ref mainSection, 10);
            DocumentHelper.AppendText(ref resultsTextFrame, $"2. {_rm.GetString("TrainingResults")}", RepositoryAnalyserPdfStyles.Header1Key);
            DocumentHelper.AppendText(ref resultsTextFrame, $"2.1. {_rm.GetString("SummaryUppercase")}", RepositoryAnalyserPdfStyles.Header2Key);

            const string rowWidth = "2cm";
            const string indexRowWidth = "1.5cm";

            if (objectToExport.Metrics.ClassificationMetrics != null)
            {
                var metrics = objectToExport.Metrics.ClassificationMetrics;
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("CrossValidationFolds")}: {metrics.SubMetrics.Count}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("DataBalancingTechnique")}: {metrics.DataBalancingTechnique}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("Accuracy")}: {metrics.Accuracy}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("AreaUnderCurve")}: {metrics.Auc}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("F1Score")}: {metrics.F1Score}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("MatthewsCorrelationCoefficient")}: {metrics.MatthewsCorrelationCoefficient}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("NegativePrecision")}: {metrics.NegativePrecision}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("PositivePrecision")}: {metrics.PositivePrecision}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("NegativeRecall")}: {metrics.NegativeRecall}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("PositiveRecall")}: {metrics.PositiveRecall}");

                DocumentHelper.AppendText(ref resultsTextFrame, $"2.1.1. {_rm.GetString("ConfusionMatrix")}", RepositoryAnalyserPdfStyles.Header3Key);
                var tp = DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("TP")}: {metrics.ConfusionMatirx.TruePositive}");
                var fp = DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("FP")}: {metrics.ConfusionMatirx.FalsePositve}");
                var fn = DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("FN")}: {metrics.ConfusionMatirx.FalseNegative}");
                var tn = DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("TN")}: {metrics.ConfusionMatirx.TrueNegative}");

                tp.Format.LeftIndent = "1.5cm";
                fp.Format.LeftIndent = "1.5cm";
                fn.Format.LeftIndent = "1.5cm";
                tn.Format.LeftIndent = "1.5cm";

                mainSection.AddPageBreak();
                var detailsFrame = DocumentHelper.AddTextFrame(ref mainSection, 2);
                DocumentHelper.AppendText(ref detailsFrame, $"2.2. {_rm.GetString("DetailsUppercase")}", RepositoryAnalyserPdfStyles.Header2Key);
                var detailsTable = DocumentHelper.CreateTable(ref detailsFrame);
                
                var indexColumn = detailsTable.Columns.AddColumn();
                indexColumn.Width = indexRowWidth;
                var matthewsCorrelationColumn = detailsTable.Columns.AddColumn();
                matthewsCorrelationColumn.Width = rowWidth;
                var accuracyColumn = detailsTable.Columns.AddColumn();
                accuracyColumn.Width = rowWidth;
                var f1ScoreColumn = detailsTable.Columns.AddColumn();
                f1ScoreColumn.Width = rowWidth;
                var aucColumn = detailsTable.Columns.AddColumn();
                aucColumn.Width = rowWidth;
                var negativePrecissionColumn = detailsTable.Columns.AddColumn();
                negativePrecissionColumn.Width = rowWidth;
                var positivePrecissionColumn = detailsTable.Columns.AddColumn();
                positivePrecissionColumn.Width = rowWidth;
                var negativeRecallColumn = detailsTable.Columns.AddColumn();
                negativeRecallColumn.Width = rowWidth;
                var positiveRecallColumn = detailsTable.Columns.AddColumn();
                positiveRecallColumn.Width = rowWidth;

                var header = detailsTable.Rows.AddRow();
                header.Format.Font = new Font("Verdana", 10) { Color = Colors.White };

                header.Shading = new Shading()
                {
                    Color = RepositoryAnalyserPdfStyles.AppGray,
                    
                };
                header.Cells[0].AddParagraph(_rm.GetString("FoldNumber"));
                header.Cells[1].AddParagraph(_rm.GetString("MCC"));
                header.Cells[2].AddParagraph(_rm.GetString("Accuracy"));
                header.Cells[3].AddParagraph(_rm.GetString("F1Score"));
                header.Cells[4].AddParagraph(_rm.GetString("AreaUnderCurve"));
                header.Cells[5].AddParagraph(_rm.GetString("NegativePrecision"));
                header.Cells[6].AddParagraph(_rm.GetString("PositivePrecision"));
                header.Cells[7].AddParagraph(_rm.GetString("NegativeRecall"));
                header.Cells[8].AddParagraph(_rm.GetString("PositiveRecall"));

                var foldIndex = 0;
                foreach (var metric in objectToExport.Metrics.ClassificationMetrics.SubMetrics)
                {
                    foldIndex++;
                    var row = detailsTable.Rows.AddRow();
                    row.Cells[0].AddParagraph(foldIndex.ToString());
                    row.Cells[1].AddParagraph(metric.MatthewsCorrelationCoefficient.ToString("F"));
                    row.Cells[2].AddParagraph(metric.Accuracy.ToString("F"));
                    row.Cells[3].AddParagraph(metrics.F1Score.ToString("F"));
                    row.Cells[4].AddParagraph(metrics.Auc.ToString("F"));
                    row.Cells[5].AddParagraph(metric.NegativePrecision.ToString("F"));
                    row.Cells[6].AddParagraph(metric.PositivePrecision.ToString("F"));
                    row.Cells[7].AddParagraph(metric.NegativeRecall.ToString("F"));
                    row.Cells[8].AddParagraph(metrics.PositiveRecall.ToString("F"));
                }
            }

            if (objectToExport.Metrics.RegressionMetrics != null)
            {
                var metrics = objectToExport.Metrics.RegressionMetrics;
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("CrossValidationFolds")}: {metrics.SubMetrics.Count}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- L1: {metrics.L1}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- L2: {metrics.L2}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("RSquared")}: {metrics.RSquared}");
                DocumentHelper.AppendText(ref resultsTextFrame, $"- {_rm.GetString("LossFn")}: {metrics.LossFn}");

                DocumentHelper.AppendText(ref resultsTextFrame, $"2.2. {_rm.GetString("DetailsUppercase")}", RepositoryAnalyserPdfStyles.Header2Key);
                var detailsTable = DocumentHelper.CreateTable(ref resultsTextFrame);

                var indexColumn = detailsTable.Columns.AddColumn();
                indexColumn.Width = indexRowWidth;
                var rmsColumn = detailsTable.Columns.AddColumn();
                rmsColumn.Width = rowWidth;
                var l1Column = detailsTable.Columns.AddColumn();
                l1Column.Width = rowWidth;
                var l2Column = detailsTable.Columns.AddColumn();
                l2Column.Width = rowWidth;
                var rSquaredColumn = detailsTable.Columns.AddColumn();
                rSquaredColumn.Width = rowWidth;
                var lossFnColumn = detailsTable.Columns.AddColumn();
                lossFnColumn.Width = rowWidth;
              
                var header = detailsTable.Rows.AddRow();
                header.Format.Font = new Font("Verdana", 10) { Color = Colors.White };

                header.Shading = new Shading()
                {
                    Color = RepositoryAnalyserPdfStyles.AppGray,

                };
                header.Cells[0].AddParagraph(_rm.GetString("FoldNumber"));
                header.Cells[1].AddParagraph("RMS");
                header.Cells[2].AddParagraph("L1");
                header.Cells[3].AddParagraph("L2");
                header.Cells[4].AddParagraph(_rm.GetString("RSquared"));
                header.Cells[5].AddParagraph(_rm.GetString("LossFn"));

                var foldIndex = 0;
                foreach (var metric in objectToExport.Metrics.RegressionMetrics.SubMetrics)
                {
                    foldIndex++;
                    var row = detailsTable.Rows.AddRow();
                    row.Cells[0].AddParagraph(foldIndex.ToString());
                    row.Cells[1].AddParagraph(metric.Rms.ToString("F"));
                    row.Cells[2].AddParagraph(metric.L1.ToString("F"));
                    row.Cells[3].AddParagraph(metric.L2.ToString("F"));
                    row.Cells[4].AddParagraph(metrics.RSquared.ToString("F"));
                    row.Cells[5].AddParagraph(metric.LossFn.ToString("F"));
                }
            }



            PdfRenderingHelper.RenderDocument(document, path);
        }
    }
}

﻿using System;
using System.Resources;
using MigraDoc.DocumentObjectModel;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.Reporting.Domain;
using RepositoryAnalyser.Reporting.Helpers;
using RepositoryAnalyser.Reporting.Interfaces;

namespace RepositoryAnalyser.Reporting.Implementation
{
    public class IssueReopenedIntegratedPredictionResultPdfExporter : IIssueReopenedIntegratedPredictionResultPdfExporter
    {
        private readonly ResourceManager _rm = Locale.Languages.Resources.ResourceManager;

        public void Export(PredictionResult<ReopenedIssueIntegratedData, ReopenedIssuePrediction> objectToExport, string path)
        {
            var document = DocumentHelper.CreateDocumentWithDefaultSettings();
            var mainSection = document.AddSection();

            var resultInterpretation = objectToExport.Prediction.IsGoingToBeReopened
                ? _rm.GetString("IssueIsGoingToBeReopened")
                : _rm.GetString("IssueIsNotGoingToBeReopened");

            DocumentHelper.DefineStyles(ref document);
            DocumentHelper.AppendFooter(ref mainSection);
            DocumentHelper.InsertLogo(ref mainSection, ReportSubject.Prediction);

            /*BASIC INFO*/
            var basicInfoTextFrame = DocumentHelper.AddTextFrame(ref mainSection, 5.5);
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"1. {_rm.GetString("BasicInformation")}", RepositoryAnalyserPdfStyles.Header1Key);

            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionSubject")}: {_rm.GetString("ReopenIssueSubject")}", Colors.DarkGreen);
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("AnalysisSource")}: {_rm.GetString("Integrated")}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("TestedIssue")}: {objectToExport.OriginalKey}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionDate")}: {objectToExport.PredictionDate:g}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionTime")}: {objectToExport.PredictionTime.ToPrettyFormat()}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("TrainingModelName")}: {objectToExport.ModelName}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionResult")}: {resultInterpretation}", Colors.DarkRed, true);

            /*INPUT DATA*/
            var inputDataTextFrame = DocumentHelper.AddTextFrame(ref mainSection, 12);
            DocumentHelper.AppendText(ref inputDataTextFrame, $"2. {_rm.GetString("InputData")}", RepositoryAnalyserPdfStyles.Header1Key);

            var inputDatatable = DocumentHelper.CreateKeyValueTable(ref inputDataTextFrame);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("Environment"), objectToExport.Data.Environment);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("Type"), objectToExport.Data.Type);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommentsCount"), objectToExport.Data.CommentsCount.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommentsLenght"), objectToExport.Data.CommentsLenght.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("ReporterCommentsCount"), objectToExport.Data.ReporterCommentsCount.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("ProjectName"), objectToExport.Data.ProjectName);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("AsigneeEmail"), objectToExport.Data.AsigneeEmail);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("ReporterEmail"), objectToExport.Data.ReporterEmail);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("Priority"), objectToExport.Data.Priority);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("TimeSpent"), objectToExport.Data.TimeSpent.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("IsSubTask"), objectToExport.Data.IsSubTask == 0 ? "yes" : "no");
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("AttachmentsCount"), objectToExport.Data.AttachmentsCount.ToString("F1"));

            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommitsCount"), objectToExport.Data.CommitsCount.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("Revision"), objectToExport.Data.Revision);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommitsMessagesCount"), objectToExport.Data.CommitsMessagesCount.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommitsMessagesLength"), objectToExport.Data.CommitsMessagesLength.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CountOfModifiedFiles"), objectToExport.Data.CountOfModifiedFiles.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("AddedLines"), objectToExport.Data.AddedLines.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("DeletedLines"), objectToExport.Data.DeletedLines.ToString("F1"));

            /*TRAINING MODEL*/
            mainSection.AddPageBreak();
            var trainingModelFrame = DocumentHelper.AddTextFrame(ref mainSection, 2);
            DocumentHelper.AppendText(ref trainingModelFrame, $"3. {_rm.GetString("TrainingModel")}", RepositoryAnalyserPdfStyles.Header1Key);
            DocumentHelper.AppendText(ref trainingModelFrame, Environment.NewLine);

            var metrics = objectToExport.TrainingModel.Metrics.ClassificationMetrics;
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("Accuracy")}: {metrics.Accuracy}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("AreaUnderCurve")}: {metrics.Auc}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("F1Score")}: {metrics.F1Score}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("MatthewsCorrelationCoefficient")}: {metrics.MatthewsCorrelationCoefficient}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("NegativePrecision")}: {metrics.NegativePrecision}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("PositivePrecision")}: {metrics.PositivePrecision}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("NegativeRecall")}: {metrics.NegativeRecall}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("PositiveRecall")}: {metrics.PositiveRecall}");

            DocumentHelper.AppendText(ref trainingModelFrame, $"3.1. {_rm.GetString("ConfusionMatrix")}", RepositoryAnalyserPdfStyles.Header2Key);
            var tp = DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("TP")}: {metrics.ConfusionMatirx.TruePositive}");
            var fp = DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("FP")}: {metrics.ConfusionMatirx.FalsePositve}");
            var fn = DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("FN")}: {metrics.ConfusionMatirx.FalseNegative}");
            var tn = DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("TN")}: {metrics.ConfusionMatirx.TrueNegative}");

            tp.Format.LeftIndent = "1.5cm";
            fp.Format.LeftIndent = "1.5cm";
            fn.Format.LeftIndent = "1.5cm";
            tn.Format.LeftIndent = "1.5cm";



            PdfRenderingHelper.RenderDocument(document, path);
        }
    }
}

﻿using System;
using System.Resources;
using MigraDoc.DocumentObjectModel;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.Reporting.Domain;
using RepositoryAnalyser.Reporting.Helpers;
using RepositoryAnalyser.Reporting.Interfaces;

namespace RepositoryAnalyser.Reporting.Implementation
{
    public class TimeToFixIntegratedPredictionResultPdfExporter : ITimeToFixIntegratedPredictionResultPdfExporter
    {
        private readonly ResourceManager _rm = Locale.Languages.Resources.ResourceManager;

        public void Export(PredictionResult<TimeToFixIntegratedData, TimeToFixPrediction> objectToExport, string path)
        {
            var document = DocumentHelper.CreateDocumentWithDefaultSettings();
            var mainSection = document.AddSection();

            var resultInterpretation = TimeSpan.FromMinutes(objectToExport.Prediction.TimeToFixInMinutes).ToPrettyFormat();
            var mmre = objectToExport.Accuracy < 0 ? _rm.GetString("IssueIsStillOpen") : objectToExport.Accuracy.ToString("P");

            DocumentHelper.DefineStyles(ref document);
            DocumentHelper.AppendFooter(ref mainSection);
            DocumentHelper.InsertLogo(ref mainSection, ReportSubject.Prediction);

            /*BASIC INFO*/
            var basicInfoTextFrame = DocumentHelper.AddTextFrame(ref mainSection, 5.5);
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"1. {_rm.GetString("BasicInformation")}", RepositoryAnalyserPdfStyles.Header1Key);

            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionSubject")}: {_rm.GetString("TimeToFixIssueSubject")}", Colors.DarkGreen);
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("AnalysisSource")}: {_rm.GetString("Integrated")}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("TestedIssue")}: {objectToExport.OriginalKey}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionDate")}: {objectToExport.PredictionDate:g}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionTime")}: {objectToExport.PredictionTime.ToPrettyFormat()}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("TrainingModelName")}: {objectToExport.ModelName}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionResult")}: {resultInterpretation}", Colors.DarkRed, true);
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("Accuracy")}: {mmre}");

            /*INPUT DATA*/
            var inputDataTextFrame = DocumentHelper.AddTextFrame(ref mainSection, 12.5);
            DocumentHelper.AppendText(ref inputDataTextFrame, $"2. {_rm.GetString("InputData")}", RepositoryAnalyserPdfStyles.Header1Key);

            var inputDatatable = DocumentHelper.CreateKeyValueTable(ref inputDataTextFrame);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("Environment"), objectToExport.Data.Environment);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("Type"), objectToExport.Data.Type);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommentsCount"), objectToExport.Data.CommentsCount.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommentsLenght"), objectToExport.Data.CommentsLenght.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("ReporterCommentsCount"), objectToExport.Data.ReporterCommentsCount.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("ProjectName"), objectToExport.Data.ProjectName);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("AsigneeEmail"), objectToExport.Data.AsigneeEmail);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("ReporterEmail"), objectToExport.Data.ReporterEmail);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("Priority"), objectToExport.Data.Priority);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("TimeSpent"), objectToExport.Data.TimeSpent.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("IsSubTask"), objectToExport.Data.IsSubTask == 0 ? "yes" : "no");
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("AttachmentsCount"), objectToExport.Data.AttachmentsCount.ToString("F1"));

            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommitsCount"), objectToExport.Data.CommitsCount.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("Revision"), objectToExport.Data.Revision);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommitsMessagesCount"), objectToExport.Data.CommitsMessagesCount.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommitsMessagesLength"), objectToExport.Data.CommitsMessagesLength.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CountOfModifiedFiles"), objectToExport.Data.CountOfModifiedFiles.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("AddedLines"), objectToExport.Data.AddedLines.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("DeletedLines"), objectToExport.Data.DeletedLines.ToString("F1"));

            /*TRAINING MODEL*/
            mainSection.AddPageBreak();
            var trainingModelFrame = DocumentHelper.AddTextFrame(ref mainSection, 2);
            DocumentHelper.AppendText(ref trainingModelFrame, $"3. {_rm.GetString("TrainingModel")}", RepositoryAnalyserPdfStyles.Header1Key);
            DocumentHelper.AppendText(ref trainingModelFrame, Environment.NewLine);

            var metrics = objectToExport.TrainingModel.Metrics.RegressionMetrics;
            DocumentHelper.AppendText(ref trainingModelFrame, $"- RMS: {metrics.Rms}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- L1: {metrics.L1}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- L2: {metrics.L2}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("RSquared")}: {metrics.RSquared}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("LossFn")}: {metrics.LossFn}");


            PdfRenderingHelper.RenderDocument(document, path);
        }
    }
}
﻿using System;
using System.Resources;
using MigraDoc.DocumentObjectModel;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.Reporting.Domain;
using RepositoryAnalyser.Reporting.Helpers;
using RepositoryAnalyser.Reporting.Interfaces;

namespace RepositoryAnalyser.Reporting.Implementation
{
    public class DevEfficiencyIntegratedPredictionResultPdfExporter : IDevEfficiencyIntegratedPredictionResultPdfExporter
    {
        private readonly ResourceManager _rm = Locale.Languages.Resources.ResourceManager;

        public void Export(PredictionResult<DeveloperEfficiencyIntegratedData, DeveloperEfficiencyPrediction> objectToExport, string path)
        {
            var document = DocumentHelper.CreateDocumentWithDefaultSettings();
            var mainSection = document.AddSection();

            var resultInterpretation = objectToExport.Prediction.Efficiency;
            var mmre = objectToExport.Accuracy.ToString("P");

            DocumentHelper.DefineStyles(ref document);
            DocumentHelper.AppendFooter(ref mainSection);
            DocumentHelper.InsertLogo(ref mainSection, ReportSubject.Prediction);

            /*BASIC INFO*/
            var basicInfoTextFrame = DocumentHelper.AddTextFrame(ref mainSection, 6);
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"1. {_rm.GetString("BasicInformation")}", RepositoryAnalyserPdfStyles.Header1Key);
            DocumentHelper.AppendText(ref basicInfoTextFrame, Environment.NewLine);

            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionSubject")}: {_rm.GetString("DeveloperEfficiencySubject")}", Colors.DarkGreen);
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("AnalysisSource")}: {_rm.GetString("Integrated")}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("TestedUser")}: {objectToExport.OriginalKey}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionDate")}: {objectToExport.PredictionDate:g}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionTime")}: {objectToExport.PredictionTime.ToPrettyFormat()}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("TrainingModelName")}: {objectToExport.ModelName}");
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("PredictionResult")}: {resultInterpretation}", Colors.DarkRed, true);
            DocumentHelper.AppendText(ref basicInfoTextFrame, $"- {_rm.GetString("Accuracy")}: {mmre}");

            /*INPUT DATA*/
            var inputDataTextFrame = DocumentHelper.AddTextFrame(ref mainSection, 13.5);
            DocumentHelper.AppendText(ref inputDataTextFrame, $"2. {_rm.GetString("InputData")}", RepositoryAnalyserPdfStyles.Header1Key);
            DocumentHelper.AppendText(ref inputDataTextFrame, Environment.NewLine);

            var inputDatatable = DocumentHelper.CreateKeyValueTable(ref inputDataTextFrame);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("DeveloperName"), objectToExport.Data.DeveloperName);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("DeveloperEmail"), objectToExport.Data.DeveloperEmail);
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CountOfClosedIssuesPerMonth"), objectToExport.Data.CountOfClosedIssuesPerMonth.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CountOfOpenedIssuesPerMonth"), objectToExport.Data.CountOfOpenedIssuesPerMonth.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CountOfReopenedIssuesPerMonth"), objectToExport.Data.CountOfReopenedIssuesPerMonth.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CountOfReportedIssuesPerMonth"), objectToExport.Data.CountOfReportedIssuesPerMonth.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CountOfAssignedIssues"), objectToExport.Data.CountOfAssignedIssues.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("CommitsCountPerMonth"), objectToExport.Data.CommitsCountPerMonth.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("AddedLinesPerMonth"), objectToExport.Data.AddedLinesPerMonth.ToString("F1"));
            DocumentHelper.AddValueToKeyValueTable(ref inputDatatable, _rm.GetString("DeletedLinesPerMonth"), objectToExport.Data.DeletedLinesPerMonth.ToString("F1"));

            /*TRAINING MODEL*/
            mainSection.AddPageBreak();
            var trainingModelFrame = DocumentHelper.AddTextFrame(ref mainSection, 2);
            DocumentHelper.AppendText(ref trainingModelFrame, $"3. {_rm.GetString("TrainingModel")}", RepositoryAnalyserPdfStyles.Header1Key);
            DocumentHelper.AppendText(ref trainingModelFrame, Environment.NewLine);

            var metrics = objectToExport.TrainingModel.Metrics.RegressionMetrics;
            DocumentHelper.AppendText(ref trainingModelFrame, $"- RMS: {metrics.Rms}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- L1: {metrics.L1}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- L2: {metrics.L2}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("RSquared")}: {metrics.RSquared}");
            DocumentHelper.AppendText(ref trainingModelFrame, $"- {_rm.GetString("LossFn")}: {metrics.LossFn}");


            PdfRenderingHelper.RenderDocument(document, path);
        }
    }
}
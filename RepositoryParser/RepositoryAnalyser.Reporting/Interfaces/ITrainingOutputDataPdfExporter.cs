﻿using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Reporting.Interfaces
{
    public interface ITrainingOutputDataPdfExporter : IPdfExporter<TrainingOutputData>
    {
    }
}

﻿namespace RepositoryAnalyser.Reporting.Interfaces
{
    public interface IPdfExporter<T> where T : class
    {
        void Export(T objectToExport, string path);
    }
}
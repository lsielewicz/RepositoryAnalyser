﻿using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Reporting.Interfaces
{
    public interface IDevEfficiencyPredictionResultPdfExporter : IPredictionResultPdfExporter<DeveloperEfficiencyData, DeveloperEfficiencyPrediction>
    {
        
    }
}
﻿using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Reporting.Interfaces
{
    public interface ITimeToFixIntegratedPredictionResultPdfExporter : IPredictionResultPdfExporter<TimeToFixIntegratedData, TimeToFixPrediction>
    {
        
    }
}
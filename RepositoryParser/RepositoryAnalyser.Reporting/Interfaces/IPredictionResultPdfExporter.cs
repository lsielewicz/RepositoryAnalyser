﻿using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Reporting.Interfaces
{
    public interface IPredictionResultPdfExporter<TData,TPrediction> : IPdfExporter<PredictionResult<TData, TPrediction>>
    {
        
    }
}
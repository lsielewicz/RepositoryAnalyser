﻿using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Reporting.Interfaces
{
    public interface IIssueReopenedIntegratedPredictionResultPdfExporter : IPredictionResultPdfExporter<ReopenedIssueIntegratedData, ReopenedIssuePrediction>
    {
        
    }
}
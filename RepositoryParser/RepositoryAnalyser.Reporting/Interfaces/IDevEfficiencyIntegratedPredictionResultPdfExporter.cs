﻿using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Reporting.Interfaces
{
    public interface IDevEfficiencyIntegratedPredictionResultPdfExporter : IPredictionResultPdfExporter<DeveloperEfficiencyIntegratedData, DeveloperEfficiencyPrediction>
    {
        
    }
}
﻿using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Reporting.Interfaces
{
    public interface ITimeToFixPredictionResultPdfExporter : IPredictionResultPdfExporter<TimeToFixIssueData, TimeToFixPrediction>
    {
        
    }
}
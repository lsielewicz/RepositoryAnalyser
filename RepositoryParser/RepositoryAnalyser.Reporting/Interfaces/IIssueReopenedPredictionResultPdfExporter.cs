﻿using RepositoryAnalyser.MachineLearning.Models;

namespace RepositoryAnalyser.Reporting.Interfaces
{
    public interface IIssueReopenedPredictionResultPdfExporter : IPredictionResultPdfExporter<ReopenedIssueData, ReopenedIssuePrediction>
    {
        
    }
}
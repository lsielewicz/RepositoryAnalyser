﻿namespace RepositoryAnalyser.Engine.Tests.Helpers
{
    public class LocalizationHelper
    {
        private static string ApplicationName = "RepositoryAnalyser";

        public static string TestDbDirectory
        {
            get { return @"Data\RepositoryAnalyserData.sqlite"; }
        }
    }
}

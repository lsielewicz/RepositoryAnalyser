﻿using System.IO;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.Engine.Tests.Helpers;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;

namespace RepositoryAnalyser.Engine.Tests.Tests
{
    [TestFixture]
    public class DataPreprocessingTests
    {
        [OneTimeSetUp]
        public void Initialize()
        {
            var dirPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var dbPath = Path.Combine(dirPath, LocalizationHelper.TestDbDirectory);
            DbService.Instance.ChangeDataBaseLocation(dbPath, dirPath);
        }

        [Test]
        public void TestIssueReopenDataConverting()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();

                var issuesCount = issues.Count();
                var issuesRows = issues.Select(i => i.ToReopenedIssueData());

                Assert.IsNotNull(issuesRows);
                Assert.IsTrue(issuesRows.Count() == issuesCount);
            }
        }

        [Test]
        public void TestIssueReopenDataCleaning()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();

                var issuesCount = issues.Count();
                var issuesRows = issues.Select(i=>i.ToReopenedIssueData()).ToList();

                Assert.IsNotNull(issuesRows);
                var cleanedDataRow = DataCleaningHelper.Clean(issuesRows);

                Assert.IsTrue(cleanedDataRow.Count < issuesCount);
            }
        }

        [Test]
        public void TestIssueReopenDataUndersampling()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();
                var issuesRows = issues.Select(i => i.ToReopenedIssueData()).ToList();

                Assert.IsNotNull(issuesRows);
                var cleanedDataRows = DataCleaningHelper.Clean(issuesRows);
                var undersampledDataRows =
                    DataRebalancingHelper.BalanceData(cleanedDataRows, DataBalancingTechnique.Undersampling);

                var positives = undersampledDataRows.Where(row => row.WasReopened).ToList();
                var negatives = undersampledDataRows.Where(row => !row.WasReopened).ToList();
                Assert.IsNotNull(undersampledDataRows);
                Assert.IsTrue(undersampledDataRows.Count < cleanedDataRows.Count);
                Assert.IsTrue(positives.Count == negatives.Count);
                
            }
        }

        [Test]
        public void TestIssueReopenDataOversampling()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();

                var issuesCount = issues.Count();
                var issuesRows = issues.Select(i => i.ToReopenedIssueData()).ToList();

                Assert.IsNotNull(issuesRows);
                var cleanedDataRows = DataCleaningHelper.Clean(issuesRows);
                var oversampledDataRows =
                    DataRebalancingHelper.BalanceData(cleanedDataRows, DataBalancingTechnique.Oversampling);

                var positives = oversampledDataRows.Where(row => row.WasReopened).ToList();
                var negatives = oversampledDataRows.Where(row => !row.WasReopened).ToList();
                Assert.IsNotNull(oversampledDataRows);
                Assert.IsTrue(oversampledDataRows.Count > cleanedDataRows.Count);
                Assert.IsTrue(positives.Count == negatives.Count);
            }
        }

        [Test]
        public void TestIssueReopenIntegratedDataConverting()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();
                var commits = session.QueryOver<Commit>().List();
                var commitsSets = session.QueryOver<CommitsSet>().List();
                var issuesCount = issues.Count();
                var issuesRows = issues.Select(i => i.ToReopenedIntegratedIssueData(commits,commitsSets));

                Assert.IsNotNull(issuesRows);
                Assert.IsTrue(issuesRows.Count() == issuesCount);
            }
        }

        [Test]
        public void TestIssueReopenIntegratedDataCleaning()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();
                var commits = session.QueryOver<Commit>().List();
                var commitsSets = session.QueryOver<CommitsSet>().List();

                var issuesCount = issues.Count();
                var issuesRows = issues.Select(i => i.ToReopenedIntegratedIssueData(commits, commitsSets)).ToList();

                Assert.IsNotNull(issuesRows);
                var cleanedDataRow = DataCleaningHelper.Clean(issuesRows);

                Assert.IsTrue(cleanedDataRow.Count < issuesCount);
            }
        }

        public void TestIssueReopenIntegratedDataUndersampling()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();
                var commits = session.QueryOver<Commit>().List();
                var commitsSets = session.QueryOver<CommitsSet>().List();

                var issuesRows = issues.Select(i => i.ToReopenedIntegratedIssueData(commits, commitsSets)).ToList();

                Assert.IsNotNull(issuesRows);
                var cleanedDataRows = DataCleaningHelper.Clean(issuesRows);
                var undersampledDataRows =
                    DataRebalancingHelper.BalanceData(cleanedDataRows, DataBalancingTechnique.Undersampling);

                var positives = undersampledDataRows.Where(row => row.WasReopened).ToList();
                var negatives = undersampledDataRows.Where(row => !row.WasReopened).ToList();
                Assert.IsNotNull(undersampledDataRows);
                Assert.IsTrue(undersampledDataRows.Count < cleanedDataRows.Count);
                Assert.IsTrue(positives.Count == negatives.Count);

            }
        }

        [Test]
        public void TestIssueReopenIntegratedDataOversampling()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();
                var commits = session.QueryOver<Commit>().List();
                var commitsSets = session.QueryOver<CommitsSet>().List();

                var issuesRows = issues.Select(i => i.ToReopenedIntegratedIssueData(commits, commitsSets)).ToList();

                Assert.IsNotNull(issuesRows);
                var cleanedDataRows = DataCleaningHelper.Clean(issuesRows);
                var oversampledDataRows =
                    DataRebalancingHelper.BalanceData(cleanedDataRows, DataBalancingTechnique.Oversampling);

                var positives = oversampledDataRows.Where(row => row.WasReopened).ToList();
                var negatives = oversampledDataRows.Where(row => !row.WasReopened).ToList();
                Assert.IsNotNull(oversampledDataRows);
                Assert.IsTrue(oversampledDataRows.Count > cleanedDataRows.Count);
                Assert.IsTrue(positives.Count == negatives.Count);
            }
        }

        [Test]
        public void TestTimeToFixDataConverting()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();

                var issuesCount = issues.Count();
                var issuesRows = issues.Select(i => i.ToTimeToFixIssueData());

                Assert.IsNotNull(issuesRows);
                Assert.IsTrue(issuesRows.Count() == issuesCount);
            }
        }

        [Test]
        public void TestTimeToFixDataCleaning()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();

                var issuesCount = issues.Count();
                var issuesRows = issues.Select(i => i.ToTimeToFixIssueData()).ToList();

                Assert.IsNotNull(issuesRows);
                var cleanedDataRow = DataCleaningHelper.Clean(issuesRows);

                Assert.IsTrue(cleanedDataRow.Count < issuesCount);
            }
        }

       [Test]
        public void TestTimeToFixIntegratedDataConverting()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();
                var commits = session.QueryOver<Commit>().List();
                var commitsSets = session.QueryOver<CommitsSet>().List();
                var issuesCount = issues.Count();
                var issuesRows = issues.Select(i => i.ToTimeToFixIntegratedIssueData(commits, commitsSets));

                Assert.IsNotNull(issuesRows);
                Assert.IsTrue(issuesRows.Count() == issuesCount);
            }
        }

        [Test]
        public void TestTimeToFixIntegratedDataCleaning()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var issues = session.QueryOver<Issue>().List();
                var commits = session.QueryOver<Commit>().List();
                var commitsSets = session.QueryOver<CommitsSet>().List();

                var issuesCount = issues.Count();
                var issuesRows = issues.Select(i => i.ToTimeToFixIntegratedIssueData(commits, commitsSets)).ToList();

                Assert.IsNotNull(issuesRows);
                var cleanedDataRow = DataCleaningHelper.Clean(issuesRows);

                Assert.IsTrue(cleanedDataRow.Count < issuesCount);
            }
        }


        [Test]
        public void DevEfficiencyDataConverting()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var users = session.QueryOver<IssueTrackerUser>().List();
                var issues = session.QueryOver<Issue>().List();

                var usersCount = users.Count();
                var usersRows = users.Select(i => i.ToEfficiencyData(issues, new EfficiencyWeightsKeeper())).ToList();


                Assert.IsNotNull(usersRows);
                Assert.IsTrue(usersRows.Count() == usersCount); 
            }
        }

        [Test]
        public void DevEfficiencyDataCleaning()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var users = session.QueryOver<IssueTrackerUser>().List();
                var issues = session.QueryOver<Issue>().List();

                var usersCount = users.Count();
                var usersRows = users.Select(i => i.ToEfficiencyData(issues, new EfficiencyWeightsKeeper())).ToList();

                Assert.IsNotNull(usersRows);
                var cleanedDataRow = DataCleaningHelper.Clean(usersRows);

                Assert.IsTrue(cleanedDataRow.Count < usersCount);
            }
        }

        [Test]
        public void DevEfficiencyIntegratedDataConverting()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var users = session.QueryOver<IssueTrackerUser>().List();
                var issues = session.QueryOver<Issue>().List();
                var commits = session.QueryOver<Commit>().List();

                var usersCount = users.Count();
                var usersRows = users.Select(i => i.ToIntegratedEfficientyData(issues, commits, new EfficiencyWeightsKeeper())).ToList();


                Assert.IsNotNull(usersRows);
                Assert.IsTrue(usersRows.Count() == usersCount);
            }
        }

        [Test]
        public void DevEfficiencyIntegratedDataCleaning()
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var users = session.QueryOver<IssueTrackerUser>().List();
                var issues = session.QueryOver<Issue>().List();

                var commits = session.QueryOver<Commit>().List();

                var usersCount = users.Count();
                var usersRows = users.Select(i => i.ToIntegratedEfficientyData(issues, commits, new EfficiencyWeightsKeeper())).ToList();

                Assert.IsNotNull(usersRows);
                var cleanedDataRow = DataCleaningHelper.Clean(usersRows);

                Assert.IsTrue(cleanedDataRow.Count < usersCount);
            }
        }
    }
}

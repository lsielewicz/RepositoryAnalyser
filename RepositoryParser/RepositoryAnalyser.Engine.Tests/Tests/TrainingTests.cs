﻿using System.IO;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using RepositoryAnalyser.DataBase.Services;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Entities.VersionControl;
using RepositoryAnalyser.Engine.Tests.Helpers;
using RepositoryAnalyser.MachineLearning.Enums;
using RepositoryAnalyser.MachineLearning.Helpers;
using RepositoryAnalyser.MachineLearning.Models;
using RepositoryAnalyser.MachineLearning.Predictors;

namespace RepositoryAnalyser.Engine.Tests.Tests
{
    [TestFixture]
    public class TrainingTests
    {
        [OneTimeSetUp]
        public void Initialize()
        {
            var dirPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var dbPath = Path.Combine(dirPath, LocalizationHelper.TestDbDirectory);
            DbService.Instance.ChangeDataBaseLocation(dbPath, dirPath);
           
        }

        [TestCase(BinaryClassificationTool.FastForestBinaryClassifier)]
        [TestCase(BinaryClassificationTool.AveragedPerceptronBinaryClassifier)]
        [TestCase(BinaryClassificationTool.FastTreeBinaryClassifier)]
        [TestCase(BinaryClassificationTool.GeneralizedAdditiveModelBinaryClassifier)]
        [TestCase(BinaryClassificationTool.LinearSvmBinaryClassifier)]
        [TestCase(BinaryClassificationTool.StochasticDualCoordinateAscentBinaryClassifier)]
        public void TestTrainingAndPredictionReopenIssue(BinaryClassificationTool classificationTool)
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var reopenIssuePredictor = new IssueReopenPredictor();
                var issues = session.QueryOver<Issue>().List().ToList();
                var issueToPredict = issues.FirstOrDefault();
                issues = issues.Skip(1).Take(200).ToList();
                var tempPath = Path.GetTempFileName();
                var trainignOutputData = reopenIssuePredictor.TrainAndSavePredictionModel(new ReopenTrainingInputData()
                {
                    DataRebalancingTechnique = DataBalancingTechnique.Oversampling,
                    ClassificationTool = classificationTool,
                    CrossValidationFoldsCount = 3,
                    IssuesData = issues,
                    PredictiveModelPath = tempPath
                });

                var predictionResult = reopenIssuePredictor.Predict(issueToPredict, trainignOutputData);

                Assert.IsNotNull(trainignOutputData);
                Assert.IsNotNull(trainignOutputData.Metrics);
                Assert.IsNotNull(predictionResult);
                Assert.IsNotNull(predictionResult.Result);


                File.Delete(tempPath);
            }
        }

        [TestCase(BinaryClassificationTool.FastForestBinaryClassifier)]
        [TestCase(BinaryClassificationTool.AveragedPerceptronBinaryClassifier)]
        [TestCase(BinaryClassificationTool.FastTreeBinaryClassifier)]
        [TestCase(BinaryClassificationTool.GeneralizedAdditiveModelBinaryClassifier)]
        [TestCase(BinaryClassificationTool.LinearSvmBinaryClassifier)]
        [TestCase(BinaryClassificationTool.StochasticDualCoordinateAscentBinaryClassifier)]
        public void TestTrainingAndIntegratedPredictionReopenIssue(BinaryClassificationTool classificationTool)
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var reopenIssuePredictor = new IssueReopenPredictor();
                var commits = session.QueryOver<Commit>().List();
                var issues = session.QueryOver<Issue>().List().ToList();
                var commitsSets = session.QueryOver<CommitsSet>().List();
                var issueToPredict = issues.FirstOrDefault();
                issues = issues.Skip(1).Take(200).ToList();
                var tempPath = Path.GetTempFileName();
                var trainignOutputData = reopenIssuePredictor.TrainAndSavePredictionModel(new ReopenTrainingIntegratedInputData()
                {
                    DataRebalancingTechnique = DataBalancingTechnique.Oversampling,
                    ClassificationTool = classificationTool,
                    CrossValidationFoldsCount = 3,
                    IssuesData = issues,
                    PredictiveModelPath = tempPath,
                    Commits = commits,
                    CommitsSets = commitsSets
                });

                var predictionResult = reopenIssuePredictor.Predict(issueToPredict, commits, trainignOutputData, commitsSets);

                Assert.IsNotNull(trainignOutputData);
                Assert.IsNotNull(trainignOutputData.Metrics);
                Assert.IsNotNull(predictionResult);
                Assert.IsNotNull(predictionResult.Result);

                File.Delete(tempPath);
            }
        }

        [TestCase(RegressionTool.GeneralizedAdditiveModelRegressor)]
        [TestCase(RegressionTool.FastTreeRegressor)]
        [TestCase(RegressionTool.FastTreeTweedieRegressor)]
        [TestCase(RegressionTool.OnlineGradientDescentRegressor)]
        [TestCase(RegressionTool.OrdinaryLeastSquaresRegressor)]
        [TestCase(RegressionTool.PoissonRegressor)]
        [TestCase(RegressionTool.StochasticDualCoordinateAscentRegressor)]
        public void TestfTimeToFixTrainingAndPrediction(RegressionTool regressionTool)
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var predictor = new TimeToFixPredictor();
                var issues = session.QueryOver<Issue>().List().ToList();
                var issueToPredict = issues.FirstOrDefault();
                issues = issues.Skip(1).Take(200).ToList();
                var tempPath = Path.GetTempFileName();
                var trainignOutputData = predictor.TrainAndSavePredictionModel(new TimeToFixInputData()
                {
                    RegressionTool = regressionTool,
                    CrossValidationFoldsCount = 3,
                    IssuesData = issues,
                    PredictiveModelPath = tempPath
                });

                var predictionResult = predictor.Predict(issueToPredict, trainignOutputData);

                Assert.IsNotNull(trainignOutputData);
                Assert.IsNotNull(trainignOutputData.Metrics);
                Assert.IsNotNull(predictionResult);
                Assert.IsNotNull(predictionResult.Result);


                File.Delete(tempPath);
            }
        }

        [TestCase(RegressionTool.GeneralizedAdditiveModelRegressor)]
        [TestCase(RegressionTool.FastTreeRegressor)]
        [TestCase(RegressionTool.FastTreeTweedieRegressor)]
        [TestCase(RegressionTool.OnlineGradientDescentRegressor)]
        [TestCase(RegressionTool.OrdinaryLeastSquaresRegressor)]
        [TestCase(RegressionTool.PoissonRegressor)]
        [TestCase(RegressionTool.StochasticDualCoordinateAscentRegressor)]
        public void TestfTimeToFixIntegratedTrainingAndPrediction(RegressionTool regressionTool)
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var predictor = new TimeToFixPredictor();
                var commits = session.QueryOver<Commit>().List();
                var issues = session.QueryOver<Issue>().List().ToList();
                var commitsSets = session.QueryOver<CommitsSet>().List();
                var issueToPredict = issues.FirstOrDefault();
                issues = issues.Skip(1).Take(200).ToList();
                var tempPath = Path.GetTempFileName();
                var trainignOutputData = predictor.TrainAndSavePredictionModel(new TimeToFixIntegratedInputData()
                {
                    RegressionTool = regressionTool,
                    CrossValidationFoldsCount = 3,
                    IssuesData = issues,
                    PredictiveModelPath = tempPath,
                    Commits = commits,
                    CommitsSets = commitsSets
                });

                var predictionResult = predictor.Predict(issueToPredict,commits,trainignOutputData, commitsSets);

                Assert.IsNotNull(trainignOutputData);
                Assert.IsNotNull(trainignOutputData.Metrics);
                Assert.IsNotNull(predictionResult);
                Assert.IsNotNull(predictionResult.Result);


                File.Delete(tempPath);
            }
        }

        [TestCase(RegressionTool.GeneralizedAdditiveModelRegressor)]
        [TestCase(RegressionTool.FastTreeRegressor)]
        [TestCase(RegressionTool.FastTreeTweedieRegressor)]
        [TestCase(RegressionTool.OnlineGradientDescentRegressor)]
        [TestCase(RegressionTool.OrdinaryLeastSquaresRegressor)]
        [TestCase(RegressionTool.PoissonRegressor)]
        [TestCase(RegressionTool.StochasticDualCoordinateAscentRegressor)]
        public void TestDeveloperEfficiencyTrainingAndPrediction(RegressionTool regressionTool)
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var predictor = new DeveloperEfficiencyPredictor();
                var issues = session.QueryOver<Issue>().List().ToList();
                var users = session.QueryOver<IssueTrackerUser>().List().ToList();
                var userToPredict = users.FirstOrDefault();
                issues = issues.Skip(1).Take(200).ToList();
                var tempPath = Path.GetTempFileName();

                var trainignOutputData = predictor.TrainAndSavePredictionModel(new DeveloperEfficiencyInputData()
                {
                    RegressionTool = regressionTool,
                    CrossValidationFoldsCount = 3,
                    IssuesData = issues,
                    UsersData = users,
                    PredictiveModelPath = tempPath,
                    EfficiencyWeightsKeeper = new EfficiencyWeightsKeeper()
                });

                var predictionResult = predictor.Predict(userToPredict,issues,trainignOutputData,new EfficiencyWeightsKeeper());

                Assert.IsNotNull(trainignOutputData);
                Assert.IsNotNull(trainignOutputData.Metrics);
                Assert.IsNotNull(predictionResult);
                Assert.IsNotNull(predictionResult.Result);

                File.Delete(tempPath);
            }
        }

        [TestCase(RegressionTool.GeneralizedAdditiveModelRegressor)]
        [TestCase(RegressionTool.FastTreeRegressor)]
        [TestCase(RegressionTool.FastTreeTweedieRegressor)]
        [TestCase(RegressionTool.OnlineGradientDescentRegressor)]
        [TestCase(RegressionTool.OrdinaryLeastSquaresRegressor)]
        [TestCase(RegressionTool.PoissonRegressor)]
        [TestCase(RegressionTool.StochasticDualCoordinateAscentRegressor)]
        public void TestDeveloperEfficiencyIntegratedTrainingAndPrediction(RegressionTool regressionTool)
        {
            using (var session = DbService.Instance.SessionFactory.OpenSession())
            {
                var predictor = new DeveloperEfficiencyPredictor();
                var issues = session.QueryOver<Issue>().List().ToList();
                var commits = session.QueryOver<Commit>().List().ToList();
                var users = session.QueryOver<IssueTrackerUser>().List().ToList();
                var userToPredict = users.FirstOrDefault();
                issues = issues.Skip(1).Take(200).ToList();
                var tempPath = Path.GetTempFileName();

                var trainignOutputData = predictor.TrainAndSavePredictionModel(new DeveloperEfficiencyIntegratedDataInput()
                {
                    RegressionTool = regressionTool,
                    CrossValidationFoldsCount = 3,
                    IssuesData = issues,
                    UsersData = users,
                    PredictiveModelPath = tempPath,
                    EfficiencyWeightsKeeper = new EfficiencyWeightsKeeper(),
                    Commits = commits
                });

                var predictionResult = predictor.Predict(userToPredict, issues, commits, trainignOutputData, new EfficiencyWeightsKeeper());

                Assert.IsNotNull(trainignOutputData);
                Assert.IsNotNull(trainignOutputData.Metrics);
                Assert.IsNotNull(predictionResult);
                Assert.IsNotNull(predictionResult.Result);

                File.Delete(tempPath);
            }
        }

    }
}

﻿namespace RepositoryAnalyser.Domain.Entities
{
    public class EntityBase
    {
        public virtual int Id { get; set; }
    }
}

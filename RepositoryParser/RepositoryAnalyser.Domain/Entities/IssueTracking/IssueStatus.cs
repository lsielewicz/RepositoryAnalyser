﻿namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class IssueStatus : EntityBase
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
}

﻿namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class TimeTrackingInformation : EntityBase
    {
        public virtual Issue Issue { get; set; }

        public virtual string OriginalEstimate { get; set; }

        public virtual string RemainingEstimate { get; set; }

        public virtual string TimeSpent { get; set; }
    }
}

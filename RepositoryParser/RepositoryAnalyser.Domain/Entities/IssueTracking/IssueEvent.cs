﻿using System;
using RepositoryAnalyser.Domain.Enums;

namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class IssueEvent : EntityBase
    {
        public virtual Issue Issue { get; set; }
        public virtual IssueTrackerUser User { get; set; }
        public virtual IssueEventType Type { get; set; }

        public virtual DateTime Date { get; set; }

    }
}

﻿namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class IssueType : EntityBase
    {
        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual bool IsSubTask { get; set; }
    }
}

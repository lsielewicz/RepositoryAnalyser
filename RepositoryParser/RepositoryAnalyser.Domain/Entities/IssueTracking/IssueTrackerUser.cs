﻿namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class IssueTrackerUser : EntityBase
    {
        public virtual string DisplayName { get; set; }
        public virtual string UserName { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string Locale { get; set; }
        public virtual string Email { get; set; }
    }
}

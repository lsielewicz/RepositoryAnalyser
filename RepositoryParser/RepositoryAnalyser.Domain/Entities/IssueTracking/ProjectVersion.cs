﻿using System;

namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class ProjectVersion : EntityBase
    {
        public virtual string Description { get; set; }
        public virtual string ProjectKey { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? ReleaseDate { get; set; }
        public virtual string Name { get; set; }
        public virtual bool IsArchieved { get; set; }
        public virtual bool IsReleased { get; set; }
    }
}

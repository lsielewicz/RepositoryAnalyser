﻿using System;
using System.Collections.Generic;

namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class Issue : EntityBase
    {
        public virtual string Description { get; set; }
        public virtual DateTime? CreatedDateTime { get; set; }
        public virtual DateTime? ClosedDateTime { get; set; }
        public virtual DateTime? UpdatedDateTime { get; set; }
        public virtual string Environemnt { get; set; }
        public virtual string Key { get; set; }
        public virtual string Summary { get; set; }
        public virtual IssueTrackerUser Asignee { get; set; }
        public virtual IssueTrackerUser Reporter { get; set; }
        public virtual IssueType Type { get; set; }
        public virtual IssueStatus Status { get; set; }
        public virtual TimeTrackingInformation TimeTracking { get; set; }
        public virtual IssueTrackingProject Project { get; set; }
        public virtual IList<ProjectVersion> AffectedVersions { get; set; }
        public virtual IList<ProjectVersion> Fixedversions { get; set; }
        public virtual IList<Comment> Comments { get; set; }
        public virtual IList<IssueEvent> Events { get; set; }

        public virtual string Priority { get; set; }

        public virtual int TimeSpent { get; set; }

        public virtual bool IsSubTask { get; set; }

        public virtual int AttachmentsCount { get; set; }
    }
}

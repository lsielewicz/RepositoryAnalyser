﻿using System.Collections.Generic;

namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class IssueTrackingProject : EntityBase
    {
        public virtual IssueTrackingSystem System { get; set; }
        public virtual string Key { get; set; }
        public virtual string Name { get; set; }
        public virtual string Url { get; set; }
        public virtual IssueTrackerUser Leader { get; set; }
        public virtual IList<Issue> Issues { get; set; }

        public virtual IList<CommitsSet> CommitsSets { get; set; }
    }
}

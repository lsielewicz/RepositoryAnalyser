﻿namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class CommitIntegrationInfo : EntityBase
    {
        public virtual CommitsSet CommitsSet { get; set; }

        public virtual string Revision { get; set; }
    }

}

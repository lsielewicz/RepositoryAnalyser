﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Enums;

namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class IssueTrackingSystem : EntityBase
    {
        public virtual string Name { get; set; }
        public virtual string Url { get; set; }
        public virtual IssueTrackingType Type { get; set; }
        public virtual IList<IssueTrackingProject> Projects { get; set; }
    }
}

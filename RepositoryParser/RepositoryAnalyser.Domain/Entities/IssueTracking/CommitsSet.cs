﻿using System.Collections.Generic;

namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class CommitsSet : EntityBase
    {
        public virtual int Number { get; set; }
        public virtual IssueTrackingProject Project { get; set; }
        public virtual IList<CommitIntegrationInfo> Commits { get; set; }
    }

}

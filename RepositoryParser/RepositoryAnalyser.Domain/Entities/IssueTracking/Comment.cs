﻿using System;

namespace RepositoryAnalyser.Domain.Entities.IssueTracking
{
    public class Comment : EntityBase
    {
        public virtual Issue Issue { get; set; }
        public virtual IssueTrackerUser User { get; set; }
        public virtual DateTime Date { get; set; }

        public virtual string Body { get; set; }
    }
}

﻿namespace RepositoryAnalyser.Domain.Enums
{
    public enum VersionControlSystemType
    {
        Git,
        Svn
    }
}
﻿namespace RepositoryAnalyser.Domain.Enums
{
    public enum ChartType
    {
        Primary,
        Secondary
    }
}

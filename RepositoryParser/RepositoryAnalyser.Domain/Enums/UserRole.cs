﻿namespace RepositoryAnalyser.Domain.Enums
{
    public enum UserRole
    {
        Reporter,
        Asignee
    }
}
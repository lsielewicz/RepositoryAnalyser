﻿namespace RepositoryAnalyser.Domain.Enums
{
    public enum PredictionType
    {
        WhichBugGetReopened,
        DevelopersEfficiency,
        TimeToFix
    }
}
﻿namespace RepositoryAnalyser.Domain.Enums
{
    public enum RepositoryCloneType
    {
        Public,
        Private
    }
}
﻿namespace RepositoryAnalyser.Domain.Enums
{
    public enum IssueTrackingType
    {
        Jira,
        Github,
        Bugzilla
    }
}
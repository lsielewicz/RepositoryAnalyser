﻿namespace RepositoryAnalyser.Domain.Enums
{
    public enum IssueEventType
    {
        Closed,
        Reopened,
        ReviewDismissed,
        Labeled,
        Reviewed,
        Merged,
        Other,
        Opened
    }
}
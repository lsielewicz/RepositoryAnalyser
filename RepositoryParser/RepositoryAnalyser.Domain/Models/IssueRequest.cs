﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.Domain.Models
{
    public class IssueRequest
    {
        public Issue Issue { get; set; }
        public IList<CommitsSet> CommitsSets { get; set; }
    }
}

﻿using System.Collections.Generic;
using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.Domain.Models
{
    public class IssueTrackerUserRequest
    {
        public IssueTrackerUser User { get; set; }
        public IList<CommitsSet> CommitsSets { get; set; }
        public IList<Issue> Issues { get; set; }
    }
}

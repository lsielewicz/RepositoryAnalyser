﻿namespace RepositoryAnalyser.Domain.Models
{
    public class ConnectionData
    {
        public ConnectionData()
        {
            Credentials = new Credentials();
        }

        public string Url { get; set; }
        public Credentials Credentials { get; set; }
    }
}

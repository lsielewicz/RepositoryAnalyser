﻿using System.Collections.Generic;
using System.Linq;
using Atlassian.Jira;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using Issue = RepositoryAnalyser.Domain.Entities.IssueTracking.Issue;
using IssueStatus = RepositoryAnalyser.Domain.Entities.IssueTracking.IssueStatus;
using IssueType = RepositoryAnalyser.Domain.Entities.IssueTracking.IssueType;
using ProjectVersion = RepositoryAnalyser.Domain.Entities.IssueTracking.ProjectVersion;

namespace RepositoryAnalyser.IssueTracking.Helpers
{
    public static class JiraTypesConvertingHelper
    {
        public static TimeTrackingInformation ToLocalDomain(this Atlassian.Jira.IssueTimeTrackingData trackingData, Issue referencedIssue = null)
        {
            if (trackingData == null)
            {
                return null;
            }
            return new TimeTrackingInformation()
            {
                Issue = referencedIssue,
                OriginalEstimate = trackingData.OriginalEstimate,
                RemainingEstimate = trackingData.RemainingEstimate,
                TimeSpent = trackingData.TimeSpent
            };
        }

        public static IssueStatus ToLocalDomain(this Atlassian.Jira.IssueStatus issueStatus)
        {
            if (issueStatus == null)
            {
                return null;
            }

            return new IssueStatus()
            {
                Name = issueStatus.Name,
                Description = issueStatus.Description
            };
        }

        public static IssueType ToLocalDomain(this Atlassian.Jira.IssueType issueType)
        {
            if (issueType == null)
            {
                return null;
            }

            return new IssueType()
            {
                Name = issueType.Name,
                Description = issueType.Description,
                IsSubTask = issueType.IsSubTask
            };
        }

        public static IList<ProjectVersion> ToLocalDomain(this ProjectVersionCollection projectVersionCollection)
        {
            if (projectVersionCollection == null)
            {
                return null;
            }
            return projectVersionCollection.Select(pv => pv.ToLocalDomain()).ToList();
        }

        public static ProjectVersion ToLocalDomain(this Atlassian.Jira.ProjectVersion projectVersion)
        {
            if (projectVersion == null)
            {
                return null;
            }
            return new ProjectVersion()
            {
                Name = projectVersion.Name,
                Description = projectVersion.Description,
                IsArchieved = projectVersion.IsArchived,
                IsReleased = projectVersion.IsReleased,
                ProjectKey = projectVersion.ProjectKey,
                ReleaseDate = projectVersion.ReleasedDate,
                StartDate = projectVersion.StartDate
            };
        }

        public static IssueTrackerUser ToLocalDomain(this JiraUser jiraUser)
        {
            if (jiraUser == null)
            {
                return null;
            }

            return new IssueTrackerUser()
            {
                Email = jiraUser.Email,
                UserName = jiraUser.Username,
                DisplayName = jiraUser.DisplayName,
                IsActive = jiraUser.IsActive,
                Locale = jiraUser.Locale
            };
        }




    }
}

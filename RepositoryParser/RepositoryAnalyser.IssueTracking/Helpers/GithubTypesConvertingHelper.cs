﻿using RepositoryAnalyser.Domain.Entities.IssueTracking;

namespace RepositoryAnalyser.IssueTracking.Helpers
{
    public static class GithubTypesConvertingHelper
    {
        public static readonly string Unknown = "Unknown";


        public static IssueStatus ToLocalDomain(this Octokit.ItemState itemState)
        {
            return new IssueStatus()
            {
                Name = itemState.ToString(),
                Description = itemState.ToString()
            };
        }

        public static IssueTrackerUser ToLocalDomain(this Octokit.User user)
        {
            if (user == null)
            {
                return null;
            }

            return new IssueTrackerUser()
            {
                Email = user.Email,
                UserName = user.Login,
                DisplayName = user.Name,
                IsActive = !user.Suspended,
                Locale = user.Location
            };
        }

        private static ProjectVersion DefaultVersion()
        {
            return new ProjectVersion()
            {
                Name = "Unknown",
                Description = "None",

            };
        }

        
    }
}

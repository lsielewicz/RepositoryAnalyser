﻿using System;
using System.Runtime.Serialization;

namespace RepositoryAnalyser.IssueTracking.Exceptions
{
    [Serializable]
    public class GithubParsingException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public GithubParsingException()
        {
        }

        public GithubParsingException(string message) : base(message)
        {
        }

        public GithubParsingException(string message, Exception inner) : base(message, inner)
        {
        }

        protected GithubParsingException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace RepositoryAnalyser.IssueTracking.Exceptions
{
    [Serializable]
    public class JiraParsingException : Exception
    {
        public JiraParsingException()
        {
        }

        public JiraParsingException(string message) : base(message)
        {
        }

        public JiraParsingException(string message, Exception inner) : base(message, inner)
        {
        }

        protected JiraParsingException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}

﻿using System.Threading.Tasks;
using RepositoryAnalyser.Domain.Models;

namespace RepositoryAnalyser.IssueTracking.Interfaces
{
    public interface IJiraIssuesParser : IIssuesTrackingParser
    {
        Task<IssueTrackerUserRequest> GetUser(ConnectionData connectionData, string userName);
        Task<IssueRequest> GetIssue(ConnectionData connectionData);
    }
}
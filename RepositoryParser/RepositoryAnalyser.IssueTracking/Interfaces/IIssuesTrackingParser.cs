﻿using System.Threading.Tasks;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Models;

namespace RepositoryAnalyser.IssueTracking.Interfaces
{
    public interface IIssuesTrackingParser
    {
        Task<IssueTrackingSystem> Parse(ConnectionData connectionData);
        


    }
}
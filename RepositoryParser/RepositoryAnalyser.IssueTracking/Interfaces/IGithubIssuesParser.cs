﻿using System.Threading.Tasks;
using RepositoryAnalyser.Domain.Models;

namespace RepositoryAnalyser.IssueTracking.Interfaces
{
    public interface IGithubIssuesParser : IIssuesTrackingParser
    {
        Task<IssueTrackerUserRequest> GetUser(ConnectionData connectionData);
        Task<IssueRequest> GetIssue(ConnectionData connectionData, bool findCommitsSets = false);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Atlassian.Jira;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Domain.Models;
using RepositoryAnalyser.IssueTracking.Exceptions;
using RepositoryAnalyser.IssueTracking.Helpers;
using RepositoryAnalyser.IssueTracking.Interfaces;
using Comment = RepositoryAnalyser.Domain.Entities.IssueTracking.Comment;
using Issue = RepositoryAnalyser.Domain.Entities.IssueTracking.Issue;
using IssueStatus = RepositoryAnalyser.Domain.Entities.IssueTracking.IssueStatus;
using IssueType = RepositoryAnalyser.Domain.Entities.IssueTracking.IssueType;

namespace RepositoryAnalyser.IssueTracking.Implementation
{
    public class JiraIssuesParser : IJiraIssuesParser
    {
        private const int MaxIssuesPerRequest = 100;

        private void GetIssueKeyFromIssueUrl(string issueUrl, out string jiraUrl, out string issueKey)
        {
            const string regexPattern = @"(.*[a-zA-Z].*\.[a-zA-Z].*\/)[a-zA-Z].*\/(.*)";
            issueKey = null;
            jiraUrl = null;
            var regexMatch = Regex.Match(issueUrl, regexPattern);
            if (regexMatch.Success && regexMatch.Groups.Count == 3)
            {
                jiraUrl = regexMatch.Groups[1].Value;
                issueKey = regexMatch.Groups[2].Value;
            }
        }

        public async Task<IssueTrackingSystem> Parse(ConnectionData connectionData)
        {
            try
            {
                var usersCache = new Dictionary<string, IssueTrackerUser>();
                var statusesCache = new Dictionary<string, IssueStatus>();
                var typesCache = new Dictionary<string, IssueType>();

                var issueTrackingSystem = new IssueTrackingSystem()
                {
                    Name = connectionData.Url,
                    Type = IssueTrackingType.Jira,
                    Url = connectionData.Url,
                    Projects = new List<IssueTrackingProject>()
                };

                var jiraInstance = Jira.CreateRestClient(connectionData.Url, connectionData.Credentials.Username, connectionData.Credentials.Password);

                var projects = await jiraInstance.Projects.GetProjectsAsync();
                jiraInstance.Issues.MaxIssuesPerRequest = MaxIssuesPerRequest;

                foreach (var project in projects)
                {
                    var leader = this.GetUser(ref usersCache, jiraInstance, project.Lead);

                    var issueTrackingProject = new IssueTrackingProject()
                    {
                        System = issueTrackingSystem,
                        Issues = new List<Issue>(),
                        Key = project.Key,
                        Url = project.Url,
                        Name = string.IsNullOrEmpty(project.Name) ? project.Key : project.Name,
                        Leader = leader
                    };

                    var startIndex = 0;
                    var totalCountOfIssues = -1;

                    do
                    {
                        var jqlQuery = $"project={project.Key}";
                        var issuesPortion = await jiraInstance.Issues.GetIssuesFromJqlAsync(jqlQuery, null, startIndex);
                        if (totalCountOfIssues == -1)
                        {
                            totalCountOfIssues = issuesPortion.TotalItems;
                        }

                        startIndex += issuesPortion.Count();

                        foreach (var issue in issuesPortion)
                        {
                            var issueToAdd = new Issue()
                            {
                                CreatedDateTime = issue.Created.HasValue ? new DateTime?(issue.Created.Value) : null,
                                ClosedDateTime = issue.ResolutionDate,
                                UpdatedDateTime = issue.Updated,
                                Key = issue.Key.Value,
                                AffectedVersions = issue.AffectsVersions.ToLocalDomain(),
                                Description = issue.Description,
                                Type = this.GetType(ref typesCache, issue.Type),
                                Asignee = this.GetUser(ref usersCache, jiraInstance, issue.Assignee),
                                Reporter = this.GetUser(ref usersCache, jiraInstance, issue.Reporter),
                                Environemnt = issue.Environment,
                                Fixedversions = issue.FixVersions.ToLocalDomain(),
                                Project = issueTrackingProject,
                                Status = this.GetStatus(ref statusesCache, issue.Status),
                                Summary = issue.Summary,
                                IsSubTask = issue.ParentIssueKey != null,
                                Priority = issue.Priority.Name,
                                TimeSpent = issue.TimeTrackingData.TimeSpentInSeconds.HasValue ? issue.TimeTrackingData.TimeSpentInSeconds.Value : 0,
                                AttachmentsCount = (await issue.GetAttachmentsAsync()).Count(),
                            };
                            issueToAdd.Events = GetEvents(issue.GetChangeLogsAsync().Result, jiraInstance, issueToAdd, ref usersCache);
                            issueToAdd.TimeTracking = issue.TimeTrackingData.ToLocalDomain(issueToAdd);
                            issueToAdd.Comments = this.GetComments(issue.GetCommentsAsync().Result, jiraInstance, issueToAdd, ref usersCache);

                            issueTrackingProject.Issues.Add(issueToAdd);
                        }
                    } while (issueTrackingProject.Issues.Count < totalCountOfIssues);

                    issueTrackingSystem.Projects.Add(issueTrackingProject);
                }


                return issueTrackingSystem;
            }
            catch (Exception ex)
            {
                throw new JiraParsingException("Something went wrong during the jira parsing process. Check inner exception for more details.", ex);
            }
            
        }

        public async Task<IssueRequest> GetIssue(ConnectionData connectionData)
        {
            var usersCache = new Dictionary<string, IssueTrackerUser>();
            var statusesCache = new Dictionary<string, IssueStatus>();
            var typesCache = new Dictionary<string, IssueType>();

            this.GetIssueKeyFromIssueUrl(connectionData.Url, out string jiraUrl, out string issueKey);

            var jiraInstance = Jira.CreateRestClient(jiraUrl, connectionData.Credentials.Username, connectionData.Credentials.Password);
            Atlassian.Jira.Issue issue=null;
            try
            {
                issue = await jiraInstance.Issues.GetIssueAsync(issueKey);
            }
            catch(Exception ex)
            {

            }

            
            if (issue != null)
            {
                var requestedIssue = new Issue()
                {
                    CreatedDateTime = issue.Created.HasValue ? new DateTime?(issue.Created.Value) : null,
                    ClosedDateTime = issue.ResolutionDate,
                    UpdatedDateTime =  issue.Updated,
                    Key = issue.Key.Value,
                    AffectedVersions = issue.AffectsVersions.ToLocalDomain(),
                    Description = issue.Description,
                    Type = this.GetType(ref typesCache, issue.Type),
                    Asignee = this.GetUser(ref usersCache, jiraInstance, issue.Assignee),
                    Reporter = this.GetUser(ref usersCache, jiraInstance, issue.Reporter),
                    Environemnt = issue.Environment,
                    Fixedversions = issue.FixVersions.ToLocalDomain(),
                    Project = new IssueTrackingProject()
                    {
                        Name = issue.Project
                    },
                    Status = this.GetStatus(ref statusesCache, issue.Status),
                    Summary = issue.Summary,
                    IsSubTask = issue.ParentIssueKey != null,
                    Priority = issue.Priority.Name,
                    TimeSpent = issue.TimeTrackingData.TimeSpentInSeconds.HasValue ? issue.TimeTrackingData.TimeSpentInSeconds.Value : 0,
                    AttachmentsCount = (await issue.GetAttachmentsAsync()).Count(),
                };
                requestedIssue.Events = GetEvents(issue.GetChangeLogsAsync().Result, jiraInstance, requestedIssue, ref usersCache);
                requestedIssue.TimeTracking = issue.TimeTrackingData.ToLocalDomain(requestedIssue);
                requestedIssue.Comments = this.GetComments(issue.GetCommentsAsync().Result, jiraInstance, requestedIssue, ref usersCache);

                return new IssueRequest()
                {
                    Issue = requestedIssue
                };
            }

            return null;
        }

        public async Task<IssueTrackerUserRequest> GetUser(ConnectionData connectionData, string userName)
        {
            var jiraInstance = Jira.CreateRestClient(connectionData.Url, connectionData.Credentials.Username, connectionData.Credentials.Password);

            var user = await jiraInstance.Users.GetUserAsync(userName);

            return new IssueTrackerUserRequest()
            {
                User = user.ToLocalDomain()
            };
        }

        private IssueType GetType(ref Dictionary<string, IssueType> typesCache, Atlassian.Jira.IssueType typeToConvert)
        {
            if (typesCache == null) 
            {
                typesCache = new Dictionary<string, IssueType>();
            }

            if (typesCache.ContainsKey(typeToConvert.Id))
            {
                return typesCache[typeToConvert.Id];
            }

            var typeToAdd = typeToConvert.ToLocalDomain();
            typesCache.Add(typeToConvert.Id, typeToAdd);

            return typeToAdd;
        }

        private IssueStatus GetStatus(ref Dictionary<string, IssueStatus> statusesCache, Atlassian.Jira.IssueStatus statusToConvert)
        {
            if (statusesCache == null)
            {
                statusesCache = new Dictionary<string, IssueStatus>();
            }

            if (statusesCache.ContainsKey(statusToConvert.Id))
            {
                return statusesCache[statusToConvert.Id];
            }

            var statusToAdd = statusToConvert.ToLocalDomain();
            statusesCache.Add(statusToConvert.Id, statusToAdd);

            return statusToAdd;
        }

        private IssueTrackerUser GetUser(ref Dictionary<string, IssueTrackerUser> usersCache, Jira jiraInstance, string userName)
        {
            if (usersCache == null)
            {
                usersCache = new Dictionary<string, IssueTrackerUser>();
            }

            if (userName == null)
            {
                return null;
            }

            if (usersCache.ContainsKey(userName))
            {
                return usersCache[userName];
            }


            var user = jiraInstance.Users.GetUserAsync(userName).Result.ToLocalDomain();
            usersCache.Add(userName, user);
            return user;

        }

        private IList<IssueEvent> GetEvents(IEnumerable<IssueChangeLog> logs, Jira jira, Issue issue,
            ref Dictionary<string, IssueTrackerUser> usersCache)
        {
            if (logs == null)
            {
                return null;
            }

            const string statusFieldName = "Status";
            const string reopenKeyword = "Reopen";
            const string closedKeyword = "Closed";
            const string reviewedKeyword = "Review";
            const string mergedKeyword = "Merge";

            var output = new List<IssueEvent>();

            foreach (var log in logs)
            {
                var statusChanges = log.Items.Where(item => item.FieldName.Equals(statusFieldName, StringComparison.OrdinalIgnoreCase)).ToList();
                foreach (var statusChange in statusChanges)
                {
                    var toStatus = statusChange.ToValue;                    
                    var type = IssueEventType.Other;

                    if (toStatus.IndexOf(reopenKeyword, StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        type = IssueEventType.Reopened;
                    }
                    else if (toStatus.IndexOf(closedKeyword, StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        type = IssueEventType.Closed;
                    }
                    else if (toStatus.IndexOf(mergedKeyword, StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        type = IssueEventType.Merged;
                    }
                    else if (toStatus.IndexOf(reviewedKeyword, StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        type = IssueEventType.Reviewed;
                    }

                    output.Add(new IssueEvent()
                    {
                        Issue = issue,
                        Date = log.CreatedDate,
                        Type = type,
                        User = this.GetUser(ref usersCache,jira,log.Author.Username),
                    });
                }
            }

            return output;
        }

        private IList<Comment> GetComments(IEnumerable<Atlassian.Jira.Comment> comments, Jira jira, Issue issue, ref Dictionary<string, IssueTrackerUser> usersCache)
        {
            if (comments == null)
            {
                return null;
            }

            var output = new List<Comment>();
            foreach (var comment in comments)
            {
                output.Add(new Comment()
                {
                    Issue = issue,
                    Body = comment.Body,
                    Date = comment.CreatedDate.GetValueOrDefault(),
                    User = this.GetUser(ref usersCache, jira, comment.Author)
                });
            }

            return output;
        }

    }
}

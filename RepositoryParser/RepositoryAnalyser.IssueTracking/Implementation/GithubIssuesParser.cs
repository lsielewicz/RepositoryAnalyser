﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Octokit;
using RepositoryAnalyser.Domain.Entities.IssueTracking;
using RepositoryAnalyser.Domain.Enums;
using RepositoryAnalyser.Domain.Models;
using RepositoryAnalyser.IssueTracking.Exceptions;
using RepositoryAnalyser.IssueTracking.Helpers;
using RepositoryAnalyser.IssueTracking.Interfaces;
using Issue = RepositoryAnalyser.Domain.Entities.IssueTracking.Issue;
using IssueEvent = RepositoryAnalyser.Domain.Entities.IssueTracking.IssueEvent;
using IssueRequest = RepositoryAnalyser.Domain.Models.IssueRequest;

namespace RepositoryAnalyser.IssueTracking.Implementation
{
    public class GithubIssuesParser : IGithubIssuesParser
    {
        private int _apiAssistanceAccountNumber = 0;
        private IGitHubClient _githubClient;
        private const string AppHeader = "Repository_Analyser";

        private string GetUserNameFromUrl(string url)
        {
            if (url.EndsWith(@"/"))
            {
                url = url.TrimEnd(new char[] {'/'});
            }

            const string regex = @"(.*[a-zA-Z].*\.[a-zA-Z].*)\/(.*)";
            var regexMatch = Regex.Match(url, regex);
            if (regexMatch.Success && regexMatch.Groups.Count == 3)
            {
                return regexMatch.Groups[2].Value;
            }

            return null;
        }

        private void GetRepositoryDataFromUrl(string url, out string owner, out string repositoryName,
            out int issueNumber)
        {
            const string githubDataRegex = @"(.*[a-zA-Z].*\.[a-zA-Z].*)\/(.*)\/(.*)\/[a-zA-Z]*\/(\d*)";
            owner = null;
            repositoryName = null;
            issueNumber = -1;

            var regexMatch = Regex.Match(url, githubDataRegex);
            if (regexMatch.Success && regexMatch.Groups.Count == 5)
            {
                owner = regexMatch.Groups[2].Value;
                repositoryName = regexMatch.Groups[3].Value;
                issueNumber = Convert.ToInt32(regexMatch.Groups[4].Value);
            }
        }

        private void GetRepositoryDataFromUrl(string url, out string owner, out string repositoryName)
        {
            const string githubDataRegex = @".*[a-zA-Z].*\.[a-zA-Z].*\/(.*)\/(.*)";
            owner = null;
            repositoryName = null;

            var regexMatch = Regex.Match(url, githubDataRegex);
            if (regexMatch.Success && regexMatch.Groups.Count == 3)
            {
                owner = regexMatch.Groups[1].Value;
                repositoryName = regexMatch.Groups[2].Value;
            }
        }

        public async Task<IssueTrackingSystem> Parse(ConnectionData connectionData)
        {
            _apiAssistanceAccountNumber = 0;
            var usersCache = new Dictionary<string, IssueTrackerUser>();
            var statusesCache = new Dictionary<ItemState, IssueStatus>();

            _githubClient = new GitHubClient(new ProductHeaderValue(AppHeader))
            {
                Credentials =
                    new Octokit.Credentials(connectionData.Credentials.Username, connectionData.Credentials.Password)
            };

            this.GetRepositoryDataFromUrl(connectionData.Url, out string owner, out string repositoryName);
            if (owner == null || repositoryName == null)
            {
                throw new GithubParsingException("Entry URL is not valid github address");
            }

            var trackingSystem = new IssueTrackingSystem()
            {
                Url = connectionData.Url,
                Name = repositoryName,
                Projects = new List<IssueTrackingProject>(),
                Type = IssueTrackingType.Github
            };

            var projectToAdd = new IssueTrackingProject()
            {
                Key = repositoryName,
                Leader = this.GetUser(ref usersCache, owner),
                Issues = new List<Issue>(),
                Name = repositoryName,
                Url = connectionData.Url,
                System = trackingSystem,
                CommitsSets = new List<CommitsSet>()
            };
            this.AssertApiLimits();
            var issues = await _githubClient.Issue.GetAllForRepository(owner, repositoryName, new RepositoryIssueRequest()
            {
                Filter = IssueFilter.All,
                State = ItemStateFilter.All,
            });
            this.AssertApiLimits();

            projectToAdd.CommitsSets = await this.GetCommitsSet(owner, repositoryName, projectToAdd);


            var issueType = new IssueType()
            {
                Description = GithubTypesConvertingHelper.Unknown,
                IsSubTask = false,
                Name = GithubTypesConvertingHelper.Unknown
            };

            foreach (var issue in issues)
            {
                var issueToAdd = new Issue()
                {
                    CreatedDateTime = issue.CreatedAt.DateTime,
                    ClosedDateTime = issue.ClosedAt.HasValue ? new DateTime?(issue.ClosedAt.Value.DateTime) : null,
                    UpdatedDateTime = issue.UpdatedAt.HasValue ? new DateTime?(issue.UpdatedAt.Value.DateTime) : null,
                    Description = issue.Body,
                    Type = issueType,
                    Key = issue.Title,
                    Asignee = this.GetUser(ref usersCache,issue.Assignee?.Login),
                    AffectedVersions = null,
                    Environemnt = GithubTypesConvertingHelper.Unknown,
                    Fixedversions = null,
                    Reporter = this.GetUser(ref usersCache,issue.User?.Login),
                    Status = this.GetStatus(ref statusesCache, issue.State.Value),
                    Summary = issue.Title,
                    TimeTracking = null,
                    Project = projectToAdd,
                    IsSubTask = false,
                    AttachmentsCount = 0,
                    TimeSpent = 0,
                    Priority = "-",
                };
                issueToAdd.Events =
                    this.GetEvents(owner, repositoryName, issue.Number, issueToAdd, ref usersCache);
                issueToAdd.Comments = issue.Comments > 0 ? this.GetComments(owner,repositoryName,issue.Number, issueToAdd,ref usersCache) : null;

                projectToAdd.Issues.Add(issueToAdd);
            }

            trackingSystem.Projects.Add(projectToAdd);

            return trackingSystem;
        }

        private async Task<IList<Issue>> GetIssues(string owner, string repositoryName)
        {
            var output = new List<Issue>();
            
            var issues = await _githubClient.Issue.GetAllForRepository(owner, repositoryName);
            this.AssertApiLimits();

            foreach (var issue in issues)
            {
                var issueType = new IssueType()
                {
                    Description = GithubTypesConvertingHelper.Unknown,
                    IsSubTask = false,
                    Name = GithubTypesConvertingHelper.Unknown
                };

                if (issue != null)
                {
                    var requestedIssue = new Issue()
                    {
                        CreatedDateTime = issue.CreatedAt.DateTime,
                        ClosedDateTime = issue.ClosedAt.HasValue ? new DateTime?(issue.ClosedAt.Value.DateTime) : null,
                        UpdatedDateTime =
                            issue.UpdatedAt.HasValue ? new DateTime?(issue.UpdatedAt.Value.DateTime) : null,
                        Description = issue.Body,
                        Type = issueType,
                        Key = issue.Title,
                        Asignee = issue.Assignee?.ToLocalDomain(),
                        AffectedVersions = null,
                        Environemnt = GithubTypesConvertingHelper.Unknown,
                        Fixedversions = null,
                        Reporter = issue.User?.ToLocalDomain(),
                        Status = issue.State.Value.ToLocalDomain(),
                        Summary = null,
                        TimeTracking = null,
                        Project = null,
                    };

                    output.Add(requestedIssue);
                }
            }

            return output;
        }

        private void AssertApiLimits()
        {
            const int minBorder = 100;
            if (_githubClient == null || _githubClient.GetLastApiInfo() == null)
            {
                return;
            }

            var limit = _githubClient.GetLastApiInfo().RateLimit;

            if (limit.Remaining <= minBorder)
            {
                Octokit.Credentials credentials = null;
                if (_apiAssistanceAccountNumber == 0)
                {
                    credentials = new Octokit.Credentials("repositoryanalyser", "Start123");
                }
                else if (_apiAssistanceAccountNumber == 1)
                {
                    credentials = new Octokit.Credentials("repositoryanalyser1", "Start123");
                }
                else if (_apiAssistanceAccountNumber == 2)
                {
                    credentials = new Octokit.Credentials("repositoryanalyser2", "Start123");
                }
                else
                {
                    credentials = new Octokit.Credentials("repositoryanalyser3", "Start123");
                }

                _githubClient = new GitHubClient(new ProductHeaderValue(AppHeader))
                {
                    Credentials = credentials
                };
                _apiAssistanceAccountNumber++;
            }
        }

    private async Task<IList<CommitsSet>> GetCommitsSet(string owner,string repositoryName, IssueTrackingProject projectToAdd)
        {
            var commitsToAdd = new List<CommitsSet>();
            var pullRequests = await _githubClient.PullRequest.GetAllForRepository(owner, repositoryName,
                new PullRequestRequest()
                {
                    State = ItemStateFilter.All
                });
            this.AssertApiLimits();
            foreach (var pullRequest in pullRequests)
            {
                var commitsSet = new CommitsSet()
                {
                    Number = pullRequest.Number,
                    Commits = new List<CommitIntegrationInfo>(),
                    Project = projectToAdd
                };

                var prCommits = await _githubClient.Repository.PullRequest.Commits(owner, repositoryName, pullRequest.Number);
                this.AssertApiLimits();
                if (prCommits != null)
                {
                    foreach (var commit in prCommits)
                    {
                        commitsSet.Commits.Add(new CommitIntegrationInfo()
                        {
                            CommitsSet = commitsSet,
                            Revision = commit.Sha
                        });
                    }
                }
                commitsToAdd.Add(commitsSet);
            }

            return commitsToAdd;
        }

        public async Task<IssueRequest> GetIssue(ConnectionData connectionData, bool findCommitsSets = false)
        {
            var usersCache = new Dictionary<string, IssueTrackerUser>();

            _githubClient = new GitHubClient(new ProductHeaderValue(AppHeader))
            {
                Credentials = new Octokit.Credentials(connectionData.Credentials.Username, connectionData.Credentials.Password)
            };

            this.GetRepositoryDataFromUrl(connectionData.Url, out string owner, out string repositoryName, out int issueNumber);
            var issue = await _githubClient.Issue.Get(owner, repositoryName, issueNumber);
            this.AssertApiLimits();

            var issueType = new IssueType()
            {
                Description = GithubTypesConvertingHelper.Unknown,
                IsSubTask = false,
                Name = GithubTypesConvertingHelper.Unknown
            };

            if (issue != null)
            {
                var requestedIssue = new Issue()
                {
                    CreatedDateTime = issue.CreatedAt.DateTime,
                    ClosedDateTime = issue.ClosedAt.HasValue ? new DateTime?(issue.ClosedAt.Value.DateTime) : null,
                    UpdatedDateTime = issue.UpdatedAt.HasValue ? new DateTime?(issue.UpdatedAt.Value.DateTime) : null,
                    Description = issue.Body,
                    Type = issueType,
                    Key = issue.Title,
                    Asignee = issue.Assignee?.ToLocalDomain(),
                    AffectedVersions = null,
                    Environemnt = GithubTypesConvertingHelper.Unknown,
                    Fixedversions = null,
                    Reporter = issue.User?.ToLocalDomain(),
                    Status = issue.State.Value.ToLocalDomain(),
                    Summary = null,
                    TimeTracking = null
                    
                };
                requestedIssue.Events = this.GetEvents(owner, repositoryName, issue.Number, requestedIssue, ref usersCache);
                requestedIssue.Comments = issue.Comments > 0 ? this.GetComments(owner, repositoryName, issue.Number, requestedIssue, ref usersCache) : null;

                IList<CommitsSet> pullRequests = null;
                if (findCommitsSets)
                {
                    pullRequests = await this.GetCommitsSet(owner, repositoryName, null);
                    
                }

                return new IssueRequest()
                {
                    Issue = requestedIssue,
                    CommitsSets = pullRequests
                };
            }

            return null;
        }

        public async Task<IssueTrackerUserRequest> GetUser(ConnectionData connectionData)
        {
            _githubClient = new GitHubClient(new ProductHeaderValue(AppHeader))
            {
                Credentials = new Octokit.Credentials(connectionData.Credentials.Username, connectionData.Credentials.Password)
            };
            
            var user = await _githubClient.User.Get(this.GetUserNameFromUrl(connectionData.Url));
            var repositories = await _githubClient.Repository.GetAllForUser(user.Login);
            this.AssertApiLimits();


            var commitsSets = new List<CommitsSet>();
            var issuesSets = new List<Issue>();
            if (repositories != null)
            {
                foreach (var repository in repositories)
                {
                    var owner = repository.Owner.Login;
                    var repositoryName = repository.Name;

                    var pullRequests = await this.GetCommitsSet(owner, repositoryName, null);
                    var issues = await this.GetIssues(owner, repositoryName);

                    commitsSets.AddRange(pullRequests);
                    issuesSets.AddRange(issues);
                }
            }
            return new IssueTrackerUserRequest()
            {
                User = user.ToLocalDomain(),
                CommitsSets = commitsSets,
                Issues = issuesSets
            };
        }

        private IList<IssueEvent> GetEvents(string owner, string repositoryName, int issueNumber, Issue issue,
            ref Dictionary<string, IssueTrackerUser> usersCache)
        {
            var output = new List<IssueEvent>();
            IReadOnlyList<EventInfo> events = null;

            events = _githubClient.Issue.Events.GetAllForIssue(owner, repositoryName, issueNumber).Result;
            AssertApiLimits();
            foreach (var ev in events)
            {
                var type = IssueEventType.Other;
                try
                {

                    if (ev != null)
                    {
                        switch (ev.Event.Value)
                        {
                            case EventInfoState.Closed:
                                type = IssueEventType.Closed;
                                break;
                            case EventInfoState.Reopened:
                                type = IssueEventType.Reopened;
                                break;
                            case EventInfoState.Merged:
                                type = IssueEventType.Merged;
                                break;
                            case EventInfoState.Labeled:
                                type = IssueEventType.Labeled;
                                break;
                            case EventInfoState.ReviewDismissed:
                                type = IssueEventType.ReviewDismissed;
                                break;
                            case EventInfoState.Reviewed:
                                type = IssueEventType.Reviewed;
                                break;
                        }
                    }
                }
                catch
                {
                    // API returns exception in case of particular types of events, which have no value, but stringValue instead
                }

                var user = ev.Actor?.Name != null ? GetUser(ref usersCache, ev.Actor.Name) : null;
                    output.Add(new IssueEvent()
                    {
                        Date = ev.CreatedAt.DateTime,
                        User = user,
                        Type = type,
                        Issue = issue
                    });
                    

            }
            

            return output;
        }

        private IList<Comment> GetComments(string owner, string repositoryName, int issueNumber, Issue issue, ref Dictionary<string, IssueTrackerUser> usersCache)
        {
            IReadOnlyList<IssueComment> comments = null;
            try
            {
                comments = _githubClient.Issue.Comment.GetAllForIssue(owner, repositoryName, issueNumber).Result;
                this.AssertApiLimits();
            }
            catch
            {
                return null;
            }

            var output = new List<Comment>();

            foreach (var comment in comments)
            {
                output.Add(new Comment()
                {
                    Issue = issue,
                    Body = comment.Body,
                    User = this.GetUser(ref usersCache, comment.User.Name),
                    Date = comment.CreatedAt.DateTime
                });
            }

            return output;
        }

        private IssueStatus GetStatus(ref Dictionary<ItemState, IssueStatus> statusesCache, ItemState stateToConvert)
        {
            if (statusesCache == null)
            {
                statusesCache = new Dictionary<ItemState, IssueStatus>();
            }

            if (statusesCache.ContainsKey(stateToConvert))
            {
                return statusesCache[stateToConvert];
            }

            var statusToAdd = stateToConvert.ToLocalDomain();
            statusesCache.Add(stateToConvert,statusToAdd);

            return statusToAdd;
        }

        private IssueTrackerUser GetUser(ref Dictionary<string, IssueTrackerUser> usersCache, string userName)
        {
            if (usersCache == null)
            {
                usersCache = new Dictionary<string, IssueTrackerUser>();
            }

            if (userName == null)
            {
                return null;
            }

            if (usersCache.ContainsKey(userName))
            {
                return usersCache[userName];
            }


            var user = _githubClient.User.Get(userName).Result.ToLocalDomain();
            AssertApiLimits();
            usersCache.Add(userName, user);
            return user;

        }
    }
}
